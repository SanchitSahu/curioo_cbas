from django.shortcuts import render, redirect
from rest_framework import status
from django.http import HttpResponse
from curioo_admin.models import Admin, Principal, Teacher, LearningConsultant


def gbl(request, token):
    admin = Admin.objects.all()
    principal = Principal.objects.all()
    teacher = Teacher.objects.all()
    consultant = LearningConsultant.objects.all()
    # comb = zip(admin, principal, teacher, consultant)

    try:
        for a in admin:
            # print("admin")
            # if request.headers.get('token') in a.device:
            #     print("if-----s")
            if 'token' in request.session and request.session['token'] in a.device or 'token' in request.headers and request.headers.get('token') in a.device:
                # print("if")
                name = a.full_name
                utype = 'admin'
                # print("name, utype, a----",name, utype, a)
                return name, utype, a
            # else:
            #     print("else")
            #     return '','',''

        for p in principal:
            # print("principal")
            if 'token' in request.session and request.session['token'] in p.device or 'token' in request.headers and request.headers.get('token') in p.device:
                # print("if 1")
                name = p.full_name
                utype = 'principal'
                return name, utype,p

        for t in teacher:
            # print("teacher")
            if 'token' in request.session and request.session['token'] in t.device or 'token' in request.headers and request.headers.get('token') in t.device:
                # print("if")
                name = t.full_name
                utype = 'teacher'
                return name, utype,t

        for c in consultant:
            if 'token' in request.session and request.session['token'] in c.device or 'token' in request.headers and request.headers.get('token') in c.device:
                name = c.full_name
                utype = 'consultant'
                return name, utype,c

    except Exception as e:
        print("e --", e)
        return HttpResponse(status=400)
