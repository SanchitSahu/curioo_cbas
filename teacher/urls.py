from django.urls import path
from django.conf.urls.static import static
from curioo_cbas import settings
from rest_framework.urlpatterns import format_suffix_patterns
from django.conf.urls import url
from .views import HomeView, get_data, ChartData
from .logout import *
from .profile import *
from .lessons import *
from .announcement import *
from .helpdesk import *
from .faqs import *
from .students import *
from .lead import *
from .ratings import *
from .dashboard import *
from .changepassword import *
from .calender import *
from .activities import *
from .analytics import *
from .lesson_report import *



urlpatterns = [
    path("logout/",Logout.as_view(), name='logout/'),
    path("profile/",Profile.as_view(), name='profile/'),

    # path("lessons/filter/",ScheduleDateFilter.as_view()),
    path("announcement/",Announcemnt.as_view(), name='announcement/'),
    path("notification/",Notification.as_view(),name="notify/"),
    path("helpdesk/",HelpDsk.as_view(),name="helpdesk/"),
    path("faq/",FAQs.as_view(),name="faq/"),

    path("allLead/", AllLeads.as_view()),
    path("lead/", Leads.as_view(), name='lead/'),

    path("ratings/", Ratings.as_view()),

    path("students/",AllStudent.as_view(), name="students/"),
    path("student/<int:student_id>/",StudentDetails.as_view()),
    path("student/searchLesson/",FilterStudentDetails.as_view()),
    
    path("lessons/",Lessn.as_view(), name="lessons/"),
    path('lesson/view/<int:lesson_id>',get_lesson.as_view(), name="lesson/view/"),
    path('lesson/',teacherLesson.as_view(),name="lesson/"),
    path("lesson/student/",StudentDetails.as_view()),
    # path("lesson/start/",StartEndClass.as_view()),
    path("lessons/searchLesson/",filter_date_lesson, name="lessons/searchLesson/"),
    path("lesson/searchLesson/",filter_date_lesson, name="lesson/searchLesson/"),
    path('changepassword/', change_password,name="changepassword/"),

    path("lesson/start/comment/<int:student_id>/",StartClass.as_view()),
    path("lesson/student/review/", StudentReview.as_view()),
    path("lesson/start/<str:schedule_id>/",StartClass.as_view() ,name="startLesson/"),
    path('calender/' ,calender, name='calender/'),

    path('activities/', Activities.as_view(),name="activities/"),
    # path('activities/searchLesson/',filter_date_activity, name="activities/searchLesson/"),

    url(r'^home/$', HomeView.as_view(), name='home'),
    url(r'^api/data/$', get_data, name='api-data'),
    # url(r'^api/chart/data/$', ChartData.as_view()),

    path('lessons', get_lessons, name='get-lessons'),
    path('analytics/', Analytics.as_view(),name="analytics/"),
    path('lesson/report/<int:lesson_id>', LessonReport.as_view(),name="lesson/report/"),
    path('mylesson/filter/', MylessonFilter),
    path('lesson/report/date/', LessonReportDate),



] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)