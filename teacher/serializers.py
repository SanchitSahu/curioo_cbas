from rest_framework import serializers, status
from rest_framework.response import Response
from teacher.status_message import success_message, error_message
from curioo_admin.models import *
from django.contrib.auth.models import update_last_login
from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.compat import get_username_field
from datetime import datetime as dt
import datetime
from datetime import date
from django.db.models import Count
from django.contrib.auth.hashers import make_password, check_password

JWT_PAYLOAD_HANDLER = api_settings.JWT_PAYLOAD_HANDLER
JWT_ENCODE_HANDLER = api_settings.JWT_ENCODE_HANDLER


class LoginSerializer(serializers.ModelSerializer):
    login_type = serializers.CharField(max_length=255)
    email = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=128, write_only=True)
    token = serializers.CharField(max_length=255, read_only=True)

    def validate(self, data):
        print("data----",data)
        login_type = data.get("login_type")
        email = data.get("email", None)
        password = data.get("password", None)

        user = True
        learning_consultant = True
        admin = True
        principal = True
        print("login_type---",login_type)
        if login_type == "teacher":
            print("in if")
            if Teacher.objects.filter(email=data['email']).exists():
                print("in if 2")
                if Teacher.objects.get(email=data['email']).is_active:
                    print("is active")
                    if Teacher.objects.get(email=data['email']).password != None:
                        user = check_password(data['password'], Teacher.objects.get(email=data['email']).password) if Teacher.objects.filter(email=data['email']).exists() else False
                        print("teacher--",user)
                        learning_consultant = False
                        admin = False
                        principal = False
                        print("in teacher----learning_consultant---admin---principal----",user, learning_consultant, admin, principal)
                    else:
                        print("password not set")
                        return {
                            'email': 'password',
                            'token': 'password',
                            'login_type':'teacher'
                        }
                else:
                    print("not active")
                    return {
                        'email': 'False',
                        'token': 'False',
                        'login_type':'teacher'
                    }

        if login_type == "learning_consultant":
            print("in if")
            if LearningConsultant.objects.filter(email=data['email']).exists():
                print("in if 2")
                if LearningConsultant.objects.get(email=data['email']).is_active:
                    print("is active")
                    if LearningConsultant.objects.get(email=data['email']).password != None:
                        learning_consultant = check_password(data['password'], LearningConsultant.objects.get(email=data['email']).password) if LearningConsultant.objects.filter(email=data['email']).exists() else False
                        print("learning_consultant--",learning_consultant)
                        user = False
                        admin = False
                        principal = False
                        print("in teacher----learning_consultant---admin---principal----",user, learning_consultant, admin, principal)
                    else:
                        print("password not set")
                        return {
                            'email': 'password',
                            'token': 'password',
                            'login_type':'learning_consultant'
                        }
                else:
                    print("not active")
                    return {
                        'email': 'False',
                        'token': 'False',
                        'login_type':'learning_consultant'
                    }

        if login_type == "admin":
            print("in if")
            if Admin.objects.filter(email=data['email']).exists():
                print("in if 2")
                admin = check_password(data['password'], Admin.objects.get(email=data['email']).password) if Admin.objects.filter(email=data['email']).exists() else False
                print("admin--",admin)
                user = False
                learning_consultant = False
                principal = False
                print("in teacher----learning_consultant---admin---principal----",user, learning_consultant, admin, principal)

        if login_type == "principal":
            print("in if")
            if Principal.objects.filter(email=data['email']).exists():
                print("in if 2")
                if Principal.objects.get(email=data['email']).is_active:
                    print("is active")
                    principal = check_password(data['password'], Principal.objects.get(email=data['email']).password) if Principal.objects.filter(email=data['email']).exists() else False
                    print("principal--",principal)
                    user = False
                    learning_consultant = False
                    admin = False
                    print("in teacher----learning_consultant---admin---principal----",user, learning_consultant, admin, principal)
                else:
                    print("not active")
                    return {
                        'email': 'False',
                        'token': 'False',
                        'login_type':'principal'
                    }
        
        print("teacher----learning_consultant---admin---principal----",user, learning_consultant, admin, principal)

        if user == True and learning_consultant == True and admin == True and principal == True:
            return {
                'email': '',
                'token': '',
                'login_type':''
            }

        if user:
            userData = Teacher.objects.get(
                email=data['email'])
            print("Teacher obj", userData)
            if userData is None:
                raise serializers.ValidationError(
                    'A user with this email and password is not found.'
                )
            try:
                payload = JWT_PAYLOAD_HANDLER(userData)
                print("payload seri----",payload)
                jwt_token = JWT_ENCODE_HANDLER(payload)
                print("jwt_token----",jwt_token)

                deviceList = userData.device
                print("deviceList---",deviceList)
                deviceList.append(jwt_token)
                userData.device = deviceList
                userData.save()
                tc = Token.objects.create(access_token=jwt_token)
                print("tc----",tc)

            except Teacher.DoesNotExist:
                raise serializers.ValidationError(
                    'User with given email and password does not exists'
                )
            return {
                'email': userData.email,
                'token': jwt_token,
                'login_type':userData.login_type
            }

        elif learning_consultant:
            LCData = LearningConsultant.objects.get(
                email=data['email'])
            if LCData is None:
                raise serializers.ValidationError(
                    'A user with this email and password is not found.'
                )
            try:
                payload = JWT_PAYLOAD_HANDLER(LCData)
                jwt_token1 = JWT_ENCODE_HANDLER(payload)

                deviceList1 = LCData.device
                deviceList1.append(jwt_token1)
                LCData.device = deviceList1
                LCData.save()
                Token.objects.create(access_token=jwt_token1)

            except LearningConsultant.DoesNotExist:
                raise serializers.ValidationError(
                    'User with given email and password does not exists'
                )
            return {
                'email': LCData.email,
                'token': jwt_token1,
                'login_type':LCData.login_type
            }

        elif admin:
            adminData = Admin.objects.get(email=data['email'])
            print("adminData---",adminData)
            if adminData is None:
                raise serializers.ValidationError(
                    'A user with this email and password is not found.'
                )
            try:
                payload = JWT_PAYLOAD_HANDLER(adminData)
                jwt_token2 = JWT_ENCODE_HANDLER(payload)
                print("Try success")
                deviceList1 = adminData.device
                deviceList1.append(jwt_token2)
                adminData.device = deviceList1
                adminData.save()
                Token.objects.create(access_token=jwt_token2)

            except Admin.DoesNotExist:
                raise serializers.ValidationError(
                    'User with given email and password does not exists'
                )
            return {
                'email': adminData.email,
                'token': jwt_token2,
                'login_type':adminData.login_type

            }

        elif principal:
            principalData = Principal.objects.get(email=data['email'])
            if principalData is None:
                raise serializers.ValidationError(
                    'A user with this email and password is not found.'
                )
            try:
                payload = JWT_PAYLOAD_HANDLER(principalData)
                jwt_token3 = JWT_ENCODE_HANDLER(payload)

                deviceList1 = principalData.device
                deviceList1.append(jwt_token3)
                principalData.device = deviceList1
                principalData.save()
                Token.objects.create(access_token=jwt_token3)

            except Admin.DoesNotExist:
                raise serializers.ValidationError(
                    'User with given email and password does not exists'
                )
            return {
                'email': principalData.email,
                'token': jwt_token3,
                'login_type':principalData.login_type

            }
        else:
            return {
                'email': '',
                'token': '',
                'login_type':''
            }

    class Meta:
        model = Teacher
        fields = ['password', 'email', 'token', 'login_type']


class LearningConsultantSerializer(serializers.ModelSerializer):
    class Meta:
        model = LearningConsultant
        fields = ["full_name", "email", "phone", "password"]


class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teacher
        fields = ["full_name", "email", "phone", "password", "image"]


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ['full_name','image','id','score', 'avgScore', 'current_level']
    
    image = serializers.SerializerMethodField("get_image_url")
    score = serializers.SerializerMethodField("get_score")
    avgScore = serializers.SerializerMethodField("get_avgScore")
    
    def get_avgScore(self, data):
        print("Id---123---",data.id)
        print("Subject id---", data)
        allReport = StudentReportData.objects.filter(student=data.id).distinct('score')
        print("allReport", allReport)
        scoreList = []
        demoList = []
        score = 0
        if len(allReport) != 0:
            lenOfReport = len(allReport)
            for report in allReport:
                score += report.score
                demoList.append(report.score)
                
            avg = score/lenOfReport
            scoreList.append(avg)
            print("\n")
            print('-'*50)
            print("Demo List",demoList)
            print("ScoreList",scoreList)
            print("Length", lenOfReport)
            return scoreList
        else:
            return []

    def get_score(self, data):
        print("Id---123---",data.id)
        print("Subject id---", data)
        allReport = StudentReportData.objects.filter(student=data.id).order_by('-id')
        print('All report', allReport)
        return allReport

    def get_image_url(self, data):
        return data.image.url
    
class StudentDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ['id','full_name','student_code','gender','age','school_name','school_grade','image','father_name','father_phone','mother_name','mother_phone','current_level','major']
    
    image = serializers.SerializerMethodField("get_image_url")
    age = serializers.SerializerMethodField("get_age")
    current_level = serializers.SerializerMethodField("get_level")
    major = serializers.SerializerMethodField("get_major")
    
    def get_image_url(self, data):
        return data.image.url
    
    def get_age(self, data):
        from datetime import date
        today = date.today() 
        return today.year - data.dob.year - ((today.month, today.day) < (data.dob.month, data.dob.day))

    def get_level(self , data):
        if data.current_level == None:
            return ''
        else:
            return data.current_level.level

    def get_major(self , data):
        if data.major == None:
            return ""
        else:
            return data.major.major
        

class Faqserializer(serializers.ModelSerializer):
    class Meta:
        model = FAQ
        fields = "__all__"


class AnnouncementSerializer(serializers.ModelSerializer):

    class Meta:
        model = Announcement
        fields = ["id", "announcement","admin","studio", "date", "time"]
    date = serializers.SerializerMethodField('get_date')
    time = serializers.SerializerMethodField('get_time')

    def get_date(self, data):
        try:
            # dateSplit = str(data.created_at).split(' ')
            # return dateSplit[0]
            dateSplit = dt.strftime(data.created_at, '%d %B, %Y')
            return dateSplit
        except Exception as e:
            print("exc--")
    
    def get_time(self, data):
        try:
            # timeSplit = str(data.created_at).split(' ')[1].split('.')
            # return timeSplit[0]
            timeSplit = dt.strftime(data.created_at, '%H:%M %p')
            return timeSplit
        except Exception as e:
            print("exc--")


class NotificationSerializer(serializers.ModelSerializer):

    class Meta:
        model = WebNotification
        fields = ["id", "notification", "date", "time"]
    date = serializers.SerializerMethodField('get_date')
    time = serializers.SerializerMethodField('get_time')

    def get_date(self, data):
        try:
            # dateSplit = str(data.created_at).split(' ')
            dateSplit = dt.strftime(data.created_at, '%d %B, %Y')
            return dateSplit
        except Exception as e:
            print("exc--")


    def get_time(self, data):
        try:
            timeSplit = dt.strftime(data.created_at, '%H:%M %p')
            return timeSplit
        except Exception as e:
            print("Exception", e)                  

class LessonDetailsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Lesson
        fields = ["id","lesson_code", "subject", "total_students", "room", "teacher", "startTime","endTime", "student",
        'subject_behaviour','overall_rating_scales','well_being_scales','comments',"student_report_data","schedule_data"]

    lesson_code = serializers.SerializerMethodField('get_lesson')
    subject = serializers.SerializerMethodField('get_subject_name')
    student = StudentSerializer(many=True, read_only=True)
    total_students = serializers.SerializerMethodField('get_student_count')
    room = serializers.SerializerMethodField('get_room')
    teacher = serializers.SerializerMethodField('get_teacher')
    startTime = serializers.SerializerMethodField('get_startTime')
    endTime = serializers.SerializerMethodField('get_endTime')
    subject_behaviour = serializers.SerializerMethodField('get_subject_behaviour')
    overall_rating_scales = serializers.SerializerMethodField('get_overall_rating_scales')
    well_being_scales = serializers.SerializerMethodField('get_well_being_scales')
    comments = serializers.SerializerMethodField('get_comments')
    schedule_data = serializers.SerializerMethodField('get_schedule_data')
    student_report_data = serializers.SerializerMethodField('get_student_report_data')

    def get_schedule_data(self, data):
        try:
            schedule_data = ScheduleClass.objects.filter(lesson=data.id)
            return schedule_data
        except Exception as e:
            print("exc-- in get all get student report data")

    def get_student_report_data(self, data):
        try:
            student_report_data = StudentReportData.objects.all().order_by('-created_at')
            return student_report_data
        except Exception as e:
            print("exc-- in get all get student report data")

    def get_comments(self, data):
        try:
            comments_list = Comments.objects.filter(subject=data.subject)
            return comments_list
        except Exception as e:
            print("exc-- in get all get well being scales")

    def get_well_being_scales(self, data):
        try:
            return WellBeingScale.objects.filter(is_active=True).order_by('id')
        except Exception as e:
            print("exc-- in get all get well being scales")

    def get_overall_rating_scales(self, data):
        try:
            
            return OverallRatingScale.objects.filter(is_active=True)
        except Exception as e:
            print("exc-- in get all get overall rating scales")
        
    def get_lesson(self, data):
        try:
            return data.lesson_code
        except Exception as e:
            print("exc-- in get all students")

    def get_subject_name(self, data):
        try:
            return data.subject
            # return data.subject.subject_name
        except Exception as e:
            print("exc-get_subject_name-")

    def get_subject_behaviour(self, data):
        try:
            behav = Behaviour.objects.filter(subject=data.subject,is_active=True)
            print("Behaviour--", behav)
            behav_list = []
            for b in behav:
                behav_list.append(b)
            return behav_list
        except Exception as e:
            print("exc-get_subject_behaviour-")


    def get_room(self, data):
        try:
            scheduleObj = ScheduleClass.objects.filter(lesson=data)
            return scheduleObj[0].room_number
        except Exception as e:
            print("exc--get_room", e)


    def get_student_count(self, data):
        try:
            return data.student.all().count()
        except Exception as e:
            print("exc--get_student_count")


    def get_teacher(self, data):
        try:
            scheduleObj = ScheduleClass.objects.filter(lesson=data)
            return scheduleObj[0].teacher
            # return scheduleObj.teacher.full_name
        except Exception as e:
            print("exc--get_teacher",e)


    def get_startTime(self, data):
        try:
            scheduleObj = ScheduleClass.objects.filter(lesson=data)
            return scheduleObj[0].class_start_time   
        except Exception as e:
            print("exc--get_startTime")

    def get_endTime(self, data):
        try:
            scheduleObj = ScheduleClass.objects.filter(lesson=data)
            return scheduleObj[0].class_end_time   
        except Exception as e:
            print("exc--get_endTime")


class LessonSerializer(serializers.ModelSerializer):

    class Meta:
        model = Lesson
        fields = ['lesson_id',"all_students", "lesson_code", "subject", "total_students", "room", "teacher", "timings","start_time","scheduled_date"]

    all_students = serializers.SerializerMethodField('get_all_students')
    lesson_code = serializers.SerializerMethodField('get_lesson')
    subject = serializers.SerializerMethodField('get_subject_name')
    total_students = serializers.SerializerMethodField('get_student_count')
    room = serializers.SerializerMethodField('get_room')
    teacher = serializers.SerializerMethodField('get_teacher')
    timings = serializers.SerializerMethodField('get_timings')
    start_time = serializers.SerializerMethodField('get_start_time')
    scheduled_date = serializers.SerializerMethodField('get_scheduled_date')
    lesson_id = serializers.SerializerMethodField('get_lesson_id')

    def get_lesson_id(self, data):
        try:
            from datetime import date,timedelta,datetime
            days_before = (date.today()-timedelta(days=7)).isoformat()
            days_before = datetime.strptime(days_before, '%Y-%m-%d').date()
            # print("days_before------",days_before)
            if date.today() > data.scheduled_date and days_before <= data.scheduled_date:
                return data.lesson.id
                
        except Exception as e:
            print("exc-- in get get_scheduled_date")

    def get_scheduled_date(self, data):
        try:
            from datetime import date,timedelta,datetime
            days_before = (date.today()-timedelta(days=7)).isoformat()
            days_before = datetime.strptime(days_before, '%Y-%m-%d').date()
            # print("days_before------",days_before)
            if date.today() > data.scheduled_date and days_before <= data.scheduled_date:
                return data.scheduled_date
                
        except Exception as e:
            print("exc-- in get get_scheduled_date")

    def get_all_students(self, data):
        try:
            return Student.objects.filter(is_active=True).count()
        except Exception as e:
            print("exc-- in get all students")
        
    def get_lesson(self, data):
        try:
            from datetime import date,timedelta,datetime
            days_before = (date.today()-timedelta(days=7)).isoformat()
            days_before = datetime.strptime(days_before, '%Y-%m-%d').date()
            # print("days_before------",days_before)
            if date.today() > data.scheduled_date and days_before <= data.scheduled_date:
                return data.lesson.lesson_code
        except Exception as e:
            print("exc-- in get all students")

    def get_subject_name(self, data):
        try:
            from datetime import date,timedelta,datetime
            days_before = (date.today()-timedelta(days=7)).isoformat()
            days_before = datetime.strptime(days_before, '%Y-%m-%d').date()
            # print("days_before------",days_before)
            if date.today() > data.scheduled_date and days_before <= data.scheduled_date:
                return data.lesson.subject.subject_name
        except Exception as e:
            print("exc-get_subject_name-")


    def get_room(self, data):
        try:
            from datetime import date,timedelta,datetime
            days_before = (date.today()-timedelta(days=7)).isoformat()
            days_before = datetime.strptime(days_before, '%Y-%m-%d').date()
            # print("days_before------",days_before)
            if date.today() > data.scheduled_date and days_before <= data.scheduled_date:
                return data.room_number
        except Exception as e:
            print("exc--get_room")


    def get_student_count(self, data):
        try:
            from datetime import date,timedelta,datetime
            days_before = (date.today()-timedelta(days=7)).isoformat()
            days_before = datetime.strptime(days_before, '%Y-%m-%d').date()
            # print("days_before------",days_before)
            if date.today() > data.scheduled_date and days_before <= data.scheduled_date:
                return data.lesson.student.all().count()
        except Exception as e:
            print("exc--get_student_count")


    def get_teacher(self, data):
        try:
            from datetime import date,timedelta,datetime
            days_before = (date.today()-timedelta(days=7)).isoformat()
            days_before = datetime.strptime(days_before, '%Y-%m-%d').date()
            # print("days_before------",days_before)
            if date.today() > data.scheduled_date and days_before <= data.scheduled_date:
                return data.teacher.full_name
        except Exception as e:
            print("exc--get_teacher")


    def get_timings(self, data):
        try:
            from datetime import date,timedelta,datetime
            days_before = (date.today()-timedelta(days=7)).isoformat()
            days_before = datetime.strptime(days_before, '%Y-%m-%d').date()
            # print("days_before------",days_before)
            if date.today() > data.scheduled_date and days_before <= data.scheduled_date:
                return '{} {}'.format(data.class_start_time,data.class_end_time)   
        except Exception as e:
            print("exc--get_timings")
    
    def get_start_time(self, data):
        try:
            from datetime import date,timedelta,datetime
            days_before = (date.today()-timedelta(days=7)).isoformat()
            days_before = datetime.strptime(days_before, '%Y-%m-%d').date()
            # print("days_before------",days_before)
            if date.today() > data.scheduled_date and days_before <= data.scheduled_date:
                data.class_start_time = (datetime.strptime(str(data.class_start_time), '%H:%M:%S'))
                data.class_start_time = datetime.strftime(data.class_start_time, '%H:%M %p')
                return data.class_start_time  
        except Exception as e:
            print("exc--get_start_time")

class TeacherLessonSerializer(serializers.ModelSerializer):

    class Meta:
        model = ScheduleClass
        fields = ["id","lesson_id","lesson_code","totalStudents","room","subject","time","all_students","scheduled_date","end_class"]

    lesson_code = serializers.SerializerMethodField('get_lesson_code')
    totalStudents = serializers.SerializerMethodField('get_student_count')
    room = serializers.SerializerMethodField('get_room')
    subject = serializers.SerializerMethodField('get_subject')
    time = serializers.SerializerMethodField('get_time')
    all_students = serializers.SerializerMethodField('get_all_students')
    lesson_id = serializers.SerializerMethodField('get_lesson_id')

    def get_lesson_id(self, data):
        try:
            return  data.lesson.id
        except Exception as e:
            print("Exception", e)

    def get_lesson_code(self, data):
        try:
            return  data.lesson.lesson_code
        except Exception as e:
            print("Exception", e)
    
    def get_student_count(self,data):
        try:
            return data.lesson.student.all().count()
        except Exception as e:
            print("Exception", e)
            
    def get_room(self,data):
        try:
            return data.room_number 
        except Exception as e:
            print("Exception", e)

    def get_subject(self, data):
        try:
            return data.lesson.subject.subject_name
        except Exception as e:
            print("Exception", e)

    def get_time(self,data):
        try:
            from datetime import datetime
            data.class_start_time = (datetime.strptime(str(data.class_start_time), '%H:%M:%S'))
            data.class_start_time = datetime.strftime(data.class_start_time, '%H:%M %p')
            return data.class_start_time 
        except Exception as e:
            print("Exception", e)

    def get_all_students(self, data):
        try:
            return Student.objects.filter(is_active=True).count()
        except Exception as e:
            print("exc-- in get all students")


class LeadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lead
        fields = ["lead_code", "full_name", "studio", "dob", "gender", "nationality", "cbas_testDate", "father_name", "father_phone",
                  "father_email", "mother_name", "mother_phone", "mother_email", "school_name", "school_grade"]

    def create(self, validated_data):
        leadobj = Lead.objects.create(lead_code=validated_data['lead_code'],full_name=validated_data['full_name'], dob=validated_data['dob'],
                                      gender=validated_data['gender'], nationality=validated_data['nationality'],
                                      cbas_testDate=validated_data['cbas_testDate'],
                                      father_name=validated_data['father_name'],
                                      father_phone=validated_data['father_phone'],
                                      father_email=validated_data['father_email'],
                                      mother_name=validated_data['mother_name'],
                                      mother_phone=validated_data['mother_phone'],
                                      mother_email=validated_data['mother_email'],
                                      school_name=validated_data['school_name'],
                                      school_grade=validated_data['school_grade'],
                                      studio_id=validated_data['studio'].id)

        return leadobj

    def update(self, instance, validated_data):
        instance.full_name = validated_data.get('full_name')
        instance.dob = validated_data.get('dob')
        instance.gender = validated_data.get('gender')
        instance.nationality = validated_data.get('nationality')
        instance.cbas_testDate = validated_data.get('cbas_testDate')
        instance.father_name = validated_data.get('father_name')
        instance.mother_name = validated_data.get('mother_name')
        instance.school_name = validated_data.get('school_name')
        instance.school_grade = validated_data.get('school_grade')
        instance.father_email = validated_data.get('father_email')
        instance.mother_email = validated_data.get('mother_email')
        instance.mother_phone = validated_data.get('mother_phone')
        instance.father_phone = validated_data.get('father_phone')
        instance.save()
        return instance


class OverallRatingScaleSerializer(serializers.ModelSerializer):
    class Meta:
        model = OverallRatingScale
        fields = ('overall_rating',)


class WellBeingScaleSerializer(serializers.ModelSerializer):
    class Meta:
        model = WellBeingScale
        fields = ('well_being_scale',)

class BehaviourSerializer(serializers.ModelSerializer):
    class Meta:
        model = Behaviour
        fields = ('behaviour', 'subject')  
      
class FilterDateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScheduleClass
        fields = ("lesson_code","id","lesson_id")   
    
    lesson_code = serializers.SerializerMethodField('get_filtered_lessons')
    from datetime import date
    

    def get_filtered_lessons(self,data):
        lesson_id =  data.id
        print("\n")
        print("Lesson_id-----",lesson_id)
        todayDate = date.today() 
        try:
            date_range = self.context['date_range']
            split_date_range = date_range.split("-")
            min_date = split_date_range[0]
            max_date = split_date_range[1]
            split_min_date = min_date.split("/")
            split_max_date = max_date.split("/")
            
            min_date = split_min_date[2].strip()+"-"+split_min_date[0].strip()+"-"+split_min_date[1].strip()
            max_date = split_max_date[2].strip()+"-"+split_max_date[0].strip()+"-"+split_max_date[1].strip()
            
            from datetime import datetime
            min_date = datetime.strptime(min_date , "%Y-%m-%d")
            max_date = datetime.strptime(max_date , "%Y-%m-%d")
            db_date = datetime.combine(data.scheduled_date, datetime.min.time())

            print("db date---",db_date, type(db_date))
            print("min_date---" ,min_date, type(min_date))
            print("max_date---" ,max_date, type(max_date))

            filtered_data = {}
            if db_date >= min_date and db_date <= max_date:
                print("data.class_start_time",data.class_start_time,type(data.class_start_time))
                
                # from datetime import date,datetime
                data.class_start_time = (datetime.strptime(str(data.class_start_time), '%H:%M:%S'))
                data.class_start_time = datetime.strftime(data.class_start_time, '%H:%M %p')
                print("data.class_start_time------",data.class_start_time,type(data.class_start_time))


                data.scheduled_date = (datetime.strptime(str(data.scheduled_date), '%Y-%m-%d'))
                data.scheduled_date = datetime.strftime(data.scheduled_date, '%B %d, %Y')
                print("data.scheduled_date------",data.scheduled_date,type(data.scheduled_date))

                filtered_data["lesson_code"] = data.lesson.lesson_code
                filtered_data["subject_name"] = data.lesson.subject.subject_name
                filtered_data["start_time"] = data.class_start_time
                filtered_data["teacher"] = data.teacher.full_name
                filtered_data["room"] = data.room_number
                filtered_data["students"] = data.lesson.student.all().count()
                filtered_data['scheduled_date'] = data.scheduled_date
                filtered_data['today'] = todayDate
                return filtered_data

        except Exception as e:
            print("Exception", e)

class FilterActivityDateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = ["lesson","student_report_data","created_at","filtered_activity"]   
    
    filtered_activity = serializers.SerializerMethodField('get_filtered_activity')

    def get_filtered_activity(self,data):
        filtered_data = {}
        from datetime import datetime as dt
        filtered_data['created_at_date'] = dt.strftime(data.student_report_data.created_at, '%d %B, %Y')
        filtered_data["student_image"] = data.student_report_data.student.image.url
        filtered_data['full_name'] = data.student_report_data.student.full_name.title()
        filtered_data['behaviour_value'] = data.student_report_data.behaviour_data.value
        filtered_data['behaviour_name'] = data.student_report_data.behaviour_data.behaviour.behaviour
        filtered_data['subject_name'] = data.lesson.subject.subject_name
        filtered_data['created_at_time'] = dt.strftime(data.student_report_data.created_at, '%H:%M %p')
        filtered_data['student_id'] = data.student_report_data.student.id
        filtered_data['subject_id'] = data.lesson.subject.id
        filtered_data['lesson_id'] = data.lesson.id
        filtered_data['well_being_scale'] = data.student_report_data.well_being.well_being_scale
        filtered_data['overall_rating'] = data.student_report_data.over_all_rating.overall_rating
        filtered_data['gender'] = data.student_report_data.student.gender
        filtered_data['studio_id'] = data.student_report_data.student.studio.id
        return filtered_data

class LevelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Level
        fields = '__all__'   

class CalenderSerializer(serializers.ModelSerializer):

    class Meta:
        model = ScheduleClass
        fields = "__all__"

class OverallSerializer(serializers.ModelSerializer):
    class Meta:
        model = OverallRatingScale
        fields = "__all__"

class WellBeingSerializer(serializers.ModelSerializer):
    class Meta:
        model = WellBeingScale
        fields = "__all__"

class FilterDateGraphSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScheduleClass
        fields = ("lesson_code","id","lesson_id")
    
    lesson_code = serializers.SerializerMethodField('get_filtered_lessons')
    from datetime import date
    

    def get_filtered_lessons(self,data):
        # print("dataaa", data)
        lesson_id =  data.id
        print("\n")
        print("Lesson_id-----",lesson_id)
        todayDate = date.today() 
        try:
            date_range = self.context['date_range']
            split_date_range = date_range.split("-")
            min_date = split_date_range[0]
            max_date = split_date_range[1]
            split_min_date = min_date.split("/")
            split_max_date = max_date.split("/")
            
            min_date = split_min_date[2].strip()+"-"+split_min_date[0].strip()+"-"+split_min_date[1].strip()
            max_date = split_max_date[2].strip()+"-"+split_max_date[0].strip()+"-"+split_max_date[1].strip()
            
            from datetime import datetime
            min_date = datetime.strptime(min_date , "%Y-%m-%d")
            max_date = datetime.strptime(max_date , "%Y-%m-%d")
            db_date = datetime.combine(data.scheduled_date, datetime.min.time())

            print("db date---",db_date, type(db_date))
            print("min_date---" ,min_date, type(min_date))
            print("max_date---" ,max_date, type(max_date))

            filtered_data = {}
            if db_date >= min_date and db_date <= max_date:
                print("data.class_start_time",data.class_start_time,type(data.class_start_time))
                
                # from datetime import date,datetime
                data.class_start_time = (datetime.strptime(str(data.class_start_time), '%H:%M:%S'))
                data.class_start_time = datetime.strftime(data.class_start_time, '%H:%M %p')
                print("data.class_start_time------",data.class_start_time,type(data.class_start_time))


                data.scheduled_date = (datetime.strptime(str(data.scheduled_date), '%Y-%m-%d'))
                data.scheduled_date = datetime.strftime(data.scheduled_date, '%B %d, %Y')
                print("data.scheduled_date------",data.scheduled_date,type(data.scheduled_date))

                filtered_data["lesson_code"] = data.lesson.lesson_code
                filtered_data["id"] = data.lesson.id
                filtered_data["subject_name"] = data.lesson.subject.subject_name
                # filtered_data["start_time"] = data.class_start_time
                filtered_data["teacher"] = data.teacher.id
                # filtered_data["room"] = data.room_number
                # filtered_data["students"] = data.lesson.student.all().count()
                # filtered_data['scheduled_date'] = data.scheduled_date
                # filtered_data['today'] = todayDate
                print("----FILTER DATA", filtered_data)
                return filtered_data

        except Exception as e:
            print("Exception", e)