from django.contrib.auth import get_user_model
from django.http import JsonResponse 
from django.shortcuts import render
from django.views.generic import View
from curioo_admin.models import *
from .serializers import *

from rest_framework.views import APIView
from rest_framework.response import Response

User = get_user_model()

class HomeView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'teacher/charts.html', {"customers": 10})



def get_data(request, *args, **kwargs):
    data = {
        "sales": 100,
        "customers": 10,
    }
    return JsonResponse(data) # http response


class ChartData(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        studentId = 1
        studentData = Student.objects.get(id=1)

        students_report_data = StudentReportData.objects.filter(student=studentData)
        print('students_report_data',students_report_data)
        default_items = []

        for report in students_report_data:
            print("behaviour_data---",report.behaviour_data.value)
            default_items.append(report.behaviour_data.value)
        qs_count1 = Behaviour.objects.all()
        print('qs_count1',qs_count1)
        serializer = BehaviourSerializer(qs_count1, many=True)
        print('serializer',serializer,"serializer data",serializer.data)
        labels = []
        for i in serializer.data:
            print('-----',i['behaviour'])
            labels.append(i['behaviour'])
        
        print("lables--",labels)
        print("default_items--",default_items)

        data = {
                "labels": labels,
                "default": default_items,
        }
        return Response(data)