from curioo_admin.models import *
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import AllowAny
from teacher.status_message import success_message, error_message

class ReportGenerate(APIView):
    permission_classes = (AllowAny,)
    def get(self, request):
        try:
            print('try')
            studentReportData = StudentReportData.objects.all()
            print("studentReportData--",studentReportData)
        except Exception as e:
            return Response(data=error_message(500,'error',str(e),{}), status=status.HTTP_500_INTERNAL_SERVER_ERROR)