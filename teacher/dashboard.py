from django.shortcuts import render, redirect
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from curioo_admin.models import Admin, Principal, Teacher, LearningConsultant
from .getUser import *
from django.http import HttpResponse
from curioo_admin.models import Lead


class AllLeads(APIView):
    permission_classes = (AllowAny,)
    def get(self, request):
        try:
            print('111')
            leads = Lead.objects.filter(is_active=True).count()
            print('leads',leads)
            data = {"status_code": 200, "status": "success",
                    "message": "All Leads get successfully", "data": leads}
            return Response(data=data, status=status.HTTP_200_OK)
        except Exception as error:
            data = {"status_code": 500, "status": "error",
                    "message": "Internal server error"}
            return Response(data=data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

def Dashboard(request):
    return redirect('lessons/')
  
