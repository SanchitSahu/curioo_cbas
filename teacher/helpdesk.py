from curioo_admin.models import HelpDesk as HD
from curioo_admin.models import Studio
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes
from django.shortcuts import render, redirect
from .getUser import gbl
from teacher.status_message import success_message, error_message
from .notify import *


class HelpDsk(APIView):
    permission_classes = (AllowAny,)
    def post(self, request):
        try:
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            name,utype,userObj = gbl(request,token)
            if userObj != None:

                name = request.data["name"]
                email = request.data["email"]
                message = request.data["message"]
                myfile = []
                position = request.data["position"]
                studio = request.data["studio"]

                for file in request.FILES.getlist('myfile'):
                    myfile.append(file)

                if name != "" and email != "" and message != "" and position != "" and studio != "select" and myfile != "":
                    hdCreate =  HD.objects.create(name=name,email=email,message=message,position=position,studio=studio)
                    hdCreate.image = myfile
                    hdCreate.save()
                return Response(data=success_message(201,"success","Data sent successfully",[]), status=status.HTTP_200_OK)
            else:
                return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            print(e)
            return render(request , "teacher/help-desk.html")

    def get(self,request):
        try:
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            name,utype,userObj = gbl(request,token)

            if userObj != None:
                studio_data = Studio.objects.all()
                count = notify(request)
                print("notify context", count)
                context = {"studio" : studio_data,'image':userObj.image,'count':count}
                return render(request , "teacher/help-desk.html" , context)
            else:
                return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            data = {"status_code": 500, "status": "error", "message": str(e)}
            return Response(data=data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)