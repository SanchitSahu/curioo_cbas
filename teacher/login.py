from curioo_admin.models import Teacher, Admin, Principal, LearningConsultant
from django.shortcuts import render, redirect
from curioo_admin.models import Token as tk
from teacher.serializers import LoginSerializer
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from teacher.status_message import success_message, error_message
from rest_framework import status
from django.contrib.auth.hashers import make_password, check_password
from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import AllowAny
from teacher.dashboard import *
from django.utils.translation import ugettext_lazy as _


class Login(RetrieveAPIView):
    permission_classes = (AllowAny,)
    serializer_class = LoginSerializer

    def get(self, request):
        try:
            print("get login")
            # for i in request.session.items():
            #     print("i--",i)
            # del request.session['token']
            if 'token' in request.session:
                print("if")
                try:
                    name, utype, userObj = gbl(request, request.session['token'])
                except Exception as error:
                    return render(request, 'login.html')
                    
                    # return Response(data=error_message(400, "error", str(error), {}), status=status.HTTP_400_BAD_REQUEST)
                if utype == 'teacher':
                    return redirect('dashboard')
                elif utype == 'principal':  
                    return redirect('admin/dashboard')
                elif utype == 'admin':  
                    return redirect('admin/dashboard')
                elif utype == 'consultant':
                    return render(request, 'demo.html')  
            else:
                
                email = ''
                password = ''
                remember_me = ''
                # if 'email' in request.session and 'password' in request.session and 'remember_me' in request.session:
                if 'email' in request.session and 'password' in request.session and 'remember_me' in request.session and request.session['remember_me'] == True:
                    email = request.session['email']
                    teacher_queryset = Teacher.objects.filter(email=email)
                    admin_queryset = Admin.objects.filter(email=email)
                    principal_queryset = Principal.objects.filter(email=email)
                    consultant_queryset = LearningConsultant.objects.filter(email=email)

                    if teacher_queryset:
                        utype1 = 'teacher'
                    elif admin_queryset:
                        utype1 = 'admin'
                    elif principal_queryset:
                        utype1 = 'principal'
                    elif consultant_queryset:
                        utype1 = 'consultant'  
                    password = request.session['password']
                    remember_me = request.session['remember_me']
                    return render(request, 'login.html',{'remember_me_on':remember_me,'password':password,'email':email,'utype2':utype1}) 
                    
                return render(request, 'login.html')
        except Exception as error:
            print("Exception : ", error)
            return Response(data=error_message(400, "error", str(error), {}), status=status.HTTP_400_BAD_REQUEST)


    def post(self, request):
        try:
            print("post login")
            print("request.data-----",request.data)

            remember_me = ''
            if 'email' not in request.data or 'password' not in request.data:
                if 'device' in request.data and request.data['device'] == 'website':
                    return Response(msg=_("email and password are required"))
                    # return render(request, 'login.html', context={'error': "email and password are required"})
                # return Response(data=error_message(400, "error", "email and password are required", {}), status=status.HTTP_400_BAD_REQUEST)

            elif request.data['email'] == '' or request.data['password'] == '':
                if 'device' in request.data and request.data['device'] == 'website':
                    return Response(msg=_("email or password cannot be null"))
                    # return render(request, 'login.html', context={'error': "email or password cannot be null"})
                # return Response(data=error_message(400, "error", "email or password cannot be null", {}), status=status.HTTP_400_BAD_REQUEST)
            else:
                print("request.data-----",request.POST)
                serializer = self.serializer_class(data=request.data)
                print("serializer----",serializer)


                serializer.is_valid(raise_exception=True)

                print("serializer.data['email']====",serializer.data['email'])
                if serializer.data['email'] == '':
                    if request.data['login_type'] == 'teacher':
                        msg = _("Invalid Teacher Credentials")
                    elif request.data['login_type'] == 'principal':
                        msg = _("Invalid Principal Credentials")
                    elif request.data['login_type'] == 'admin':
                        msg = _("Invalid Admin Credentials")
                    elif request.data['login_type'] == 'learning_consultant':
                        msg = _("Invalid Learning Consultant Credentials")
                    
                    if 'device' in request.data and request.data['device'] == 'website':
                        print("yes ")
                        # return Response(data=msg)
                        return Response(data=error_message(400, "error", msg, {}), status=status.HTTP_400_BAD_REQUEST)

                if serializer.data['email'] == 'False' and serializer.data['token'] == 'False':
                    if request.data['login_type'] == 'teacher':
                        msg = _("Teacher has been deleted please contact to admin.")
                    elif request.data['login_type'] == 'principal':
                        msg = _("Principal has been deleted please contact to admin.")
                    elif request.data['login_type'] == 'admin':
                        msg = _("Admin has been deleted please contact to admin.")
                    elif request.data['login_type'] == 'learning_consultant':
                        msg = _("Learning Consultant has been deleted please contact to admin.")
                    
                    if 'device' in request.data and request.data['device'] == 'website':
                        return Response(msg)
                
                if serializer.data['email'] == 'password' and serializer.data['token'] == 'password':
                    print('if not set pass')
                    if request.data['login_type'] == 'teacher':
                        msg = _("Before you login please check your mail and set your password.")
                    elif request.data['login_type'] == 'principal':
                        msg = _("Before you login please check your mail and set your password.")
                    elif request.data['login_type'] == 'admin':
                        msg = _("Before you login please check your mail and set your password.")
                    elif request.data['login_type'] == 'learning_consultant':
                        msg = _("Before you login please check your mail and set your password.")
                    
                    if 'device' in request.data and request.data['device'] == 'website':
                        print('response---msg')
                        # return Response(msg)
                        return Response(data=error_message(400, "error", msg, {}), status=status.HTTP_400_BAD_REQUEST)


                if 'device' in request.data and request.data['device'] == 'website':
                    if 'remember_me' in request.data:
                        remember_me = request.data['remember_me']
                    # else:
                        # if 'remember_me' in request.session:
                            # del request.session['remember_me']
                            # del request.session['password']
                            # del request.session['email']

                    if remember_me == 'on':
                        request.session['email'] = serializer.data['email']
                        request.session['password'] = request.data['password']
                        request.session['remember_me'] = True
                    else:
                        request.session['email'] = serializer.data['email']
                        request.session['password'] = request.data['password']
                        request.session['remember_me'] = False

                    print("remember ME STATUS ----------",request.session['remember_me'])
                    request.session['token'] = serializer.data['token']

                    name, utype, userObj = gbl(request, request.session['token'])
                    request.session['full_name'] = name
                    request.session['user_type'] = utype.capitalize()
                    if utype == "admin" or "principal":
                        request.session['image'] = userObj.image.url

                    if utype == 'teacher':
                        return redirect('dashboard')
                    elif utype == 'principal':  
                        return redirect('admin/dashboard')
                    elif utype == 'admin':
                        return redirect('admin/dashboard')
                    elif utype == 'consultant':
                        return render(request, 'demo.html')
                return Response(data=success_message(200, "success", "User logged in  successfully", serializer.data),status=status.HTTP_200_OK)
        except Exception as error:
            print("Exception in login post-- - ",error)
            return Response(data=error_message(400, "error", str(error), {}), status=status.HTTP_400_BAD_REQUEST)