from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import AllowAny

from teacher.notify import notify
from teacher.status_message import error_message
from django.shortcuts import render, redirect
from .notify import *
from .announcement import *


class Index(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        try:
            print("index")
            # for i in request.session:
            #     print('i---',request.session[i])
            if 'token' in request.session:

                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            if 'token' in request.session:
                userObj = gbl(request,token)
                if userObj != None:
                    count = notify(request)
                    print("notify context", count)
                else:
                    return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)

                if userObj[1] == 'admin' or userObj[1] == 'principal':
                    return redirect('admin/dashboard')

                return render(request , "teacher/index_after_login.html" , context={'image':userObj[2].image,'count':count})
            else:
                # faqs = FAQ.objects.all()
                # serializer = Faqserializer(faqs, many=True)
                return render(request , "index.html")
        except Exception as error:
            print("Exception in index - ",error)
            return Response(data=error_message(400,"error",str(error),[]), status=status.HTTP_400_BAD_REQUEST)