from django.shortcuts import redirect
from rest_framework import status
from rest_framework.response import Response
from curioo_admin.models import Teacher, LearningConsultant

def get_user_obj(token):
    try:
        print('token',token)
        data= Teacher.objects.all()
        for user in data:
            device_token = user.device
            for valid_token in device_token:
                if valid_token == token:
                    return user
    except Exception as e:
        print('error --',e)
        return Response({"error":"something wrong"},status=HTTP_400_BAD_REQUEST)


def user_verify(access_token):
    try:
        print('access_token',access_token)
        data= LearningConsultant.objects.all()
        for user in data:
            device_token = user.device
            for valid_token in device_token:
                if valid_token == access_token:
                    return user
    except Exception as e:
        print('error --',e)
        return Response({"error":"something wrong"},status=HTTP_400_BAD_REQUEST)
