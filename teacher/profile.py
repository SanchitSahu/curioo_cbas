from curioo_admin.models import *
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .serializers import *
from .validations import *
from rest_framework.permissions import AllowAny
from django.shortcuts import render
from .getUser import gbl
from .notify import *

class Profile(APIView):
    permission_classes = (AllowAny,)
    def get(self, request):
        try:
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            userObj = gbl(request,token)
            if userObj != None:
                valid_user = user_verify(token)
                print('else1111111')
                count = notify(request)
                print("notify context", count)
                if valid_user:
                    print('if 1')
                    queryset = LearningConsultant.objects.filter(id=valid_user.id)
                    serializer = LearningConsultantSerializer(queryset,many=True)
                    if serializer:
                        data = {"status_code":200,"status":"success","message":"User details","data":serializer.data}
                        return Response(data=data,status=status.HTTP_200_OK)
                    else:
                        data = {"status_code":400,"status":"error","message":serializer.errors,"data":[]}
                        return Response(data=data,status=status.HTTP_404_NOT_FOUND)
                valid_teacher = get_user_obj(token)
                if valid_teacher:
                    print('if 2')
                    
                    queryset = Teacher.objects.filter(id=valid_teacher.id)
                    serializer = TeacherSerializer(queryset,many=True)
                    if serializer:
                        return render(request, 'teacher/teacher-profile.html',{'teacher_data':serializer.data,'image':userObj[2].image,'count':count})
                    else:
                        data = {"status_code":400,"status":"error","message":serializer.errors,"data":[]}
                        return Response(data=data,status=status.HTTP_404_NOT_FOUND)        
                else:
                    data = {"status_code":400,"status":"error","message":"Not Valid user","data":[]}
                    return Response(data=data,status=status.HTTP_404_NOT_FOUND)
            else:
                return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            print(error)
            data = {"status_code": 500, "status": "error", "message": "Internal server error"}
            return Response(data=data,status=status.HTTP_500_INTERNAL_SERVER_ERROR)