from curioo_admin.models import Behaviour, OverallRatingScale, WellBeingScale, BehaviourData, StudentReportData, Comments, Student, Subject
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import AllowAny
from teacher.status_message import success_message, error_message
from .serializers import BehaviourSerializer, OverallRatingScaleSerializer, WellBeingScaleSerializer

class Ratings(APIView):
    permission_classes = (AllowAny,)
    def get(self, request):
        try:
            allDetails = {}
            behaviours, overallRatingScale, wellBeingScale = Behaviour.objects.all(), OverallRatingScale.objects.all(),  WellBeingScale.objects.all()
            serializer = BehaviourSerializer(behaviours, many=True)
            allDetails['Behaviour'] = serializer.data
            
            serializer = OverallRatingScaleSerializer(overallRatingScale, many=True)
            allDetails['OverallRating'] = serializer.data

            serializer = WellBeingScaleSerializer(wellBeingScale, many=True)
            allDetails['WellBeingScale'] = serializer.data

            data = {"status_code": 200, "status": "success","message": "Behaviour data get sucessfully","data" :allDetails}
            return Response(data=data, status=status.HTTP_200_OK)
        except Exception as e:
            data = {"status_code": 500, "status": "error", "message": str(e)}
            return Response(data=data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request):
        try:
            print('REQUESTED DATA',request.data)
            print('length',len(request.data['behaviour']))
            for i in range(0,len(request.data['behaviour'])):
                print('data')
                behaviour = Behaviour.objects.filter(id=request.data['behaviour'][i]['behaviour_id'])
                print('data behaviour',behaviour, behaviour.count())

                overall_rating = OverallRatingScale.objects.filter(id=request.data['overall'])
                print('data overall_rating',overall_rating, overall_rating.count())

                well_being_scale = WellBeingScale.objects.filter(id=request.data['wellbeing'])
                print('data well_being_scale',well_being_scale, well_being_scale.count())

                student = Student.objects.filter(id=request.data['student_id'])
                print('data student',student)

                subject = Subject.objects.filter(id=request.data['subject_id'])
                print('data subject',subject)
                

                # user = gbl(request)

                if behaviour.count() == 0 or overall_rating.count() == 0 or well_being_scale.count() == 0:
                    print('11111')
                    return Response(data=error_message(400, "error", "Invalid Details", {}), status=status.HTTP_400_BAD_REQUEST)
                else:
                    print('else')
                    behaviourObj = Behaviour.objects.get(id=request.data['behaviour'][i]['behaviour_id'])
                    print('data behaviourObj',behaviourObj)

                    overall_rating_obj = OverallRatingScale.objects.get(id=request.data['overall'])
                    print('data overall_rating_obj',overall_rating_obj)

                    well_being_scale_obj = WellBeingScale.objects.get(id=request.data['wellbeing'])
                    print('data well_being_scale_obj',well_being_scale_obj)

                    student_obj = Student.objects.get(id=request.data['student_id'])
                    print('data student_obj',student_obj)

                    subject_obj = Subject.objects.get(id=request.data['subject_id'])
                    print('data subject_obj',subject_obj)

                    behaviourObjCreate = BehaviourData.objects.create(behaviour=behaviourObj, value=request.data['behaviour'][i]['behaviour_value'])

                    StudentReportDataObj = StudentReportData.objects.create(student=student_obj,behaviour_data = behaviourObjCreate,over_all_rating=overall_rating_obj,well_being=well_being_scale_obj)
                    print('StudentReportDataObj',StudentReportDataObj)

            commentsObj = Comments.objects.create(commented_by_role='teacher',commented_on_role='student',commented_on_id=request.data['student_id'],subject=subject_obj,message=request.data['comment'])
            
            print("commentsObj--",commentsObj)

            return Response(data=success_message(200, "success", "Ratings details get  successfully", {}), status=status.HTTP_200_OK)
                # else:
                #     return Response(data=error_message(400, "error", "Invalid Details", {}), status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response(data=error_message(400, "error", str(e), {}), status=status.HTTP_400_BAD_REQUEST)

