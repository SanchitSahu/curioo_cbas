from curioo_admin.models import *
from curioo_admin.models import Token as tk
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import AllowAny
from django.shortcuts import render, redirect
from teacher.status_message import success_message, error_message


class Logout(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        try:
            if 'device' in request.data and request.data['device'] == 'website':
                token = request.session['token']
                print("token-------logout")
            else:
                token = request.META['HTTP_TOKEN']
            
            if token != '':
                print("in if")
                allTeacher = Teacher.objects.all()
                for teacher in allTeacher:
                    at = [devi for devi in teacher.device if devi == token]
                    if at != []:
                        teacher.device.remove(at[0])
                        if tk.objects.filter(access_token=token).exists():
                            tk.objects.get(access_token=token).delete()
                            teacher.save()

                        if 'device' in request.data and request.data['device'] == 'website':
                            del request.session['token']
                            del request.session['full_name']
                            del request.session['user_type']
                            if 'image' in request.session:
                                del request.session['image']

                            # return redirect('login/')
                            return Response('logout')
                        return Response(data=success_message(200,"success","Teacher logout successfully",{}), status=status.HTTP_200_OK)

                allLearningConsultant = LearningConsultant.objects.all()

                for learning_consultant in allLearningConsultant:
                    aa = [devi for devi in learning_consultant.device if devi == token]
                    if aa != []:
                        learning_consultant.device.remove(aa[0])

                        for i in request.session.items():
                            if tk.objects.filter(access_token=token).exists():
                                tk.objects.get(access_token=token).delete()
                                learning_consultant.save()

                        if 'device' in request.data and request.data['device'] == 'website':
                            del request.session['token']
                            del request.session['full_name']
                            # return redirect('login/')
                            return Response('logout')

                        return Response(data=success_message(200,"success","Learning-consultant logout successfully",{}), status=status.HTTP_200_OK)

                allPrincipal = Principal.objects.all()

                for principal in allPrincipal:
                    ap = [devi for devi in principal.device if devi == token]
                    if ap != []:
                        principal.device.remove(ap[0])
                        for i in request.session.items():
                            if tk.objects.filter(access_token=token).exists():
                                tk.objects.get(access_token=token).delete()
                                principal.save()
                        if 'device' in request.data and request.data['device'] == 'website':
                            del request.session['token']
                            del request.session['full_name']
                            if 'image' in request.session:
                                del request.session['image']
                            # return redirect('login/')
                            return Response('logout')

                        return Response(data=success_message(200,"success","Principal logout successfully",{}),status=status.HTTP_200_OK)

                allAdmin = Admin.objects.all()

                for admin in allAdmin:
                    ap = [devi for devi in admin.device if devi == token]
                    if ap != []:
                        admin.device.remove(ap[0])
                        for i in request.session.items():
                            if tk.objects.filter(access_token=token).exists():
                                tk.objects.get(access_token=token).delete()
                                admin.save()

                        if 'device' in request.data and request.data['device'] == 'website':
                            del request.session['token']
                            del request.session['full_name']
                            if 'image' in request.session:
                                del request.session['image']
                            # return redirect('login/')
                            return Response('logout')
                        return Response(data=success_message(200,"success","Admin logout successfully",{}), status=status.HTTP_200_OK)                
            if 'device' in request.data and request.data['device'] == 'website':
                return redirect('login/')
            return Response(data=error_message(400,"error","invalid token",{}), status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            data = {"status_code": 500, "status": "error", "message": str(e)}
            return Response(data=data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
