from django.http import JsonResponse, HttpResponse

from curioo_admin.models import *
from curioo_admin.models import Lesson as less
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .serializers import *
from .validations import *
from rest_framework.permissions import AllowAny
from teacher.status_message import success_message, error_message
from .getUser import gbl
from django.shortcuts import render, redirect
from rest_framework.decorators import api_view, permission_classes
import json
from rest_framework.renderers import JSONRenderer
from .notify import *

class Analytics(APIView):
    permission_classes = (AllowAny,)
    
    def get(self, request):
        try:
            from datetime import date,timedelta
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            name,utype,userObj = gbl(request,token)
            if userObj != None:
                teacher_obj = Teacher.objects.get(id=userObj.id)

                print("teacher_obj--",teacher_obj)
                # try:
                # if ScheduleClass.objects.filter(teacher=teacher_obj,end_class__isnull=False).exists():
                #     print("----",ScheduleClass.objects.filter(teacher=teacher_obj,end_class__isnull=False))
                #     print("if")
                # else:
                #     print("else")
                if ScheduleClass.objects.filter(teacher=teacher_obj,end_class__isnull=False).exists():
                    latest_schedule_of_teacher = ScheduleClass.objects.filter(teacher=teacher_obj,end_class__isnull=False).latest('id')
                    latest_lesson = Lesson.objects.get(id = latest_schedule_of_teacher.lesson.id)

                # except Exception as error:
                    # print("error-",error)
                else:
                    latest_schedule_of_teacher, latest_lesson = '',''

                # if latest_schedule_of_teacher:
                print("latest_schedule_of_teacher--",latest_schedule_of_teacher)
                print(latest_schedule_of_teacher)
                print(latest_lesson)
                # else:
                subjects = teacher_obj.subject.filter(is_active = True)
                levels = Level.objects.filter(is_active = True)
                print("subjects ",subjects)
                print("teacher_obj-----login----",teacher_obj)
                all_schedule = ScheduleClass.objects.filter(is_active = True)
                for schedule in all_schedule:
                    print("schedule ----",schedule)
                    if teacher_obj == schedule.teacher:
                        print("teacher obj----",schedule.teacher)


                count = notify(request)

                print("notify context", count)
                return render(request, 'teacher/analytics.html', {'latest_schedule':latest_schedule_of_teacher,'teacher_obj':teacher_obj,'image':userObj.image,'count':count,'subjects':subjects,"levels":levels})
            else:
                return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)
                
        except Exception as e:
            return Response(data=error_message(500,'error',str(e),[]), status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
@permission_classes([AllowAny])
def get_lessons(request):
    try:
        requested_subject = request.GET.get('selected_subject_name')
        requested_level = request.GET.get('selected_level_name')
        print("inlesson",requested_subject,requested_level)

        Lesson_obj = Lesson.objects.filter(is_active=True)

        if requested_level!="all":
            Lesson_obj = Lesson_obj.filter(level=requested_level)

        if requested_subject!="all":
            Lesson_obj = Lesson_obj.filter(subject=requested_subject)

        print(Lesson_obj)
        data = [{"id":obj.id,"lesson_code":obj.lesson_code} for obj in Lesson_obj]
        return Response(data)
    except Exception as e:
        print(e)
        return JsonResponse(data=[], safe=False)



