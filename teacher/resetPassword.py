from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from curioo_admin.models import *
from teacher.status_message import success_message, error_message
from django.shortcuts import render
from rest_framework.permissions import AllowAny
from django.contrib.auth.hashers import make_password, check_password



class ResetPassword(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, user_code):
        print("ResetPassword get", user_code)
        splitUserCode1 = user_code.split('_')
        print("splitUserCode1 :",splitUserCode1)
        splitUserCode2 = splitUserCode1[2].split('0')
        id = splitUserCode2[1]
        u_type = splitUserCode2[0]
        print("id type :",id, u_type, splitUserCode2)
        if u_type == "t":
            teacher = Teacher.objects.get(id=int(id))
            if teacher:
                teacherObj = Teacher.objects.get(id=int(id))
                print("teacher password --",teacherObj.password)
                password = teacherObj.password
        elif u_type == "p":
            principal = Principal.objects.get(id=int(id))
            if principal:
                principalObj = Principal.objects.get(id=int(id))
                print("Principal password --",principalObj.password)
                password = principalObj.password
        elif u_type == "l":
            lc = LearningConsultant.objects.get(id=int(id))
            if lc:
                lcObj = LearningConsultant.objects.get(id=int(id))
                print("LearningConsultant password --",lcObj.password)
                password = lcObj.password
        elif u_type == "a":
            admin = Admin.objects.get(id=int(id))
            if admin:
                admin = Admin.objects.get(id=int(id))
                print("admin password --",admin.password)
                password = admin.password
        return render(request,'teacher/reset_password.html',{'user_code':user_code,'password':password,'forgot':True})

    def post(self, request, user_code):
        try:
            print("ResetPassword post", request.data['password'])
            print("ResetPassword get", user_code)
            splitUserCode1 = user_code.split('_')
            print("splitUserCode1 :",splitUserCode1)
            splitUserCode2 = splitUserCode1[2].split('0')
            id = int(splitUserCode2[1])
            user_type = splitUserCode2[0]
            password = request.data['password']
            confirm_password = request.data['confirm_password']
            print('id->',id, type(id))
            print('password->',password)
            print('confirm_password->',confirm_password)
            print('user_type->',user_type)

        except Exception as e:
            print("Exception in Change password 1:",e)
            return Response(data=error_message(400,"error","Invalid data",{}), status=status.HTTP_400_BAD_REQUEST)
        
        if password != confirm_password or password == '' or confirm_password == '':
            # if 'device' in request.data and request.data['device'] == "website":
            #     return render(request,'teacher/reset_password.html',context={'error':'Password and Confirm Password are wrong.'})
            return Response(data=error_message(400,"error","Password and Confirm Password are wrong.",{}), status=status.HTTP_400_BAD_REQUEST)

        elif user_type == 't' and Teacher.objects.filter(id=id).exists():
            print("teacher")
            teacherData = Teacher.objects.get(id=id)
            print("teacherData :",teacherData)
            teacherData.password = make_password(password)
            teacherData.save()
            # if 'device' in request.data and request.data['device'] == "website":
                # return render(request,'teacher/reset_password.html',context={'success':'pass change'})
            return Response(data=success_message(200,"success","Password change.",{}), status=status.HTTP_200_OK)
        elif user_type == 'l' and LearningConsultant.objects.filter(id=id).exists():
            print("learningConsultant")
            LearningConsultantData = LearningConsultant.objects.get(id=id)
            print("LearningConsultantData :",LearningConsultantData)
            LearningConsultantData.password = make_password(password)
            LearningConsultantData.save()
            # if 'device' in request.data and request.data['device'] == "website":
            #     return render(request,'login.html',context={"success" : "Password change."})
            return Response(data=success_message(200,"success","Password change.",{}), status=status.HTTP_200_OK)
        elif user_type == 'p' and Principal.objects.filter(id=id).exists():
            print("Principal")
            PrincipalData = Principal.objects.get(id=id)
            print("PrincipalData :",PrincipalData)
            PrincipalData.password = make_password(password)
            PrincipalData.save()
            # if 'device' in request.data and request.data['device'] == "website":
            #     return render(request,'login.html',context={"success" : "Password change."})
            return Response(data=success_message(200,"success","Password change.",{}), status=status.HTTP_200_OK)
        elif user_type == 'a' and Admin.objects.filter(id=id).exists():
            print("admin")
            adminData = Admin.objects.get(id=id)
            print("adminData :",adminData)
            adminData.password = make_password(password)
            adminData.save()
            # if 'device' in request.data and request.data['device'] == "website":
            #     return render(request,'login.html',context={"success" : "Password change."})
            return Response(data=success_message(200,"success","Password change.",{}), status=status.HTTP_200_OK)
        else:
            # if 'device' in request.data and request.data['device'] == "website":
            #     return render(request,'login.html',context={"error" : "Invalid data."})
            return Response(data=error_message(400,"error","Invalid data",{}), status=status.HTTP_400_BAD_REQUEST)
