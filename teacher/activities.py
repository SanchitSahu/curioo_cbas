from curioo_admin.models import *
from curioo_admin.models import Lesson as less
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .serializers import *
from .validations import *
from rest_framework.permissions import AllowAny
from teacher.status_message import success_message, error_message
from .getUser import gbl
from django.shortcuts import render, redirect
from rest_framework.decorators import api_view, permission_classes
import json
from rest_framework.renderers import JSONRenderer
from .notify import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse

class Activities(APIView):
    permission_classes = (AllowAny,)
    
    def get(self, request):
        try:
            from datetime import date,timedelta
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            name,utype,userObj = gbl(request,token)
            if userObj != None:
                
                teacher_obj = Teacher.objects.get(full_name=name)
                activities = Activity.objects.all().order_by('-created_at')
                comments = Comments.objects.filter(commented_by_id=teacher_obj.id).order_by('-created_at')
                count = notify(request)

                # page = request.GET.get('page',1)
                # paginator = Paginator(activities, 3)
                # try:
                #     activities = paginator.page(page)
                #     print("Try",activities)
                # except PageNotAnInteger:
                #     activities = paginator.page(1)
                #     print("Except PageNotInteger",activities)
                # except EmptyPage:
                #     activities = paginator.page(paginator.num_pages)
               
                print("notify context", count)
                return render(request, 'teacher/activities.html', {'activities':activities, 'comments':comments, 'teacher_obj':teacher_obj,'image':userObj.image,'count':count})
                
            else:
                return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)
                
        except Exception as e:
            data = {"status_code": 500, "status": "error", "message": str(e)}
            return Response(data=data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request):
        try:
            from datetime import date,timedelta
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            name,utype,userObj = gbl(request,token)
            if userObj != None:
                print("Name --",name)
                print("Type--",utype)
                print("-"*50)
                print(request.POST)
                date_range = request.POST['date_range']
                split_date_range = date_range.split("-")
                min_date = split_date_range[0]
                max_date = split_date_range[1]
                split_min_date = min_date.split("/")
                split_max_date = max_date.split("/")
                
                min_date = split_min_date[2].strip()+"-"+split_min_date[0].strip()+"-"+split_min_date[1].strip()
                max_date = split_max_date[2].strip()+"-"+split_max_date[0].strip()+"-"+split_max_date[1].strip()
                
                from datetime import datetime
                min_date = datetime.strptime(min_date , "%Y-%m-%d")
                max_date = datetime.strptime(max_date , "%Y-%m-%d")

                teacher_obj = Teacher.objects.get(full_name=name)
                count = notify(request)
                all_activities = Activity.objects.filter(created_at__date__range=(min_date, max_date)).order_by('created_at')
                serializer = FilterActivityDateSerializer(all_activities , many = True)
               
                comments = Comments.objects.filter(commented_by_id=teacher_obj.id).order_by('-created_at')
                # commets_serializer = FilterCommentsSerializer(comments,many=True)
                print("comments",comments)
                commentList = [ {'lesson': comment.lesson.id, 'subject': comment.subject.id, 'commented_by_id':comment.commented_by_id,'commented_on_id':comment.commented_on_id, 'message':comment.message} for comment in comments ]
                print("\n")
                print("-"*30)


                return Response({'activities':serializer.data,'comments':commentList,'teacher_obj':teacher_obj.id})
                
            else:
                return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)
                
        except Exception as e:
            data = {"status_code": 500, "status": "error", "message": str(e)}
            return Response(data=data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

