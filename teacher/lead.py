from django.shortcuts import redirect
from django.views.decorators.cache import cache_control
from rest_framework.views import APIView

from .getUser import gbl
from .serializers import *
from rest_framework.permissions import AllowAny
from curioo_admin.views import student_code


class Leads(APIView):
    permission_classes = (AllowAny,)

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def post(self, request):
        try:
            name, user_type, user = gbl(request, request.session['token'])
        except Exception as e:
            print('inside exception : ', e)
            return redirect('login/')
        try:
            print('request.data  : ', request.data)
            studio = Studio.objects.get(id=request.data['studio'])

            lead_cod = student_code(studio.studio_code, studio.id)

            request.data['lead_code'] = lead_cod
            serializer = LeadSerializer(data=request.data)
            if serializer.is_valid():
                lead = serializer.save()
                image = request.FILES.get('image')
                if image != '' and image is not None:
                    lead.image = image
                lead.added_by_id = user.id
                lead.added_by_type = user_type.capitalize()
                lead.save()

                Principals = Principal.objects.filter(is_active=True, studio=lead.studio)
                Admins = Admin.objects.all()
                notification_msg = "A new lead was added in " + lead.studio.name + " Studio."
                for Principal_obj in Principals:
                    AdminNotification.objects.create(notification=notification_msg, principal=Principal_obj)

                for Admin_obj in Admins:
                    AdminNotification.objects.create(notification=notification_msg, admin=Admin_obj)

                return Response(data=success_message(200, "success", "Lead added successfully", serializer.data),
                                status=status.HTTP_200_OK)
            return Response(serializer.errors)
        except Exception as e:
            print('exception : ', e)
            return Response(data=error_message(400, "error", 'Bad Request. Please try again.', {}),
                            status=status.HTTP_400_BAD_REQUEST)

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, request):
        try:
            name, user_type, user = gbl(request, request.session['token'])
        except Exception as e:
            print('inside exception : ', e)
            return redirect('login/')
        try:
            if 'id' in request.headers:
                lead_id = request.headers.get('id')
                leads = Lead.objects.filter(id=lead_id)
            else:
                leads = Lead.objects.all().filter(is_active=True)
            if leads:
                serializer = LeadSerializer(leads, many=True)
                return Response(data=success_message(200, "success", "Lead details get  successfully", serializer.data), status=status.HTTP_200_OK)
        except Exception as e:
            return Response(data=error_message(400, "error", str(e), {}), status=status.HTTP_400_BAD_REQUEST)

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def delete(self, request):
        try:
            name, user_type, user = gbl(request, request.session['token'])
        except Exception as e:
            print('inside exception : ', e)
            return redirect('login/')
        try:
            lead_id = request.headers.get('id')
            leads = Lead.objects.filter(id=lead_id)
            if leads:
                Lead.objects.filter(id=lead_id).update(is_active=False)
                return Response(data=success_message(200, "success", "Lead deleted successfully", {}), status=status.HTTP_200_OK)
        except Exception as e:
            return Response(data=error_message(400, "error", str(e), {}), status=status.HTTP_400_BAD_REQUEST)

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def put(self, request):
        try:
            name, user_type, user = gbl(request, request.session['token'])
        except Exception as e:
            print('inside exception : ', e)
            return redirect('login/')
        try:
            lead_id = request.headers.get('id')
            leads = Lead.objects.get(id=lead_id)
            serializer = LeadSerializer(leads, data=request.data, partial=True)
            if serializer.is_valid():
                lead = serializer.save()
                image = request.FILES.get('image')
                if (request.data['added_by_type'] and request.data['added_by_id']) is not None:
                    lead.added_by_type = request.data['added_by_type']
                    lead.added_by_id = request.data['added_by_id']
                    lead.save()
                if image != '' and image is not None:
                    lead.image = image
                    lead.save()
                return Response(data=success_message(200, "success", "Lead details Updated successfully", serializer.data), status=status.HTTP_200_OK)
        except Exception as e:
            return Response(data=error_message(400, "error", str(e), {}), status=status.HTTP_400_BAD_REQUEST)
