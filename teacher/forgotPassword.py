from django.core import mail
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from curioo_admin.models import Teacher, Admin, Principal, LearningConsultant
from curioo_cbas import settings
from teacher.status_message import success_message, error_message
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
import random
import string
from random import randint, sample
from rest_framework.permissions import AllowAny
from .notify import *
import curioo_cbas.settings as settings


class ForgetPassword(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        print("ForgetPassword get")
        # count = notify(request)
        # print("notify context", count)
        return render(request, 'forget.html')

    def post(self, request):
        try:
            print('forgot post')
            email = request.data['email']
        except Exception as e:
            print("Exception in Forgot password :",e)
            if 'device' in request.data and request.data['device'] == "website":
                return render(request,'forget.html',context={'error_message':"Invalid data"})
            else:
                return Response(data=error_message(400,"error","Invalid data",{}), status=status.HTTP_400_BAD_REQUEST)

        if Teacher.objects.filter(email=email).exists():
            user_type = 'teacher'
            Details = Teacher.objects.get(email=email)
        elif Principal.objects.filter(email=email).exists():
            user_type = 'principal'
            Details = Principal.objects.get(email=email)
        elif Admin.objects.filter(email=email).exists():
            user_type = 'admin'
            Details = Admin.objects.get(email=email)
        elif LearningConsultant.objects.filter(email=email).exists():
            user_type = 'learningConsultant'
            Details = LearningConsultant.objects.get(email=email)
        else:
            if 'device' in request.data and request.data['device'] == "website":
                return render(request,'forget.html',context={'error_message':"User with this email is not registered yet"})
            else:
                return Response(data=error_message(400,"error","User with this email is not registered yet",{}), status=status.HTTP_400_BAD_REQUEST)
                
        if user_type == "learningConsultant":
            code = "_u_l0"
        elif user_type == "teacher":
            code = "_u_t0"
        elif user_type == "admin":
            code = "_u_a0"
        elif user_type == "principal":
            code = "_u_p0"
        
        user_code = "".join(random.choices(string.ascii_uppercase + string.digits, k = 20))+code+str(Details.id)
        current_site = get_current_site(request)
        domain = current_site.domain

        url = f"http://{domain}/resetPassword/"+user_code
        print('url',url)

        print("password before mail :", Details.password, type(Details.password))
        print("image ---")
        try:
            subject = 'Forget Password Link for CURIOO'
            if user_type == "admin":
                html_message = render_to_string('teacher/mail.html', {'first_name': Details.full_name, 'url': url,
                                                                      'user_type': user_type.capitalize(),
                                                                      'password': Details.password})
            else:
                html_message = render_to_string('teacher/mail.html', {'first_name': Details.full_name, 'url': url,
                                                                      'user_type':user_type.capitalize(),
                                                                      'password':Details.password,
                                                                      'studio': Details.studio.name})
            plain_message = strip_tags(html_message)
            from_email = settings.DEFAULT_FROM_EMAIL
            to = email
            mail.send_mail(subject, plain_message, from_email,[to], html_message=html_message, fail_silently=False)
            print('mail--',mail)
            return Response(data=success_message(200,"success","Password reset link has been sent to your registered email address",{}), status=status.HTTP_200_OK)
        except Exception as error:
            print("error in forgot mail-----",error)
            return Response(data=error_message(511,"error","Please check your network connection.",{}), status=status.HTTP_511_NETWORK_AUTHENTICATION_REQUIRED)
