from curioo_admin.models import FAQ
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from teacher.serializers import Faqserializer
from rest_framework.permissions import AllowAny
from django.shortcuts import render
from .getUser import gbl
from .notify import *
from teacher.status_message import error_message


class FAQs(APIView):
    permission_classes = (AllowAny,)
    def get(self, request):
        try:
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            if 'token' in request.session:
                userObj = gbl(request,token)
                if userObj != None:
                    faqs = FAQ.objects.all()
                    serializer = Faqserializer(faqs, many=True)
                    print("-----data-----" , serializer.data)
                    count = notify(request)
                    print("notify context", count)
                    # data = {"status_code": 200, "status": "success","message": "FAQ data get sucessfully","data" :serializer.data }
                    # return Response(data=data, status=status.HTTP_200_OK)
                else:
                    return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)
                
                return render(request , "teacher/faq-login.html" , context={'faq_data':serializer.data,'image':userObj[2].image,'count':count})
            else:
                faqs = FAQ.objects.all()
                serializer = Faqserializer(faqs, many=True)
                return render(request , "faq-before-login.html" , context={'faq_data':serializer.data})
        except Exception as error:
            print("Exception in index - ",error)
            return Response(data=error_message(400,"error",str(error),[]), status=status.HTTP_400_BAD_REQUEST)