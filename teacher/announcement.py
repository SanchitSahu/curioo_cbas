from curioo_admin.models import *
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .serializers import *
from rest_framework.permissions import AllowAny
from django.shortcuts import render
from datetime import datetime
from .getUser import gbl
from .notify import *


class Announcemnt(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        try:
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            userObj = gbl(request,token)
            if userObj != None:
                print("userObj",userObj[2])
                teacher_obj = Teacher.objects.get(id=userObj[2].id)
                announcement = Announcement.objects.all().order_by('created_at')
                serializer = AnnouncementSerializer(announcement, many=True)
                count = notify(request)
                print("notify context", count)
                # data = {"status_code": 200, "status": "success",
                #         "message": "Announcements", "data": serializer.data}
                # return Response(data=data, status=status.HTTP_200_OK)
                
                # for announce in announcement:
                #     print('\nType of created_at', datetime.strftime(announce.created_at, '%d %B,%Y'))
                #     print('\nType of created_at', datetime.strftime(announce.created_at, '%H:%M %p'))
                
                return render(request,'teacher/announcement.html',{'announcements':serializer.data,'image':userObj[2].image,'count':count,'teacher_obj':teacher_obj})
            else:
                return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            print(error)
            data = {"status_code": 500, "status": "error",
                    "message": "Internal server error"}
            return Response(data=data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class Notification(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        try:
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            userObj = gbl(request,token)
            if userObj != None:
                print('notifications hey')
                print(userObj[2].id)
                print(userObj[1])
                notification = WebNotification.objects.filter(respective_user_id = userObj[2].id , respective_user_type = userObj[1]).order_by("-created_at")
                print("notification", notification)
                for seen in notification:

                    print('seen',seen.created_at)
                    seen.is_seen = True
                    seen.save()
                serializer = NotificationSerializer(notification, many=True)
                # data = {"status_code": 200, "status": "success",
                #         "message": "Notification", "data": serializer.data}
                context = {'data' : serializer.data,'image':userObj[2].image}
                for obj in serializer.data:
                    print("data------" , obj)
                return render(request , "teacher/notification.html" , context)
            # return Response(data=data, status=status.HTTP_200_OK)
            else:
                return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            print(error)
            data = {"status_code": 500, "status": "error",
                    "message": "Internal server error"}
            return Response(data=data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)