from curioo_admin.models import *
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .serializers import *
from .validations import *
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render,HttpResponse
from django.contrib.auth.hashers import make_password, check_password
from teacher.status_message import success_message, error_message
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny


@csrf_exempt
@api_view(['POST'])
@permission_classes((AllowAny,))
def change_password(request):
    """ Teacher change password after Login. """
    try:
        print("change_password----")
        token = request.session['token']
        if token=="" or token==" ":
            data = {"status_code":404,"status":"error","message":"access token not available"}
            return Response(data=data,status=status.HTTP_404_NOT_FOUND)
        else:
            if request.method == "POST":
                valid_teacher = get_user_obj(token)
                if valid_teacher:
                    old_password = request.POST.get('old_password')
                    new_passowrd = request.POST.get('new_password')
                    c_passowrd = request.POST.get('cnew_password')
                    
                    queryset = Teacher.objects.get(id=valid_teacher.id)
                    # print("old pass--1-",old_password)

                    # old_password = make_password(old_password)
                    # print("old pass---",old_password)
                    # print("queryset.password---",queryset.password)
                    if not check_password(old_password,queryset.password):
                        print("invalid old pass")
                        return Response(data=error_message(400,"error","Invalid Old Password.",{}), status=status.HTTP_404_NOT_FOUND)
                    elif c_passowrd != new_passowrd :
                        print("not match new conf")
                        return Response(data=error_message(400,"error","Confirm and New password does not match .",{}), status=status.HTTP_404_NOT_FOUND)

                    else:
                        print("update")
                        queryset.password = make_password(new_passowrd)
                        queryset.save()
                        return Response(data=success_message(200,"success","You've changed your password Successfully.",{}), status=status.HTTP_200_OK)

                else:
                    data = {"status_code":400,"status":"error","message":"Not Valid user","data":[]}
                    return Response(data=data,status=status.HTTP_404_NOT_FOUND)
    except Exception as error:
        print("error---",error)
        data = {"status_code": 500, "status": "error", "message": "Internal server error"}
        return Response(data=data,status=status.HTTP_500_INTERNAL_SERVER_ERROR)