from curioo_admin.models import *
from curioo_admin.models import Lesson as less
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .serializers import *
from .validations import *
from rest_framework.permissions import AllowAny
from teacher.status_message import success_message, error_message
from .getUser import gbl
from django.shortcuts import render, redirect
from rest_framework.decorators import api_view, permission_classes
import json
from rest_framework.renderers import JSONRenderer
from .notify import *
from teacher.serializers import FilterDateGraphSerializer as fd

class LessonReport(APIView):
    permission_classes = (AllowAny,)
    
    def get(self, request, lesson_id):
        try:
            from datetime import date,timedelta
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            name,utype,userObj = gbl(request,token)
            if userObj != None:
                teacher_obj = Teacher.objects.get(full_name=name)
                # all_schedule = ScheduleClass.objects.all()
                # for schedule in all_schedule:

                    # print("schedule ----",schedule)
                if Lesson.objects.filter(id=lesson_id, is_active=True):
                    lessObj = Lesson.objects.get(id=lesson_id, is_active=True)
                    if ScheduleClass.objects.filter(lesson=lessObj).exists():
                        scheduleDetails = ScheduleClass.objects.get(lesson=lessObj,is_active=True)
                        print("scheduleDetails----",scheduleDetails)
                    else:
                        print("else 1")
                        scheduleDetails = {}
                else:
                    print("else 2")
                    scheduleDetails = {}

                count = notify(request)

                print("notify context", count)
                return render(request, 'teacher/lesson_report.html', {'scheduleDetails':scheduleDetails,'image':userObj.image,'count':count})
                
            else:
                return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)
                
        except Exception as e:
            return Response(data=error_message(500,'error',str(e),[]), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST'])
@permission_classes([ AllowAny, ])
def LessonReportDate(request):
    try:
        print("date range", request.POST['LessonId'])
        LessonId = request.data['LessonId']
        LessonData = Lesson.objects.get(id=LessonId, is_active = True)
        StudentsinLesson =LessonData.student.all()
        subject_of_lesson = LessonData.subject.subject_name

        motivational_dict_avg = {}
        wellbeing_dict_avg = {}
        cnt = 0
        all_lesson_of_subject = Lesson.objects.filter(subject=LessonData.subject.id, is_active = True)
        for lesson_obj in all_lesson_of_subject:
            studREport_for_avg = StudentReportData.objects.filter(lesson=lesson_obj.id)
            for student in studREport_for_avg:
                if student.student.full_name in motivational_dict_avg:
                    motivational_dict_avg[student.student.full_name].append(student.over_all_rating.value)
                else:
                    motivational_dict_avg[student.student.full_name] = []
                    motivational_dict_avg[student.student.full_name].append(student.over_all_rating.value)


                if student.student.full_name in wellbeing_dict_avg:
                    wellbeing_dict_avg[student.student.full_name].append(student.well_being.value)
                else:
                    wellbeing_dict_avg[student.student.full_name] = []
                    wellbeing_dict_avg[student.student.full_name].append(student.well_being.value)

        print("Average bef Rating", motivational_dict_avg)
        print("Average bef Wellbeing", wellbeing_dict_avg)

        for key in motivational_dict_avg.keys():
            motivational_dict_avg[key] = round(sum(motivational_dict_avg[key])/len(motivational_dict_avg[key]),2)
            wellbeing_dict_avg[key] = round(sum(wellbeing_dict_avg[key])/len(wellbeing_dict_avg[key]),2)

        print("Average Rating",motivational_dict_avg)
        print("Average Wellbeing",wellbeing_dict_avg)

        from collections import Counter
        result = {"behaviour_chart": {}, "average_chart": {}, "behaviour_avg_chart": {}}
        final_behaviour_list = []
        for lesson in all_lesson_of_subject:
            students = lesson.student.all()
            for student in students:
                print("STUDENT DETAILS")
                behaviour_dict = {}
                behaviour_data = BehaviourData.objects.filter(lesson=LessonId, student=student)
                print("behaviour data", behaviour_data)
                for behaviour_data in behaviour_data:
                    behaviour_dict.update({behaviour_data.behaviour.behaviour: behaviour_data.value})
                    print(behaviour_dict)
                #avg_score_list.update({student.full_name: sum(behaviour_dict.values())})
                final_behaviour_list.append(behaviour_dict)
        print("final behavior list : ",final_behaviour_list)

        total = sum(map(Counter, final_behaviour_list), Counter())
        N = float(len(final_behaviour_list))

        result['behaviour_avg_chart'].update({"labels": [k for k, v in total.items()]})
        result['behaviour_avg_chart'].update({"data": [round((v / N),2) for k, v in total.items()]})
        # result['behaviour_avg_chart'].update({"label": LessonData.subject.subject_name.lower()})

        StudREport = StudentReportData.objects.filter(lesson=LessonId)
        student = LessonData.student.all()

        final_behaviour_list = []
        avg_score_list = {}

        for student in student:
            print("STUDENT DETAILS")
            behaviour_dict = {}
            behaviour_data = BehaviourData.objects.filter(lesson=LessonId, student=student)
            print("behaviour data", behaviour_data)
            for behaviour_data in behaviour_data:
                behaviour_dict.update({behaviour_data.behaviour.behaviour: behaviour_data.value})
                print(behaviour_dict)
            avg_score_list.update({student.full_name: sum(behaviour_dict.values())})
            final_behaviour_list.append(behaviour_dict)

        print("final ",final_behaviour_list)
        print("avg ",avg_score_list)

        total = sum(map(Counter, final_behaviour_list), Counter())
        N = float(len(final_behaviour_list))


        result['average_chart'] = avg_score_list
        result['behaviour_chart'].update({"labels": [k for k, v in total.items()]})
        result['behaviour_chart'].update({"data": [v / N for k, v in total.items()]})
        result['behaviour_chart'].update({"label": LessonData.subject.subject_name.lower()})
        print("result ",result)

        min_max_data = {}
        rating_data = {}
        wellbeing_data = {}
        motivational_dict = {}
        wellbeing_dict = {}

        ratings = OverallRatingScale.objects.filter(is_active=True)
        for obj in ratings:
            rating_data[obj.value] = obj.overall_rating

        wellbeing = WellBeingScale.objects.filter(is_active=True)
        for obj in wellbeing:
            wellbeing_data[obj.value] = obj.well_being_scale

        # print("max rating",max(rating_data.keys()))
        # print("min rating",min(rating_data.keys()))

        min_max_data["max rating"]=max(rating_data.keys())
        min_max_data["min rating"] = min(rating_data.keys())
        min_max_data["max wellbeing"] = max(wellbeing_data.keys())
        min_max_data["min wellbeing"] = min(wellbeing_data.keys())

        for student in StudREport:
            if student.student.id in motivational_dict:
                pass
            else:
                motivational_dict[student.student.full_name] = student.over_all_rating.value

            if student.student.id in wellbeing_dict:
                pass
            else:
                wellbeing_dict[student.student.full_name] = student.well_being.value

        schedule_obj = ScheduleClass.objects.get(lesson=LessonId)
        schedule_obj_dict = {"lesson_code":schedule_obj.lesson.lesson_code,"subject":schedule_obj.lesson.subject.subject_name,"time":schedule_obj.class_start_time,"date":schedule_obj.scheduled_date,"room":schedule_obj.room_number}

        data={"wellbeing_avg":wellbeing_dict_avg,"motivation_avg":motivational_dict_avg,"schedule_obj":schedule_obj_dict,"chart":result,"motivation": motivational_dict,"wellbeing":wellbeing_dict,"subject":subject_of_lesson.lower(),"wellbeing_data":wellbeing_data,"rating_data":rating_data,"min_max_data":min_max_data}
        print(data)
        return Response(data)
    except Exception as e:
        print(e)