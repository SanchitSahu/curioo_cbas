from curioo_admin.models import *
from .serializers import *
from rest_framework.response import Response
from rest_framework.views import APIView
from teacher.status_message import success_message, error_message
from rest_framework import status
from rest_framework.permissions import AllowAny
import logging
from datetime import datetime
from .getUser import gbl
from django.shortcuts import render, redirect
from curioo_admin.validations import calculate_age
from rest_framework.decorators import api_view,permission_classes
from .notify import *



@api_view(['GET','POST'])
@permission_classes([AllowAny, ])
def calender(request):
    dictt = { "english": "#F3ACB3", "entrepreneur": "#1691B3", "technology": "#F6BE57", "creative design": "#EB5858",
                            "default": ["#0f6b84", "#678388", "#9a5653", "#FFEB3B", "#FF9800", "#673AB7"] }
    if request.method == 'GET':
        try:
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            userObj = gbl(request,token)
            if userObj != None:
                print("----userOBJ", userObj)
                classes = ScheduleClass.objects.filter(teacher = userObj[2])
                count = notify(request)
                print("notify context", count)
                context = {'class': classes , 'image':userObj[2].image,'count':count}
                return render(request,'teacher/calender.html' , context)
            else:
                return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print("Exception in Calender(get) --- ",e)
            return Response(data=error_message(400,"error",str(e),{}), status=status.HTTP_400_BAD_REQUEST)


    if request.method == 'POST':
        try:
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            userObj = gbl(request,token)
            if userObj != None:
                color_dict = {}
                color_count = 0
                subjects = Subject.objects.all()
                for subject in subjects:
                    if subject.subject_name.lower() in dictt:
                        color_dict[subject.subject_name.lower()] = dictt[subject.subject_name.lower()] 
                    else:
                        color_dict[subject.subject_name.lower()] = dictt["default"][color_count]
                        color_count = color_count + 1
                print("color_dict", color_dict)
                dates = request.POST['date']
                lesson_code = []
                subject_code = []
                lesson_id = []
                classes = ScheduleClass.objects.filter(teacher = userObj[2])
                print("dateeeeeeeee" , dates)
                print(datetime.strftime(datetime.strptime(dates, '%Y-%m-%d'),'%d %B %Y'))
                count = notify(request)
                print("notify context", count)
                show_date = ScheduleClass.objects.filter(scheduled_date = dates, teacher = userObj[2])
                for date in show_date:
                    lesson_code.append(date.lesson.lesson_code)
                    subject_code.append(date.lesson.subject.subject_name)
                    lesson_id.append(date.lesson.id)

                serializers = CalenderSerializer(show_date , many=True)
                print("last---",serializers.data)

                context = {'count' : count ,'dates' : datetime.strftime(datetime.strptime(dates, '%Y-%m-%d'),'%d %B %Y') , 'hi':'hi' , 'show_date' : serializers.data , 'code' : lesson_code , 'subject' : subject_code, 'dict':color_dict}
                return Response(context)
            else:
                return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print("Exception in Calender(post) --- ",e)
            return Response(data=error_message(400,"error",str(e),{}), status=status.HTTP_400_BAD_REQUEST)