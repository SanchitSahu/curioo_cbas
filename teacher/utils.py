from calendar import timegm
from datetime import datetime
from rest_framework_jwt.compat import get_username_field
from rest_framework_jwt.settings import api_settings


def jwt_payload_handler(userData):
    print("jwt_payload_handler-----", userData, 'id--', userData.id, 'login_type----',userData.login_type)
    username_field = get_username_field()
    print("username_field --", username_field)
    if userData:
        print('userData', userData)
        payload = {'user_id': userData.id, 'email': userData.email,
                   'exp': datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA, username_field: userData.email}
        print("payload -1-", payload)
        if api_settings.JWT_ALLOW_REFRESH:
            print("if 1")
            payload['orig_iat'] = timegm(
                datetime.utcnow().utctimetuple()
            )
            print("payload['orig_iat']-----",payload['orig_iat'])

        if api_settings.JWT_AUDIENCE is not None:
            print("if 2")
            payload['aud'] = api_settings.JWT_AUDIENCE
            print("payload['aud']----",payload['aud'])

        if api_settings.JWT_ISSUER is not None:
            print("if 3")
            payload['iss'] = api_settings.JWT_ISSUER
            print("payload['iss']----",payload['iss'])
        
        print("payload final ----",payload)
        return payload
