from curioo_admin.models import *
from .serializers import *
from rest_framework.response import Response
from rest_framework.views import APIView
from teacher.status_message import success_message, error_message
from rest_framework import status
from rest_framework.permissions import AllowAny
import logging
from .getUser import gbl
from django.shortcuts import render, redirect
from curioo_admin.validations import calculate_age
from teacher.status_message import success_message, error_message
from .notify import *
import copy
from datetime import date, timedelta


class AllStudent(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        try:
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            userObj = gbl(request, token)
            if userObj != None:
                allStudents = Student.objects.filter(is_active=True, studio = userObj[2].studio)
                print("allStudents--",allStudents)
                level = Level.objects.filter(is_active=True)
                serializer = StudentSerializer(allStudents, many=True)
                allScoreList = []

                level_exists = False
                for sd in serializer.data:
                    print("sd['score']------------",sd)
                    if sd['current_level'] != None:
                        level_exists = True
                    for score1 in sd['score']:
                        if score1.score not in allScoreList:
                            allScoreList.append(score1.score)
                sum = 0
                allScoreListLength = len(allScoreList)
                if allScoreListLength != 0:
                    for index in range(0, len(allScoreList)):
                        sum = sum + float(allScoreList[index])
                    score = sum/allScoreListLength
                    score = round(score, 2)
                else:
                    score = 0

                count = notify(request)
                print("notify context", count)
                if 'token' in request.headers:  # for API
                    return Response(data=success_message(200, "success", "Display Students", serializer.data), status=status.HTTP_200_OK)
                    # return render(request,"allStudent.html",context={'all_student':serializer.data})

                elif 'token' in request.session:  # for WEBPAGE
                    return render(request, "teacher/allStudent.html", context={'count': count, 'all_students': serializer.data, 'level': level, 'image': userObj[2].image, 'score': score, 'levelExists': level_exists})
            else:
                return Response(data=error_message(400, "error", "invalid token", []), status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print("Exception in All Student --- ", e)
            return Response(data=error_message(400, "error", str(e), {}), status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        # try:

        if 'token' in request.session:
                token = request.session['token']

        elif 'token' in request.headers:
            token = request.headers.get('token')

        userObj = gbl(request, token)
        if userObj != None:
            level = request.POST['level']
            age_range = request.POST['age_range']
            stud_name = request.POST['stud_name']

            level_show = Level.objects.filter(is_active=True)
            level_serializer = LevelSerializer(level_show, many=True)
            names_show = Student.objects.filter(is_active=True)
            names_show_serializer = StudentDetailsSerializer(
                names_show, many=True)

            if level != "Select Level" and age_range == "Select Age" and stud_name == "Select Name":
                print("in if level 1")
                all_student = Student.objects.filter(
                    current_level=Level.objects.get(level=level),is_active=True)
                allStudentSerilizer = StudentDetailsSerializer(
                    all_student, many=True)
                print("userObj.image---", userObj[2].image)
                context = {}
                context["all_student"] = allStudentSerilizer.data
                context['names'] = names_show_serializer.data
                context['level'] = level
                context['level_show'] = level_serializer.data
                # context = {"all_student" : all_student , 'levels' : level , 'level' : level_show , 'all_students' : names_show ,'image':userObj[2].image}
                print("done 1")
                return Response(data=success_message(200, "success", "Data get successfully.", context), status=status.HTTP_200_OK)

            if level != "Select Level" and age_range != "Select Age" and stud_name == "Select Name":
                print("in if level 1 & 2")
                stud = Student.objects.filter(
                    current_level=Level.objects.get(level=level),is_active=True)
                ranges = age_range.split("-")
                min_range = ranges[0]
                max_range = ranges[1]

                student_details = []

                for birth in stud:
                    current_age = calculate_age(birth.dob)
                    if current_age >= int(min_range) and current_age <= int(max_range):
                        stud_show = Student.objects.get(
                            id=birth.id, current_level=Level.objects.get(level=level),is_active=True)
                        allStudentSerilizer = StudentDetailsSerializer(
                            stud_show)
                        student_details.append(allStudentSerilizer.data)
                        # student.append(birth)
                    else:
                        pass
                context = {}
                context["all_student"] = student_details
                context['names'] = names_show_serializer.data
                context['level'] = level_serializer.data
                context['age_range'] = age_range
                # context['age_range_show'] = allStudentSerilizer.data
                # context = {"all_student" : student , 'levels' : level , 'ages' : age_range , 'level' : level_show , 'all_students' : names_show,'image':userObj[2].image}
                print("done 1&2")
                return Response(data=success_message(200, "success", "Data get successfully.", context), status=status.HTTP_200_OK)

            if level != "Select Level" and age_range == "Select Age" and stud_name != "Select Name":
                print("in if level 1&3")
                all_student = Student.objects.filter(
                    current_level=Level.objects.get(level=level), full_name=stud_name,is_active=True)
                # context = {"all_student" : all_student , 'levels' : level , 'names' : stud_name , 'level' : level_show , 'all_students' : names_show,'image':userObj[2].image}
                allStudentSerilizer = StudentDetailsSerializer(
                    all_student, many=True)
                context = {}
                context["all_student"] = allStudentSerilizer.data
                context['names'] = stud_name
                context['level'] = level_serializer.data
                context['names_show'] = names_show_serializer.data
                print("done 1&3")
                return Response(data=success_message(200, "success", "Data get successfully.", context), status=status.HTTP_200_OK)

            if level == "Select Level" and age_range != "Select Age" and stud_name == "Select Name":
                print("in if age 2")
                ranges = age_range.split("-")
                min_range = ranges[0]
                max_range = ranges[1]

                stud = Student.objects.filter(is_active=True)
                stud_show = ""
                student_details = []
                for birth in stud:
                    current_age = calculate_age(birth.dob)
                    if int(current_age) >= int(min_range) and int(current_age) <= int(max_range):
                        # student.append(birth)
                        stud_show = Student.objects.get(id=birth.id,is_active=True)
                        allStudentSerilizer = StudentDetailsSerializer(
                            stud_show)
                        student_details.append(allStudentSerilizer.data)
                        print("student id -----********-", birth.id)
                    else:
                        pass
                print("student_details---", student_details)
                print("userObj.image---", userObj[2].image)
                context = {}
                context["all_student"] = student_details
                context['names'] = names_show_serializer.data
                context['level'] = level_serializer.data
                context['age_range'] = age_range
                # context['age_range_show'] = allStudentSerilizer.data
                # context = {"all_student" : student , 'ages' : age_range , 'level' : level_show , 'all_students' : names_show,'image':userObj[2].image}
                print("done 2")
                return Response(data=success_message(200, "success", "Data get successfully.", context), status=status.HTTP_200_OK)

            if level == "Select Level" and age_range != "Select Age" and stud_name != "Select Name":
                print("in if age 2&3")
                ranges = age_range.split("-")
                min_range = ranges[0]
                max_range = ranges[1]

                student_details = []

                stud = Student.objects.filter(full_name=stud_name,is_active=True)
                for birth in stud:
                    current_age = calculate_age(birth.dob)
                    if current_age >= int(min_range) and current_age <= int(max_range):
                        # student.append(birth)
                        stud_show = Student.objects.get(id=birth.id,is_active=True)
                        allStudentSerilizer = StudentDetailsSerializer(
                            stud_show)
                        student_details.append(allStudentSerilizer.data)
                    else:
                        pass
                context = {}
                context["all_student"] = student_details
                context['names'] = names_show_serializer.data
                context['level'] = level_serializer.data
                context['age_range'] = age_range
                # context = {"all_student" : student , 'ages' : age_range , 'names' : stud_name , 'level' : level_show , 'all_students' : names_show,'image':userObj[2].image}
                print("done 2&3")
                return Response(data=success_message(200, "success", "Data get successfully.", context), status=status.HTTP_200_OK)

            if level == "Select Level" and age_range == "Select Age" and stud_name != "Select Name":
                print("in if 3")
                all_student = Student.objects.filter(full_name=stud_name,is_active=True)
                allStudentSerilizer = StudentDetailsSerializer(
                    all_student, many=True)
                print("userObj.image---", userObj[2].image)
                context = {}
                context["all_student"] = allStudentSerilizer.data
                context['names'] = stud_name
                context['level'] = level_serializer.data
                context['names_show'] = names_show_serializer.data
                print("done 3")
                return Response(data=success_message(200, "success", "Data get successfully.", context), status=status.HTTP_200_OK)

            if level != "Select Level" and age_range != "Select Age" and stud_name != "Select Name":
                print("in if level 1 & 2 & 3")
                stud = Student.objects.filter(
                    current_level=Level.objects.get(level=level), full_name=stud_name,is_active=True)
                ranges = age_range.split("-")
                min_range = ranges[0]
                max_range = ranges[1]

                student_details = []

                for birth in stud:
                    current_age = calculate_age(birth.dob)
                    if current_age >= int(min_range) and current_age <= int(max_range):
                        stud_show = Student.objects.get(id=birth.id,is_active=True)
                        allStudentSerilizer = StudentDetailsSerializer(
                            stud_show)
                        student_details.append(allStudentSerilizer.data)
                    else:
                        pass
                context = {}
                context["all_student"] = student_details
                context['names'] = names_show_serializer.data
                context['level'] = level_serializer.data
                context['age_range'] = age_range
                # context = {"all_student" : student , 'levels' : level , 'ages' : age_range , 'names' : stud_name , 'level' : level_show , 'all_students' : names_show,'image':userObj[2].image}
                print("done 1&2&3")

            if level == "Select Level" and age_range == "Select Age" and stud_name == "Select Name":
                print("in if level 1 & 2 & 3 all")

                stud = Student.objects.filter(is_active=True)
                allStudentSerilizer = StudentDetailsSerializer(stud, many=True)
                context = {}
                context["all_student"] = allStudentSerilizer.data
                # context['names'] = names_show_serializer.data
                # context['level'] = level_serializer.data
                # context['age_range'] = age_range
                print("done 1&2&3 all")

                return Response(data=success_message(200, "success", "Data get successfully.", context), status=status.HTTP_200_OK)
                # return render(request,"teacher/allStudent.html" , context)
        # except Exception as e:
        #     print("Exception in All Student filter --- ",e)
        #     return Response(data=error_message(400,"error",str(e),{}), status=status.HTTP_400_BAD_REQUEST)


class StudentDetails(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, student_id):
        try:
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            userObj = gbl(request, token)
            if userObj != None:
                is_student = Student.objects.filter(id=int(student_id),is_active=True)
                if is_student:
                    studentData = Student.objects.get(id=int(student_id),is_active=True)
                    behaviour_data = BehaviourData.objects.filter(
                        student=studentData)
                    serializer = StudentDetailsSerializer(studentData)
                    print("serializer data-", serializer.data)
                    # subjectObj = Subject.objects.all()
                    subjectObj = BehaviourData.objects.filter(
                        student=studentData)
                    sub_data = {}
                    for obj in subjectObj:
                        sub_data[obj.subject.id] = obj.subject.subject_name
                    print('subjectObj', sub_data)
                    count = notify(request)
                    print("notify context", count)
                if 'token' in request.headers:  # for API
                    return Response(data=success_message(200, "success", "Display Student Details", serializer.data), status=status.HTTP_200_OK)
                    # return render(request,"studentDetail.html",context={'student_data':serializer.data})

                elif 'token' in request.session:  # for WEBPAGE
                    print('11111111111')
                    return render(request, "teacher/studentDetail.html", context={'count': count, 'student_data': serializer.data, 'image': userObj[2].image, 'subjectObj': sub_data})
            else:
                return Response(data=error_message(400, "error", "invalid token", []), status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print("Exception in Student details --- ", e)
            return Response(data=error_message(400, "error", str(e), {}), status=status.HTTP_400_BAD_REQUEST)


class ChartData(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, format=None):
        try:
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            userObj = gbl(request, token)
            if userObj != None:
                studentId = request.data['studentId'] # 1
                # print("student id" , studentId)
                subjectId = request.data.getlist('subjectId[]') # ['1', '2', '3', '4']
                # studio_id = userObj[2].studio
                studentData = Student.objects.filter(id=studentId,is_active=True) # Student object (1)
                if studentData:
                    studentData = Student.objects.get(id=studentId,is_active=True) # Student object (1)
                    students_report_data = StudentReportData.objects.filter(student=studentData)
                    behaviour_data = BehaviourData.objects.filter(student=studentData)
                wellbeing_objs = WellBeingScale.objects.filter(is_active=True)
                overall_objs = OverallRatingScale.objects.filter(is_active=True)
                wellbeing_labels = {}
                for obj in wellbeing_objs:
                    wellbeing_labels[int(obj.value)] = obj.well_being_scale
                wellbeing_minmax = []
                wellbeing_minmax.append(min(wellbeing_labels.keys()))
                wellbeing_minmax.append(max(wellbeing_labels.keys()))
                overall_labels = {}
                for obj in overall_objs:
                    overall_labels[int(obj.value)] = obj.overall_rating
                overall_minmax = []
                overall_minmax.append(min(overall_labels.keys()))
                overall_minmax.append(max(overall_labels.keys()))
                # print("min-max------======------=-=-=-=-=-=-=-=-=-=", overall_minmax, wellbeing_minmax)


                default_items_dict = {} # {}
                labels_dict = {} # {}
                motivational_dict = {}
                wellbeing_dict = {}
                avg_count = {}
                # for sub in subjectId:
                #     default_items_dict[sub] = {} #default_items_dict[1]= {}
                #     labels_dict[sub] = [] #labels_dict[1]=[]
                #For pie chart and subject chart
                for behaviour in behaviour_data:
                    default_items_dict[behaviour.subject.id] = {}
                    labels_dict[behaviour.subject.id] = []
                for behaviour in behaviour_data:
                    default_items_dict[behaviour.subject.id][behaviour.behaviour.behaviour] = []
                    labels_dict[behaviour.subject.id].append(behaviour.behaviour.behaviour)
                for behaviour in behaviour_data:
                    default_items_dict[behaviour.subject.id][behaviour.behaviour.behaviour].append(behaviour.value)
                # print("-----123456778-----default_items_dict", default_items_dict)
                
        #-------- avg score start
                default_sub_marks = copy.deepcopy(default_items_dict)
                lesson = Lesson.objects.filter(student = studentData, is_active=True)
                sub_lesson_count = {}
                for lesson_count in lesson:
                    if lesson_count.subject.id in sub_lesson_count.keys():
                        sub_lesson_count[lesson_count.subject.id] = sub_lesson_count[lesson_count.subject.id] + 1
                    else:
                        sub_lesson_count[lesson_count.subject.id] = 0
                        sub_lesson_count[lesson_count.subject.id] = sub_lesson_count[lesson_count.subject.id] + 1
                # print("sub_lesson_count", sub_lesson_count)

                # print("lesson_count_id",lesson_count.subject.id)
                print("default_sub_marks", default_sub_marks)

                for sub1 in default_sub_marks:
                    for behav1 in default_sub_marks[sub1]:
                        # print("123456",sub1)
                        # print("456789", behav1)
                        default_sub_marks[sub1][behav1] = sum(default_sub_marks[sub1][behav1])
                print("*****default_items_dict", default_sub_marks)
                print("*****default_items_dict", len(default_sub_marks))
                # Behaviour.objects.filter()
                sum_total = {}
                total_all = float(0)
                # for i in range(1,len(default_sub_marks)+1):
                print("------------------default_sub_marks", default_sub_marks)
                for i in default_sub_marks:
                    # print("----len of each sub-----" , len(default_sub_marks[i]))
                    sum_total[i] = ((sum(default_sub_marks[i].values()))/((sub_lesson_count[i])*(len(default_sub_marks[i])*4)))*100
                    # print("percentage--------", sum_total[i])
                    # print("---i------",i,"----",((sum(default_sub_marks[i].values()))/((sub_lesson_count[i])*12))*100)

                # print("len(sum_total)" , len(sum_total))
                print("sum_total", sum_total)
                for tot in sum_total:
                    if tot in sum_total.keys():
                        # print("if")
                        total_all = total_all + sum_total[tot]
                        print("values---",sum_total[tot])
                        print("total_all",total_all)
                    else:
                        print("else")
                    # total_all = (sum(sum_total[tot]))/4
                if len(sum_total) != 0:
                    print("FINAL AVG SCORE: ","{0:.2f}".format(total_all/len(sum_total)))
                    avg_final_show = "{0:.2f}".format(total_all/len(sum_total))
                else:
                    # print("FINAL AVG SCORE: ", 0)
                    avg_final_show = 0
    #------- avg score end
                for sub in default_items_dict:
                    for behav in default_items_dict[sub]:
                        default_items_dict[sub][behav] = sum(default_items_dict[sub][behav])/len(default_items_dict[sub][behav])
                # print("**---////-----***default_items_dict", default_items_dict)
                #For motivational chart
                for student in students_report_data:
                    if student.subject.id in motivational_dict:
                        motivational_dict[student.subject.id].append(student.over_all_rating.value)
                    else:
                        motivational_dict[student.subject.id] = []
                        motivational_dict[student.subject.id].append(student.over_all_rating.value)
                    if student.subject.id in wellbeing_dict:
                        wellbeing_dict[student.subject.id].append(student.well_being.value)
                    else:
                        wellbeing_dict[student.subject.id] = []
                        wellbeing_dict[student.subject.id].append(student.well_being.value)
                for student in motivational_dict:
                    motivational_dict[student] = sum(motivational_dict[student])/len(motivational_dict[student])
                for student in wellbeing_dict:
                    wellbeing_dict[student] = sum(wellbeing_dict[student])/len(wellbeing_dict[student])
                print("motivational_dict", motivational_dict)
# Maths : {on task : avg(20), collab : avg(30)}, 
# Science :{on task : avg(25), collab : avg(35)}
#---------------- Avg line for motivation and subject chart start
                all_avg_data = {}
                all_student = Student.objects.filter(studio = userObj[2].studio)
                for stu in all_student:
                    all_student_report_data = StudentReportData.objects.filter(student = stu)
                    for rep in all_student_report_data:
                        if rep.subject.id in all_avg_data:
                            if rep.behaviour_data.behaviour.behaviour in all_avg_data[rep.subject.id]:
                                all_avg_data[rep.subject.id][rep.behaviour_data.behaviour.behaviour].append(rep.behaviour_data.value)
                                # print("all_avg_data", all_avg_data)
                            else:
                                all_avg_data[rep.subject.id][rep.behaviour_data.behaviour.behaviour] = []
                                all_avg_data[rep.subject.id][rep.behaviour_data.behaviour.behaviour].append(rep.behaviour_data.value)
                                # print("all_avg_data else", all_avg_data)
                        else:
                            all_avg_data[rep.subject.id] = {}
                            if rep.behaviour_data.behaviour.behaviour in all_avg_data[rep.subject.id]:
                                all_avg_data[rep.subject.id][rep.behaviour_data.behaviour.behaviour].append(rep.behaviour_data.value)
                                # print("else all_avg_data", all_avg_data)
                            else:
                                all_avg_data[rep.subject.id][rep.behaviour_data.behaviour.behaviour] = []
                                all_avg_data[rep.subject.id][rep.behaviour_data.behaviour.behaviour].append(rep.behaviour_data.value)
                                # print("else all_avg_data else", all_avg_data)
                for avg_data in all_avg_data:
                    for beh in all_avg_data[avg_data]:
                        all_avg_data[avg_data][beh] = "{0:.2f}".format(sum(all_avg_data[avg_data][beh])/len(all_avg_data[avg_data][beh]))
                # print("overall avg data", all_avg_data)

                avg_motivational_dict = {}

                for stu in all_student:
                    all_student_report_data = StudentReportData.objects.filter(student = stu)
                    for student in all_student_report_data:
                        if student.subject.id in avg_motivational_dict:
                            # print(avg_motivational_dict)
                            avg_motivational_dict[student.subject.id].append(student.over_all_rating.value)
                        else:
                            avg_motivational_dict[student.subject.id] = []
                            avg_motivational_dict[student.subject.id].append(student.over_all_rating.value)
                for student in avg_motivational_dict:
                    avg_motivational_dict[student] = sum(avg_motivational_dict[student])/len(avg_motivational_dict[student])
                print("avg_motivational_dict----", avg_motivational_dict)

#---------------- Avg line for motivation and subject chart end


    #---- dynamic wellbeing n motivational start
                value_key = {}

                itemMaxValue = max(motivational_dict.items(), key=lambda x: x[1])
                # print('Maximum Value in Dictionary : ', itemMaxValue[1])
                # Iterate over all the items in dictionary to find keys with max value
                for key, value in motivational_dict.items():
                    if value == itemMaxValue[1]:
                        value_key[key] = value
                # print('Keys MOTIVATION with maximum Value in Dictionary : ', value_key)

                for key, value in value_key.items():
                    # valuesplit = (str(value)).split(".")
                    # if valuesplit[1] == 0:
                    text = OverallRatingScale.objects.get(value = int(value),is_active=True)
                    # print("text----in if---------" , text.overall_rating)
                    # print("text----in if---------" , text.id)
                    # else:
                    #     text = OverallRatingScale.objects.get(value = int(value))
                overallScale = OverallRatingScale.objects.filter(is_active=True)
                overall_serializers = OverallSerializer(overallScale , many=True)
    #---- motivational end

                wellbeing_valueKey = {}
                itemMaxValue = max(wellbeing_dict.items(), key=lambda x: x[1])
                # print('Maximum Value in Dictionary : ', itemMaxValue[1])
                # Iterate over all the items in dictionary to find keys with max value
                for key, value in wellbeing_dict.items():
                    if value == itemMaxValue[1]:
                        wellbeing_valueKey[key] = value
                # print('Keys WELL BEING with maximum Value in Dictionary : ', wellbeing_valueKey)
                
                for key, value in wellbeing_valueKey.items():
                    # valuesplit = (str(value)).split(".")
                    # if valuesplit[1] == 0:
                    img = WellBeingScale.objects.get(value = int(value), is_active=True)
                    # print("img----in if---------" , img.well_being_scale)
                    # print("img----in if---------" , img.id)
                    # else:
                    #    img = WellBeingScale.objects.get(value = int(value))
                wellbeingScale = WellBeingScale.objects.filter(is_active=True)
                wellbeing_serializers = WellBeingSerializer(wellbeingScale , many=True)
    #---- dynamic wellbeing end

                #For well being chart                 
                # print("avg_default_items_dict--",default_items_dict)
                # print("labels_dict---",labels_dict)
                # print("Motivational dict.................",motivational_dict)
                subject_code_dict = {}
                lesson_count = {}
                lesson_count_end = 0

                subject_codes = Subject.objects.all()
                print("subject_codes --->", subject_codes)
                for code in subject_codes:
                    subject_code_dict[code.id] = code.subject_code
                    lesson_obj = Lesson.objects.filter(subject = code, studio = userObj[2].studio)
                    print("lesson_obj", lesson_obj)
                    for lesson_start in lesson_obj:
                        print("studio --->", userObj[2].studio)
                        print("lesson --->", lesson_start)
                        try:
                            endLesson = ScheduleClass.objects.get(lesson = lesson_start)
                        except:
                            continue
                        print("endlesson--->", endLesson.end_class, "lesson id--->",lesson_start.id )
                        if endLesson.end_class != None:
                            if code.id in lesson_count:
                                lesson_count_end = lesson_count_end + 1
                                lesson_count[code.id] = lesson_count_end
                                print("lesson_count", lesson_count)
                                print(" ")
                            else:
                                lesson_count_end = 1
                                lesson_count[code.id] = lesson_count_end
                                print("lesson_count", lesson_count)
                                print(" ")
                        else:
                            # lesson_count_end = 0
                            if code.id in lesson_count:
                                lesson_count[code.id] = lesson_count_end
                                print("lesson_count", lesson_count)
                                print(" ")
                            else:
                                lesson_count_end = 0
                                lesson_count[code.id] = lesson_count_end
                                print("lesson_count", lesson_count)
                                print(" ")
                    
                print("FINAL-----lesson_count", lesson_count)

                data = {
                        "motivation": motivational_dict,
                        "labels": labels_dict,
                        "default": default_items_dict,
                        "wellbeing_labels": wellbeing_labels,
                        "wellbeing_minmax": wellbeing_minmax,
                        "overall_labels":overall_labels,
                        "overall_minmax": overall_minmax,
                        "wellbeing": wellbeing_dict,
                        "highest_motivation": value_key,
                        "wellbeing_valueKey": wellbeing_valueKey,
                        "all_avg_data":all_avg_data,
                        "avg_motivational_dict": avg_motivational_dict,
                        # "wellbeingImg": img.well_being_scale,
                        "wellbeingId": img.id,
                        # "wellbeingScale": wellbeing_serializers.data,
                        # "overallText": text.overall_rating,
                        "overallId": text.id,
                        "avg_final_show": avg_final_show,
                        "overallScale": overall_serializers.data,
                        "subject_code_dict":subject_code_dict,
                        "lesson_count":lesson_count,
                }

                return Response(data)
            else:
                return Response(data=error_message(400, "error", "invalid token", []), status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print("e---",e)

class AdminChartData(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, format=None):
        try:
            studentId = request.data['studentId'] # 1
            subjectId = request.data.getlist('subjectId[]') # ['1', '2', '3', '4']
            studentData = Student.objects.filter(id=studentId,is_active=True) # Student object (1)
            if studentData:
                studentData = Student.objects.get(id=studentId,is_active=True) # Student object (1)
                students_report_data = StudentReportData.objects.filter(student=studentData)
                behaviour_data = BehaviourData.objects.filter(student=studentData)

            default_items_dict = {} # {}
            labels_dict = {} # {}
            motivational_dict = {}
            wellbeing_dict = {}
            avg_count = {}
            # for sub in subjectId:
            #     default_items_dict[sub] = {} #default_items_dict[1]= {}
            #     labels_dict[sub] = [] #labels_dict[1]=[]
            #For pie chart and subject chart
            for behaviour in behaviour_data:
                default_items_dict[behaviour.subject.id] = {}
                labels_dict[behaviour.subject.id] = []
            for behaviour in behaviour_data:
                default_items_dict[behaviour.subject.id][behaviour.behaviour.behaviour] = []
                labels_dict[behaviour.subject.id].append(behaviour.behaviour.behaviour)
            for behaviour in behaviour_data:
                default_items_dict[behaviour.subject.id][behaviour.behaviour.behaviour].append(behaviour.value)
            for sub in default_items_dict:
                for behav in default_items_dict[sub]:
                    default_items_dict[sub][behav] = sum(default_items_dict[sub][behav])/len(default_items_dict[sub][behav])
            print("default_items_dict", default_items_dict)
            #For motivational chart
            for student in students_report_data:
                if student.subject.id in motivational_dict:
                    motivational_dict[student.subject.subject_name].append(student.over_all_rating.value)
                else:
                    motivational_dict[student.subject.subject_name] = []
                    motivational_dict[student.subject.subject_name].append(student.over_all_rating.value)
                if student.subject.id in wellbeing_dict:
                    wellbeing_dict[student.subject.subject_name].append(student.well_being.value)
                else:
                    wellbeing_dict[student.subject.subject_name] = []
                    wellbeing_dict[student.subject.subject_name].append(student.well_being.value)
            for student in motivational_dict:
                motivational_dict[student] = sum(motivational_dict[student])/len(motivational_dict[student])
            for student in wellbeing_dict:
                wellbeing_dict[student] = sum(wellbeing_dict[student])/len(wellbeing_dict[student])
            print("motivational_dict", motivational_dict)
            print("wellbeing_dict", wellbeing_dict)
            #For well being chart                 
            print("avg_default_items_dict--",default_items_dict)
            print("labels_dict---",labels_dict)
            data = {
                    "motivation": motivational_dict,
                    "labels": labels_dict,
                    "default": default_items_dict,
                    "wellbeing": wellbeing_dict,
            }
            return Response(data)
        except Exception as e:
            print("e---",e)

class FilterStudentDetails(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, format=None):
        if 'token' in request.session:
                token = request.session['token']

        elif 'token' in request.headers:
            token = request.headers.get('token')

        userObj = gbl(request, token)
        if userObj != None:
            # print("date range", request.POST['studentId'])
            studentId = request.data['studentId']
            studentData = Student.objects.filter(id=studentId,is_active=True)
            if studentData:
                studentData = Student.objects.get(id=studentId,is_active=True)
                students_report_data = StudentReportData.objects.filter(student=studentData)
                behaviour_data = BehaviourData.objects.filter(student=studentData)
            wellbeing_objs = WellBeingScale.objects.filter(is_active=True)
            overall_objs = OverallRatingScale.objects.filter(is_active=True)
            wellbeing_labels = {}
            for obj in wellbeing_objs:
                wellbeing_labels[int(obj.value)] = obj.well_being_scale
            wellbeing_minmax = []
            wellbeing_minmax.append(min(wellbeing_labels.keys()))
            wellbeing_minmax.append(max(wellbeing_labels.keys()))
            overall_labels = {}
            for obj in overall_objs:
                overall_labels[int(obj.value)] = obj.overall_rating
            overall_minmax = []
            overall_minmax.append(min(overall_labels.keys()))
            overall_minmax.append(max(overall_labels.keys()))
            # print("min-max------======------=-=-=-=-=-=-=-=-=-=", overall_minmax, wellbeing_minmax)

            default_items_dict = {}
            labels_dict = {}
            motivational_dict = {}
            wellbeing_dict = {}

            all_schedule = ScheduleClass.objects.all().order_by('scheduled_date')
            serializer = FilterDateSerializer(all_schedule , many = True,context=request.data)
            # print("serializer----",serializer.data)
            lesson_codes = []
            for data in serializer.data:
                if data["lesson_code"] != None:
                    lesson_codes.append(data["lesson_id"])

            #For Subject Bar chart and Pie chart date filter
            for behaviour in behaviour_data:
                if behaviour.lesson.id in lesson_codes:
                    if behaviour.subject.id in default_items_dict.keys():
                        if behaviour.behaviour.behaviour in default_items_dict[behaviour.subject.id].keys():
                            default_items_dict[behaviour.subject.id][behaviour.behaviour.behaviour].append(behaviour.value)
                        else:
                            default_items_dict[behaviour.subject.id][behaviour.behaviour.behaviour] = []
                            labels_dict[behaviour.subject.id].append(behaviour.behaviour.behaviour)
                            default_items_dict[behaviour.subject.id][behaviour.behaviour.behaviour].append(behaviour.value)
                    else:
                        default_items_dict[behaviour.subject.id] = {}
                        labels_dict[behaviour.subject.id] = []
                        if behaviour.behaviour.behaviour in default_items_dict[behaviour.subject.id].keys():
                            default_items_dict[behaviour.subject.id][behaviour.behaviour.behaviour].append(behaviour.value)
                        else:
                            default_items_dict[behaviour.subject.id][behaviour.behaviour.behaviour] = []
                            labels_dict[behaviour.subject.id].append(behaviour.behaviour.behaviour)
                            default_items_dict[behaviour.subject.id][behaviour.behaviour.behaviour].append(behaviour.value)
    #-------- avg score start
            default_sub_marks = copy.deepcopy(default_items_dict)
            lesson = Lesson.objects.filter(student = studentData)
            sub_lesson_count = {}
            for lesson_count in lesson:
                if lesson_count.id in lesson_codes:
                    if lesson_count.subject.id in sub_lesson_count.keys():
                        sub_lesson_count[lesson_count.subject.id] = sub_lesson_count[lesson_count.subject.id] + 1
                    else:
                        sub_lesson_count[lesson_count.subject.id] = 0
                        sub_lesson_count[lesson_count.subject.id] = sub_lesson_count[lesson_count.subject.id] + 1
            print("sub_lesson_count", sub_lesson_count)

            print("lesson_count_id",lesson_count.subject.id)
            print("default_sub_marks", default_sub_marks)

            for sub1 in default_sub_marks:
                for behav1 in default_sub_marks[sub1]:
                    # print("123456",sub1)
                    # print("456789", behav1)
                    default_sub_marks[sub1][behav1] = sum(default_sub_marks[sub1][behav1])
            print("*****default_items_dict", default_sub_marks)
            print("*****default_items_dict", len(default_sub_marks))
            # Behaviour.objects.filter()
            sum_total = {}
            total_all = float(0)
            # for i in range(1,len(default_sub_marks)+1):
            print("------------------default_sub_marks", default_sub_marks)
            for i in default_sub_marks:
                print("----len of each sub-----" , len(default_sub_marks[i]))
                sum_total[i] = ((sum(default_sub_marks[i].values()))/((sub_lesson_count[i])*(len(default_sub_marks[i])*4)))*100
                # print("percentage--------", sum_total[i])
                # print("---i------",i,"----",((sum(default_sub_marks[i].values()))/((sub_lesson_count[i])*12))*100)

            print("len(sum_total)" , len(sum_total))
            print("sum_total", sum_total)
            for tot in sum_total:
                if tot in sum_total.keys():
                    print("if")
                    total_all = total_all + sum_total[tot]
                    print("values---",sum_total[tot])
                    print("total_all",total_all)
                else:
                    print("else")
                # total_all = (sum(sum_total[tot]))/4
            if len(sum_total) != 0:
                print("FINAL AVG SCORE: ","{0:.2f}".format(total_all/len(sum_total)))
                avg_final_show = "{0:.2f}".format(total_all/len(sum_total))
                print("avg_final_show", avg_final_show)
            else:
                print("FINAL AVG SCORE: ", 0)
                avg_final_show = 0
    #------- avg score end                    
            for sub in default_items_dict:
                for behav in default_items_dict[sub]:
                    default_items_dict[sub][behav] = sum(default_items_dict[sub][behav])/len(default_items_dict[sub][behav])
            
            #For motivational Chart date filter
            for student in students_report_data:
                if student.behaviour_data.lesson.id in lesson_codes:
                    if student.subject.id in motivational_dict:
                        motivational_dict[student.subject.id].append(student.over_all_rating.value)
                    else:
                        motivational_dict[student.subject.id] = []
                        motivational_dict[student.subject.id].append(student.over_all_rating.value)
                    if student.subject.id in wellbeing_dict:
                        wellbeing_dict[student.subject.id].append(student.well_being.value)
                    else:
                        wellbeing_dict[student.subject.id] = []
                        wellbeing_dict[student.subject.id].append(student.well_being.value)
            for student in motivational_dict:
                motivational_dict[student] = sum(motivational_dict[student])/len(motivational_dict[student])
            for student in wellbeing_dict:
                wellbeing_dict[student] = sum(wellbeing_dict[student])/len(wellbeing_dict[student])

    #---------------- Avg line for motivation and subject chart start
            all_avg_data = {}
            all_student = Student.objects.filter(studio = userObj[2].studio)
            for stu in all_student:
                all_student_report_data = StudentReportData.objects.filter(student = stu)
                for rep in all_student_report_data:
                    if rep.subject.id in all_avg_data:
                        if rep.behaviour_data.behaviour.behaviour in all_avg_data[rep.subject.id]:
                            all_avg_data[rep.subject.id][rep.behaviour_data.behaviour.behaviour].append(rep.behaviour_data.value)
                            # print("all_avg_data", all_avg_data)
                        else:
                            all_avg_data[rep.subject.id][rep.behaviour_data.behaviour.behaviour] = []
                            all_avg_data[rep.subject.id][rep.behaviour_data.behaviour.behaviour].append(rep.behaviour_data.value)
                            # print("all_avg_data else", all_avg_data)
                    else:
                        all_avg_data[rep.subject.id] = {}
                        if rep.behaviour_data.behaviour.behaviour in all_avg_data[rep.subject.id]:
                            all_avg_data[rep.subject.id][rep.behaviour_data.behaviour.behaviour].append(rep.behaviour_data.value)
                            # print("else all_avg_data", all_avg_data)
                        else:
                            all_avg_data[rep.subject.id][rep.behaviour_data.behaviour.behaviour] = []
                            all_avg_data[rep.subject.id][rep.behaviour_data.behaviour.behaviour].append(rep.behaviour_data.value)
                            # print("else all_avg_data else", all_avg_data)
            for avg_data in all_avg_data:
                for beh in all_avg_data[avg_data]:
                    all_avg_data[avg_data][beh] = "{0:.2f}".format(sum(all_avg_data[avg_data][beh])/len(all_avg_data[avg_data][beh]))
            # print("overall avg data", all_avg_data)

            avg_motivational_dict = {}

            for stu in all_student:
                all_student_report_data = StudentReportData.objects.filter(student = stu)
                for student in all_student_report_data:
                    if student.subject.id in avg_motivational_dict:
                        # print(avg_motivational_dict)
                        avg_motivational_dict[student.subject.id].append(student.over_all_rating.value)
                    else:
                        avg_motivational_dict[student.subject.id] = []
                        avg_motivational_dict[student.subject.id].append(student.over_all_rating.value)
            for student in avg_motivational_dict:
                avg_motivational_dict[student] = sum(avg_motivational_dict[student])/len(avg_motivational_dict[student])
            # print("avg_motivational_dict----", avg_motivational_dict)

    #---------------- Avg line for motivation and subject chart end

    #---- dynamic wellbeing n motivational start
            value_key = {}
            # print("Motivational division", motivational_dict)
            # print("wellbeing_division", wellbeing_dict)
            if motivational_dict != {}:
                itemMaxValue = max(motivational_dict.items(), key=lambda x: x[1])
                # print('Maximum Value in Dictionary : ', itemMaxValue[1])
                # Iterate over all the items in dictionary to find keys with max value
                for key, value in motivational_dict.items():
                    if value == itemMaxValue[1]:
                        value_key[key] = value
                # print('Keys MOTIVATION with maximum Value in Dictionary : ', value_key)

                for key, value in value_key.items():
                    # valuesplit = (str(value)).split(".")
                    # if valuesplit[1] == 0:
                    text = OverallRatingScale.objects.get(value = int(value))
                    # print("text----in if---------" , text.overall_rating)
                    text_overall_rating = text.overall_rating
                    # print("text----in if---------" , text.id)
                    text_id = text.id
                    # else:
                    #     text = OverallRatingScale.objects.get(value = int(value))
                overallScale = OverallRatingScale.objects.all()
                overall_serializers = OverallSerializer(overallScale , many=True)
                overall_serializers = overall_serializers.data
            else:
                value_key = {}
                text_id = 0
                overall_serializers = {}

            if wellbeing_dict != {}:
                wellbeing_valueKey = {}
                itemMaxValue = max(wellbeing_dict.items(), key=lambda x: x[1])
                # print('Maximum Value in Dictionary : ', itemMaxValue[1])
                # Iterate over all the items in dictionary to find keys with max value
                for key, value in wellbeing_dict.items():
                    if value == itemMaxValue[1]:
                        wellbeing_valueKey[key] = value
                # print('Keys WELL BEING with maximum Value in Dictionary : ', wellbeing_valueKey)
                
                for key, value in wellbeing_valueKey.items():
                    # valuesplit = (str(value)).split(".")
                    # if valuesplit[1] == 0:
                    img = WellBeingScale.objects.get(value = int(value))
                    # print("img----in if---------" , img.well_being_scale)
                    # print("img----in if---------" , img.id)
                    img_id = img.id
                    # else:
                    #    img = WellBeingScale.objects.get(value = int(value))
                wellbeingScale = WellBeingScale.objects.all()
                wellbeing_serializers = WellBeingSerializer(wellbeingScale , many=True)
            else:
                wellbeing_valueKey = {}
                img_id = 0
    #---- dynamic wellbeing n motivational end

            # print("motivational_dict", motivational_dict)
            # print("wellbeing_dict", wellbeing_dict)

            # print("default_items_dict",default_items_dict)
            # print("lesson_code---", lesson_codes)
            # print("labels---", labels_dict)
            if motivational_dict == {}:
                avg_motivational_dict = {}

            subject_code_dict = {}
            lesson_count = {}
            lesson_count_end = 0

            subject_codes = Subject.objects.all()
            for code in subject_codes:
                subject_code_dict[code.id] = code.subject_code
                lesson_obj = Lesson.objects.filter(subject = code, studio = userObj[2].studio)
                print("lesson_obj --->", lesson_obj)
                for lesson_start in lesson_obj:
                    print("studio --->", userObj[2].studio)
                    print("lesson --->", lesson_start)
                    try:
                        endLesson = ScheduleClass.objects.get(lesson = lesson_start)
                    except:
                        continue
                    print("endlesson--->", endLesson.end_class, "lesson id--->",lesson_start.id )
                    if endLesson.end_class != None:
                        if code.id in lesson_count:
                            lesson_count_end = lesson_count_end + 1
                            lesson_count[code.id] = lesson_count_end
                            print("lesson_count", lesson_count)
                            print(" ")
                        else:
                            lesson_count_end = 1
                            lesson_count[code.id] = lesson_count_end
                            print("lesson_count", lesson_count)
                            print(" ")
                    else:
                        # lesson_count_end = 0
                        if code.id in lesson_count:
                            lesson_count[code.id] = lesson_count_end
                            print("lesson_count", lesson_count)
                            print(" ")
                        else:
                            lesson_count_end = 0
                            lesson_count[code.id] = lesson_count_end
                            print("lesson_count", lesson_count)
                            print(" ")
                
            print("FINAL-----lesson_count", lesson_count)


            data = {
                "motivation": motivational_dict,
                "labels": labels_dict,
                "default": default_items_dict,
                "wellbeing_labels": wellbeing_labels,
                "wellbeing_minmax": wellbeing_minmax,
                "overall_labels":overall_labels,
                "overall_minmax": overall_minmax,
                "wellbeing": wellbeing_dict,
                "all_avg_data":all_avg_data,
                "avg_motivational_dict": avg_motivational_dict,
                "highest_motivation": value_key,
                "wellbeing_valueKey": wellbeing_valueKey,
                # "wellbeingImg": img.well_being_scale,
                "wellbeingId": img_id,
                # "wellbeingScale": wellbeing_serializers.data,
                # "overallText": text.overall_rating,
                "overallId": text_id,
                "avg_final_show": avg_final_show,
                "overallScale": overall_serializers,
                "subject_code_dict":subject_code_dict,
                "lesson_count":lesson_count,
            }
            return Response(data)
        else:
            return Response(data=error_message(400, "error", "invalid token", []), status=status.HTTP_400_BAD_REQUEST)
class AnalyticschartData(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, format=None):
        try:
            print("date range", request.POST['LessonId'])
            LessonId = request.data['LessonId']
            LessonData = Lesson.objects.get(id=LessonId, is_active = True)
            StudentsinLesson =LessonData.student.all()
            subject_of_lesson = LessonData.subject.subject_name

            motivational_dict_avg = {}
            wellbeing_dict_avg = {}
            cnt = 0
            all_lesson_of_subject = Lesson.objects.filter(subject=LessonData.subject.id, is_active = True)
            for lesson_obj in all_lesson_of_subject:
                studREport_for_avg = StudentReportData.objects.filter(lesson=lesson_obj.id)
                for student in studREport_for_avg:
                    if student.student.full_name in motivational_dict_avg:
                        motivational_dict_avg[student.student.full_name].append(student.over_all_rating.value)
                    else:
                        motivational_dict_avg[student.student.full_name] = []
                        motivational_dict_avg[student.student.full_name].append(student.over_all_rating.value)


                    if student.student.full_name in wellbeing_dict_avg:
                        wellbeing_dict_avg[student.student.full_name].append(student.well_being.value)
                    else:
                        wellbeing_dict_avg[student.student.full_name] = []
                        wellbeing_dict_avg[student.student.full_name].append(student.well_being.value)

            print("Average bef Rating", motivational_dict_avg)
            print("Average bef Wellbeing", wellbeing_dict_avg)

            for key in motivational_dict_avg.keys():
                motivational_dict_avg[key] = round(sum(motivational_dict_avg[key])/len(motivational_dict_avg[key]),2)
                wellbeing_dict_avg[key] = round(sum(wellbeing_dict_avg[key])/len(wellbeing_dict_avg[key]),2)

            print("Average Rating",motivational_dict_avg)
            print("Average Wellbeing",wellbeing_dict_avg)

            from collections import Counter
            result = {"behaviour_chart": {}, "average_chart": {}, "behaviour_avg_chart": {}}
            final_behaviour_list = []
            for lesson in all_lesson_of_subject:
                students = lesson.student.all()
                for student in students:
                    print("STUDENT DETAILS")
                    behaviour_dict = {}
                    behaviour_data = BehaviourData.objects.filter(lesson=LessonId, student=student)
                    print("behaviour data", behaviour_data)
                    for behaviour_data in behaviour_data:
                        behaviour_dict.update({behaviour_data.behaviour.behaviour: behaviour_data.value})
                        print(behaviour_dict)
                    #avg_score_list.update({student.full_name: sum(behaviour_dict.values())})
                    final_behaviour_list.append(behaviour_dict)
            print("final behavior list : ",final_behaviour_list)

            total = sum(map(Counter, final_behaviour_list), Counter())
            N = float(len(final_behaviour_list))

            result['behaviour_avg_chart'].update({"labels": [k for k, v in total.items()]})
            result['behaviour_avg_chart'].update({"data": [round((v / N),2) for k, v in total.items()]})
            # result['behaviour_avg_chart'].update({"label": LessonData.subject.subject_name.lower()})

            StudREport = StudentReportData.objects.filter(lesson=LessonId)
            student = LessonData.student.all()

            final_behaviour_list = []
            avg_score_list = {}

            for student in student:
                print("STUDENT DETAILS")
                behaviour_dict = {}
                behaviour_data = BehaviourData.objects.filter(lesson=LessonId, student=student)
                print("behaviour data", behaviour_data)
                for behaviour_data in behaviour_data:
                    behaviour_dict.update({behaviour_data.behaviour.behaviour: behaviour_data.value})
                    print(behaviour_dict)
                avg_score_list.update({student.full_name: sum(behaviour_dict.values())})
                final_behaviour_list.append(behaviour_dict)

            print("final ",final_behaviour_list)
            print("avg ",avg_score_list)

            total = sum(map(Counter, final_behaviour_list), Counter())
            N = float(len(final_behaviour_list))


            result['average_chart'] = avg_score_list
            result['behaviour_chart'].update({"labels": [k for k, v in total.items()]})
            result['behaviour_chart'].update({"data": [v / N for k, v in total.items()]})
            result['behaviour_chart'].update({"label": LessonData.subject.subject_name.lower()})
            print("result ",result)

            min_max_data = {}
            rating_data = {}
            wellbeing_data = {}
            motivational_dict = {}
            wellbeing_dict = {}

            ratings = OverallRatingScale.objects.filter(is_active=True)
            for obj in ratings:
                rating_data[obj.value] = obj.overall_rating

            wellbeing = WellBeingScale.objects.filter(is_active=True)
            for obj in wellbeing:
                wellbeing_data[obj.value] = obj.well_being_scale

            # print("max rating",max(rating_data.keys()))
            # print("min rating",min(rating_data.keys()))

            min_max_data["max rating"]=max(rating_data.keys())
            min_max_data["min rating"] = min(rating_data.keys())
            min_max_data["max wellbeing"] = max(wellbeing_data.keys())
            min_max_data["min wellbeing"] = min(wellbeing_data.keys())

            for student in StudREport:
                if student.student.id in motivational_dict:
                    pass
                else:
                    motivational_dict[student.student.full_name] = student.over_all_rating.value

                if student.student.id in wellbeing_dict:
                    pass
                else:
                    wellbeing_dict[student.student.full_name] = student.well_being.value

            schedule_obj = ScheduleClass.objects.get(lesson=LessonId)
            schedule_obj_dict = {"lesson_code":schedule_obj.lesson.lesson_code,"subject":schedule_obj.lesson.subject.subject_name,"time":schedule_obj.class_start_time,"date":schedule_obj.scheduled_date,"room":schedule_obj.room_number}

            data={"wellbeing_avg":wellbeing_dict_avg,"motivation_avg":motivational_dict_avg,"schedule_obj":schedule_obj_dict,"chart":result,"motivation": motivational_dict,"wellbeing":wellbeing_dict,"subject":subject_of_lesson.lower(),"wellbeing_data":wellbeing_data,"rating_data":rating_data,"min_max_data":min_max_data}
            print(data)
            return Response(data)
        except Exception as e:
            print(e)
