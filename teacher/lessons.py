
from curioo_admin.models import *
from curioo_admin.models import Lesson as less
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .serializers import *
from .validations import *
from rest_framework.permissions import AllowAny
from teacher.status_message import success_message, error_message
from .getUser import gbl
from django.http import HttpResponse
from django.shortcuts import render, redirect
from rest_framework.decorators import api_view, permission_classes
import json
from django.http import HttpResponse
from django.db.models.fields.files import ImageFieldFile, FileField
import re
from .notify import *
from django.utils.translation import LANGUAGE_SESSION_KEY


class Lessn(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        try:
            from datetime import date,timedelta,datetime
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')
            # print('request.COOKIES : ', request.session['django_language'])
            # print('request.COOKIES : ', request.COOKIES)

            name,utype,userObj = gbl(request,token)
            if userObj != None:
                all_students_count = Student.objects.filter(is_active=True,studio=userObj.studio).count()
                all_schedule = ScheduleClass.objects.filter(studio = userObj.studio)
                serializer = LessonSerializer(all_schedule, many=True)
                count = notify(request)
                print("notify context", count)
                sdCount = 0
                allLesson = []
                for sd in serializer.data:
                    if None not in sd.values():
                        allLesson.append(serializer.data[sdCount])
                    sdCount = sdCount + 1 
                
                if 'token' in request.headers: #for API
                    return Response(data=success_message(200,"success","Lesson data get sucessfully",serializer.data), status=status.HTTP_200_OK)

                elif 'token' in request.session: # for WEBPAGE
                    return render(request,"teacher/lesson.html",context={'lesson':allLesson,'image':userObj.image,'count':count,'all_students_count':all_students_count})
                
            else:
                return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)
                
        except Exception as e:
            data = {"status_code": 500, "status": "error", "message": str(e)}
            return Response(data=data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class get_lesson(APIView):
    permission_classes = (AllowAny,)

    def get(self, request,lesson_id):    
        try:
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            name,utype,userObj = gbl(request,token)
            if userObj != None:
                lesson_queryset = less.objects.filter(id=lesson_id)
                if lesson_queryset:
                    lesson_obj = less.objects.get(id=lesson_id)
                    print("lesson_obj--",lesson_obj)
                    count = notify(request)
                    print("notify context", count)
                    serializer = LessonDetailsSerializer(lesson_queryset, many=True)
                    if 'token' in request.headers: #for API
                        return Response(data=success_message(200,"success","Lesson details get sucessfully",serializer.data), status=status.HTTP_200_OK)

                    elif 'token' in request.session: # for WEBPAGE
                        return render(request,"teacher/lessonDetails.html",context={'lessonDetails':serializer.data,'image':userObj.image,'count':count})
                
            else:
                return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            data = {"status_code": 500, "status": "error", "message": str(e)}
            return Response(data=data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class teacherLesson(APIView):
    permission_classes = (AllowAny,)
    def get(self, request):    
        try:
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            name,utype,userObj = gbl(request,token)
            if userObj != None:
                AllData = {}
                dictt = { "english": "#F3ACB3", "entrepreneur": "#1691B3", "technology": "#F6BE57", "creative design": "#EB5858",
                            "default": ["#0f6b84", "#678388", "#9a5653", "#FFEB3B", "#FF9800", "#673AB7"] }
                todaySchedule = {}
                futureSchedule = {}
                FinalFutureSchedule = {}
                FinaltodaySchedule = {}
                count_1 = 0
                color_dict = {}
                color_count = 0
                subjects = Subject.objects.all()
                for subject in subjects:
                    if subject.subject_name.lower() in dictt:
                        color_dict[subject.subject_name.lower()] = dictt[subject.subject_name.lower()] 
                    else:
                        color_dict[subject.subject_name.lower()] = dictt["default"][color_count]
                        color_count = color_count + 1
                print("color_dict", color_dict)
                count = notify(request)
                print("notify context", count)
                # count = 0
                future = 0
                student_count = 0
                allStu = Student.objects.filter(is_active=True,studio=userObj.studio).count()
                if Teacher.objects.filter(id=userObj.id).exists():
                    teacher_data = Teacher.objects.get(id=userObj.id)
                    if ScheduleClass.objects.filter(teacher=teacher_data).exists():
                        schedule_data = ScheduleClass.objects.filter(teacher=teacher_data)
                        student_count = Student.objects.filter(is_active=True).count()
                        serializer = TeacherLessonSerializer(schedule_data,many=True)
                        from datetime import date,datetime,timedelta
                        for schedule in serializer.data:
                            print("Date: ",schedule['scheduled_date'])
                            schedule['scheduled_date'] = datetime.strptime(schedule['scheduled_date'], '%Y-%m-%d').date()
                            print("serializer.data['scheduled_date']--2--",schedule['scheduled_date'], type(schedule['scheduled_date']), type(date.today()))
                            after7days = (date.today()+timedelta(days=7)).isoformat()
                            after7days = datetime.strptime(after7days, '%Y-%m-%d').date()
                            if date.today() < schedule['scheduled_date'] and after7days >= schedule['scheduled_date']:
                                futureSchedule["futureSchedule"] = schedule
                                FinalFutureSchedule['futureSchedule_'+str(future+1)] = futureSchedule
                                futureSchedule = {}
                                AllData["teachers_lessons"] = FinalFutureSchedule
                                future = future + 1
                            
                            print('today--',date.today(),' == ','scheduled_date ',schedule['scheduled_date'])
                            if date.today() == schedule['scheduled_date']:
                                todaySchedule['scheduleId'] = schedule['id']
                                todaySchedule['lessonCode'] = schedule['lesson_code']
                                todaySchedule['subject'] = schedule['subject']
                                todaySchedule['start_time'] = schedule['time']
                                todaySchedule['room'] = schedule['room']
                                todaySchedule['end_class'] = schedule['end_class']
                                todaySchedule['lesson_id'] = schedule['lesson_id']
                                FinaltodaySchedule['lesson_'+str(count_1+1)] = todaySchedule
                                todaySchedule = {}
                                print("FinaltodaySchedule----",FinaltodaySchedule)
                                AllData["today_lessons"] = FinaltodaySchedule
                                count_1 = count_1 + 1
                            
                if 'token' in request.headers: #for API
                    return Response(data=success_message(200,"success","Teacher's Lessons get sucessfully",AllData), status=status.HTTP_200_OK)

                elif 'token' in request.session: # for WEBPAGE
                    from datetime import date
                    todayDate = date.today()
                    return render(request,"teacher/my_lesson.html",context={'my_lesson':AllData,'name':name,'image':userObj.image,'student_count':allStu,'todayDate':todayDate,'count':count, 'color_dict':color_dict})

            else:
                return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            data = {"status_code": 500, "status": "error", "message": str(e)}
            return Response(data=data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request):    
        try:
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            name,utype,userObj = gbl(request,token)
            if userObj != None:
                end_id = request.POST['id'].split("_")[1]
                value = request.POST['type']
                from datetime import datetime
                schedule_obj = ScheduleClass.objects.get(id = end_id)
                schedule_obj.end_class = datetime.now()
                schedule_obj.save()

                return Response(data=success_message(200,"success","update done.",[]), status=status.HTTP_200_OK)
            
        except Exception as e:
            data = {"status_code": 500, "status": "error", "message": str(e)}
            return Response(data=data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class StartClass(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, schedule_id):
        try:
            print("come")
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            name,utype,userObj = gbl(request,token)

            if userObj != None:
                if ScheduleClass.objects.filter(id=schedule_id).exists():
                    scheduleObj = ScheduleClass.objects.get(id=schedule_id)
                    from datetime import datetime, date
                    print("datetime.now()" , str(datetime.now()))
                    splitDatetime = str(datetime.now()).split('.')
                    print("splitDatetime" , splitDatetime[0])
                    dbTime = str(date.today())+ " " +str(scheduleObj.class_start_time)
                    print("dbTime" , dbTime)
                    if splitDatetime[0] >= dbTime:
                        print("in 1st if")
                        if scheduleObj.start_class == None:
                            print("in 2nd if")
                            scheduleObj.start_class = datetime.now()
                            class_status = 'class start'
                        print("in 2nd else")
                    
        
                        scheduleObj.save()
                        count = notify(request)
                        print("notify context", count)
                        scheduleObj.save()
                        lesson = Lesson.objects.filter(id=scheduleObj.lesson.id, is_active=True)
                        serializer = LessonDetailsSerializer(lesson, many=True)
                        if 'token' in request.headers: #for API
                            return Response(data=success_message(200,"success","class start",serializer.data), status=status.HTTP_200_OK)

                        elif 'token' in request.session: # for WEBPAGE
                            return render(request,"teacher/startClass.html",context={'classDetails':serializer.data,'schedule_id':schedule_id,'count':count,'image':userObj.image, 'lesson_id':scheduleObj.lesson.id})
                    
                    else:
                        print("in 1st else")
                        
                        if 'token' in request.headers: #for API
                            return Response(data=success_message(200,"success","you can not start class",{}), status=status.HTTP_200_OK)
                        elif 'token' in request.session: # for WEBPAGE
                            # return redirect('lesson/')
                            return Response(data=error_message(404,"error","you can not start class",{}), status=status.HTTP_404_NOT_FOUND)

            else:
                return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            data = {"status_code": 500, "status": "error", "message": str(e)}
            return Response(data=data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class StudentReview(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        try:
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            name,utype,userObj = gbl(request,token)

            if userObj != None:
                # Student Review
                schedule_id = request.POST['schedule_id']
                student_id = request.POST['student_id']
                subject_id = request.POST['subject_id']
                lesson_id = request.POST['lesson_id']

                overallscale = request.POST['overallscale']
                wellbeingscale = request.POST['well_being_scale']

                behaviour_value = 0
                count = 0
                student_report_id = []
                print(request.POST)
                pattern = '[0-9]'
     
                for key,value in request.POST.items():
                    
                    if "behaviour" in key:
                        newval = value.split()
               
                        behaviour_obj = Behaviour.objects.get(id = newval[0], is_active=True)
                        student_obj = Student.objects.get(id=student_id, is_active=True)
                        subject_obj = Subject.objects.get(id=subject_id[0],is_active=True)
                        lesson_object = Lesson.objects.get(id = lesson_id, is_active=True)
                        behaviour_dt = BehaviourData.objects.create(
                            lesson = lesson_object,
                            subject = subject_obj,
                            student = student_obj,
                            behaviour=behaviour_obj,
                            value = newval[1]
                            )
                        behaviour_dt.save()
                        behaviour_dt = BehaviourData.objects.get(id=behaviour_dt.id)
                        count += 1
                        behaviour_value += behaviour_dt.value

                        student_obj = Student.objects.get(id=student_id)
                        overllratingscale_obj = OverallRatingScale.objects.get(id=overallscale)
                        wellbeingscale_obj = WellBeingScale.objects.get(id=wellbeingscale)
                        subject_obj = Subject.objects.get(id=subject_id[0])
                        
                        student_report_data = StudentReportData.objects.create(
                            lesson = lesson_object,
                            subject = subject_obj,
                            student = student_obj,
                            behaviour_data = behaviour_dt,
                            over_all_rating = overllratingscale_obj,
                            well_being = wellbeingscale_obj
                        )
                        student_report_data.save()
                        student_report_id.append(student_report_data.id)
                avg = behaviour_value / count
                
                for i in range(len(student_report_id)):
                    lesson_object = Lesson.objects.get(id = lesson_id, is_active=True)
                    student_report_data_obj = StudentReportData.objects.get(id=student_report_id[i])
                    student_report_data_obj.score = avg
                    student_report_data_obj.save()

                    activity_obj = Activity.objects.create(
                        lesson = lesson_object,
                        student_report_data=student_report_data_obj,
                    )
                    activity_obj.save()
                    
                # End Student Review

                # Comment
                print('for comment')
                commentbyrole = request.POST['comment_by_role']
                commentbyid = request.POST['comment_by_id']
                commentonrole = request.POST['comment_on_role']
                commentonid = request.POST['comment_on_id']
                subj = request.POST['subject_id']
                msg = request.POST['message']
                print('for comment')
                if msg.strip() != "":
                    subject_obj = Subject.objects.get(id = subj)
                    comment = Comments.objects.create(
                        lesson = lesson_object,
                        commented_by_role = commentbyrole.capitalize(),
                        commented_by_id = commentbyid,
                        commented_on_role = commentonrole.capitalize(),
                        commented_on_id = commentonid,
                        subject = subject_obj,
                        message = msg
                    )
                    comment.save()
                    print('for comment')
                    from datetime import datetime as dt
                    teacher_obj = Teacher.objects.get(full_name=name)
                    img = teacher_obj.image
                    data = {
                        "score":student_report_data_obj.score,
                        "teacher_image":img.url,
                        "teacher_pos":comment.commented_by_role.capitalize(),
                        "comment_time": dt.strftime(comment.created_at, '%H:%M %p'),
                        "comment_date": dt.strftime(comment.created_at, '%d/%m/%Y'),
                        "message": comment.message
                        }
                    # End Comment
                else:
                    data = {
                        "score":student_report_data_obj.score,
                        "message": None,
                        }
                return HttpResponse(json.dumps(data), content_type='application/json')
                
                # return redirect('/teacher/lesson/start/'+schedule_id+'/')
            else:
                return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            print("Exception : ", e)
            data = {"status_code": 500, "status": "error", "message": str(e)}
            return Response(data=data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST'])
@permission_classes([AllowAny, ])
def filter_date_lesson(request):
    try:
        if 'token' in request.session:
            token = request.session['token']

        elif 'token' in request.headers:
            token = request.headers.get('token')
        print("token -",token)
        name,utype,userObj = gbl(request,token)
        if userObj != None:
            all_schedule = ScheduleClass.objects.filter(studio = userObj.studio).order_by('scheduled_date')
            
            serializer = FilterDateSerializer(all_schedule , many = True,context=request.data)
            print("serializer----",serializer.data)
            for i in serializer.data:
                print('i---',i)
            return Response(i for i in serializer.data if i['lesson_code'] != None)
            
        else:
            return Response(data=error_message(400,"error","invalid token",[]), status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:
            data = {"status_code": 500, "status": "error", "message": str(e)}
            return Response(data=data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST'])
@permission_classes([AllowAny, ])
def MylessonFilter(request):
    if request.method == 'POST':
        print("in filter date lesson")
        try:
            if 'token' in request.session:
                token = request.session['token']

            elif 'token' in request.headers:
                token = request.headers.get('token')

            userObj = gbl(request, token)
            if userObj != None:

                schedule = ScheduleClass.objects.filter(teacher = userObj[2])
                serializers = FilterDateSerializer(schedule,many = True,context=request.data )
                print("serializer----",serializers.data)
                for i in serializers.data:
                    print('i---',i)
                return Response(i for i in serializers.data if i['lesson_code'] != None)

            else:
                return Response(data=error_message(400, "error", "invalid token", []), status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print("Exception in My LEsson filter --- ", e)
            return Response(data=error_message(400, "error", str(e), {}), status=status.HTTP_400_BAD_REQUEST)
        return Response("hi")

