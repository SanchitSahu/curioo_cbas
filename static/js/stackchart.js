AmCharts.makeChart("stackchart",
				{
					"type": "serial",
					"categoryField": "category",
					/*"startDuration": 1,
	"startEffect": "easeOutSine",*/
					"theme": "light",
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "Strict",
							"type": "column",
							"valueField": "column-1"
						},
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-2",
							"title": "Conservative",
							"type": "column",
							"valueField": "column-2"
						},
						{
							"fillAlphas": 1,
							"fillColors": "#CD6A6A",
							"fontSize": 0,
							"id": "AmGraph-3",
							"lineAlpha": 0,
							"showBalloon": false,
							"tabIndex": 0,
							"title": "Moderate",
							"type": "column",
							"valueField": "column-3"
						},
						{
							"fillAlphas": 1,
							"fillColors": "#2B80B1",
							"id": "AmGraph-4",
							"lineAlpha": 0,
							"title": "Liberal",
							"type": "column",
							"valueField": "column-3"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"stackType": "regular",
							"title": "Teacher Count"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"useGraphSettings": true
					},
					"titles": [
						/*{
							"id": "Title-1",
							"size": 15,
							"text": "Chart Title"
						}*/
					],
					"dataProvider": [
						{
							"category": "English",
							"column-1": "80",
							"column-2": 5,
							"column-3": 82,
							"column-4": 2
						},
						{
							"category": "Entrepreneur",
							"column-1": 6,
							"column-2": "70",
							"column-3": 100,
							"column-4": 11
						},
						{
							"category": "Technology",
							"column-1": 2,
							"column-2": 3,
							"column-3": 97,
							"column-4": 26
						},
						{
							"category": "Design",
							"column-1": "10",
							"column-2": "11",
							"column-3": 94,
							"column-4": 26
						}
					]
				}
			);

AmCharts.makeChart("stackchart1",
				{
					"type": "serial",
					"categoryField": "category",
					/*"startDuration": 1,
	"startEffect": "easeOutSine",*/
					"theme": "light",
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "Strict",
							"type": "column",
							"valueField": "column-1"
						},
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-2",
							"title": "Conservative",
							"type": "column",
							"valueField": "column-2"
						},
						{
							"fillAlphas": 1,
							"fillColors": "#CD6A6A",
							"fontSize": 0,
							"id": "AmGraph-3",
							"lineAlpha": 0,
							"showBalloon": false,
							"tabIndex": 0,
							"title": "Moderate",
							"type": "column",
							"valueField": "column-3"
						},
						{
							"fillAlphas": 1,
							"fillColors": "#2B80B1",
							"id": "AmGraph-4",
							"lineAlpha": 0,
							"title": "Liberal",
							"type": "column",
							"valueField": "column-3"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"stackType": "regular",
							"title": "Student Count"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"useGraphSettings": true
					},
					"titles": [
						
					],
					"dataProvider": [
						{
							"category": "English",
							"column-1": "80",
							"column-2": 5,
							"column-3": 82,
							"column-4": 2
						},
						{
							"category": "Entrepreneur",
							"column-1": 6,
							"column-2": "70",
							"column-3": 100,
							"column-4": 11
						},
						{
							"category": "Technology",
							"column-1": 2,
							"column-2": 3,
							"column-3": 97,
							"column-4": 26
						},
						{
							"category": "Design",
							"column-1": "10",
							"column-2": "11",
							"column-3": 94,
							"column-4": 26
						}
					]
				}
			);