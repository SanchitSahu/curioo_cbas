console.log("forgot password js")

$(document).ready(function(){


    $("#errormodal").click(function(){
        $(this).removeClass("show")
        $(this).css("display","none")
    })

    $('#forgot-password-form').submit(function(e){
        e.preventDefault();
        url = $(this).attr('data-url')

        $.ajax({
            type: "POST",
            url: url,
            data: $('#forgot-password-form').serialize(),
            success: function (data) {
                $("#forgotpwd").css('display','none')
                $("#forgotpwd").removeClass('show')
                $("#successmodal").css('display','block')
                $("#successmodal").addClass('show')
                $("#successmodal").attr('aria-hidden','false')
                $("body").removeClass('modal-open')
                $("body").removeAttr('style')

            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
                $("#forgoterrormodal").css("display","block")
                $("#forgoterrormodal").addClass('show')
                $("#forgoterrormodal").removeAttr('aria-hidden')
                $("#forgotpwd").css('display','none')
                $("#forgotpwd").removeClass('show')
                $("#forgotpwd").attr('aria-hidden','true')
                $(".error-msg").text(data["responseJSON"]["message"])
                $("body").removeClass('modal-open')
                $("body").removeAttr('style')
                $('.modal-backdrop').remove()

            },
        });

        $(".forgot-close-btn").click(function(){
            $("#successmodal").css('display','none')
            $("#successmodal").removeClass('show')
            $("#successmodal").attr('aria-hidden','true')

            $('#forgoterrormodal').removeClass('show')
            $('#forgoterrormodal').css('display','none')
            $("body").removeClass('modal-open')
            $("body").removeAttr('style')
            $('.modal-backdrop').remove()
            $("#forgotpwd").css('display','none')
            $("#forgotpwd").attr('aria-hidden','true')
            $("#forgotpwd").removeClass('show')

        })
    })


})