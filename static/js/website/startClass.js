console.log("start class js")
function checkComment(student_id){
// var student_id = document.getElementById('student_id').value.trim();
// $("form#myForm"+student_id).submit(function(e) {
    // e.preventDefault(); 
    var formData = new FormData(document.getElementById("myForm"+student_id));
    console.log(formData);
    var ulTag = document.querySelector("#comment_"+student_id);
    var score = document.querySelectorAll("#score_"+student_id)
    url = $("#myForm"+student_id).attr('data-url')
    
    currentUrl = window.location.href;
    // console.log(currentUrl);
    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        success: function (msg) {
            console.log(msg);
            
            if(msg.message !== null){
                var node = document.createElement("LI");                 // Create a <li> node
                // var textnode = document.createTextNode(msg);         // Create a text node
                node.innerHTML = `<span class="user-pic">
                                    <img src="`+msg.teacher_image+`" alt="user pic">
                                </span>
                                <div class="comment-text">
                                    <h6>`+msg.teacher_pos+`</h6>
                                    <p>`+msg.message+`
                                    </p>
                                </div>
                                <div class="timestamp">
                                    <p>
                                        `+msg.comment_time+`
                                    </p>
                                    <p>
                                        `+msg.comment_date+`
                                    </p>
                                </div>`;
                
                ulTag.appendChild(node);
            }
            score[0].textContent = parseFloat(msg.score).toFixed(2);
            score[1].textContent = parseFloat(msg.score).toFixed(2);
            console.log(msg);
            console.log("Current url:",currentUrl);
            
            
            $("#successmodal"+student_id).css('display','block')
            $("#successmodal"+student_id).addClass('show')
            $("#successmodal"+student_id).attr('aria-hidden','false')
            // $("body").removeClass('modal-open')
            // $("body").removeAttr('style')
            $("#myForm"+student_id)[0].reset();
            setTimeout(function(){window.location = currentUrl;}, 2000);
            
        },
        erro:function(err){
            var node = document.createElement("LI");                 // Create a <li> node
            // var textnode = document.createTextNode(msg);         // Create a text node
            node.innerHTML = err.responseText;
            ulTag.appendChild(node);
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
}


$(".forgot-close-btn").click(function(){
    $("#successmodal").css('display','none')
    $("#successmodal").removeClass('show')
})

$(".startclass-close-btn").click(function(){
    $('#startclasserrormodal').css('display','none')
    $("body").removeClass('modal-open')
    $('.modal-backdrop').remove()
})