console.log("help desk .js")

    function uploadFile(event, id) {
        console.log('id--',id)
        console.log('event--',event)

        var extensionHelpDeskImage = $('#'+id).val().substring($("#"+id).val().lastIndexOf('.') + 1)
        console.log('extensionHelpDeskImage---',extensionHelpDeskImage)

        if(extensionHelpDeskImage == 'png' || extensionHelpDeskImage == 'jpg' || extensionHelpDeskImage == 'jpeg')
        {

            splitId = id.split('_')
            intId = parseInt(splitId[1])

            newHtmlInputBtn = `<input type="file"  accept="image/*" name="myfile" onchange="uploadFile(event,'help-desk-file_`+(intId+1)+`')" id="help-desk-file_`+(intId+1)+`" >`
            newHtmlLi = `<li>
                        <div class="img-wrapper">
                            <a onclick="customRemoveElement('output_`+(intId)+`')"><i class="fa fa-times" aria-hidden="true"></i></a>
                            <span><img id="output_`+(intId)+`" width="200" /></span>
                        </div>                                                        
                    </li>`
            
            $('.help-desk-images').append(newHtmlLi)
            console.log('file----',$('#'+id)[0].files);
            $('#'+id).css('display','none')
            $('.upload-btn-wrapper').append(newHtmlInputBtn)
            $("#output_"+(intId)).fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));
        }else{
            $("#errormodal").css("display","block")
            $("#errormodal").addClass('show')
            $("#errormodal").removeAttr('aria-hidden')
            $(".error-msg").text("Please Choose PNG or JPG or JPEG Image")
        }    
    }


function  customRemoveElement(id){
    console.log('id---',id)
    splitId = id.split('_')
    $("#"+id).parent().parent().parent().remove()
    $("#help-desk-file_"+splitId[1]).remove()
}

$(document).ready(function(){
    $("#help-desk-form").submit(function(e){
        e.preventDefault()
        if($("[name='studio']").val() != "select"){
            var formData = new FormData(this);
            $.ajax({
                type: "POST",
                url: $(this).attr('data-url'),
                data:formData,
                success: function (data) {
                    console.log(data);
                    console.log('Submission was successful.');
                    $("#successmodal").css('display','block')
                    $("#successmodal").addClass('show')
                    $("#successmodal").attr('aria-hidden','false')
                    $("body").removeClass('modal-open')
                    $("body").removeAttr('style')
                },
                error: function (data) {
                    console.log('An error occurred.');
                    console.log(data);
                    $("#errormodal").css("display","block")
                    $("#errormodal").addClass('show')
                    $("#errormodal").removeAttr('aria-hidden')
                    $(".error-msg").text(data["responseJSON"]["message"])
                },
                processData: false,
                contentType: false,
                cache: false,
            });
        }else{
            $("#errormodal").css("display","block")
            $("#errormodal").addClass('show')
            $("#errormodal").removeAttr('aria-hidden')
            $(".error-msg").text("Please Choose Studio")
        }
    })

    $(".help-desk-close-btn").click(function(){
        $('.error-modal').removeClass('show')
        $('.error-modal').css('display','none')
        $("#successmodal").css('display','none')
        $("#successmodal").removeClass('show')
        $("body").removeClass('modal-open')
        $("body").removeAttr('style')
        $('.modal-backdrop').remove()
        $("#help-desk-form")[0].reset()
        $("[name='myfile']").remove()
        newHtmlInputBtn = `<input type="file"  accept="image/*" name="myfile" onchange="uploadFile(event,'help-desk-file_1')" id="help-desk-file_1" >`
        $('.upload-btn-wrapper').append(newHtmlInputBtn)
        $('.help-desk-images').find('li').remove()
    })
})