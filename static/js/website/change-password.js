
console.log("change password js")

$(document).ready(function(){


    $('#change-password-form').submit(function(e){
        e.preventDefault()
        url = $(this).attr('data-url')
        console.log("url--",url)

        $.ajax({
            type: "POST",
            url: url,
            data: $('#change-password-form').serialize(),
            success: function (data) {
                console.log('Submission was successful.');
                $("#changepwd").css('display','none')
                $("#changepwd").removeClass('show')
                $("#successmodal").css('display','block')
                $("#successmodal").addClass('show')
                $("#successmodal").attr('aria-hidden','false')
                $("body").removeClass('modal-open')
                $("body").removeAttr('style')
                $("#change-password-form")[0].reset()

            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
                $("#errormodal").css("display","block")
                $("#errormodal").addClass('show')
                $("#errormodal").removeAttr('aria-hidden')
                $("#changepwd").css('display','none')
                $("#changepwd").removeClass('show')
                $("#changepwd").attr('aria-hidden','true')
                $(".error-msg").text(data["responseJSON"]["message"])

            },
        });

        $(".change-model-close").click(function(){
            $('.error-modal').removeClass('show')
            $('.error-modal').css('display','none')
            $("#successmodal").css('display','none')
            $("#successmodal").removeClass('show')
            $("body").removeClass('modal-open')
            $("body").removeAttr('style')
            $('.modal-backdrop').remove()
            // $("#changepwd").css('display','none')
            // $("#changepwd").attr('aria-hidden','true')
            // $("#changepwd").removeClass('show')

        })
    })


})