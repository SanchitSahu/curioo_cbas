AmCharts.makeChart("columnchartdiv1",
				{
                    "type": "serial",
                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
					"categoryField": "category",
					"columnSpacing": 0,
					"columnWidth": 0.17,
					"autoMarginOffset": -1,
					"marginBottom": 23,
					"marginLeft": 21,
					"marginTop": 24,
					"plotAreaBorderColor": "#715454",
					"hideCredits":true,
					/*"startDuration": 1,
	"startEffect": "easeOutSine",*/
					"hideBalloonTime": 145,
					"processTimeout": -3,
					"theme": "default",
					"categoryAxis": {
						"autoRotateAngle": 20,
		"autoRotateCount": 1,
						"widthField": "",
                        "gridAlpha": 0,
                        "axisColor": "#707070",
						"color": "#62CFE3",
						
                    },
                    "colors": [
                        "#F3ACB3", 
                        "#62CFE3",                     
                                                 
                    ],
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"labelText": "[[value]]",
							"title": "graph 1",
							"type": "column",
							"valueField": "column-1",
							
							
						},
						{
							// "balloonText": "[[title]] of [[category]]:[[value]]",
							"bullet": "round",
							"id": "AmGraph-2",
							// "labelText": "[[value]]",
							"lineThickness": 4,
							// "title": "graph 2",
							"valueField": "column-2",
							"type": "smoothedLine",
							"bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletBorderColor": "#62CFE3",
							"bulletColor": "#FFFFFF",
							
						}
					],
					"guides": [
						{
							"angle":""
						}
					],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"color":"#62CFE3"
							/*"autoRotateCount": 20*//*,
							"title": "Axis title"*/
						}
					],
					"allLabels": [],
					"balloon": {
						"animationDuration": 1
					},
					"legend": {
						"enabled": false,
                        "useGraphSettings": true,
                        "color": "#009ABE",
					},
					// "titles": [
					// 	{
					// 		"id": "Title-1",
					// 		"size": 15,
					// 		"text": "Chart Title"
					// 	}
					// ],
					"dataProvider": [
						{
							"category": "On Task",
							"column-1": "4.5",
							"column-2": "0"
						},
						{
							"category": "Preserverence",
							"column-1": "2.5",
							"column-2": "4.5"
						},
						{
							"category": "Problem Solving",
							"column-1": "3",
							"column-2": "3"
						},
						{
							"category": "Collaboration",
							"column-1": "3",
							"column-2": "1"
						},
						{
							"category": "Self Confidance",
							"column-1": "2.5",
							"column-2": "2"
						}
					]
				}
			);



AmCharts.makeChart("columnchartdiv2",
				{
                    "type": "serial",
                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
					"categoryField": "category",
					"columnSpacing": 0,
					"columnWidth": 0.17,
					"autoMarginOffset": -1,
					"marginBottom": 23,
					"marginLeft": 21,
					"marginTop": 24,
					"hideCredits":true,
					"plotAreaBorderColor": "#715454",
					/*"startDuration": 1,
	"startEffect": "easeOutSine",*/
					"hideBalloonTime": 145,
					"processTimeout": -3,
					"theme": "default",
					"categoryAxis": {
						"autoRotateAngle": 20,
		"autoRotateCount": 1,
						"widthField": "",
                        "gridAlpha": 0,
                        "axisColor": "#707070",
						"color": "#62CFE3",
						
                    },
                    "colors": [
                        "#1691B3", 
                        "#62CFE3",                     
                                                 
                    ],
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"labelText": "[[value]]",
							"title": "graph 1",
							"type": "column",
							"valueField": "column-1",
							
							
						},
						{
							// "balloonText": "[[title]] of [[category]]:[[value]]",
							"bullet": "round",
							"id": "AmGraph-2",
							// "labelText": "[[value]]",
							"lineThickness": 4,
							// "title": "graph 2",
							"valueField": "column-2",
							"type": "smoothedLine",
							"bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletBorderColor": "#62CFE3",
                            "bulletColor": "#FFFFFF",
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"color": "#62CFE3",
						}
					],
					"allLabels": [],
					"balloon": {
						"animationDuration": 1
					},
					"legend": {
						"enabled": false,
                        "useGraphSettings": true,
                        "color": "#009ABE",
					},
					// "titles": [
					// 	{
					// 		"id": "Title-1",
					// 		"size": 15,
					// 		"text": "Chart Title"
					// 	}
					// ],
					"dataProvider": [
						{
							"category": "On Task",
							"column-1": "4.5",
							"column-2": "0"
						},
						{
							"category": "Preserverence",
							"column-1": "2.5",
							"column-2": "4.5"
						},
						{
							"category": "Problem Solving",
							"column-1": "3",
							"column-2": "3"
						},
						{
							"category": "Collaboration",
							"column-1": "3",
							"column-2": "1"
						},
						{
							"category": "Self Confidance",
							"column-1": "2.5",
							"column-2": "2"
						}
					]
				}
			);



AmCharts.makeChart("columnchartdiv3",
				{
                    "type": "serial",
                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
					"categoryField": "category",
					"columnSpacing": 0,
					"columnWidth": 0.17,
					"autoMarginOffset": -1,
					"marginBottom": 23,
					"marginLeft": 21,
					"marginTop": 24,
					"hideCredits":true,
					"plotAreaBorderColor": "#715454",
					/*"startDuration": 1,
	"startEffect": "easeOutSine",*/
					"hideBalloonTime": 145,
					"processTimeout": -3,
					"theme": "default",
					"categoryAxis": {
						"autoRotateAngle": 20,
		"autoRotateCount": 1,
						"widthField": "",
                        "gridAlpha": 0,
                        "axisColor": "#707070",
						"color": "#62CFE3",
						
                    },
                    "colors": [
                        "#EB5858", 
                        "#62CFE3",                     
                                                 
                    ],
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"labelText": "[[value]]",
							"title": "graph 1",
							"type": "column",
							"valueField": "column-1",
							
							
						},
						{
							// "balloonText": "[[title]] of [[category]]:[[value]]",
							"bullet": "round",
							"id": "AmGraph-2",
							// "labelText": "[[value]]",
							"lineThickness": 4,
							// "title": "graph 2",
							"valueField": "column-2",
							"type": "smoothedLine",
							"bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletBorderColor": "#62CFE3",
                            "bulletColor": "#FFFFFF",
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"color": "#62CFE3",
						}
					],
					"allLabels": [],
					"balloon": {
						"animationDuration": 1
					},
					"legend": {
						"enabled": false,
                        "useGraphSettings": true,
                        "color": "#009ABE",
					},
					// "titles": [
					// 	{
					// 		"id": "Title-1",
					// 		"size": 15,
					// 		"text": "Chart Title"
					// 	}
					// ],
					"dataProvider": [
						{
							"category": "On Task",
							"column-1": "4.5",
							"column-2": "0"
						},
						{
							"category": "Preserverence",
							"column-1": "2.5",
							"column-2": "4.5"
						},
						{
							"category": "Problem Solving",
							"column-1": "3",
							"column-2": "3"
						},
						{
							"category": "Collaboration",
							"column-1": "3",
							"column-2": "1"
						},
						{
							"category": "Self Confidance",
							"column-1": "2.5",
							"column-2": "2"
						}
					]
				}
			);

AmCharts.makeChart("columnchartdiv4",
				{
					"type": "serial",
					"hideCredits":true,
                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
					"categoryField": "category",
					"columnSpacing": 0,
					"columnWidth": 0.17,
					"autoMarginOffset": -1,
					"marginBottom": 23,
					"marginLeft": 21,
					"marginTop": 24,
					"plotAreaBorderColor": "#715454",
					/*"startDuration": 1,
	"startEffect": "easeOutSine",*/
					"hideBalloonTime": 145,
					"processTimeout": -3,
					"theme": "default",
					"categoryAxis": {
						"autoRotateAngle": 20,
		"autoRotateCount": 1,
						"widthField": "",
                        "gridAlpha": 0,
                        "axisColor": "#707070",
						"color": "#62CFE3",
						
                    },
                    "colors": [
                        "#F6BE57", 
                        "#62CFE3",                     
                                                 
                    ],
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"labelText": "[[value]]",
							"title": "graph 1",
							"type": "column",
							"valueField": "column-1",
							
							
						},
						{
							// "balloonText": "[[title]] of [[category]]:[[value]]",
							"bullet": "round",
							"id": "AmGraph-2",
							// "labelText": "[[value]]",
							"lineThickness": 4,
							// "title": "graph 2",
							"valueField": "column-2",
							"type": "smoothedLine",
							"bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletBorderColor": "#62CFE3",
                            "bulletColor": "#FFFFFF",
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"color": "#62CFE3",
						}
					],
					"allLabels": [],
					"balloon": {
						"animationDuration": 1
					},
					"legend": {
						"enabled": false,
                        "useGraphSettings": true,
                        "color": "#009ABE",
					},
					// "titles": [
					// 	{
					// 		"id": "Title-1",
					// 		"size": 15,
					// 		"text": "Chart Title"
					// 	}
					// ],
					"dataProvider": [
						{
							"category": "On Task",
							"column-1": "4.5",
							"column-2": "0"
						},
						{
							"category": "Preserverence",
							"column-1": "2.5",
							"column-2": "4.5"
						},
						{
							"category": "Problem Solving",
							"column-1": "3",
							"column-2": "3"
						},
						{
							"category": "Collaboration",
							"column-1": "3",
							"column-2": "1"
						},
						{
							"category": "Self Confidance",
							"column-1": "2.5",
							"column-2": "2"
						}
					]
				}
			);
			AmCharts.makeChart("columnchartdivgeneral",
				{
                    "type": "serial",
                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
					"categoryField": "category",
					"columnSpacing": 0,
					"columnWidth": 0.17,
					"autoMarginOffset": -1,
					"marginBottom": 23,
					"marginLeft": 21,
					"marginTop": 24,
					"hideCredits":true,
					"plotAreaBorderColor": "#715454",
					"startDuration": 0,
					"hideBalloonTime": 145,
					"processTimeout": -3,
					"theme": "default",
					
					"categoryAxis": {
						"autoRotateAngle": 20,
						"autoRotateCount": 1,
						"widthField": "",
                        "gridAlpha": 0,
                        "axisColor": "#707070",
						"color": "#63cfe3",
						
                    },
                    "colors": [
                        "#F3ACB3", 
                        "#62CFE3",                     
                                                 
                    ],
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"labelText": "[[value]]",
							"title": "graph 1",
							"type": "column",
							"valueField": "column-1",
							
							
						},
						{
							// "balloonText": "[[title]] of [[category]]:[[value]]",
							"bullet": "round",
							"id": "AmGraph-2",
							// "labelText": "[[value]]",
							"lineThickness": 4,
							// "title": "graph 2",
							"valueField": "column-2",
							"type": "smoothedLine",
							"bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletBorderColor": "#62CFE3",
                            "bulletColor": "#FFFFFF",
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"color": "#63cfe3",
							// "title": "Axis title"
						}
					],
					"allLabels": [],
					"balloon": {
						"animationDuration": 0
					},
					"legend": {
						"enabled": false,
                        "useGraphSettings": true,
                        "color": "#009ABE",
					},
					// "titles": [
					// 	{
					// 		"id": "Title-1",
					// 		"size": 15,
					// 		"text": "Chart Title"
					// 	}
					// ],
					"dataProvider": [
						{
							"category": "Stud 1",
							"column-1": "80",
							"column-2": "10"
						},
						{
							"category": "Stud 2",
							"column-1": "40",
							"column-2": "35"
						},
						{
							"category": "Stud 3",
							"column-1": "20",
							"column-2": "2"
						},
						{
							"category": "Stud 4",
							"column-1": "50",
							"column-2": "40"
						},
						{
							"category": "Stud 5",
							"column-1": "60",
							"column-2": "5"
						}
					]
				}
			);