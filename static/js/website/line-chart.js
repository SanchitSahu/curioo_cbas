AmCharts.makeChart("scalechart",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 0,
					"hideCredits":true,
					"categoryAxis": {
						"autoRotateAngle": 9,
						"widthField": "",
                        "gridAlpha": 0,
                        "axisColor": "#707070",
                        "color": "#63cfe3"
                    },
                    "colors": [
                        "#62CFE3",                                                                      
                    ],
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"bullet": "round",
							"id": "AmGraph-1",
							"title": "graph 1",
                            "valueField": "column-1",
                            "lineThickness": 4,
                            "bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletBorderColor": "#62CFE3",
                            "bulletColor": "#FFFFFF",
                            
						},
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
                            "bullet": "square",                            
							"id": "AmGraph-2",
							"title": "graph 2",
                            "valueField": "column-2"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"color": "#63cfe3",
							// "title": "Axis title"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false,
                        "useGraphSettings": true,
                        "color": "#63cfe3",
					},
					// "titles": [
					// 	{
					// 		"id": "Title-1",
					// 		"size": 15,
					// 		"text": "Chart Title"
					// 	}
					// ],
					"dataProvider": [
						{
							"category": "4",
							"column-1": "2"
						},
						{
							"category": "2",
							"column-1": "4"
						},
						{
							"category": "2",
							"column-1": "2"
						},
						{
							"category": "2",
							"column-1": "5"
						}
					]
				}
			);