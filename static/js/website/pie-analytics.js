AmCharts.makeChart("pieanalytic",
    {
        "type": "pie",
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "titleField": "category",
        "valueField": "column-1",
        "allLabels": [],
        "balloon": {},
        "radius": "35%",
        "startDuration": 0,
        "legend": {
            "enabled": true,
            "color": "#221815",
            "markerType": "square",
            "valueAlign": "left",
            "fontSize": 17,
        },
	"titles": [],
        "colors": [
            "#5A8DC4",
            "#D9B352",  
            "#D5616E",
            "#3EB55A",                           
        ],
        "gradientRatio": [],
        "labelTickAlpha": 0,
        "labelTickColor": "#C82222",
        "outlineThickness": 2,
        "titleField": "country",
        "valueField": "litres",
        "color": "#FFFFFF",
        "fontSize": 12,
        "handDrawn": false,
        "handDrawScatter": 0,
        "handDrawThickness": 0,
        "theme": "chalk",
        "allLabels": [],
        "balloon": {},
        "titles": [],
        "dataProvider": [
            {
                "country": "Very Good",
                "litres": 2
            },
            {
                "country": "Negative",
                "litres": 2
            },
            {
                "country": "Nuetral",
                "litres": 1
            },
            {
                "country": "Upbeat",
                "litres": 1
            }
        ]
    }
);