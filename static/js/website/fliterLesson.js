console.log("filter lesson")
// var default_date_count = 0;
// var unknown_color_date = {}
$('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
    console.log("url-----",window.location.pathname)
    
    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    if (window.location.pathname.split("/")[2] == "student" || window.location.pathname.split("/")[2] == "lesson"){
        console.log("123")
        return 0;
    }
    html_data = `<div class="col-lg-4 lesson-details" >
                <div class="score-card border1">
                        <div class="detail detaila text-center pad-20 colora1">
                        <span>
                            <p class="font-16 text-right">{{l.start_time}}</p>
                            
                            <h6 class="font1-20">  <img src="{% static 'img/website/curioo.png' %}" alt="effort" class="mr-3"> {{l.lesson_code}}</h6>
                        </span>
                            <div class="effect-card">
                    <ul>
                        <li>
                            Student Number
                            <p>{{l.total_students}}</p>
                        </li>
                            <li>
                                Lesson Code
                            <p>{{l.lesson_code}}</p>
                        </li>
                            <li>
                            Subject
                            <p>{{l.subject}}</p>
                        </li>
                            <li>
                            Teacher
                            <p>{{l.teacher}}</p>
                        </li>
                            <li>
                                Scheduled Date
                            <p>{{l.scheduled_date}}</p>
                        </li>
                            <li>
                            Room
                            <p>{{l.room}}</p>
                        </li>
                    </ul>
                    </div>
                    </div>
                    
                    <div class="width-100">
                        <a class="width-col-50" href="view-lesson.html"><div class="pad-10 text-center font-16 border-right-a"><img src="{% static 'img/website/view.png' %}" class="mr-1" alt="effort"> View</div></a>
                        <a href="#" onclick="alert('show lesson report in progress')" class="width-col-50"><div class="pad-10 text-center font-16"><img src="{% static 'img/website/insights.png' %}" class="mr-1 mb-1" alt="effort"> Insights</div></a>
                        
                    </div>
                </div>
            </div>`
    $.ajax({        
            method: "POST",
            url: 'searchLesson/',
            data:{
                'date_range':$(this).val()
            },
            success: function(data){
                console.log("success in post--------")
                console.log("**********",data)
                $('.lesson-details').remove()
                var insights_bool;
                var borderColor = { 
                    "english": "#F3ACB3", 
                    "entrepreneur": "#1691B3", 
                    "technology": "#F6BE57", 
                    "creative design": "#EB5858", 
                    "default": ["#0f6b84", "#678388", "#9a5653", "#FFEB3B", "#FF9800", "#673AB7"] };
    
                for(var i=0; i<data.length; i++){
                    var div = 
                `<div class="col-lg-4 lesson-details" >
                <div id="`+data[i]["lesson_code"]["lesson_code"]+`" class="score-card border1 ">
                    <input id="hid_`+data[i]["lesson_code"]["lesson_code"]+`" type="hidden" value="`+data[i]["lesson_code"]["subject_name"]+`">
                        <div class="detail detaila text-center pad-20 colora1">
                        <span>
                            <p class="font-16 text-right">`+data[i]['lesson_code']['start_time']+`</p>
                            
                            <h6 class="font1-20">  <img src="/static/img/website/curioo.png" alt="effort" class="mr-3">` +data[i]["lesson_code"]["lesson_code"]+`</h6>
                        </span>
                            <div class="effect-card">
                    <ul>
                        <li>
                            Student Number
                            <p>`+data[i]['lesson_code']['students']+`</p>
                        </li>
                            <li>
                                Lesson Code
                            <p>`+data[i]["lesson_code"]["lesson_code"]+`</p>
                        </li>
                            <li>
                            Subject
                            <p>`+data[i]["lesson_code"]["subject_name"]+`</p>
                        </li>
                            <li>
                            Teacher
                            <p>`+data[i]['lesson_code']['teacher']+`</p>
                        </li>
                            <li>
                                Scheduled Date
                            <p>`+data[i]['lesson_code']['scheduled_date']+`</p>
                        </li>
                            <li>
                            Room
                            <p>`+data[i]['lesson_code']['room']+`</p>
                        </li>
                            
                    </ul>
                    </div>
                    </div>
                    
                    <div class="width-100">
                        `;
                        
                        if (data[i]['lesson_code']['today'] > data[i]['lesson_code']['schedule_date']){
                            insights_bool = true
                            div = div + 
                            `<a id="view_`+data[i]['lesson_code']['lesson_code']+`" class="width-col-50" href="/teacher/lesson/view/`+data[i]['lesson_id']+`"><div class="pad-10 text-center font-16 border-right-a"><img src="/static/img/website/view.png" class="mr-1" alt="effort"> View</div></a>
                            <a id="insight_`+data[i]['lesson_code']['lesson_code']+`" href="#" onclick="alert('show lesson report in progress')" class="width-col-50"><div class="pad-10 text-center font-16"><img src="/static/img/website/insights.png" class="mr-1 mb-1" alt="effort"> Insights</div></a> </div>
                            </div>`;
                        }else{
                            div = div + `<a id="view_`+data[i]['lesson_code']['lesson_code']+`" class="width-col-50" href="/teacher/lesson/view/`+data[i]['lesson_id']+`"><div class="pad-10 text-center font-16 border-right-a"><img src="/static/img/website/view.png" class="mr-1" style="float:center;" alt="effort"> View</div></a></div></div>`;
                        }
                   
           
            $('#searchLesson').append(div);
            console.log("code-----",data[i]['lesson_code']['lesson_code'])
            var lessonCode = data[i]['lesson_code']['lesson_code']
            var subName = data[i]["lesson_code"]["subject_name"]

            console.log("lessonCode", lessonCode)
            
            // <script src="{% static 'js/website/jquery-3.4.1.min.js' %}" ></script>
                
                
                // // console.log("subjectName ", subjectName);
                // var el = document.getElementsByClassName("score-card border1");
                // var color;
                
                $("#"+lessonCode).hover(function(){
                    console.log("code in hover-----",this.id)

                    var subjectName = document.getElementById("hid_"+this.id).value.toLowerCase();
                    if (borderColor.hasOwnProperty(subjectName)) {
                        console.log("if if", subjectName)
                        color = borderColor[subjectName];
                    }else{
                        if (unknown_color.hasOwnProperty(subjectName)){
                            console.log("in if ");
                            
                            color = unknown_color[subjectName];
                            console.log("default in if color", color);
                        }else{
                            console.log("in second else");
                            
                            unknown_color[subjectName] = borderColor["default"][default_count]
                            color = unknown_color[subjectName];
                            default_count++;
                            console.log("default count", unknown_color);
                            console.log("default color", color);
                        }                        
                    }
                    document.getElementById(this.id).style.border = '2px solid '+color;
                    document.getElementById('view_'+this.id).style.borderTop = '1px solid '+color;
                    if (insights_bool == true){
                    document.getElementById('insight_'+this.id).style.borderLeft = '1px solid '+color;
                    document.getElementById('insight_'+this.id).style.borderTop = '1px solid '+color;
                }
                }, function () {
                    document.getElementById(this.id).style.border = '0px solid '+color;
                    document.getElementById('view_'+this.id).style.borderTop = '0px solid '+color;
                    if (insights_bool == true){
                        document.getElementById('insight_'+this.id).style.borderLeft = '0px solid '+color;
                        document.getElementById('insight_'+this.id).style.borderTop = '0px solid '+color;
                        
                    }
                });
                insights_bool = false
            }

            },
            error: function(error_data){
                console.log("error")
                console.log(error_data)
            }
        })
});  

$("#search_lesson").on('keyup',function(){
    console.log("in search")
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("search_lesson");
    filter = input.value.toUpperCase();
    ul = document.getElementById("searchLesson");
    console.log("ulllll" , ul)
    li = $(ul).find('ul')
    console.log("divvvv" , li)
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("p")[1];
        console.log("aaaaaa" , a)
        txtValue = a.textContent || a.innerText;
        console.log("txtvalu" , txtValue)
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            console.log("in if")
            li[i].style.display = "";
            li[i].parentElement.parentElement.parentElement.parentElement.style.display = "block";
        } else {
            console.log("in else")
            li[i].style.display = "none";
            li[i].parentElement.parentElement.parentElement.parentElement.style.display = "none";
        }
    }
})

