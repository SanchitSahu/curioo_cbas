console.log("reset password js")

$(document).ready(function(){
    $('#reset-password-form').submit(function(e){
            e.preventDefault();
            url = $(this).attr('data-url')
            console.log("url--",url)

            $.ajax({
                type: "POST",
                url: url,
                data: $('#reset-password-form').serialize(),
                success: function (data) {
                    console.log('Submission was successful.');
                    console.log(data);
                    $("#reset-password-form").css('display','none')
                    $("#reset-password-form").removeClass('in')
                    $("#success-msg").css('display','block')
                    $("#success-msg").addClass('in')
                    $("#success-msg").attr('aria-hidden','false')
                    $("body").removeClass('modal-open')
                    $("body").removeAttr('style')

                },
                error: function (data) {
                    console.log('An error occurred.');
                    console.log(data['responseJSON']['message']);
                    $("#errormodal").addClass('in')
                    $("#errormodal").css('display','block')
                    $(".error-msg").text(data['responseJSON']['message'])
                },
            });

        });
    
    $(".error-close-btn").click(function(){
        $("#errormodal").css('display','none')
        $("#errormodal").removeClass('in')
        $("#errormodal").attr('aria-hidden','true')
        // $('.modal-backdrop').removeClass('show')
    })
});