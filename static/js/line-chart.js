AmCharts.makeChart("scalechart",
				{
					"type": "serial",
					"categoryField": "category",
					/*"startDuration": 1,
					"startEffect": "easeOutSine",*/
					"categoryAxis": {
						"autoRotateAngle": 9,
						"widthField": "",
                        "gridAlpha": 0,
                        "axisColor": "#707070",
                        "color": "#009ABE"
                    },
                    "colors": [
                        "#62CFE3",                                                                      
                    ],
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"bullet": "round",
							"id": "AmGraph-1",
							"title": "[[title]]",
                            "valueField": "column-1",
                            "lineThickness": 4,
                            "bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletBorderColor": "#62CFE3",
                            "bulletColor": "#FFFFFF",
                            
						},
						{
							"balloonText": "[[category]]:[[value]]",
                            "bullet": "square",                            
							"id": "AmGraph-2",
							/*"title": "graph 2",*/
							 "bullet": "round",
                            "valueField": "column-2",
                             "bulletBorderAlpha": 1,
                            "bulletBorderColor": "#62CFE3",
                            "bulletColor": "#FFFFFF"
						},
						{
							"balloonText": "[[category]]:[[value]]",
                            "bullet": "square",                            
							"id": "AmGraph-2",
							/*"title": "graph 2",*/
							 "bullet": "round",
                            "valueField": "column-2",
                             "bulletBorderAlpha": 1,
                            "bulletBorderColor": "#62CFE3",
                            "bulletColor": "#FFFFFF"
						},
						{
							"balloonText": "[[category]]:[[value]]",
                            "bullet": "square",                            
							"id": "AmGraph-2",
							/*"title": "graph 2",*/
							 "bullet": "round",
                            "valueField": "column-2",
                             "bulletBorderAlpha": 1,
                            "bulletBorderColor": "#62CFE3",
                            "bulletColor": "#FFFFFF"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"color":"#009ABE",
							"fillColor":"#009ABE",
							"minimum":1,
							"maximum":4,
							/*"labelFunction": function(value) {
												var res;
												for(var i=1;i<=5;i++){
													 yv = Math.floor(Math.random() + 5);
													console.log(i);
													console.log("yv"+yv);
													if(i === 1){res = "EN";}
													else if(i === 2){res = "CD";}
													else if(i === 3){res = "EP";}
													else if(i === 4){res = "TP";}
													console.log(res);
													return res;
												}
													
													if(yv == 1){return "EN";}
													else if(yv == 2){return "CD";}
													else if(yv == 3){return "EP";}
													else if(yv == 4){return "TP";}
												
											}
												*/
												
											

							/*,
							"title": "Axis title"*/
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false,
                        "useGraphSettings": true,
                        "color": "#009ABE",
					},
					"titles": [
						/*{
							"id": "Title-1",
							"size": 15,
							"text": "Chart Title"
						}*/
					],
					"dataProvider": [
						{
							"category": "Negative",
							"column-1": "1"
						},
						{
							"category": "Neutral ",
							"column-1": "3"
						},
						/*{
							"category": "Neutral ",
							"column-2": "37"
						},*/
						{
							"category": "Upbeat",
							"column-1": "2"
						},
						{
							"category": "Very good",
							"column-1": "4"
						}
					]
				}
			);











