console.log("graph date filter")

$("#graphDate").submit(function(e){
    e.preventDefault();   
    var fromDate = $("#from_date").val()
    var toDate = $("#to_date").val()
    console.log("form date---",fromDate,typeof(fromDate),"to date---",toDate,typeof(toDate))
    console.log("url----",$("#graphDate").attr("data-url"))
    console.log("csrf---",$("[name='csrfmiddlewaretoken']").val())
    console.log("student id---",$(".date_submit").attr("id"))
    $.ajax({        
        method: "POST",
        url:$("#graphDate").attr("data-url"),
        data:{
            'from_date':fromDate,
            'to_date':toDate,
            'sid':$(".date_submit").attr("id")
        },
        success: function(data){
            console.log("success")
            console.log("**********",data)
            pie_labels1 = [];
            pie_colors1 = [];
            pie_values1 = {};
            overall_summ = 0
            for (var key_pie in data['default']){
                var list_values = Object.values(data["default"][key_pie])
                console.log("List_values", list_values);
                var sum = 0;
                for (var v = 0; v<list_values.length; v++){                   
                    sum = sum + parseInt(list_values[v]);
                }
                overall_summ = overall_summ + sum/list_values.length;                
                pie_values1[key_pie] = sum/list_values.length;
            }
            for (key_pie in pie_values1){

                console.log("key", key_pie);
                document.getElementById("percent_"+key_pie).innerHTML = ((pie_values1[key_pie] * 100)/overall_summ).toFixed(1)+"%";
                pie_values1[key_pie] = ((pie_values1[key_pie] * 100)/overall_summ).toFixed(1);
            }
            console.log("sub_data------------",sub_data)
            
            for (var key in sub_data) {

                pie_labels1.push(key);
                console.log("sub_data[key][0]--------------",sub_data[key])
                pie_colors1.push(sub_data[key][0]);
                console.log(sub_data[key]);
                console.log("pie_colors1--------099999999999------------",pie_colors1)
            }      
            console.log("Pie labels", pie_labels1);
            console.log("subjects.length----",subjects.length)
            var e = document.getElementById("subcharts")
                var child = e.lastElementChild;  
                while (child) { 
                    e.removeChild(child); 
                    child = e.lastElementChild; 
                }
            for(var i=0; i<subjects.length; i++){

                console.log(i);
                console.log("subject data",sub_data);
                console.log("data---",data.labels,data.labels.length)
                // if data.labels.length != undefined:             
                labels = data.labels[subjects[i]]
                console.log("labels----",labels)
                console.log("")
                defaultData = data.default[subjects[i]]
                console.log("defaultData--123--",defaultData)
                //removing extra decimals----------
                console.log("++++++",data["default"][i])
                if(defaultData != undefined){
                    for (var decimal_key in data["default"][i]){
                        if(decimal_key.length != 0){
                            data["default"][i][decimal_key] = data["default"][i][decimal_key].toFixed(2);
                        }
                    }
                    console.log("decimal_key ", Object.values(data["default"]));
                }
                
                setChart(i, subjects[i],labels,defaultData, data)
            }
            var e = document.getElementById("chartdiv1")
                var child = e.lastElementChild;  
                while (child) { 
                    e.removeChild(child); 
                    child = e.lastElementChild; 
                } 
            setPieChart(pie_labels1, pie_values1, pie_colors1)
            // setPieChart();   
            console.log("data555555555555",data) 
             
            var e = document.getElementById("motivation-ch")
                var child = e.lastElementChild;  
                while (child) { 
                    e.removeChild(child); 
                    child = e.lastElementChild; 
                } 
            setMotivationalChart(data);
            
            var e = document.getElementById("scale-chart")
            var child = e.lastElementChild;  
            while (child) { 
                e.removeChild(child); 
                child = e.lastElementChild; 
            } 
            setWellbeing(data);
            console.log("defaultData---",defaultData);
        },
        error: function(error){
            console.log("error---",error)
        },
        
    });
})
// pie chart function
function setPieChart(pie_labels1, pie_values1, pie_colors1){
    console.log("in set pie chart");
    chartHtml = `<canvas id="pie_`+1+`" style="display:"block"></canvas>`;
    console.log("chartHtml-----------------------------------",chartHtml)
    $('#chartdiv1 div').remove()
    $('#chartdiv1 iframe').remove()
    $('#chartdiv1').append(chartHtml)
    // $('#chartdiv1').append(`<p>Progress</p>`)

    // var ctx = document.getElementById("myChart");
    // console.log("ctx--",ctx)
    var chartId = document.getElementById("pie_"+1);
    console.log("pie_colors1---123--",pie_colors1,Object.values(pie_values1))
    var myChart = new Chart(chartId, {
        type: 'pie',
    data: {
        labels: pie_labels1,
        datasets: [{
        backgroundColor: pie_colors1,
        data: Object.values(pie_values1)
        }]
        }
    });    

}

// motivational chart (bar chart)

function setMotivationalChart(data) {

        console.log('data000000000000000',data)
        console.log("in set Motivational chart");
        chartHtml = `<canvas id="motivational_scale" style="display:"block"></canvas>`;

        $('#motivation-ch').append(chartHtml)
        // var ctx = document.getElementById("myChart");
        // console.log("ctx--",ctx)


        let pie_colors_motivation = [];
        console.log("data[==============",data["motivation"])
        console.log("Object.keys(data-------------",Object.keys(data["motivation"]))
        var countColor = 0
        Object.keys(data["motivation"]).forEach(data => {
            countColor = countColor + 1
            console.log("data-------",data)
            console.log("push data------------",dict[data.toLowerCase()], typeof(dict[data.toLowerCase()]))
            if(dict[data.toLowerCase()] == undefined){
                pie_colors_motivation.push(dict['default'][countColor]);
                console.log("dict-----",dict['default'][countColor])
            }else{
                pie_colors_motivation.push(dict[data.toLowerCase()]);
            }
        })

        console.log("pie_colors_motivation------",pie_colors_motivation)

        var chartId = document.getElementById("motivational_scale");
        var myChart = new Chart(chartId, {
            type: 'bar',
            data: {
                labels: Object.keys(data["motivation"]),
                datasets: [{
                    label: '# of Votes',
                    backgroundColor: pie_colors_motivation,
                    data: Object.values(data["motivation"])
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            min: 0,
                            max: 5,
                            stepSize: 1,
                            suggestedMin: 0.5,
                            suggestedMax: 5.5,
                            callback: function (label, index, labels) {
                                switch (label) {
                                    case 0:
                                        return 'No efforts';
                                    case 1:
                                        return 'Low efforts';
                                    case 2:
                                        return 'Ok efforts';
                                    case 3:
                                        return 'Good efforts';
                                    case 4:
                                        return 'Great efforts';
                                }
                            }
                        },
                    }],
                },
                legend: {
                    display: false
                },
            }
        });
    }

// wellbeing (line chart) scale

function setWellbeing(data){
    console.log("in set well being chart");
    chartHtml = `<canvas id="well_being_scale" style="display:"block"></canvas>`;
        
        $('#scale-chart').append(chartHtml)
        // var ctx = document.getElementById("myChart");
        // console.log("ctx--",ctx)
        var chartId = document.getElementById("well_being_scale");
    var myChart = new Chart(chartId, {
        type: 'line',
        data: {
            labels: Object.keys(data["wellbeing"]),
            datasets: [{
                fill: false,
                borderColor: "#43b1bf",
                data: Object.values(data["wellbeing"])
            }]
            },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        max: 5,
                        stepSize: 1,
                        suggestedMin: 0.5,
                        suggestedMax: 5.5,
                        callback: function(label, index, labels) {
                            switch (label) {
                            case 0:
                                return 'Negative';
                            case 1:
                                return 'Neutral';
                            case 2:
                                return 'Upbeat';
                            case 3:
                                return 'Very-Good';
                            }
                        }
                        },
                }]
            },
            legend: {
                display: false
                },
        }
    });   
}
// bar chart
default_count = 0;
function setChart(i, subLen,labels,defaultData, data){
    console.log("data[].length", data["default"].length);
    
    // if(data["default"].length != undefined){
        // var data_list = Object.values(data["default"][subLen]);
        // console.log("data_list", data_list);
        var values, labels;
        console.log("data", data["default"][subLen]);
        // if (Object.values(data["default"][subLen]) == undefined){
        //     values = []
        // }else{
        //     values = Object.values(data["default"][subLen])
        // }
        try{
            values = Object.values(data["default"][subLen])
            lables = Object.keys(data["default"][subLen])
        }catch(e){
            values = []
            labels = []
        }
    
    
        console.log("22222222222",labels);
        var dis = "none";
        if (i == 0){
            dis = "block";
        }
        if (dict.hasOwnProperty(document.getElementById("Name_"+subLen).innerHTML.toLowerCase())){
            bckclr = dict[document.getElementById("Name_"+subLen).innerHTML.toLowerCase()];
        }else{
            bckclr = dict["default"][default_count];
            default_count++;
        }
        
        console.log("ididd", bckclr);
        

        // document.getElementById("subTitle_"+subLen).style.display = dis;
        chartHtml = `<canvas id="myChart_`+subLen+`" style="display:`+dis+`;" >
        </canvas>`
        $('#subcharts').append(chartHtml)
        console.log("subcharts", document.getElementById("subcharts"))
        // var ctx = document.getElementById("myChart");
        // console.log("ctx--",ctx)
        var chartId = document.getElementById("myChart_"+subLen);
        console.log("chartId--",chartId)
        console.log("Subject id ", subLen)

        var myChart = new Chart(chartId, {
            type: 'bar',
            
                data: {
                    labels: labels,
                    datasets: [{
                        label: $("#subTitle_"+subLen).text(),
                        data: values,
                        backgroundColor: bckclr,
                        // borderColor: [
                        //     'rgba(255,99,132,1)',
                        //     'rgba(54, 162, 235, 1)',
                        //     'rgba(255, 206, 86, 1)',
                        //     'rgba(75, 192, 192, 1)',
                        //     'rgba(153, 102, 255, 1)',
                        //     'rgba(255, 159, 64, 1)'
                        // ],
                        borderWidth: 1
                    }]
                },

            options: {
                // code for showing bar value on the the top of bar 
                "hover": {
                    "animationDuration": 0
                },
                "animation": {
                    "duration": 1,
                    "onComplete": function() {
                        var chartInstance = this.chart,
                            ctx = chartInstance.ctx;

                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';

                        this.data.datasets.forEach(function(dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function(bar, index) {
                                var data = dataset.data[index];
                                ctx.fillText(data, bar._model.x, bar._model.y - 5);
                            });
                        });
                    }
                }
            },
            legend: {
            //   "display": false
            },
            tooltips: {
            "enabled": false
            },
            scales: {
                yAxes: [{
                    // display: false,

                    ticks: {
                        // max: Math.max(...data.datasets[0].data) + 10,
                        // display: false,
                        beginAtZero: true
                    }
                }],
            }
// end of code for showing bar value on the the top of bar 
        });

    // }
}


// })
// bar chart function hide show according to subject
function showGraph(subId) {
    // alert('1')
    console.log("subjects----",subjects)
    console.log("11111155555111111",subjects.length,"2222222555222",subId.split("_")[1],"33333555333");  
    for(var i = 0; i < subjects.length; i++){
        console.log('subject55555555555',subjects[i]);
        // document.getElementById("subTitle_"+subjects[i]).style.display = "none";
        document.getElementById("myChart_"+subjects[i]).style.display = "none";
    }
    document.getElementById("myChart_"+subId.split("_")[1]).style.display = "block";
    // document.getElementById("subTitle_"+subId.split("_")[1]).style.display = "block";
}