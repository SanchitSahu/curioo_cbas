AmCharts.makeChart("scorelinechart",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 0,
					"categoryAxis": {
						"autoRotateAngle": 9,
						"widthField": "",
                        "gridAlpha": 0,
                        "axisColor": "#707070",
                        "color": "#009ABE"
                    },
                    "colors": [
                        "#62CFE3",                                                                      
                    ],
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"bullet": "round",
							"id": "AmGraph-1",
							"title": "graph 1",
                            "valueField": "column-1",
                            "lineThickness": 4,
                            "bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletBorderColor": "#62CFE3",
                            "bulletColor": "#FFFFFF",
                            
						},
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
                            "bullet": "square",                            
							"id": "AmGraph-2",
							"title": "graph 2",
                            "valueField": "column-2"
						}
					],
					"guides": [],
					"valueAxes": [
						{
                            "id": "ValueAxis-1",
                            "autoGridCount": false,
							// "title": "Axis title"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false,
                        "useGraphSettings": true,
                        "color": "#009ABE",
					},
					"dataProvider": [
						{
							"category": "Stud1",
							"column-1": "12"
						},
						{
							"category": "Stud2",
							"column-1": "30"
						},
						{
							"category": "Stud3",
							"column-1": "27"
						},
						{
							"category": "Stud4",
							"column-1": "10"
                        },
                        {
							"category": "Stud5",
							"column-1": "22"
                        },
                        {
							"category": "Stud6",
							"column-1": "17"
						},
					]
				}
			);