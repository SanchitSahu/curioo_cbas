	AmCharts.makeChart("motivation-chart",
				{
                    "type": "serial",
                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
					"categoryField": "category",
					"animation": false,
					"columnSpacing": 0,
					"columnWidth": 0.17,
					"autoMarginOffset": -1,
					"marginBottom": 23,
					"marginLeft": 21,
					"marginTop": 24,
					"plotAreaBorderColor": "#715454",
					"startDuration": 0,
					"hideBalloonTime": 145,
					"processTimeout": -3,
					"theme": "default",
					"categoryAxis": {
						"autoRotateAngle": 9,
						"widthField": "",
                        "gridAlpha": 0,
                        "axisColor": "#707070",
                        "color": "#009ABE"
                    },
                    "colors": [
                        "#F6BE57",                         
                        "#62CFE3",                                                                   
                                                 
                    ],
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"labelText": "[[value]]",
							"title": "graph 1",
							"type": "column",
							"valueField": "column-1",
							"colorField": "color",							
						},
						{
							"balloonText": "[[title]] of [[category1]]:[[value]]",
							"bullet": "round",
							"id": "AmGraph-2",
							// "labelText": "[[value]]",
							"lineThickness": 4,
							"title": "graph 2",
                            "valueField": "column-2",
                            "type": "smoothedLine",
                            "bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletBorderColor": "#62CFE3",
							"bulletColor": "#FFFFFF",
						
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							// "title": "Axis title"
						}
					],
					"allLabels": [],
					"balloon": {
						"animationDuration": 0
					},
					"legend": {
						"enabled": false,
						"useGraphSettings": true
					},
					// "titles": [
					// 	{
					// 		"id": "Title-1",
					// 		"size": 15,
					// 		"text": "Chart Title"
					// 	}
					// ],
					"dataProvider": [
						{
							"category": "Tech Brain",	
							"color": "#F6BE57",						
							"column-1": "80",
							"category1": "Brain",
							"column-2": "45"
						},
						{
							"category": "Creative Design",
							"color": "#EB5858",
							"column-1": "20",
							"category1": "Brain",
							"column-2": "20"
						},
						{
							"category": "Entrepreneur",
							"color": "#1691B3",
							"column-1": "20",
							"category1": "Brain",
							"column-2": "2",
							
						},
						{
							"category": "English",
							"color": "#F3ACB3",
							"column-1": "50",
							"category1": "Brain",
							"column-2": null
						},
					]
				}
			);