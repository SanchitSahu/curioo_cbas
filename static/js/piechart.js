AmCharts.makeChart("chartdiv",
    {
        "type": "pie",
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "titleField": "category",
        "valueField": "column-1",
        "allLabels": [],
        "balloon": {},
        "radius": "35%",
        "legend": {
            "enabled": false,
            "color": "#221815",
            "markerType": "square",
            "valueAlign": "left",
            "fontSize": 17,
        },
	"titles": [],
        "colors": [
            "#F6BE57",
            "#EB5858",  
            "#1691B3",
            "#F3ACB3",                           
        ],
        "gradientRatio": [],
        "labelTickAlpha": 0,
        "labelTickColor": "#C82222",
        "outlineThickness": 2,
        "titleField": "country",
        "valueField": "litres",
        "color": "#FFFFFF",
        "fontSize": 12,
        "handDrawn": false,
        "handDrawScatter": 0,
        "handDrawThickness": 0,
        "theme": "chalk",
        "allLabels": [],
        "balloon": {},
        "titles": [],
        "dataProvider": [
            {
                "country": "Tech Brain",
                "litres": 60
            },
            {
                "country": "Creative Design",
                "litres": 20
            },
            {
                "country": "Entrepreneur",
                "litres": 10
            },
            {
                "country": "English",
                "litres": 10
            }
        ]
    }
);
AmCharts.makeChart("chartdiv1",
    {
        "type": "pie",
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "titleField": "category",
        "valueField": "column-1",
        "allLabels": [],
        "balloon": {},
        "radius": "35%",
        "legend": {
            "enabled": true,
            "color": "#221815",
            "markerType": "circle",
            "valueAlign": "center",
            "fontSize": 15,
        },
    "titles": [],
        "colors": [
            "#F6BE57",
            "#EB5858",  
            "#1691B3",
            "#F3ACB3",                           
        ],
        "gradientRatio": [],
        "labelTickAlpha": 0,
        "labelTickColor": "#C82222",
        "outlineThickness": 2,
        "titleField": "country",
        "valueField": "litres",
        "color": "#FFFFFF",
        "fontSize": 12,
        "handDrawn": false,
        "handDrawScatter": 0,
        "handDrawThickness": 0,
        "theme": "chalk",
        "allLabels": [],
        "balloon": {},
        "titles": [],
        "dataProvider": [
            {
                "country": "Moderate",
                "litres": 20
            },
            {
                "country": "Liberal",
                "litres": 20
            },
            {
                "country": "Conservative",
                "litres": 50
            },
            {
                "country": "Strict",
                "litres": 10
            }
        ]
    }
);