AmCharts.makeChart("motivation-chart",
				{
                    "type": "serial",
                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
					"categoryField": "category",
					"columnSpacing": 0,
					"columnWidth": 0.17,
					"autoMarginOffset": -1,
					"marginBottom": 23,
					"marginLeft": 21,
					"marginTop": 24,
					"plotAreaBorderColor": "#715454",
					/*"startDuration": 1,
					"startEffect": "easeOutSine",*/
					"hideBalloonTime": 145,
					"processTimeout": -3,
					"theme": "default",
					"categoryAxis": {
						"autoRotateAngle": 9,
						"widthField": "",
                        "gridAlpha": 0,
                        "axisColor": "#707070",
                        "color": "#009ABE"
                    },
                    "colors": [
                        "#F6BE57",                         
                        "#62CFE3",                                      
                                                 
                    ],
					"trendLines": [],
					"graphs": [
						{
							/*"balloonText": "[[title]] of [[category]]:[[value]]",*/
							"balloonText": "[[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"labelText": "[[value]]",
							/*"title": "graph 1",*/
							"type": "column",
							"valueField": "column-1",
							"colorField": "color",
							"lineColorField": "color"
						},
						{
							// "balloonText": "[[title]] of [[category]]:[[value]]",
							"bullet": "round",
							"id": "AmGraph-2",
							// "labelText": "[[value]]",
							"lineThickness": 4,
							/*"title": "graph 2",*/
                            "valueField": "column-2",
                            "type": "smoothedLine",
                            "bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletBorderColor": "#62CFE3",
                            "bulletColor": "#FFFFFF",
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1","color":"#009ABE",
							"fillColor":"#009ABE"/*,
							"title": "Axis title"*/
						}
					],
					"allLabels": [],
					"balloon": {
						"animationDuration": 1
					},
					"legend": {
						"enabled": false,
						"useGraphSettings": true
					},
					// "titles": [
					// 	{
					// 		"id": "Title-1",
					// 		"size": 15,
					// 		"text": "Chart Title"
					// 	}
					// ],
					"dataProvider": [
						{
							"category": "Tech Brain",
							"column-1": "80",
							"column-2": "20",
							"color":"#F6BE57"
						},
						{
							"category": "Creative Design",
							"column-1": "20",
							"column-2": "",
							"color":"#EB5858"
						},
						{
							"category": "Entrepreneur",
							"column-1": "20",
							"column-2": "2",
							"color":"#1691B3"
						},
						{
							"category": "English",
							"column-1": "50",
							"column-2": null,
							"color":"#F3ACB3"
						},
					]
				}
			);