AmCharts.makeChart("chartdivTB",
				{
					"type": "serial",
					"categoryField": "category",
					/*"startDuration": 1,*/
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 0.7,
							"id": "AmGraph-1",
							"lineAlpha": 0,
							/*"title": "graph 1",*/
							"type": "smoothedLine",
							"valueField": "column-1"
						},
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 0.7,
							"id": "AmGraph-2",
							"lineAlpha": 0,
							/*"title": "graph 2",*/
							"valueField": "column-2"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false
					},
					"titles": [
						/*{
							"id": "Title-1",
							"size": 15,
							"text": "Chart Title"
						}*/
					],
					"dataProvider": [
						{
							"category": "Liberal",
							"column-1": "2",
							"column-2": ""
						},
						{
							"category": "Moderate",
							"column-1": 6,
							"column-2": ""
						},
						{
							"category": "Coservative",
							"column-1": "5",
							"column-2": "3"
						},
						{
							"category": "Strict",
							"column-1": 1,
							"column-2": ""
						}
					]
				}
			);

AmCharts.makeChart("chartdivE",
				{
					"type": "serial",
					"categoryField": "category",
					/*"startDuration": 1,*/
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 0.7,
							"id": "AmGraph-1",
							"lineAlpha": 0,
							/*"title": "graph 1",*/
							"type": "smoothedLine",
							"valueField": "column-1"
						},
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 0.7,
							"id": "AmGraph-2",
							"lineAlpha": 0,
							/*"title": "graph 2",*/
							"valueField": "column-2"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false
					},
					"titles": [
						/*{
							"id": "Title-1",
							"size": 15,
							"text": "Chart Title"
						}*/
					],
					"dataProvider": [
						{
							"category": "Liberal",
							"column-1": "2",
							"column-2": ""
						},
						{
							"category": "Moderate",
							"column-1": 6,
							"column-2": "3"
						},
						{
							"category": "Coservative",
							"column-1": "5",
							"column-2": ""
						},
						{
							"category": "Strict",
							"column-1": 1,
							"column-2": ""
						}
					]
				}
			);

AmCharts.makeChart("chartdivEntr",
				{
					"type": "serial",
					"categoryField": "category",
					/*"startDuration": 1,*/
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 0.7,
							"id": "AmGraph-1",
							"lineAlpha": 0,
							/*"title": "graph 1",*/
							"type": "smoothedLine",
							"valueField": "column-1"
						},
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 0.7,
							"id": "AmGraph-2",
							"lineAlpha": 0,
							/*"title": "graph 2",*/
							"valueField": "column-2"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false
					},
					"titles": [
						/*{
							"id": "Title-1",
							"size": 15,
							"text": "Chart Title"
						}*/
					],
					"dataProvider": [
						{
							"category": "Liberal",
							"column-1": "2",
							"column-2": ""
						},
						{
							"category": "Moderate",
							"column-1": 6,
							"column-2": "5"
						},
						{
							"category": "Coservative",
							"column-1": "5",
							"column-2": ""
						},
						{
							"category": "Strict",
							"column-1": 1,
							"column-2": ""
						}
					]
				}
			);