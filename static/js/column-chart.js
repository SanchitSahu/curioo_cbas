AmCharts.makeChart("columnchartdiv1",
				{
                    "type": "serial",
                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
					"categoryField": "category",
					"columnSpacing": 0,
					"columnWidth": 0.17,
					"autoMarginOffset": -1,
					"marginBottom": 23,
					"marginLeft": 21,
					"marginTop": 24,
					"plotAreaBorderColor": "#715454",

					/*"startDuration": 1,
	"startEffect": "easeOutSine",*/
					"hideBalloonTime": 145,
					"processTimeout": -3,
					"theme": "default",
					"categoryAxis": {
						"autoRotateAngle": 20,
					"autoRotateCount": 1,
					"autoWrap": true,
						"widthField": "",
                        "gridAlpha": 0,
                        "axisColor": "#707070",
						"color": "#009ABE",
						
                    },
                    "colors": [
                        "#F3ACB3", 
                        "#62CFE3",                     
                                                 
                    ],
					"trendLines": [],
					"graphs": [
						{
							/*"balloonText": "[[title]] of [[category]]:[[value]]",*/
							"balloonText": "[[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"labelText": "[[value]]",
							/*"title": "graph 1",*/
							"type": "column",
							"valueField": "column-1",
							
							
						},
						{
							// "balloonText": "[[title]] of [[category]]:[[value]]",
							"bullet": "round",
							"id": "AmGraph-2",
							// "labelText": "[[value]]",
							"lineThickness": 4,
							// "title": "graph 2",
							"valueField": "column-2",
							"type": "smoothedLine",
							"bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletBorderColor": "#62CFE3",
                            "bulletColor": "#FFFFFF",
						}
					],
					"guides": [
						{
							"angle":""
						}
					],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"color": "#009ABE",
							"fillColor": "#009ABE"

							/*"autoRotateCount": 20*//*,
							"title": "Axis title"*/
						}
					],
					"allLabels": [],
					"balloon": {
						"animationDuration": 1
					},
					"legend": {
						"enabled": false,
                        "useGraphSettings": true,
                        "color": "#009ABE",
					},
					// "titles": [
					// 	{
					// 		"id": "Title-1",
					// 		"size": 15,
					// 		"text": "Chart Title"
					// 	}
					// ],
					"dataProvider": [
						{
							"category": "On Task",
							"column-1": "80",
							"column-2": "10"
						},
						{
							"category": "Perseverance",
							"column-1": "40",
							"column-2": "35"
						},
						{
							"category": "Problem Solving",
							"column-1": "20",
							"column-2": "2"
						},
						{
							"category": "Collaboration",
							"column-1": "50",
							"column-2": "40"
						},
						{
							"category": "Self Confidence",
							"column-1": "60",
							"column-2": "5"
						}
					]
				}
			);



AmCharts.makeChart("columnchartdiv2",
				{
                    "type": "serial",
                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
					"categoryField": "category",
					"columnSpacing": 0,
					"columnWidth": 0.17,
					"autoMarginOffset": -1,
					"marginBottom": 23,
					"marginLeft": 21,
					"marginTop": 24,
					"plotAreaBorderColor": "#715454",
					/*"startDuration": 1,
	"startEffect": "easeOutSine",*/
					"hideBalloonTime": 145,
					"processTimeout": -3,
					"theme": "default",
					"categoryAxis": {
						"autoRotateAngle": 20,
		"autoRotateCount": 1,
						"widthField": "",
                        "gridAlpha": 0,
                        "axisColor": "#707070",
						"color": "#009ABE",
						
                    },
                    "colors": [
                        "#1691B3", 
                        "#62CFE3",                     
                                                 
                    ],
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"labelText": "[[value]]",
							/*"title": "graph 1",*/
							"type": "column",
							"valueField": "column-1",
							
							
						},
						{
							// "balloonText": "[[title]] of [[category]]:[[value]]",
							"bullet": "round",
							"id": "AmGraph-2",
							// "labelText": "[[value]]",
							"lineThickness": 4,
							// "title": "graph 2",
							"valueField": "column-2",
							"type": "smoothedLine",
							"bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletBorderColor": "#62CFE3",
                            "bulletColor": "#FFFFFF",
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"color":"#009ABE",
							"fillColor":"#009ABE"

						}
					],
					"allLabels": [],
					"balloon": {
						"animationDuration": 1
					},
					"legend": {
						"enabled": false,
                        "useGraphSettings": true,
                        "color": "#009ABE",
					},
					// "titles": [
					// 	{
					// 		"id": "Title-1",
					// 		"size": 15,
					// 		"text": "Chart Title"
					// 	}
					// ],
					"dataProvider": [
						{
							"category": "On Task",
							"column-1": "80",
							"column-2": "10"
						},
						{
							"category": "Perseverance",
							"column-1": "40",
							"column-2": "35"
						},
						{
							"category": "Problem Solving",
							"column-1": "20",
							"column-2": "2"
						},
						{
							"category": "Collaboration",
							"column-1": "50",
							"column-2": "40"
						},
						{
							"category": "Self Confidence",
							"column-1": "60",
							"column-2": "5"
						}
					]
				}
			);



AmCharts.makeChart("columnchartdiv3",
				{
                    "type": "serial",
                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
					"categoryField": "category",
					"columnSpacing": 0,
					"columnWidth": 0.17,
					"autoMarginOffset": -1,
					"marginBottom": 23,
					"marginLeft": 21,
					"marginTop": 24,
					"plotAreaBorderColor": "#715454",
					/*"startDuration": 1,
	"startEffect": "easeOutSine",*/
					"hideBalloonTime": 145,
					"processTimeout": -3,
					"theme": "default",
					"categoryAxis": {
						"autoRotateAngle": 20,
		"autoRotateCount": 1,
						"widthField": "",
                        "gridAlpha": 0,
                        "axisColor": "#707070",
						"color": "#009ABE",
						
                    },
                    "colors": [
                        "#EB5858", 
                        "#62CFE3",                     
                                                 
                    ],
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"labelText": "[[value]]",
							/*"title": "graph 1",*/
							"type": "column",
							"valueField": "column-1",
							
							
						},
						{
							// "balloonText": "[[title]] of [[category]]:[[value]]",
							"bullet": "round",
							"id": "AmGraph-2",
							// "labelText": "[[value]]",
							"lineThickness": 4,
							// "title": "graph 2",
							"valueField": "column-2",
							"type": "smoothedLine",
							"bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletBorderColor": "#62CFE3",
                            "bulletColor": "#FFFFFF",
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"color":"#009ABE",
							"fillColor":"#009ABE"
						}
					],
					"allLabels": [],
					"balloon": {
						"animationDuration": 1
					},
					"legend": {
						"enabled": false,
                        "useGraphSettings": true,
                        "color": "#009ABE",
					},
					// "titles": [
					// 	{
					// 		"id": "Title-1",
					// 		"size": 15,
					// 		"text": "Chart Title"
					// 	}
					// ],
					"dataProvider": [
						{
							"category": "On Task",
							"column-1": "80",
							"column-2": "10"
						},
						{
							"category": "Perseverance",
							"column-1": "40",
							"column-2": "35"
						},
						{
							"category": "Problem Solving",
							"column-1": "20",
							"column-2": "2"
						},
						{
							"category": "Collaboration",
							"column-1": "50",
							"column-2": "40"
						},
						{
							"category": "Self Confidence",
							"column-1": "60",
							"column-2": "5"
						}
					]
				}
			);

AmCharts.makeChart("columnchartdiv4",
				{
                    "type": "serial",
                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
					"categoryField": "category",
					"columnSpacing": 0,
					"columnWidth": 0.17,
					"autoMarginOffset": -1,
					"marginBottom": 23,
					"marginLeft": 21,
					"marginTop": 24,
					"plotAreaBorderColor": "#715454",
					/*"startDuration": 1,
	"startEffect": "easeOutSine",*/
					"hideBalloonTime": 145,
					"processTimeout": -3,
					"theme": "default",
					"categoryAxis": {
						"autoRotateAngle": 20,
		"autoRotateCount": 1,
						"widthField": "",
                        "gridAlpha": 0,
                        "axisColor": "#707070",
						"color": "#009ABE",
						
                    },
                    "colors": [
                        "#F6BE57", 
                        "#62CFE3",                     
                                                 
                    ],
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"labelText": "[[value]]",
							/*"title": "graph 1",*/
							"type": "column",
							"valueField": "column-1",
							
							
						},
						{
							// "balloonText": "[[title]] of [[category]]:[[value]]",
							"bullet": "round",
							"id": "AmGraph-2",
							// "labelText": "[[value]]",
							"lineThickness": 4,
							// "title": "graph 2",
							"valueField": "column-2",
							"type": "smoothedLine",
							"bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletBorderColor": "#62CFE3",
                            "bulletColor": "#FFFFFF",
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"color":"#009ABE",
							"fillColor":"#009ABE"
						}
					],
					"allLabels": [],
					"balloon": {
						"animationDuration": 1
					},
					"legend": {
						"enabled": false,
                        "useGraphSettings": true,
                        "color": "#009ABE",
					},
					// "titles": [
					// 	{
					// 		"id": "Title-1",
					// 		"size": 15,
					// 		"text": "Chart Title"
					// 	}
					// ],
					"dataProvider": [
						{
							"category": "On Task",
							"column-1": "80",
							"column-2": "10"
						},
						{
							"category": "Perseverance",
							"column-1": "40",
							"column-2": "35"
						},
						{
							"category": "Problem Solving",
							"column-1": "20",
							"column-2": "2"
						},
						{
							"category": "Collaboration",
							"column-1": "50",
							"column-2": "40"
						},
						{
							"category": "Self Confidence",
							"column-1": "60",
							"column-2": "5"
						}
					]
				}
			);