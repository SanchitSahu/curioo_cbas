from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf.urls import url

from curioo_admin.change_to_chinese import Chinese
from curioo_cbas import settings
from curioo_admin import methods
from rest_framework_simplejwt import views as jwt_views
from teacher.forgotPassword import *
from teacher.resetPassword import *
from teacher.login import *
from teacher.index import *
from teacher.students import *
from teacher.dashboard import *
from teacher.index import *
from teacher.setpassword import *
from curioo_admin.teacher import ViewTeacherReport
from curioo_admin.teacher import ChartTeacherData


urlpatterns = [
     path('i18n/', include('django.conf.urls.i18n')),

     path('admin/', include('curioo_admin.urls')),
     path('api/token/', jwt_views.TokenObtainPairView.as_view(),
     name='token_obtain_pair'),
     path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(),
     name='token_refresh'),
     path('curioo_admin/', admin.site.urls),
     path('teacher/', include('teacher.urls')),

     path('changeLanguage', Chinese.as_view(), name='changeLanguage'),

     path('login/', Login.as_view(), name='login/'),
     path('login', Login.as_view(), name='login'),
     path('', Index.as_view(), name='index/'),
     path('dashboard',Dashboard,name='dashboard'),
     path('forgetPassword/', ForgetPassword.as_view(), name='forgetPassword/'),
     path('resetPassword/<str:user_code>/', ResetPassword.as_view(), name='resetPassword/'),
     path('setPassword/<str:user_code>/', SetPassword.as_view(), name='setPassword/'),


     # ADMIN ANNOUNCEMENT URLS #
     path("admin/announcement", methods.announcement, name="announcement"),
     path("admin/announcements", methods.announcement_list, name="announce_list"),
     path("announcement/<int:id>", methods.announcements_delete, name="announce_delete"),

     # ADMIN PRINCIPAL URLS #
     path('admin/principal', methods.principal, name="principal"),
     path('admin/principals', methods.principals_list, name="principals_list"),
     path('admin/principals/<int:id>', methods.principal_details, name="principal_details"),
     path('admin/principal/<int:id>', methods.principal_edit, name="principal_edit"),
     path('principalDelete/<int:id>', methods.principal_delete, name="principal_delete"),
     path('passwd', methods.passwd, name="passwd"),

     # LEARNING CONSULTANT #
     path('admin/learningConsultant', methods.learning_consultant, name="learning_consultant"),
     path('admin/learningConsultants', methods.learning_consultant_list, name="list_learning_consultant"),
     path('admin/learningConsultants/<int:id>', methods.learning_consultant_details,
          name="details_learning_consultant"),
     path('admin/learningConsultant/<int:id>', methods.learning_consultant_edit,
          name="learning_consultant_edit"),
     path('learningConsultantDelete/<int:id>', methods.learning_consultant_delete,
          name="learning_consultant_delete"),
     path('admin/learning_data_table_url', methods.learningConsultantListApi.as_view(),
          name="learning_data_table_url"),


     # STUDIO
     path("admin/studio", methods.studio, name="studio_add"),
     path("admin/studios", methods.studio_list, name="studio_list"),
     path('admin/studio/<int:id>', methods.studio_edit, name="studio_edit"),
     path('studioDelete/<int:id>', methods.studio_delete, name="studio_delete"),
     path('admin/studios/<int:id>', methods.studio_details, name="studio_details"),

     # SUBJECTS
     re_path(r'^admin/subject/(?P<id>[0-9]+)?$', methods.SubjectView.as_view(), name="subjects"),
     re_path(r'^admin/subjects/(?P<id>[0-9]+)?$', methods.SubjectListView.as_view(), name="subject_list"),
     path('subjectDelete/<int:id>', methods.subject_delete, name="subject_delete"),
     re_path('admin/subjectEdit/(?P<id>[0-9]+)?$', methods.SubjectUpdateView.as_view(), name="subject_edit"),

     # LESSONS
     re_path(r"^lessonDetails/(?P<id>[0-9]+)?$", methods.LessonDetailsView.as_view(), name="lesson_details"),
     re_path(r"^admin/lesson/(?P<id>[0-9]+)?$", methods.LessonView.as_view(), name="lesson"),
     # re_path(r"^lessons/(?P<id>[0-9]+)?$", methods.LessonListView.as_view(), name="lessonList"),
     re_path(r"^admin/lessonsEdit/(?P<id>[0-9]+)?$", methods.LessonUpdateView.as_view(), name="lessonEdit"),
     path("lessonDelete/<int:id>", methods.delete_lesson, name="lessonDelete"),
     re_path(r"^admin/addLessonStudent/(?P<id>[0-9]+)?$", methods.AddLessonStudent.as_view(),
               name="add_lesson_student"),
     re_path(r"^admin/getLessonStudent/(?P<id>[0-9]+)?$", methods.GetLessonStudent.as_view(),
               name="get_lesson_student"),
     re_path(r"^deleteLessonStudent/(?P<id>[0-9]+)?$", methods.RemoveLessonStudent.as_view(),
               name="remove_lesson_student"),
     path('lesson_data_table_url', methods.lessonListApi.as_view(),
          name="lesson_data_table_url"),

     # SCHEDULE
     re_path(r"^admin/schedule/(?P<id>[0-9]+)?$", methods.ScheduleView.as_view(), name="schedule"),
     re_path(r"^admin/scheduleEdit/(?P<id>[0-9]+)?$", methods.ScheduleEditView.as_view(), name="scheduleDetails"),
     re_path(r"^admin/schedules/(?P<id>[0-9]+)?$", methods.ScheduleListView.as_view(), name="scheduleEdit"),
     path("scheduleDelete/<int:id>", methods.schedule_delete),
     url(r'^api/chart/data/$', ChartData.as_view()),
     url(r'^api/chart/data/admin/$', AdminChartData.as_view()),
     url(r'^api/chart/data/analytics/$', AnalyticschartData.as_view()),
     # url(r'^api/chart/data/teacher/$', ViewTeacherReport.as_view()),
     # path('teacher/charts/', ChartTeacherData,'teacher/charts/'),
     path('admin/getTeachers', methods.get_teachers, name='getTeachers'),
     path('admin/getTeachers-api', methods.get_teachers_lesson, name='getTeachers-api'),
     path('admin/getLesson-api', methods.get_lesson, name='getLesson-api'),

     re_path(r"^scheduleView/(?P<id>[0-9]+)?$", methods.ScheduleView.as_view(),name="scheduleView"),
     re_path(r"^scheduleLessonView/(?P<id>[0-9]+)?$", methods.ScheduleLessonEditView.as_view(),name="schedullessonedit"),
     # re_path(r"^admin/scheduleCalander/", methods.ScheduleCalanderView.as_view(),name="scheduleCalander"),

     re_path(r"^admin/scheduleCalander/(?P<id>[0-9]+)?$", methods.ScheduleCalanderView.as_view(),name="scheduleCalander"),
     re_path(r"^admin/scheduleFilter/(?P<id>[0-9]+)?$", methods.ScheduleListViewCalnderData.as_view(),name="scheduleCalanderfilter"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
