"""
A module for Teacher Create, Update, List, View and Delete operations for admin panel.
"""
import copy
import json
from datetime import datetime
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.cache import cache_control
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from curioo_admin.serializers import TeacherSerializer, StudioSerializer, EditTeacherSerializer, CommentsSerializer
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from curioo_admin.models import Teacher, Studio, Level, Subject, Major, ScheduleClass, Lesson, Comments, Student, \
    Principal, Admin, AdminNotification, BehaviourData, LearningConsultant, StudentReportData
from curioo_admin.validations import calculate_age
from teacher.getUser import gbl
from teacher.status_message import error_message
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core import mail
import random
import string
from curioo_cbas import settings
from teacher.serializers import FilterDateGraphSerializer
from rest_framework.decorators import api_view, permission_classes

from django.template.loader import render_to_string



def get_studios():
    """ To get only active studios list for filtering in Teacher List page in Admin Panel. """
    try:
        studios = Studio.objects.filter(is_active=True, status='Active')
        serializer = StudioSerializer(studios, many=True)
        return json.loads(json.dumps(serializer.data))
    except Exception:
        return []  # If no Studio are there OR if something was wrong, return empty list


def get_teacher_details():
    """ To get Teacher related data for Add and Edit Method here. """
    try:
        levels, subjects = Level.objects.filter(is_active=True), Subject.objects.filter(is_active=True)
        majors = Major.objects.filter(is_active=True)
        levels = [{'level_id': level.id, 'level_name': level.level} for level in levels]
        subjects = [{'subject_id': subject.id, 'subject_name': subject.subject_name} for subject in subjects]
        majors = [{'major_id': major.id, 'major_name': major.major} for major in majors]
        studios = get_studios()
        return levels, subjects, studios, majors
    except Exception:
        return [], [], [], []


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def teachers_list(request):
    """ To get only active Teacher List in Admin Panel. """
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    try:
        studios = get_studios()
        teachers = Teacher.objects.filter(is_active=True)
        print("TEacher ---",teachers)
        serializer = TeacherSerializer(teachers, many=True)
        return render(request, template_name="curioo_admin/teachers.html",
                      context={"studios": studios, "teachers": json.loads(json.dumps(serializer.data)),
                               'teacher_class': True, "user": user})
    except Exception as e:
        print(e)
        return redirect(to='admin/dashboard')


def teacher_code():
    try:
        s = Teacher.objects.latest('id')
        s_id = s.id
        s_id += 1
    except Exception as e:
        s_id = 1
    return 'Tr' + str(s_id)


def employee_code(studio_id, studio_code):
    try:

        teachers = Teacher.objects.filter(studio_id=studio_id).count()
        lc = LearningConsultant.objects.filter(studio_id=studio_id).count()
        principals = Principal.objects.filter(studio_id=studio_id).count()

        count = teachers + lc + principals
        count += 1
    except Exception as e:
        count = 1
    code = studio_code + 'E' + str(count)
    return code.upper()


@csrf_exempt
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def add_teacher(request):
    """ To Add teacher from Admin Panel. """
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    if request.method == "GET":
        levels, subjects, studios, majors = get_teacher_details()
        length = len(subjects)
        return render(request, template_name="curioo_admin/addNewTeacher.html",
                      context={'levels': levels, 'subjects': subjects, 'studios': studios, 'majors': majors,
                               'teacher_class': True, "user": user, "len": length})
    if request.method == "POST":
        try:
            print("inside add teacher post")
            data = request.POST
            print(data)

            if Teacher.objects.filter(email=data['email']).exists() or \
                    Principal.objects.filter(email=data['email']).exists() or \
                    LearningConsultant.objects.filter(email=data['email']).exists() or \
                    Admin.objects.filter(email=data['email']).exists():
                msg = "User with this email address already exists."
                return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)

            if Teacher.objects.filter(position_id=data['position_id']).exists():
                msg = "Teacher with this position id already exists."
                return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
            studio_obj = Studio.objects.get(id=int(data['studio_id']))
            teacher = Teacher.objects.create(teacher_code=employee_code(data['studio_id'], studio_obj.studio_code), full_name=data['full_name'],
                                             dob=data['dob'],
                                             gender=data['gender'], nationality=data['nationality'],
                                             doj=data['doj'], position=data['position'],
                                             position_id=data['position_id'],
                                             studio_id=data['studio_id'], education_level=data['education_level'],
                                             education_major=data['education_major'], email=data['email'],
                                             phone=int(data['phone']), emergency_phone=int(data['emergency_phone']),
                                             emergency_contact_name=data['emergency_contact_name'])
            subjects = data['subjects']
            subjects = subjects.split(",")
            subjects.pop()
            subjects_list = [Subject.objects.get(id=int(subject_id)) for subject_id in subjects if subject_id != '']
            teacher.subject.set(subjects_list)

            image = request.FILES.get('image')
            print(image)
            if image != '' and image is not None:
                teacher.image = image
                teacher.save()

            try:
                user_code = "".join(random.choices(string.ascii_uppercase + string.digits, k=20))+"_u_t0"+str(teacher.id)
                current_site = get_current_site(request)
                domain = current_site.domain

                url = f"http://{domain}/setPassword/"+user_code
                print('url', url)

                print("password before mail :", teacher.password)
                try:
                    subject = 'Set Password Link for CURIOO'
                    html_message = render_to_string('teacher/mail.html', {'first_name': teacher.full_name, 'url': url, 'role_creation': True,
                                                                        'password':teacher.password, 'appointed_role': 'Teacher', 'studio': teacher.studio.name})
                    plain_message = strip_tags(html_message)
                    from_email = settings.DEFAULT_FROM_EMAIL
                    to = teacher.email
                    mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)
                    print("mail--",mail)
                except Exception as error:
                    print("error in forgot mail-----",error)
                    return Response(data=error_message(511,"error","Please check your network connection.",{}), status=status.HTTP_511_NETWORK_AUTHENTICATION_REQUIRED)
            except Exception as e:
                print("exception : ", e)
                teacher.delete()
                return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content="Something went wrong. Please try again.")

            principals = Principal.objects.filter(studio=data['studio_id'], is_active=True)
            studio = Studio.objects.get(id=data['studio_id'])
            admins = Admin.objects.all()

            if principals is not None:
                for principal in principals:
                    notification_msg = 'A new teacher was added in ' + studio.name + ' Studio.'
                    AdminNotification.objects.create(notification=notification_msg, principal=principal)

            if admins is not None:
                for admin in admins:
                    notification_msg = 'A new teacher was added in ' + studio.name + ' Studio.'
                    AdminNotification.objects.create(notification=notification_msg, admin=admin)

            msg = "A teacher is created successfully."
            return HttpResponse(status=status.HTTP_200_OK, content=msg)
        except Exception as e:
            print("exception in add teacher: ", e)
            msg = "Enter proper details."
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def view_teacher(request, teacher_id):
    """ To View Teacher in Admin Panel. """
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    if request.method == "GET":
        try:
            teacher = Teacher.objects.get(id=teacher_id)
            subjects = [subject.subject_name for subject in teacher.subject.all()]
            schedule_classes = ScheduleClass.objects.filter(teacher=teacher)
            comments = Comments.objects.filter(commented_on_id=teacher_id, commented_on_role='Teacher',is_active=True)
            comment = CommentsSerializer(comments, many=True)
            ret_data = comment.data
            print('ret_data : ', json.loads(json.dumps(ret_data)))
            return render(request, template_name="curioo_admin/teacherDetail.html",
                          context={'teacher': teacher, 'subjects': subjects, 'schedule_classes': schedule_classes,
                                   'teacher_class': True, 'comments': json.loads(json.dumps(ret_data))})

        except Exception:
            msg = "Invalid Request."  # No teacher can be fetched for specific teacher_id
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)


@csrf_exempt
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def edit_teacher(request, teacher_id):
    """ To edit specified teacher in Admin Panel. """
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    if request.method == "GET":
        try:
            print('inside edit teacher get')
            levels, subjects, studios, majors = get_teacher_details()
            teacher = Teacher.objects.get(id=teacher_id)
            current_subjects = [subject.subject_name for subject in teacher.subject.all()]
            length = len(subjects)
            return render(request, template_name="curioo_admin/editTeacherDetail.html",
                          context={'levels': levels, 'subjects': subjects, 'studios': studios, 'teacher': teacher,
                                   'current_subjects': current_subjects, 'majors': majors, "len": length})
        except Exception as e:
            print('exception in get edit teacher : ', e)
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
    if request.method == "POST":
        try:
            print('inside edit teacher post')
            data = copy.deepcopy(request.POST)
            teacher = Teacher.objects.get(id=teacher_id)
            print(request.POST)

            if teacher.email != data['email']:
                if Teacher.objects.filter(email=data['email']).exists():
                    msg = "Teacher with this email address already exists."
                    return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)

            if teacher.position_id != data['position_id']:
                if Teacher.objects.filter(position_id=data['position_id']).exists():
                    msg = "Teacher with this position ID already exists."
                    return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)

            subjects = data['subjects']
            subjects = subjects.split(",")
            subjects.pop()
            subjects_list = [Subject.objects.get(id=int(subject_id)) for subject_id in subjects if subject_id != '']
            teacher.subject.set(subjects_list)

            serializer = EditTeacherSerializer(instance=teacher, data=data, partial=True)
            if serializer.is_valid(raise_exception=True):
                teacher.studio_id = data['studio_id']
                teacher.save()
                serializer.save()

            image = request.FILES.get('image')
            if image != '' and image is not None:
                teacher.image = image
                teacher.save()
            return HttpResponse(status=200)
        except Exception as e:
            print('exception in post edit teacher : ', e)
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content="Invalid Edit Request")


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def delete_teacher(request, teacher_id):
    """ To delete teacher in Admin Panel. """
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    try:
        print("hey")
        Teacher.objects.filter(id=teacher_id).update(is_active=False)
        return HttpResponse(status=status.HTTP_200_OK)
    except Exception:
        msg = "Invalid Request."  # No teacher can be fetched for specific teacher_id
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)


class TeacherListApi(APIView):
    permission_classes = (AllowAny,)

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, request):
        try:
            teacher_status = request.GET['status']
            teacher_studio = request.GET['studio']

            if teacher_status == "Active":
                if teacher_studio == "All Studio":
                    teachers = Teacher.objects.filter(is_active=True)
                else:
                    teachers = Teacher.objects.filter(studio__studio_code=teacher_studio, is_active=True)
            else:
                if teacher_studio == "All Studio":
                    teachers = Teacher.objects.filter(is_active=False)
                else:
                    teachers = Teacher.objects.filter(studio__studio_code=teacher_studio, is_active=False)

            data = [{"id": teacher.id, "code": teacher.teacher_code, "name": teacher.full_name,
                     "center": teacher.studio.studio_code + ', ' + teacher.studio.name + ', ' + teacher.studio.city} for
                    teacher in teachers]
            return Response(data={'data': data}, status=status.HTTP_200_OK)

        except Exception as e:
            return Response(data=error_message(400, "error", str(e), {}), status=status.HTTP_400_BAD_REQUEST)


def ViewTeacherReport(request):
        try:
            print("in ViewTeacherReport")

            try:
                name, user_type, user = gbl(request, request.session['token'])
                print('user_type : ', user_type)
            except Exception as e:
                print('inside exception : ', e)
                return redirect('login/')
            if user_type == 'principal':
                studios = Studio.objects.filter(is_active=True, status='Active', id=user.studio.id)
                allStudBehaviourData = BehaviourData.objects.all()
                studentCount = Student.objects.filter(is_active=True, expiry_date__gt=datetime.now(), studio_id=user.studio.id).count()
                allSubject = Subject.objects.filter(is_active=True)
                teacher = Teacher.objects.filter(studio_id=user.studio.id, is_active=True)
            else:
                studios = get_studios()
                allStudBehaviourData = BehaviourData.objects.all()
                studentCount = Student.objects.filter(is_active=True, expiry_date__gt=datetime.now()).count()
                allSubject = Subject.objects.filter(is_active=True)
                teacher = Teacher.objects.filter(is_active=True)

            serializer = TeacherSerializer(teacher, many=True)

            totalValue = 0

            for data in allStudBehaviourData:
                print("data----",data)
                print('data value ----',data.value)
                totalValue = totalValue + int(data.value)
            
            print("total value ----",totalValue)

            if studentCount != 0:
                avg = totalValue/studentCount
            else:
                avg = ' '
            print("avg ----",avg)

            return render(request=request, template_name='curioo_admin/viewReport-tech.html', context={'studios':studios})
        except Exception as e:
            print(e)
            return Response(data=error_message(400, "error", str(e), {}), status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([ AllowAny, ])
def ChartTeacherData(request):

    if request.method == "POST":
        print("IN POSTTTTTT---------------------------------------")

        studioId = request.POST['studioId']
        status = request.POST['teacher_status']
        # teacher_obj = Teacher.objects.filter(studio = studioId)
        # serializer_teacher = TeacherSerializer(teacher_obj, many=True)
        if "date_range" in request.POST:
            print("---***Date Range", request.POST["date_range"])
            dataRange = request.POST["date_range"]
            schedule_obj = ScheduleClass.objects.filter(studio = int(studioId))
            dateserializer = FilterDateGraphSerializer(schedule_obj, many=True, context = request.POST)
            print("---DATEEEEE1234567788-----" , schedule_obj)
            print("---DATEEEEE-----" , dateserializer.data)
            schedule_obj = dateserializer.data
            daterange = True
        else:
            daterange = False
            schedule_obj = ScheduleClass.objects.filter(studio = int(studioId))

        # print("STATUS*******************",status)
        status_check = True
        active_status = True
        if status == "active":
            active_status = True
        elif status == "inactive":
            active_status = False
        else:
            status_check = False
        # print("STATUS*******************",active_status)
        
        print("---schedule------", schedule_obj)
        teacherLesson = {}
        if daterange == False:
            print("---in if-----")
            for obj in schedule_obj:
                if Teacher.objects.get(id = obj.teacher.id).is_active == active_status and status_check == True:
                    teacherLesson[obj.teacher.id] = []
                elif status_check == False:
                    teacherLesson[obj.teacher.id] = []
            for obj in schedule_obj:
                if Teacher.objects.get(id = obj.teacher.id).is_active == active_status and status_check == True:
                    # print("objjjjjj teacher" , obj.teacher)
                    # print("objjjjj lesson" , obj.lesson)
                    teacherLesson[obj.teacher.id].append(obj.lesson.lesson_code)
                elif status_check == False:
                    # print("objjjjjj teacher" , obj.teacher)
                    # print("objjjjj lesson" , obj.lesson)
                    teacherLesson[obj.teacher.id].append(obj.lesson.lesson_code)
            # print("---lessonteacher", teacherLesson)
        else:
            print("-----else------", dateserializer.data)
            for obj in dateserializer.data:
                print("Teacher id -----------", obj)
                if obj['lesson_code'] != None:
                    if Teacher.objects.get(id = obj['lesson_code']['teacher']).is_active == active_status and status_check == True:
                        teacherLesson[obj['lesson_code']["teacher"]] = []
                    elif status_check == False:
                        teacherLesson[obj['lesson_code']["teacher"]] = []
            for obj in dateserializer.data:
                if obj['lesson_code'] != None:
                    if Teacher.objects.get(id = obj['lesson_code']["teacher"]).is_active == active_status and status_check == True:
                        # print("objjjjjj teacher" , obj.teacher)
                        # print("objjjjj lesson" , obj.lesson)
                        teacherLesson[obj['lesson_code']["teacher"]].append(obj['lesson_code']["lesson_code"])
                    elif status_check == False:
                        # print("objjjjjj teacher" , obj.teacher)
                        # print("objjjjj lesson" , obj.lesson)
                        teacherLesson[obj['lesson_code']["teacher"]].append(obj['lesson_code']["lesson_code"])
            # print("---lessonteacher", teacherLesson)

        # if 'teacher_id' in request.data:
        #     teacher_id = request.POST['teacher_id']
        #     tobj = Teacher.objects.get(id=teacher_id)
        #     schedule_obj = ScheduleClass.objects.filter(teacher = tobj )
        #     teacherLesson = {}
        #     # for obj in schedule_obj:
        #     #     teacherLesson[obj.teacher.id] = []
        #     teacherLesson[teacher_id] = []
        #     for obj in schedule_obj:
        #         teacherLesson[teacher_id].append(obj.lesson.lesson_code)
        # else:
        #     stobj = Studio.objects.get(studio_code = studioId)
        #     schedule_obj = ScheduleClass.objects.filter(studio = stobj )
        #     teacherLesson = {}
        #     for obj in schedule_obj:
        #         teacherLesson[obj.teacher.id] = []
        #     for obj in schedule_obj:
        #         teacherLesson[obj.teacher.id].append(obj.lesson.lesson_code)

        
        feedback = {}
        feedback["Strict"] = 0
        feedback["Conservative"] = 0
        feedback["Moderate"] = 0
        feedback["Liberal"] = 0
        sum_behaviour = {}
        max = 0
        count_strict = 0
        count_moderate = 0
        count_conservative = 0
        count_liberal = 0

        for key, values in teacherLesson.items():
            data_behaviour = []
            data_wellbeing = []
            data_overall = []
            count = len(values)
            for count in range(0,count):
                lesson_obj = Lesson.objects.get(lesson_code = teacherLesson[key][count])
                filter_stud_data = StudentReportData.objects.filter(lesson = lesson_obj)
                for i in filter_stud_data:
                    data_behaviour.append(i.behaviour_data.value)
                    data_wellbeing.append(i.well_being.value)
                    data_overall.append(i.over_all_rating.value)
                    max = max + 4
            sum_behaviour[key] = sum(data_behaviour)/len(data_behaviour)
            if sum_behaviour[key] > 0 and sum_behaviour[key] <= 1:
                count_strict = count_strict + 1
                feedback["Strict"] = count_strict
            if sum_behaviour[key] > 1 and sum_behaviour[key] <= 2:
                count_conservative = count_conservative + 1
                feedback["Conservative"] = count_conservative
            if sum_behaviour[key] > 2 and sum_behaviour[key] <= 3:
                count_moderate = count_moderate + 1
                feedback["Moderate"] = count_moderate
            if sum_behaviour[key] > 3 and sum_behaviour[key] <= 4:
                count_liberal = count_liberal + 1
                feedback["Liberal"] = count_liberal
            
        teacherDict = {}

        if 'teacher_id' not in request.data:
            studioWiseTeacher = Teacher.objects.filter(studio = int(studioId), is_active=True)


            for t in studioWiseTeacher:
                teacherDict[t.id] = t.full_name
        
        data = {
            "sum_behaviour" :sum_behaviour,
            "feedback": feedback,
            # 'teacher_detail':serializer_teacher.data,
        }
        return Response(data)

# 1{
#     english:{
#         moderate:0,
#         strict:0
#     },
#     creative:{
#         mode
#     }
# }

@api_view(['POST'])
@permission_classes([ AllowAny, ])
def SubwiseFeedback(request):
    if request.method == 'POST':
        
        # count_Strict = 0
        # count_Conservative = 0
        # count_Moderate = 0
        # count_Liberal = 0
        studioID = request.POST['studioId']
        teacher_status = request.POST['teacher_status']
        
        print("******* active/inactive---------", teacher_status)
        print("//////////////////studio id-------",studioID )
        if teacher_status == "active":
            active_status = True
        elif teacher_status == "inactive":
            active_status = False
        # # print("tyyyyyyyppppeeeeeee",typee)
       

        if teacher_status == "Status":
            teacher_obj = Teacher.objects.filter(studio = int(studioID))
            teacher_options = Teacher.objects.filter(studio = int(studioID))
            serailizer_teacherOptions = TeacherSerializer(teacher_options, many=True)
        else:
            teacher_obj = Teacher.objects.filter(is_active = active_status , studio = int(studioID))
            teacher_options = Teacher.objects.filter(studio = int(studioID), is_active = active_status)
            serailizer_teacherOptions = TeacherSerializer(teacher_options, many=True)

        # subject_total = {}
        # subject_behaviour = []
        # # average_val = float(0)
        avg = {}

        # if 'teacher_id' in request.data:
        #     teachObj = Teacher.objects.get(id=int(request.POST['teacher_id']))
        # else:
        #     teacher_obj = Teacher.objects.all()
        
        if 'teacher_id' in request.data:
            typee = {"Strict":0, "Conservative":0, "Moderate":0, "Liberal":0}
            class_obj = ScheduleClass.objects.filter(teacher = teachObj.id)
            daterange = False
            if "date_range" in request.POST:
                daterange = True
                class_obj = FilterDateGraphSerializer(class_obj, many=True, context = request.POST)
            # print("class_obj------", class_obj)
            subject_behaviour = {}
            if daterange == False:
                print("-------in if--------")
                for lesson in class_obj:
                    print("subject---", lesson.lesson.subject.subject_name)
                    report = StudentReportData.objects.filter(lesson = lesson.lesson)
                    for value in report:
                        if value.subject.subject_name in subject_behaviour:
                            subject_behaviour[value.subject.subject_name].append(value.behaviour_data.value) 
                        else:
                            subject_behaviour[value.subject.subject_name] = []
                            subject_behaviour[value.subject.subject_name].append(value.behaviour_data.value)
                    print("value-----" , value.behaviour_data.value)
            else:
                print("-------in else--------")
                for lesson in class_obj.data:
                    if lesson["lesson_code"] != None:
                        print("subject---", lesson["lesson_code"]["id"])
                        report = StudentReportData.objects.filter(lesson = lesson["lesson_code"]["id"])
                        for value in report:
                            if value.subject.subject_name in subject_behaviour:
                                subject_behaviour[value.subject.subject_name].append(value.behaviour_data.value) 
                            else:
                                subject_behaviour[value.subject.subject_name] = []
                                subject_behaviour[value.subject.subject_name].append(value.behaviour_data.value)
                        print("value-----" , value.behaviour_data.value)                      
            print("Subject_behaviour-------------------", subject_behaviour)
            for sub in subject_behaviour:
                if sub in avg.keys():
                    average = sum(subject_behaviour[sub])/len(subject_behaviour[sub])
                    if average > 0 and average <= 1:
                        avg[sub]["Strict"] = avg[sub]["Strict"] + 1
                    if average > 1 and average <= 2:
                        avg[sub]["Conservative"] = avg[sub]["Conservative"] + 1
                    if average > 2 and average <= 3:
                        avg[sub]["Moderate"] = avg[sub]["Moderate"] + 1
                    if average > 3 and average <= 4:
                        avg[sub]["Liberal"] = avg[sub]["Liberal"] + 1
                else:
                    avg[sub] = copy.deepcopy(typee)
                    average = sum(subject_behaviour[sub])/len(subject_behaviour[sub])
                    if average > 0 and average <= 1:
                        avg[sub]["Strict"] = avg[sub]["Strict"] + 1
                    if average > 1 and average <= 2:
                        avg[sub]["Conservative"] = avg[sub]["Conservative"] + 1
                    if average > 2 and average <= 3:
                        avg[sub]["Moderate"] = avg[sub]["Moderate"] + 1
                    if average > 3 and average <= 4:
                        avg[sub]["Liberal"] = avg[sub]["Liberal"] + 1
        else:  
            for teachObj in teacher_obj:
                typee = {"Strict":0, "Conservative":0, "Moderate":0, "Liberal":0}
                class_obj = ScheduleClass.objects.filter(teacher = teachObj.id)
                daterange = False
                if "date_range" in request.POST:
                    daterange = True
                    class_obj = FilterDateGraphSerializer(class_obj, many=True, context = request.POST)
                # print("class_obj------", class_obj)
                subject_behaviour = {}
                if daterange == False:
                    print("-------in if--------")
                    for lesson in class_obj:
                        print("subject---", lesson.lesson.subject.subject_name)
                        report = StudentReportData.objects.filter(lesson = lesson.lesson)
                        for value in report:
                            if value.subject.subject_name in subject_behaviour:
                                subject_behaviour[value.subject.subject_name].append(value.behaviour_data.value) 
                            else:
                                subject_behaviour[value.subject.subject_name] = []
                                subject_behaviour[value.subject.subject_name].append(value.behaviour_data.value)
                        print("value-----" , value.behaviour_data.value)
                else:
                    print("-------in else--------")
                    for lesson in class_obj.data:
                        if lesson["lesson_code"] != None:
                            print("subject---", lesson["lesson_code"]["id"])
                            report = StudentReportData.objects.filter(lesson = lesson["lesson_code"]["id"])
                            for value in report:
                                if value.subject.subject_name in subject_behaviour:
                                    subject_behaviour[value.subject.subject_name].append(value.behaviour_data.value) 
                                else:
                                    subject_behaviour[value.subject.subject_name] = []
                                    subject_behaviour[value.subject.subject_name].append(value.behaviour_data.value)
                            print("value-----" , value.behaviour_data.value)                      
                print("Subject_behaviour-------------------", subject_behaviour)
                for sub in subject_behaviour:
                    if sub in avg.keys():
                        print("-------in if----------")
                        average = sum(subject_behaviour[sub])/len(subject_behaviour[sub])
                        if average > 0 and average <= 1:
                            avg[sub]["Strict"] = avg[sub]["Strict"] + 1
                        if average > 1 and average <= 2:
                            avg[sub]["Conservative"] = avg[sub]["Conservative"] + 1
                        if average > 2 and average <= 3:
                            avg[sub]["Moderate"] = avg[sub]["Moderate"] + 1
                        if average > 3 and average <= 4:
                            avg[sub]["Liberal"] = avg[sub]["Liberal"] + 1
                    else:
                        print("-------else----------")
                        avg[sub] = copy.deepcopy(typee)
                        average = sum(subject_behaviour[sub])/len(subject_behaviour[sub])
                        if average > 0 and average <= 1:
                            avg[sub]["Strict"] = avg[sub]["Strict"] + 1
                        if average > 1 and average <= 2:
                            avg[sub]["Conservative"] = avg[sub]["Conservative"] + 1
                        if average > 2 and average <= 3:
                            avg[sub]["Moderate"] = avg[sub]["Moderate"] + 1
                        if average > 3 and average <= 4:
                            avg[sub]["Liberal"] = avg[sub]["Liberal"] + 1
                print("avgggg", avg)

        data = {
            'avg' : avg,
            'teacher_option': serailizer_teacherOptions.data,
        }
        return Response(data)

@api_view(['POST'])
@permission_classes([ AllowAny, ])
def SubjectTeacherChart(request):
    if request.method == "POST":
        if "teacherID" in request.POST:
            id = request.POST['teacherID']
            print("=====idddd======", id)
        subject_behaviour = []
        subject_total = {}

        teacher_obj = ScheduleClass.objects.filter(teacher = id)
        print("teacher obj-----", teacher_obj)

        for lesson in teacher_obj:
            report = StudentReportData.objects.filter(lesson = lesson.lesson , subject = lesson.lesson.subject)

            teacher_obj = ScheduleClass.objects.filter(teacher = int(id))
            print("teacher obj-----", teacher_obj)

            for lesson in teacher_obj:
                print("lesson----", lesson.lesson)
                print("subject---", lesson.lesson.subject)
                report = StudentReportData.objects.filter(lesson = lesson.lesson , subject = lesson.lesson.subject)
                print("report----" , report)
                subject_behaviour = []
                subject_total[lesson.lesson.subject.subject_name] = {}

                for value in report:
                    subject_behaviour.append(value.behaviour_data.value) 
                    print("value-----" , value.behaviour_data.value)
                    
                print("subject_behaviour------", sum(subject_behaviour))
                subject_total[lesson.lesson.subject.subject_name]["Liberal"] = 0
                subject_total[lesson.lesson.subject.subject_name]["Moderate"] = 0
                subject_total[lesson.lesson.subject.subject_name]["Conservative"] = 0
                subject_total[lesson.lesson.subject.subject_name]["Strict"] = 0
                avg = sum(subject_behaviour)/len(subject_behaviour)
                if avg > 0 and avg <= 1:
                    typee = "Strict"
                if avg > 1 and avg <= 2:
                    typee = "Conservative"
                if avg > 2 and avg <= 3:
                    typee = "Moderate"
                if avg > 3 and avg <= 4:
                    typee = "Liberal"
                

                subject_total[lesson.lesson.subject.subject_name][typee] = avg
            print("subject_total-------", subject_total)
        data = {
            'subject_total': subject_total,
        }
        return Response(data)

@api_view(['GET'])
@permission_classes([ AllowAny, ])
def TeacherFeedback(request, tid):
    print("In teacher feedback page")
    if request.method == "GET":
        tdata = Teacher.objects.get(id=int(tid))
        return render(request, 'curioo_admin/teacherFeedbackReport.html',context={'teacher':tdata})

@api_view(['POST'])
@permission_classes([ AllowAny, ])
def GetTeacherStudioWise(request):

    print("get teacher on change studio ")
    
    if request.method == "POST":
        studioID = request.POST['studioId']
        teacher_status = request.POST['teacher_status']
        print("teacher_status---",teacher_status)
        if teacher_status == 'active':
            teacher_status = True
        else:
            teacher_status = False
        sdata = Studio.objects.get(studio_code=studioID)
        teachers = {}
        print("sdata---",sdata, teacher_status, type(teacher_status))
        if Teacher.objects.filter(studio=sdata, is_active=teacher_status):
            print("if 1")
            for t in Teacher.objects.filter(studio=sdata, is_active=teacher_status):
                teachers[t.id] = t.full_name
            data = {
                'teachers' : teachers,
            }
            return Response(data)
        else:
            print("else ")
            data = {
            }
            return Response(data)

@api_view(['POST'])
@permission_classes([ AllowAny, ])
def GetPieData(request):
    if request.method == 'POST':
        daterange = False
        allteacherFeedback = False
        oneTeacherFeedback = False
        active_status = False
        teacherLesson = {}

        print("IN GET PIE DATA---------------------------------------")
        print("data---",request.POST)
        if 'studioId' in request.POST and 'teacher_status' in request.POST:
            print("come in  1")
            studioId = request.POST['studioId']
            print("studioId--",studioId)
            status = request.POST['teacher_status']
            print("status--",status)
            if status == 'active':
                active_status = True
                print("active_status----",active_status)
            allteacherFeedback = True
            print("allteacherFeedback---",allteacherFeedback)
            if "date_range" in request.POST:
                print("come in 2")
                dataRange = request.POST["date_range"]
                daterange = True

        elif 'teacherId' in request.POST and 'teacher_status' in request.POST:
            print("come in 3")
            teacherId = request.POST['teacherId']
            status = request.POST['teacher_status']
            oneTeacherFeedback = True
            if "date_range" in request.POST:
                print("come in 4")
                dataRange = request.POST["date_range"]
                daterange = True

        elif 'teacherId' in request.POST:
            print("come in 5")
            teacherId = request.POST['teacherId']
            oneTeacherFeedback = True
            if "date_range" in request.POST:
                print("come in 4")
                dataRange = request.POST["date_range"]
                daterange = True

        else:
            print("come in 6")
            studioId = request.GET['studioId']
            status = request.GET['teacher_status']


        
        print("allteacherFeedback----",allteacherFeedback)
        if allteacherFeedback == True:
            print("in allteacherFeedback True")
            stobj = Studio.objects.get(id=int(studioId))
            print("stobj",stobj)
            print("come in 7",daterange)

            if daterange == True:
                schedule_obj = ScheduleClass.objects.filter(studio = stobj)
                print("schedule_obj--",schedule_obj)
                dateserializer = FilterDateGraphSerializer(schedule_obj, many=True, context = request.POST)
                print("---DATEEEEE-----" , dateserializer.data)
                schedule_obj = dateserializer.data
                print("schedule_obj---after dateserializer ",schedule_obj)

                for obj in schedule_obj:
                    if obj['lesson_code'] != None:
                        print("schedule_obj--teacher id-",obj['id'], type(obj['id']), "active_status---",active_status)
                        print("jalpa 1--",Teacher.objects.get(id = ScheduleClass.objects.get(id=obj['id']).teacher.id).is_active)
                        if Teacher.objects.get(id = ScheduleClass.objects.get(id=obj['id']).teacher.id).is_active == active_status:
                            print("jalpa")
                            teacherLesson[ScheduleClass.objects.get(id=obj['id']).teacher.id] = []
                print("teacherLesson 1",teacherLesson)

                for obj in schedule_obj:
                    if obj['lesson_code'] != None:
                        if Teacher.objects.get(id = ScheduleClass.objects.get(id=obj['id']).teacher.id).is_active == active_status:
                            teacherLesson[ScheduleClass.objects.get(id=obj['id']).teacher.id].append(obj['lesson_code'])
                print("teacherLesson 2----", teacherLesson)

            elif daterange == False:
                print("date range ",daterange)
                schedule_obj = ScheduleClass.objects.filter(studio = stobj)
                print("---schedule------", schedule_obj)

                for obj in schedule_obj:
                    if Teacher.objects.get(id = obj.teacher.id).is_active == active_status:
                        teacherLesson[obj.teacher.id] = []
                print("teacherLesson 1",teacherLesson)

                for obj in schedule_obj:
                    if Teacher.objects.get(id = obj.teacher.id).is_active == active_status:
                        teacherLesson[obj.teacher.id].append(obj.lesson.lesson_code)
                print("teacherLesson 2----", teacherLesson)
       
        else:
            print("oneTeacherFeedback --77777777")
            if daterange == True:
                print("tttttttttttttttt")
                schedule_obj = ScheduleClass.objects.filter(teacher = Teacher.objects.get(id=int(teacherId)))
                print("schedule_obj--0909009009",schedule_obj)
                dateserializer = FilterDateGraphSerializer(schedule_obj, many=True, context = request.POST)
                print("---DATEEEEE-----" , dateserializer.data)
                schedule_obj = dateserializer.data
                print("schedule_obj---after dateserializer ",schedule_obj)

                for obj in schedule_obj:
                    if obj['lesson_code'] != None:
                        print("schedule_obj--teacher id-",obj['id'], type(obj['id']), "active_status---",active_status)
                        print("jalpa 1--",Teacher.objects.get(id = ScheduleClass.objects.get(id=obj['id']).teacher.id).is_active)
                        if Teacher.objects.get(id = ScheduleClass.objects.get(id=obj['id']).teacher.id):
                            print("jalpa")
                            teacherLesson[ScheduleClass.objects.get(id=obj['id']).teacher.id] = []
                print("teacherLesson 1",teacherLesson)

                for obj in schedule_obj:
                    if obj['lesson_code'] != None:
                        if Teacher.objects.get(id = ScheduleClass.objects.get(id=obj['id']).teacher.id):
                            teacherLesson[ScheduleClass.objects.get(id=obj['id']).teacher.id].append(obj['lesson_code'])
                print("teacherLesson 2----", teacherLesson)

            elif daterange == False:
                print("date range ",daterange)
                schedule_obj = ScheduleClass.objects.filter(teacher = Teacher.objects.get(id=int(teacherId)))
                print("---schedule-----oneTeacherFeedback-", schedule_obj)

                for obj in schedule_obj:
                    teacherLesson[obj.teacher.id] = []
                print("teacherLesson 1",teacherLesson)

                for obj in schedule_obj:
                    teacherLesson[obj.teacher.id].append(obj.lesson.lesson_code)
                print("teacherLesson 2----", teacherLesson)
           
        feedback = {}
        feedback["Strict"] = 0
        feedback["Conservative"] = 0
        feedback["Moderate"] = 0
        feedback["Liberal"] = 0
        sum_behaviour = {}
        max = 0
        count_strict = 0
        count_moderate = 0
        count_conservative = 0
        count_liberal = 0

        for key, values in teacherLesson.items():
            print("key--",key,"value-",values)
            data_behaviour = []
            data_wellbeing = []
            data_overall = []
            count = len(values)
            for count in range(0,count):
                print("date range--!!",daterange)
                if daterange == True:
                    print("lesson code",teacherLesson[key][0]["lesson_code"])
                    lesson_obj = Lesson.objects.get(lesson_code = teacherLesson[key][count]["lesson_code"])
                else:
                    print("date range false")
                    lesson_obj = Lesson.objects.get(lesson_code = teacherLesson[key][count])
                    print("lesson_obj--",lesson_obj)
                filter_stud_data = StudentReportData.objects.filter(lesson = lesson_obj)
                print("filter_stud_data---",filter_stud_data)
                for i in filter_stud_data:
                    data_behaviour.append(i.behaviour_data.value)
                    data_wellbeing.append(i.well_being.value)
                    data_overall.append(i.over_all_rating.value)
                    max = max + 4
            sum_behaviour[key] = sum(data_behaviour)/len(data_behaviour)
            print("sum_behaviour--",sum_behaviour[key])
            if sum_behaviour[key] > 0 and sum_behaviour[key] <= 1:
                count_strict = count_strict + 1
                feedback["Strict"] = count_strict
            if sum_behaviour[key] > 1 and sum_behaviour[key] <= 2:
                count_conservative = count_conservative + 1
                feedback["Conservative"] = count_conservative
            if sum_behaviour[key] > 2 and sum_behaviour[key] <= 3:
                count_moderate = count_moderate + 1
                feedback["Moderate"] = count_moderate
            if sum_behaviour[key] > 3 and sum_behaviour[key] <= 4:
                count_liberal = count_liberal + 1
                feedback["Liberal"] = count_liberal
        print("feedback---",feedback)


        teacherDict = {}

        if 'teacherId' not in request.data:
            if status == 'active':
                studioWiseTeacher = Teacher.objects.filter(studio = stobj, is_active=True)
            else:
                studioWiseTeacher = Teacher.objects.filter(studio = stobj, is_active=False)

            for t in studioWiseTeacher:
                teacherDict[t.id] = t.full_name
        
        data = {
            "sum_behaviour" :sum_behaviour,
            "feedback": feedback,
            'teachers':teacherDict
        }
        return Response(data)


@api_view(['POST'])
@permission_classes([ AllowAny, ])
def GetBarData(request):
    if request.method == 'POST':
        print("in SubwiseFeedback")
        print("data----",request.data)

        daterange = False
        allteacherFeedback = False
        oneTeacherFeedback = False
        active_status = False

        if 'studioId' in request.POST and 'teacher_status' in request.POST:
            print(" SubwiseFeedback come in 1")
            studioID = request.POST['studioId']
            teacher_status = request.POST['teacher_status']
            allteacherFeedback = True
            if teacher_status == 'active':
                print("SubwiseFeedback come in 2")
                active_status = True

            if 'date_range' in request.POST:
                print("SubwiseFeedback come in 3")
                dateRange = request.POST['date_range']
                daterange = True

        if 'teacherId' in request.POST:
            print("SubwiseFeedback come in 4")
            print("oneTeacherFeedback SubwiseFeedback only teacher id")
            teacherId = request.POST['teacherId']
            print("teacherId--",teacherId)
            oneTeacherFeedback = True
        
            if 'date_range' in request.POST:
                print("SubwiseFeedback come in 5")
                dateRange = request.POST['date_range']
                daterange = True

        if allteacherFeedback == True:
            print("in allteacherFeedback subjectwise")
            stobj = Studio.objects.get(id = int(studioID))
            print("stobj-SubwiseFeedback--",stobj)



        # if teacher_status == "active":
        #     print("if 111")
        #     active_status = True
        # elif teacher_status == "inactive":
        #     print("elif 111")
        #     active_status = False
        # # print("tyyyyyyyppppeeeeeee",typee)
        # print("--subjectwise---active_status",active_status)
            if active_status == False:
                print("SubwiseFeedback active")
                teacher_obj = Teacher.objects.filter(studio = stobj)
                print("teacher_obj---SubwiseFeedback-",teacher_obj)
            else:
            # print("else 222")
                print("SubwiseFeedback inactive")
                teacher_obj = Teacher.objects.filter(is_active = active_status , studio = stobj)
                print("teacher_obj---SubwiseFeedback-0000",teacher_obj)
        # subject_total = {}
        # subject_behaviour = []
        # # average_val = float(0)
        elif oneTeacherFeedback == True:
            teacher_obj = Teacher.objects.filter(id = int(teacherId))


        avg = {}

    # if 'teacherId' in request.data:
    #     print("yesssss")
    #     teachObj = Teacher.objects.get(id=int(request.POST['teacherId']))
    # else:
    #     print("nooooo")
    #     teacher_obj = Teacher.objects.all()
    
    # if 'teacherId' not in request.data:
    #     print("if ")
        typee = {"Strict":0, "Conservative":0, "Moderate":0, "Liberal":0}
        for teachObj in teacher_obj:
            print("teachObj 000",teachObj)
            class_obj = ScheduleClass.objects.filter(teacher = teachObj.id)
            print("class_obj 000",class_obj)
            # daterange = False
            # print("post data---",request.data)
            # if "date_range" in request.POST:
                # print("in post date range")
            subject_behaviour = {}
            print("daterange-----0000",daterange)
            if daterange == True:
                class_obj = FilterDateGraphSerializer(class_obj, many=True, context = request.POST)
                print("-------in else--------")
                for lesson in class_obj.data:
                    if lesson["lesson_code"] != None:
                        print("subject---", lesson["lesson_code"]["id"])
                        report = StudentReportData.objects.filter(lesson = lesson["lesson_code"]["id"])
                        for value in report:
                            if value.subject.subject_name in subject_behaviour:
                                subject_behaviour[value.subject.subject_name].append(value.behaviour_data.value) 
                            else:
                                subject_behaviour[value.subject.subject_name] = []
                                subject_behaviour[value.subject.subject_name].append(value.behaviour_data.value)
                        print("value-----" , value.behaviour_data.value)                      
                print("Subject_behaviour-------------------", subject_behaviour)

            else:
                for lesson in class_obj:
                    print("subject---", lesson.lesson.subject.subject_name)
                    report = StudentReportData.objects.filter(lesson = lesson.lesson)
                    print("report 0000",report)
                    for value in report:
                        if value.subject.subject_name in subject_behaviour:
                            subject_behaviour[value.subject.subject_name].append(value.behaviour_data.value) 
                        else:
                            subject_behaviour[value.subject.subject_name] = []
                            subject_behaviour[value.subject.subject_name].append(value.behaviour_data.value)
                    print("value-----" , value.behaviour_data.value)
                print("subject_behaviour00000000",subject_behaviour)
        
        print("avg---000",avg)
        for sub in subject_behaviour:
            print("sub777777",sub)
            if sub in avg.keys():
                print("avg sub if")
                average = sum(subject_behaviour[sub])/len(subject_behaviour[sub])
                if average > 0 and average <= 1:
                    avg[sub]["Strict"] = avg[sub]["Strict"] + 1
                if average > 1 and average <= 2:
                    avg[sub]["Conservative"] = avg[sub]["Conservative"] + 1
                if average > 2 and average <= 3:
                    avg[sub]["Moderate"] = avg[sub]["Moderate"] + 1
                if average > 3 and average <= 4:
                    avg[sub]["Liberal"] = avg[sub]["Liberal"] + 1
            else:
                print("avg 000 else")
                print("copy.deepcopy(typee)----",copy.deepcopy(typee), typee)
                avg[sub] = copy.deepcopy(typee)
                print("avg 4444",avg[sub])
                print("avg88888",avg)
                average = sum(subject_behaviour[sub])/len(subject_behaviour[sub])
                print("average--000",average)
                if average > 0 and average <= 1:
                    avg[sub]["Strict"] = avg[sub]["Strict"] + 1
                if average > 1 and average <= 2:
                    avg[sub]["Conservative"] = avg[sub]["Conservative"] + 1
                if average > 2 and average <= 3:
                    avg[sub]["Moderate"] = avg[sub]["Moderate"] + 1
                if average > 3 and average <= 4:
                    avg[sub]["Liberal"] = avg[sub]["Liberal"] + 1
        print("avg----3456",avg)

        data = {
            'avg' : avg,
        }
        return Response(data)
