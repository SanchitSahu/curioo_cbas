from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.cache import cache_control
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from curioo_admin.models import Behaviour, Subject, OverallRatingScale
from teacher.getUser import gbl


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def subject_behaviour_list(request):
    """ To get only active Subject Behaviour List in Admin Panel. """
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    try:
        subjects = Subject.objects.filter(is_active=True)
        try:
            subject_first_id = subjects[0].id
        except Exception:
            subject_first_id = None
        return render(request, template_name="curioo_admin/behavior.html",
                      context={"subjects": subjects, 'behaviour_class': True, 'first_subject_id': subject_first_id})
    except Exception as e:
        print('Exception : ', e)
        return redirect(to='admin/dashboard')


@api_view(['GET'])
@permission_classes([AllowAny])
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def specific_subject_id(request, subject_id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    behaviours = Behaviour.objects.filter(subject_id=subject_id, is_active=True)
    behaviour_data = [{'behaviour_id': behaviour.id, 'behaviour_name': behaviour.behaviour} for behaviour in behaviours]
    ret_data = {"data": behaviour_data}
    return Response(status=status.HTTP_200_OK, data=ret_data)


@csrf_exempt
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def subject_behaviour_add(request, subject_id):
    """ To Add Subject Behaviour in Admin Panel. """
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    try:
        if request.method == "POST":
            behaviour = request.POST['behaviour']
            subject = Subject.objects.get(id=subject_id, is_active=True)

            if Behaviour.objects.filter(subject_id=subject_id, behaviour__iexact=behaviour).exists():
                msg = "Behaviour already exists in this subject."
                return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)

            b1=Behaviour.objects.create(subject=subject, behaviour=behaviour)
            print("in add")
            print(subject)
            print(b1)
            return HttpResponse(status=status.HTTP_200_OK)
    except Exception as e:
        print(e)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content="invalid request")


@csrf_exempt
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def subject_behaviour_edit(request, behaviour_id):
    """ To edit Subject Behaviour in Admin Panel. """
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    try:
        if request.method == "POST":
            behaviour = request.POST['behaviour']
            print("in edit")
            behaviour_obj = Behaviour.objects.get(id=behaviour_id)
            if behaviour_obj.behaviour.lower() != behaviour.lower():
                if Behaviour.objects.filter(subject=behaviour_obj.subject, behaviour__iexact=behaviour).exists():
                    msg = "Behaviour already exists in this subject."
                    return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
            Behaviour.objects.filter(id=behaviour_id, is_active=True).update(behaviour=behaviour)
            return HttpResponse(status=status.HTTP_200_OK)
    except Exception as e:
        print(e)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content="invalid request")


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def subject_behaviour_delete(request, behaviour_id):
    """ To delete Subject Behaviour in Admin Panel. """
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    try:
        Behaviour.objects.filter(id=behaviour_id, is_active=True).update(is_active=False)
        return HttpResponse(status=status.HTTP_200_OK)

    except Exception as e:
        msg = "Invalid Request."  # No behaviour can be fetched for specific behaviour_id
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
