"""
Django DB models for Curioo Project. All models are listed here for better usability.
"""
from django.contrib.auth.models import AbstractUser
from django.db import models
from ckeditor.fields import RichTextField
from django.contrib.postgres.fields import ArrayField

TITLES = [
    ('Mr', 'Mr'),
    ('Miss', 'Miss'),
    ('Mrs', 'Mrs')
]


class Admin(models.Model):
    """ Model for Admin User. """
    login_type = models.CharField(max_length=30, null=True, blank=False, default='admin')
    full_name = models.CharField(max_length=30, null=True, blank=False)
    phone = models.BigIntegerField(null=True, blank=True)
    email = models.EmailField(null=True, blank=False, unique=True)
    password = models.CharField(max_length=255, null=True, blank=False)
    image = models.ImageField(upload_to='admin_profile/', default='img/my-ac-img.png')
    device = ArrayField(models.CharField(max_length=1000), blank=True, default=list)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)


# def studio_code():
#     try:
#         s_id = Studio.objects.latest('id')
#         return 'S' + str(s_id.id + 1)
#     except:
#         return 'S' + str(1)


class Studio(models.Model):
    """ Model for Studio. """
    # studio_code = models.CharField(max_length=255, null=True, blank=True, unique=True, default=studio_code)
    studio_code = models.CharField(max_length=255, null=True, blank=True, unique=True)
    name = models.CharField(max_length=255, null=True, blank=True)
    country = models.CharField(max_length=255, null=True, blank=True)
    location = models.CharField(max_length=255, null=True, blank=True)
    state = models.CharField(max_length=255, null=True, blank=True)
    city = models.CharField(max_length=255, null=True, blank=True)
    opening_date = models.DateField(null=True, blank=True)
    status = models.CharField(max_length=255, null=True, blank=True)
    contact_name = models.CharField(max_length=255, null=True, blank=True)
    contact_number = models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField(null=True, blank=True, unique=True)
    address = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, editable=True, null=True, blank=True)
    is_active = models.BooleanField(default=True)


def subject_code():
    try:
        s_id = Subject.objects.latest('id')
        return 'sub' + str(s_id.id + 1)
    except:
        return 'sub' + str(1)


class Subject(models.Model):
    """ Model for Subject. """
    subject_code = models.CharField(max_length=30, null=True, blank=True, unique=True)
    subject_name = models.CharField(max_length=30, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)
    is_active = models.BooleanField(default=True)


class Level(models.Model):
    """ Model for User Level. """
    level = models.CharField(max_length=30, null=True, blank=False,unique=True)
    level_shortcut = models.CharField(max_length=30, null=True, blank=False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)
    is_active = models.BooleanField(default=True)


class Major(models.Model):
    """ Model for User Major. """
    major = models.CharField(max_length=30, null=True, blank=False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)
    is_active = models.BooleanField(default=True)


def principal_code():
    try:
        p_id = Principal.objects.latest('id')
        return 'P' + str(p_id.id + 1)
    except:
        return 'P' + str(1)


class Principal(models.Model):
    """ Model for Principal User. """
    login_type = models.CharField(max_length=30, null=True, blank=False, default='principal')
    principal_code = models.CharField(max_length=30, null=True, blank=True, unique=True, )
    password = models.CharField(max_length=255, null=True, blank=True)
    title = models.CharField(choices=TITLES, max_length=255, null=True, blank=True)
    full_name = models.CharField(max_length=255, null=True, blank=True)
    phone = models.BigIntegerField(null=True, blank=True)
    address = models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField(null=True, blank=True, unique=True)
    gender = models.CharField(max_length=255, null=True, blank=True)
    nationality = models.CharField(max_length=255, null=True, blank=True)
    position = models.CharField(max_length=255, null=True, blank=True)
    positionId = models.CharField(max_length=255, null=True, blank=True,unique=True)
    studio = models.ForeignKey(to=Studio, on_delete=models.CASCADE,null=True)
    highest_education = models.CharField(max_length=255, null=True, blank=True)
    education_major = models.CharField(max_length=255, null=True, blank=True)
    emergency_contact_name = models.CharField(max_length=255, null=True, blank=True)
    emergency_number = models.BigIntegerField(null=True, blank=True)
    dob = models.DateField(null=True, blank=True)
    doj = models.DateField(null=True, blank=True)
    image = models.ImageField(upload_to='principal_profile/', default='default.jpg',null=True)
    device = ArrayField(models.CharField(max_length=1000), blank=True, default = list)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)
    is_active = models.BooleanField(default=True)


class Token(models.Model):
    device_id = models.CharField(max_length=64)
    access_token = models.CharField(max_length=1000, null=False, blank=False)
    device_type = models.CharField(max_length=64, null=True)
    device_fcm_token = models.CharField(max_length=64, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)


class Teacher(models.Model):
    """ Model for Teachers. """
    login_type = models.CharField(max_length=30, null=True, blank=False, default='teacher')
    teacher_code = models.CharField(max_length=30, null=True, blank=False, unique=True)
    full_name = models.CharField(max_length=255, null=True, blank=False)
    phone = models.BigIntegerField(null=True, blank=False)
    emergency_contact_name = models.CharField(max_length=255, null=True, blank=False)
    emergency_phone = models.BigIntegerField(null=True, blank=False)
    email = models.EmailField(null=True, blank=False, unique=True)
    password = models.CharField(max_length=255, null=True, blank=True)
    studio = models.ForeignKey(to=Studio, on_delete=models.CASCADE,null=True)
    subject = models.ManyToManyField(to=Subject)
    dob = models.DateField(blank=False, null=True)
    doj = models.DateField(blank=False, null=True)
    image = models.ImageField(upload_to='teacher_profile/', default='img/default-user.png')
    gender = models.CharField(max_length=30, null=True, blank=False)
    nationality = models.CharField(max_length=30, null=True, blank=False)
    education_level = models.CharField(max_length=30, null=True, blank=False)
    education_major = models.CharField(max_length=30, null=True, blank=False)
    position = models.CharField(max_length=30, null=True, blank=False)
    position_id = models.CharField(max_length=30, null=True, blank=False)
    device = ArrayField(models.CharField(max_length=1000), blank=True, default = list)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)
    is_active = models.BooleanField(default=True)


def learning_consultant_code():
    try:
        lc_id = LearningConsultant.objects.latest('id')
        return 'LC' + str(lc_id.id + 1)
    except:
        return 'LC' + str(1)


class LearningConsultant(models.Model):
    """ Model for Learning Consultant. """
    login_type = models.CharField(max_length=30, null=True, blank=False, default='learning_consultant')
    studio = models.ForeignKey(to=Studio, on_delete=models.CASCADE, null=True)
    learning_consultant_code = models.CharField(max_length=30, null=True, blank=True, unique=True)
    full_name = models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField(null=True, blank=True, unique=True)
    # image = models.ImageField(upload_to='learning_consultant_profile/', default='default.jpg', null=True, blank=True)
    image = models.ImageField(upload_to='learning_consultant_profile/', null=True, blank=True)
    phone = models.BigIntegerField(null=True, blank=True)
    dob = models.DateField(null=True, blank=True)
    doj = models.DateField(null=True, blank=True)
    password = models.CharField(max_length=255, null=True, blank=True)
    device = ArrayField(models.CharField(max_length=1000), blank=True,null=True, default=list)
    gender = models.CharField(max_length=255, null=True, blank=True)
    nationality = models.CharField(max_length=255, null=True, blank=True)
    position = models.CharField(max_length=255, null=True, blank=True)
    positionId = models.CharField(max_length=255, null=True, blank=True, unique=True)
    highest_education = models.CharField(max_length=255, null=True, blank=True)
    education_major = models.CharField(max_length=255, null=True, blank=True)
    emergency_contact_name = models.CharField(max_length=255, null=True, blank=True)
    emergency_number = models.CharField(max_length=255, null=True, blank=True)
    address = models.CharField(max_length=255, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)
    is_active = models.BooleanField(default=True)


def lead_code():
    try:
        l_id = Lead.objects.latest('id')
        return 'L'+str(l_id.id+1)
    except:
        return 'L'+str(1)


class Lead(models.Model):
    """ Model for Lead. """
    lead_code = models.CharField(max_length=30, null=False, blank=False, unique=True)
    full_name = models.CharField(max_length=255, null=False, blank=False)
    studio = models.ForeignKey(to=Studio, on_delete=models.CASCADE, null=True)
    image = models.ImageField(upload_to='lead_profile/', default='lead_profile/default-user.png')
    dob = models.DateField(blank=False, null=False)
    gender = models.CharField(max_length=30, null=False, blank=False)
    nationality = models.CharField(max_length=30, null=True, blank=False)
    cbas_testDate = models.DateField(blank=True, null=True)
    father_name = models.CharField(max_length=255, null=False, blank=False)
    father_phone = models.BigIntegerField(null=False, blank=False)
    father_email = models.EmailField(null=False, blank=False)
    mother_name = models.CharField(max_length=255, null=False, blank=False)
    mother_phone = models.BigIntegerField(null=False, blank=False)
    mother_email = models.EmailField(null=False, blank=False)
    school_name = models.CharField(max_length=255, null=False, blank=False)
    school_grade = models.CharField(max_length=255, null=False, blank=False)
    enrollment_date = models.DateField(blank=False, null=True)
    expiry_date = models.DateField(blank=False, null=True)
    start_curioo_level = models.ForeignKey(to=Level, on_delete=models.CASCADE, related_name='lead_start_curioo_level', null=True)
    current_curioo_level = models.ForeignKey(to=Level, on_delete=models.CASCADE, related_name='lead_current_curioo_level', null=True)
    major = models.ForeignKey(to=Major, on_delete=models.CASCADE, null=True)
    added_by_type = models.CharField(max_length=255, null=True, blank=False)
    added_by_id = models.IntegerField(null=True, blank=False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)
    is_active = models.BooleanField(default=True)
    is_converted = models.BooleanField(default=False)


class Student(models.Model):
    """ Model for Subject. """
    student_code = models.CharField(max_length=30, null=True, blank=False, unique=True)
    studio = models.ForeignKey(to=Studio, on_delete=models.CASCADE, null=True)
    full_name = models.CharField(max_length=255, null=True, blank=False)
    father_name = models.CharField(max_length=255, null=True, blank=False)
    father_phone = models.BigIntegerField(null=True, blank=False)
    father_email = models.EmailField(null=True, blank=False)
    mother_name = models.CharField(max_length=255, null=True, blank=False)
    mother_phone = models.BigIntegerField(null=True, blank=False)
    mother_email = models.EmailField(null=True, blank=False)
    dob = models.DateField(blank=False, null=True)
    doj = models.DateField(blank=False, null=True)
    expiry_date = models.DateField(blank=False, null=True)
    cbas_testDate = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=30, null=True, blank=False)
    nationality = models.CharField(max_length=30, null=True, blank=False)
    school_name = models.CharField(max_length=255, null=True, blank=False)
    school_grade = models.CharField(max_length=255, null=True, blank=False)
    diagnostic_report = models.FileField(null=True, upload_to='student/diagnostic/')
    start_level = models.ForeignKey(to=Level, on_delete=models.CASCADE, null=True, related_name='student_start_curioo_level')
    current_level = models.ForeignKey(to=Level, on_delete=models.CASCADE, null=True, related_name='student_current_curioo_level')
    added_by_type = models.CharField(max_length=255, null=True, blank=False)
    added_by_id = models.IntegerField(null=True, blank=False)
    major = models.ForeignKey(to=Major, on_delete=models.CASCADE, null=True)
    image = models.ImageField(upload_to='lead_profile/', default='img/user-img-pr.png')
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)
    is_active = models.BooleanField(default=True)


def lesson_code():
    try:
        s = Lesson.objects.latest('id')
        return 'Le'+str(s.id + 1)
    except Exception as e:
        return 'Le'+str(1)


class Lesson(models.Model):
    """ Model for Subject. """
    lesson_code = models.CharField(max_length=30, null=True, blank=True, unique=True)
    studio = models.ForeignKey(to=Studio, on_delete=models.CASCADE, null=True, blank=True)
    level = models.ForeignKey(to=Level, on_delete=models.CASCADE,null=True, blank=True)
    subject = models.ForeignKey(to=Subject, on_delete=models.CASCADE,null=True, blank=True)
    student = models.ManyToManyField(Student, related_name='lesson_student')
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)
    is_scheduled = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)


class Comments(models.Model):
    """ Model for Comments When curioo_admin made comments on Student or Teacher. """
    lesson = models.ForeignKey(to=Lesson, on_delete=models.CASCADE, null=True, blank=True)
    commented_by_role = models.CharField(max_length=30, null=True, blank=False)  # Admin/Principal
    commented_by_id = models.IntegerField(null=True, blank=False)  # Store the ID of respective commentator
    commented_on_role = models.CharField(max_length=30, null=True, blank=False)  # Teacher/Student
    commented_on_id = models.IntegerField(null=True, blank=False)  # Store the ID on whom comment was made
    subject = models.ForeignKey(to=Subject, on_delete=models.CASCADE, null=True, blank=True)  # If commented_on role is student, then subject_id on which teacher is commenting
    message = models.CharField(max_length=255, null=True, blank=False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    is_active = models.BooleanField(default=True)

def schedule_class_code():
    try:
        s = ScheduleClass.objects.latest('id')
        return 'SC'+str(s.id)
    except Exception as e:
        return 'SC'+str(1)

class ScheduleClass(models.Model):
    """ Model for Class Schedule. """
    # schedule_code = models.CharField(max_length=30, blank=True, unique=True, default=schedule_class_code)
    studio = models.ForeignKey(to=Studio, on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=30, null=True, blank=False)
    scheduled_date = models.DateField(blank=False,null=True)
    class_start_time = models.TimeField(blank=False,null=True)
    class_end_time = models.TimeField(blank=False,null=True)
    lesson = models.ForeignKey(to=Lesson, on_delete=models.CASCADE, related_name="lesson",null=True)
    teacher = models.ForeignKey(to=Teacher, on_delete=models.CASCADE, related_name="teacher",null=True)
    lesson_status = models.CharField(max_length=255, blank=False, null=True, default="Upcoming")
    room_number = models.CharField(max_length=30, null=True, blank=False)
    start_class = models.TimeField(blank=True, null=True)
    end_class = models.TimeField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)
    is_active = models.BooleanField(default=True)


class Behaviour(models.Model):
    """ Model for storing students behaviour. """
    subject = models.ForeignKey(to=Subject, on_delete=models.CASCADE)
    behaviour = models.CharField(max_length=255, null=True, blank=False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)
    is_active = models.BooleanField(default=True)


class OverallRatingScale(models.Model):
    """ Overall Rating Scale in Subject Behaviour Part. """
    overall_rating = models.CharField(max_length=255, null=True, blank=False)
    value = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)
    is_active = models.BooleanField(default=True)


class WellBeingScale(models.Model):
    """ Wellbeing Scale in Subject Behaviour Part. """
    well_being_scale = models.CharField(max_length=255, null=True, blank=False)
    value = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)
    is_active = models.BooleanField(default=True)


class BehaviourData(models.Model):
    lesson = models.ForeignKey(to=Lesson, on_delete=models.CASCADE)
    subject = models.ForeignKey(to=Subject, on_delete=models.CASCADE)
    student = models.ForeignKey(to=Student, on_delete=models.CASCADE)
    behaviour = models.ForeignKey(to=Behaviour, on_delete=models.CASCADE)
    value = models.IntegerField()


class StudentReportData(models.Model):
    lesson = models.ForeignKey(to=Lesson, on_delete=models.CASCADE)
    subject = models.ForeignKey(to=Subject, on_delete=models.CASCADE)
    student = models.ForeignKey(to=Student, on_delete=models.CASCADE)
    behaviour_data = models.ForeignKey(to=BehaviourData, on_delete=models.CASCADE)
    over_all_rating = models.ForeignKey(to=OverallRatingScale, on_delete=models.CASCADE)
    well_being = models.ForeignKey(to=WellBeingScale, on_delete=models.CASCADE)
    score = models.FloatField(default=0)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)


class ProgressReport(models.Model):
    """ Model for students progress report. """
    student = models.ForeignKey(to=Student, on_delete=models.CASCADE)
    subject = models.ForeignKey(to=Subject, on_delete=models.CASCADE)
    teacher = models.ForeignKey(to=Teacher, on_delete=models.CASCADE)
    behaviour = models.ForeignKey(to=Behaviour, on_delete=models.CASCADE)
    lesson = models.ForeignKey(to=Lesson, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)
    is_active = models.BooleanField(default=True)


class Announcement(models.Model):
    """ Model for announcements. """
    announcement = models.CharField(max_length=255, blank=False, null=True)
    admin = models.ForeignKey(to=Admin, on_delete=models.CASCADE, null=True, blank=True)
    principal = models.ForeignKey(to=Principal, on_delete=models.CASCADE, null=True, blank=True)
    studio = models.ForeignKey(to=Studio, on_delete=models.CASCADE,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)


class WebNotification(models.Model):
    """ Model for natifications. """
    notification = models.CharField(max_length=255, blank=False, null=True)
    respective_user_type = models.CharField(max_length=30, blank=False, null=True)
    respective_user_id = models.IntegerField(blank=False, null=True)
    is_seen = models.BooleanField(default=False , null = False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)


class AdminNotification(models.Model):
    """ Model for natifications. """
    notification = models.CharField(max_length=255, blank=False, null=True)
    admin = models.ForeignKey(to=Admin, on_delete=models.CASCADE, null=True, blank=True)
    principal = models.ForeignKey(to=Principal, on_delete=models.CASCADE, null=True, blank=True)
    is_seen = models.BooleanField(default=False , null = False)
    is_announcement = models.BooleanField(default=False ,null=True)
    announcement = models.ForeignKey(to=Announcement, on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)


class FAQ(models.Model):
    """ Model for FAQs. """
    question = models.CharField(max_length=255, blank=False, null=True)
    answer = models.CharField(max_length=255, blank=False, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)
    is_active = models.BooleanField(default=True)

class HelpDesk(models.Model):
    """ Model for HelpDesk. """
    studio = models.CharField(max_length=255, blank=False, null=True)
    name = models.CharField(max_length=255, blank=False, null=True)
    email = models.EmailField(null=True, blank=False)
    message = models.CharField(max_length=255, blank=False, null=True)
    image = ArrayField(models.FileField(null=True, upload_to='teacher/helpDesk/'), blank=True, default=list)
    position = models.CharField(max_length=255, blank=False, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)


class CMS(models.Model):
    """ Model for CMS Data which will displayed on Curioo Website. """
    languages = [
        ('English', 'English'),
        ('Spanish', 'Spanish')
    ]
    language = models.CharField(choices=languages, max_length=255, null=True, blank=False)
    title = models.CharField(max_length=255, null=True, blank=False)
    image = models.ImageField(null=True, upload_to='cms/image/')
    content = RichTextField(null=True, blank=False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)
    
class Activity(models.Model):
    lesson = models.ForeignKey(to=Lesson, on_delete=models.CASCADE)
    student_report_data = models.ForeignKey(to=StudentReportData, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=True)