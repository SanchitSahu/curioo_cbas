import copy
import json
import logging
from datetime import datetime
from django.contrib.auth.hashers import make_password, check_password
from itertools import chain
from django.contrib.sites.shortcuts import get_current_site
from django.core import mail
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.html import strip_tags
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
import logging

from rest_framework.views import APIView

from curioo_admin.models import Admin, Principal, Student, Teacher, Subject, Lead, Lesson, Studio, LearningConsultant, \
    Level, Major, Comments, ScheduleClass, AdminNotification, BehaviourData, Announcement, FAQ
from curioo_admin.serializers import StudentSerializer, FullStudentSerializer, CommentsSerializer, \
    AdminStudentDropdownSerializer, PrincipalStudentDropdownSerializer, LearningConsultantStudentDropdownSerializer, \
    EditStudentSerializer, EditStudentContactSerializer, StudentFilterSerializer, CsvStudentSerializer
from curioo_admin.validations import calculate_age
from curioo_cbas import settings
from teacher.getUser import gbl

logger = logging.getLogger(__name__)

def download_students(request, student_id):
    """ Specific Students View """
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    try:
        students = Student.objects.get(id=student_id)
        print("students", students.current_level)
        age = calculate_age(students.dob)
        # serializer = FullStudentSerializer(students)
        # print(serializer.data)
        classes = ScheduleClass.objects.filter(lesson__student=students, scheduled_date=datetime.today())
        print(classes)
        comments = Comments.objects.filter(commented_on_id=student_id, commented_on_role='Student')
        print('comments : ', comments)
        comment = CommentsSerializer(comments, many=True)
        ret_data = comment.data
        print('ret_data : ', json.loads(json.dumps(ret_data)))
        subjectObj = BehaviourData.objects.filter(student=students)
        sub_data = {}
        for obj in subjectObj:
            sub_data[obj.subject.id] = obj.subject.subject_name
        print('subjectObj',sub_data)
        return render(request=request, template_name='curioo_admin/downloadReport.html', context={"students": students,
                                                                                                 'student_class': True,
                                                                                                 'age': age,
                                                                                                 'comments': ret_data,
                                                                                                 'classes': classes,
                                                                                                 'subjectObj':sub_data,
                                                                                                 "commnets": json.loads(
                                                                                                     json.dumps(
                                                                                                         ret_data))})
    except Exception as e:
        print(e)
        msg = 'Invalid Request'
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)