""" Views.py for admin panel of Curioo CBAS. """
import copy
import json
import logging
from datetime import datetime
from django.contrib.auth.hashers import make_password, check_password
from itertools import chain
from django.contrib.sites.shortcuts import get_current_site
from django.core import mail
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.html import strip_tags
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.translation import activate, ugettext
from django.views import View
from django.views.decorators.cache import cache_control
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import ListAPIView
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
import logging
from rest_framework.views import APIView
from curioo_admin.models import Admin, Principal, Student, Teacher, Subject, Lead, Lesson, Studio, LearningConsultant, \
    Level, Major, Comments, ScheduleClass, AdminNotification, BehaviourData, Announcement, FAQ, StudentReportData
from curioo_admin.serializers import StudentSerializer, FullStudentSerializer, CommentsSerializer, \
    AdminStudentDropdownSerializer, PrincipalStudentDropdownSerializer, LearningConsultantStudentDropdownSerializer, \
    EditStudentSerializer, EditStudentContactSerializer, StudentFilterSerializer, CsvStudentSerializer
from curioo_admin.validations import calculate_age
from curioo_cbas import settings
from teacher.getUser import gbl
from teacher.status_message import error_message

logger = logging.getLogger(__name__)


# @csrf_exempt
# def login(request):
#     """ Login Function for Admin/Principal Login. """
#     if request.method == "GET":
#         return render(request=request, template_name='curioo_admin/home-login.html')
#     if request.method == "POST":
#         try:
#             is_admin, is_principal = False, False
#             email = request.POST['email']
#             if Admin.objects.filter(email=email).exists():
#                 user, is_admin = Admin.objects.get(email=email), True
#             elif Principal.objects.filter(email=email).exists():
#                 user, is_principal = Principal.objects.get(email=email), True
#             else:
#                 msg = "No user is signed up with this email"
#                 return render(request=request, template_name='curioo_admin/home-login.html',
#                               context={"error": True, "message": msg})
#         except Exception as e:
#             print('Exception while fetching  Admin : ', e)
#             msg = "Invalid Credentials"
#             return render(request=request, template_name='curioo_admin/home-login.html',
#                           context={"error": True, "message": msg})
#         try:
#             password = request.POST['password']
#             if not check_password(password=password, encoded=user.password):
#                 msg = "Invalid Password for entered email address"
#                 return render(request=request, template_name='curioo_admin/home-login.html',
#                               context={"error": True, "message": msg})
#             else:
#                 request.session['session_id'] = user.id
#                 request.session['user_type'] = 'Admin' if is_admin else 'Principal'
#                 request.session['full_name'] = user.full_name
#                 return redirect(to='admin/dashboard')
#         except Exception as e:
#             msg = "Invalid Request"
#             return render(request=request, template_name='curioo_admin/home-login.html',
#                           context={"error": True, "message": msg})
#
#
# def logout(request):
#     """ Logout Function for Admin/Principal. """
#     try:
#         del request.session['session_id']
#         del request.session['user_type']
#         del request.session['full_name']
#     except KeyError:
#         pass
#     return redirect(to='admin/')

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@csrf_exempt
def reset_password(request):
    """ When Admin/Principal forget Password, from this Function password reset link sent them over mail. """
    if request.method == "POST":
        is_admin, is_principal = False, False
        try:
            email = request.POST['email']
            if Admin.objects.filter(email=email).exists():
                user, is_admin = Admin.objects.get(email=email), True
            elif Principal.objects.filter(email=email).exists():
                user, is_principal = Principal.objects.get(email=email), True
            else:
                msg = "No User is signed Up with this email."
                return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
        except Exception as e:
            msg = "No User is signed Up with this email."
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
        else:
            current_site = get_current_site(request)
            try:
                subject = 'User Forget Password for Curioo Behavioural and Aptitude System'
                uid = urlsafe_base64_encode(force_bytes(user.id))
                if is_admin: user_type = urlsafe_base64_encode(force_bytes("Admin"))
                if is_principal: user_type = urlsafe_base64_encode(force_bytes("Principal"))
                domain = current_site.domain
                html_message = render_to_string('curioo_admin/forget_password_mail.html',
                                                {'first_name': user.full_name, "user_type": user_type, "uidb64": uid,
                                                "domain": domain})
                plain_message = strip_tags(html_message)
                from_email = settings.DEFAULT_FROM_EMAIL
                to = user.email
                mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)
            except Exception as error:
                print("error in forgot mail-----",error)
                return Response(data=error_message(511,"error","Please check your network connection.",{}), status=status.HTTP_511_NETWORK_AUTHENTICATION_REQUIRED)
            msg = "Please check your email for a link to reset your password."
            return HttpResponse(status=200, content=msg)

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@csrf_exempt
def password(request, uidb64, user_type):
    """ When Admin/Principal clicks on link given in mail, for getting page and for updating password. """
    if request.method == "GET":
        return render(request=request, template_name='curioo_admin/reset-password.html',
                      context={'uidb64': uidb64, 'user_type': user_type})
    if request.method == "POST":
        try:
            new_passowrd = request.POST.get('new_password')
            uid, user_type = urlsafe_base64_decode(uidb64).decode(), urlsafe_base64_decode(user_type).decode()
            if user_type == 'Admin': user = Admin.objects.filter(id=uid)
            if user_type == 'Principal': user = Principal.objects.filter(id=uid)
            user.update(password=make_password(new_passowrd))

            msg = "Password has been changed successfully. Now You can Login into your account."
            return HttpResponse(status=200, content=msg)
        except Exception as e:
            msg = "Invalid Request"
            return HttpResponse(status=400, content=msg)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def dashboard(request):
    """ Admin/Principal Dashboard. """
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    if user_type == "admin":
        try:
            students = Student.objects.filter(is_active=True, expiry_date__gt=datetime.now()).count()
            teachers = Teacher.objects.filter(is_active=True).count()
            subjects = Subject.objects.filter(is_active=True).count()
            leads = Lead.objects.filter(is_active=True, is_converted=False).count()
            studios = Studio.objects.filter(is_active=True).count()
            learning_consultant = LearningConsultant.objects.filter(is_active=True).count()
            principals = Principal.objects.filter(is_active=True).count()
            data = {'students': students, 'teachers': teachers, 'subjects': subjects, 'leads': leads, 'studios': studios,
                    'learning_consultant': learning_consultant, "principals": principals, 'dashboard_class': True}
            return render(request=request, template_name='curioo_admin/dashboard.html', context=data)
        except Exception as e:
            print('error : ', e)
            logger.error('Error while Visiting Admin Dashboard : ', e)
            return redirect('login')
    elif user_type == "principal":
        try:
            students = Student.objects.filter(studio_id=user.studio.id, is_active=True, expiry_date__gt=datetime.now()).count()
            teachers = Teacher.objects.filter(studio_id=user.studio.id, is_active=True).count()
            subjects = Subject.objects.filter(is_active=True).count()
            leads = Lead.objects.filter(studio_id=user.studio.id, is_active=True, is_converted=False).count()
            studios = Studio.objects.filter(is_active=True).count()
            learning_consultant = LearningConsultant.objects.filter(studio_id=user.studio.id, is_active=True).count()
            principals = Principal.objects.filter(is_active=True).count()
            data = {'students': students, 'teachers': teachers, 'subjects': subjects, 'leads': leads, 'studios': studios,
                    'learning_consultant': learning_consultant, "principals": principals, 'dashboard_class': True}
            return render(request=request, template_name='curioo_admin/dashboard.html', context=data)
        except Exception as e:
            print('error : ', e)
            logger.error('Error while Visiting Admin Dashboard : ', e)
            return redirect('login')
    else:
        return redirect('login')

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@csrf_exempt
def change_password(request):
    """ Admin/Principal change password after Login. """
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print("e : ", e)
        return redirect(to='/admin/dashboard')
    else:
        # if request.method == "GET":
        #     return render(request, 'curioo_admin/change-password.html')
        if request.method == "POST":
            try:
                old_password = request.POST.get('old_password')
                new_passowrd = request.POST.get('new_password')
                print("after got passwords ")

                if not check_password(old_password, user.password):
                    print("inside if ")
                    msg = "Invalid Old Password."
                    return HttpResponse(status=400, content=msg)
                else:
                    print("inside else ")
                    user.password = make_password(new_passowrd)
                    user.save()
                    msg = "You've changed your password Successfully."
                    return HttpResponse(status=200, content=msg)
            except Exception as e:
                print("inside exception ", e)
                logger.error('Error while admin change password : ', e)
                msg = "Invalid Request"
                return redirect('admin/dashboard')


# def students(request):
#     """ Students list """
#     students = Student.objects.filter(is_active=True, expiry_date__gt=datetime.now())
#     serializer = StudentSerializer(students, many=True)
#     print("serializer data : ", serializer.data)
#     return render(request=request, template_name='dashboard.html', context={"students": serializer.data})
#
#
# def addStudent(request):
#     try:
#         uid = request.session['session_id']
#         user_type = request.session['user_type']
#
#         print("session_id : ", uid, "user_type : ", user_type)
#     except Exception as e:
#         print("user needs to be logged in : ", e)
#         return redirect(to='/admin/')
#
#     if request.method == "POST":
#         data = request.POST
#
#         studio = data['studio']
#         data['studio'] = Studio.objects.get(id=studio)
#
#         level = data['level']
#         data['level'] = Level.objects.get(id=level)
#
#         major = data['major']
#         data['major'] = Major.objects.get(id=major)
#
#         serializer = FullStudentSerializer(data=data)
#         if serializer.is_valid():
#             serializer.save()
#
#         return redirect('/admin/students')


# class StudentList(DetailView):
#     model = Student
#     template_name = 'students.html'
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         # Add in a QuerySet of all the books
#         context['student_list'] = Student.objects.all()
#         return context


def student_code(studio_code, studio_id):
    try:
        s = Student.objects.filter(studio_id=studio_id).count()
        l = Lead.objects.filter(studio_id=studio_id).count()

        count = s + l
        count += 1
    except Exception as e:
        count = 1
    code = studio_code + str(count)
    return code.upper()


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def add_student(request):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    if request.method == "GET":
        try:
            # levels = Level.objects.filter(is_active=True)
            # majors = Major.objects.filter(is_active=True)
            studios = Studio.objects.filter(is_active=True, status='Active')
            grade_dropdown = [x for x in range(1, 13)]
            # return render(request=request, template_name='curioo_admin/addNewStudent.html',
            #               context={'student_class': True, 'studios': studios, 'grade_dropdown': grade_dropdown})

            if user_type == "admin":
                return render(request=request, template_name='curioo_admin/addNewStudent.html',
                              context={'student_class': True, 'studios': studios, 'grade_dropdown': grade_dropdown,
                                       'user_type': user_type})
            if user_type == "principal":
                return render(request=request, template_name='curioo_admin/addNewStudent.html',
                              context={'student_class': True, 'studios': studios, 'grade_dropdown': grade_dropdown,
                                       "studio_id": user.studio.id, "studio_name": user.studio.name,
                                       'user_type': user_type, "user": user})
        except Exception as e:
            print('e : ', e)
            return redirect('admin/dashboard')
            # return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='Invalid Request')

    if request.method == "POST":
        try:
            data = copy.deepcopy(request.POST)
            studio_id = data['studio']
            studio = Studio.objects.get(id=studio_id)
            data['student_code'] = student_code(studio.studio_code, studio_id)
            data['image'] = request.FILES.get('image')

            print(' data : ', data)

            serializer = FullStudentSerializer(data=data)
            if serializer.is_valid(raise_exception=True):
                student = serializer.save()
                print()
                if not student:
                    return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='Invalid Request')
            print('creation_status : ', student)
            if student:
                student.added_by_type = user_type.capitalize()
                student.added_by_id = user.id
                student.save()
                print('serializer.instance : ', student)
                Principals = Principal.objects.filter(is_active=True, studio=studio_id)
                Admins = Admin.objects.all()

                notification_msg = "A new student was added in " + student.studio.name + " Studio."
                for Principal_obj in Principals:
                    AdminNotification.objects.create(notification=notification_msg, principal=Principal_obj)
                for Admin_obj in Admins:
                    AdminNotification.objects.create(notification=notification_msg, admin=Admin_obj)

            return HttpResponse(status=status.HTTP_200_OK, content='New Student added successfully.')
        except Exception as e:
            print('e : ', e)
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='Invalid Request')


class Students(View):
    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, request):
        try:
            name, user_type, user = gbl(request, request.session['token'])
        except Exception as e:
            print('inside exception : ', e)
            return redirect('login/')

        try:
            if user_type == 'admin':
                students = Student.objects.filter(is_active=True, expiry_date__gt=datetime.now())
            elif user_type == 'principal':
                students = Student.objects.filter(is_active=True, expiry_date__gt=datetime.now(), studio=user.studio.id)
            else:
                print("other than admin or principal")
                students = Student.objects.filter(is_active=True, expiry_date__gt=datetime.now())
            serializer = StudentSerializer(students, many=True)
            levels = Level.objects.filter(is_active=True)
            majors = Major.objects.filter(is_active=True)
            studios = Studio.objects.filter(is_active=True, status='Active')
            # print(students, '\n', serializer.data)
            return render(request=request, template_name='curioo_admin/students.html',
                          context={"students": serializer.data, "levels": levels, "majors": majors, "studios": studios,
                                   'student_class': True, 'user_type': user_type, "user": user})
        except Exception as e:
            print("Exception in student list get request : ", e)
            return redirect('admin/dashboard')
            # return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='Invalid Request')

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def patch(self, request):
        try:
            name, user_type, user = gbl(request, request.session['token'])
        except Exception as e:
            print('inside exception : ', e)
            return redirect('login/')

        try:
            serializer = FullStudentSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
        except Exception:
            return redirect('admin/dashboard')
            # return HttpResponse(status=status.HTTP_400_BAD_REQUEST, msg='Invalid Request')


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def view_students(request, student_id):
    """ Specific Students View """
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    try:
        students = Student.objects.get(id=student_id)
        age = calculate_age(students.dob)
        # serializer = FullStudentSerializer(students)
        # print(serializer.data)
        classes = ScheduleClass.objects.filter(lesson__student=students, scheduled_date=datetime.today())
        print(classes)
        comments = Comments.objects.filter(commented_on_id=student_id, commented_on_role='Student',is_active=True)
        print('comments : ', comments)
        comment = CommentsSerializer(comments, many=True)
        ret_data = comment.data
        print('ret_data : ', json.loads(json.dumps(ret_data)))
        subjectObj = BehaviourData.objects.filter(student=students)
        sub_data = {}
        for obj in subjectObj:
            sub_data[obj.subject.id] = obj.subject.subject_name
        print('subjectObj',sub_data)
        return render(request=request, template_name='curioo_admin/studentDetail.html', context={"students": students,
                                                                                                 'student_class': True,
                                                                                                 'age': age,
                                                                                                 'comments': ret_data,
                                                                                                 'classes': classes,
                                                                                                 'subjectObj':sub_data,
                                                                                                 "commnets": json.loads(
                                                                                                     json.dumps(
                                                                                                         ret_data))})
    except Exception as e:
        print(e)
        msg = 'Invalid Request'
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def edit_student(request, student_id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    if request.method == "GET":
        try:
            student = Student.objects.get(id=student_id)
            levels = Level.objects.filter(is_active=True)
            majors = Major.objects.filter(is_active=True)
            grade_dropdown = [str(x) for x in range(1, 13)]
            age = calculate_age(student.dob)
            if student.added_by_type == "Admin": added_by = Admin.objects.all()
            if student.added_by_type == "Principal": added_by = Principal.objects.filter(is_active=True)
            if student.added_by_type == "Learning Consultant": added_by = LearningConsultant.objects.filter(
                is_active=True)
            return render(request=request, template_name='curioo_admin/editStudentDetail.html',
                          context={'student_class': True, 'levels': levels, 'majors': majors, 'student': student,
                                   'grade_dropdown': grade_dropdown, 'age': age, 'added_by': added_by})
        except Exception as e:
            print('e : ', e)
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content={'message': 'Invalid Request'})

    if request.method == "POST":
        try:
            student = Student.objects.get(id=student_id)
            data = copy.deepcopy(request.POST)
            print(' data : ', data)

            if data['start_level'] == 'Select Start Curioo Level':
                print('inside if')
                data['start_level'] = None

            if data['current_level'] == 'Select Current Curioo Level':
                data['current_level'] = None

            if data['major'] == 'Select Curioo Major':
                data['major'] = None


            serializer = EditStudentSerializer(instance=student, data=data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()

            image = request.FILES.get('image')
            if image != '' and image is not None:
                student.image = image
                student.save()
            return HttpResponse(status=200)
        except Exception as e:
            print('e : ', e)
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content={'message': 'Invalid Request'})


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def edit_student_contact(request, student_id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    if request.method == "GET":
        try:
            student = Student.objects.get(id=student_id)
            age = calculate_age(student.dob)
            return render(request=request, template_name='curioo_admin/editContactInfo.html',
                          context={'student_class': True, 'student': student, 'age': age})
        except Exception as e:
            print('e : ', e)
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content={'message': 'Invalid Request'})

    if request.method == "POST":
        try:
            student = Student.objects.get(id=student_id)
            data = copy.deepcopy(request.POST)
            print(' data : ', data)

            serializer = EditStudentContactSerializer(instance=student, data=data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()

            return HttpResponse(status=200, content='Student Contact details updated successfully')
        except Exception as e:
            print('e : ', e)
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='Invalid Request')


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def delete_students(request, student_id):
    """ Specific Students View """
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    try:
        Student.objects.filter(id=student_id).update(is_active=False)

        # To remove this student from lesson in which he is added
        student = Student.objects.get(id=student_id)
        for lesson in student.lesson_student.all():
            lesson.student.remove(student)

        return HttpResponse(status=status.HTTP_200_OK)
    except Exception as e:
        print('exception : ', e)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, msg='Invalid Request')


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@csrf_exempt
def comments(request, role, commented_on_id):
    """
    Comments Listing. In order to get data, we've to pass role to whom comment is made(Student/Teacher) and their ID.
    """
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    if request.method == 'GET':
        try:
            comments = Comments.objects.filter(commented_on_role=role, commented_on_id=commented_on_id)
            serializer = CommentsSerializer(comments, many=True)
            print('json.loads(json.dumps(serializer.data)) :', json.loads(json.dumps(serializer.data)))
            return HttpResponse(status=200, content=json.loads(json.dumps(serializer.data)))
            # return render(request=request, template_name='comments.html', context={'comments': serializer.data})
        except Exception:
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST, msg='Invalid Request')

    if request.method == 'POST':
        try:
            message = request.POST['message']
            print('inside post', message)
            comment = Comments.objects.create(commented_by_role=user_type.capitalize(), commented_by_id=user.id,
                                              commented_on_role=role, commented_on_id=commented_on_id, message=message)
            return JsonResponse(status=status.HTTP_200_OK, data={'success_text': "Comment made successfully",
                                                                 'comment_by': name, 'comment': message,
                                                                 'created_at': comment.created_at.strftime(
                                                                     '%d:%m:%Y %H:%M'),
                                                                 'image': user.image.url})
        except Exception as e:
            print('exception : ', e)
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='Invalid Request')


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def delete_comment(request,comment_id):
    """ Specific Comment Delete View """
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    try:
        print("Inside Delete Comment")
        comment_obj = Comments.objects.get(id = comment_id)
        if user_type == "principal":
            if comment_obj.commented_by_role != "Principal":
                return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='Invalid Request You can not delete the comment.')
            else:
                Comments.objects.filter(id=comment_id).update(is_active=False)
                return HttpResponse(status=status.HTTP_200_OK)
        else:
            Comments.objects.filter(id=comment_id).update(is_active = False)
            return HttpResponse(status=status.HTTP_200_OK)
    except Exception as e:
        print(e)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='Invalid Request')



@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def profile(request):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')
    return render(request, template_name='curioo_admin/profile.html', context={'admin': user})


# class StudentListApi(APIView):
#     permission_classes = (AllowAny,)
#
#     def get(self, request):
#         try:
#
#             student_studio = request.GET['studio']
#             student_level = request.GET['level']
#             student_major = request.GET['major']
#             student_status = request.GET['status']
#             student_age = request.GET['age']
#             student_added_by = request.GET['added_by']
#
# if student_status == "Active":
#     if student_studio == "All Studio" and student_level=="All level" and student_major=="All Major":
#         students = Student.objects.filter(is_active=True, added_by_type=student_added_by)
#     elif student_studio == "All Studio" and student_level=="All level" and student_major!="All Major":
#         students = Student.objects.filter(is_active=True, added_by_type=student_added_by,
#                                           major__major=student_major)
#     elif student_studio == "All Studio" and student_level !="All level" and student_major!="All Major":
#         students = Student.objects.filter(is_active=True, start_level__level=student_level,
#                                           major__major=student_major,
#                                           added_by_type=student_added_by)
#     elif student_studio == "All Studio" and student_level !="All level" and student_major=="All Major":
#

# if student_studio == "All Studio":
#     students = Student.objects.filter(is_active=True,start_level__level=student_level,major__major=student_major,added_by_type=student_added_by)
#     if student_level == "All":
#         students = Student.objects.filter(is_active=True, major__major=student_major, added_by_type=student_added_by)
#         if student_major == "All":
#             students = Student.objects.filter(is_active=True, added_by_type=student_added_by)
#         else:
#             students = Student.objects.filter(is_active=True, added_by_type=student_added_by,major__major=student_major)
#     else:
#         students = Student.objects.filter(is_active=True, start_level__level=student_level,major__major=student_major,
#                                           added_by_type=student_added_by)
# elif student_level == "All":
#     students = Student.objects.filter(studio__studio_code=student_studio, is_active=True,major__major=student_major,added_by_type=student_added_by)
#
# else:
#     students = Student.objects.filter(studio__studio_code=student_studio, is_active=True,
#                                       major__major=student_major, added_by_type=student_added_by)

#
#     elif student_status == "Inactive":
#         if student_studio == "All":
#             students = Student.objects.filter(is_active=False,major__major=student_major,added_by_type=student_added_by)
#         else:
#             students = Student.objects.filter(studio__studio_code=student_studio, is_active=False,major__major=student_major,added_by_type=student_added_by)
#
#
#
#     else:
#         if student_studio == "All":
#             students = Student.objects.filter(expiry_date__gt=datetime.now(),major__major=student_major,added_by_type=student_added_by)
#         else:
#             students = Student.objects.filter(studio__studio_code=student_studio,expiry_date__gt=datetime.now(),major__major=student_major,added_by_type=student_added_by)
#
#
#
#     data = [{"id": student.id, "code": student.student_code, "name": student.full_name, "age":calculate_age(student.dob),
#              "level":student.level.level,"major":student.major.major} for
#             student in students]
#
#     return Response(data={'data': data}, status=status.HTTP_200_OK)
#
# except Exception as e:
#     return Response(data=error_message(400, "error", str(e), {}), status=status.HTTP_400_BAD_REQUEST)
#
#


@api_view(['GET'])
@permission_classes([AllowAny])
def get_users(request):
    try:
        requested_role = request.GET.get('selected_role')
        if requested_role == "Admin":
            admins = Admin.objects.all()
            serializer = AdminStudentDropdownSerializer(admins, many=True)
            return JsonResponse(data=serializer.data, safe=False)
        elif requested_role == "Principal":
            principals = Principal.objects.filter(is_active=True)
            serializer = PrincipalStudentDropdownSerializer(principals, many=True)
            return JsonResponse(data=serializer.data, safe=False)
        elif requested_role == "Learning Consultant":
            learning_consultants = LearningConsultant.objects.filter(is_active=True)
            serializer = LearningConsultantStudentDropdownSerializer(learning_consultants, many=True)
            return JsonResponse(data=serializer.data, safe=False)
        else:
            return JsonResponse(data=[], safe=False)
    except Exception as e:
        return JsonResponse(data=[], safe=False)


class StudentFilterAPI(ListAPIView):
    serializer_class = StudentFilterSerializer
    permission_classes = (AllowAny,)

    def get_queryset(self):
        try:
            print("inside try")
            query_list = Student.objects.all()
            student_studio = self.request.GET['studio']
            student_level = self.request.GET['level']
            student_major = self.request.GET['major']
            student_status = self.request.GET['status']
            student_age = self.request.GET['age']
            student_added_by_selection = self.request.GET['added_by_selection']
            student_added_by_selection_id = self.request.GET['list_of_added_by']

            print(self.request.GET)
            # country = self.request.query_params.get('country', None)

            if student_status == "Active":
                print("inside if")
                query_list = query_list.filter(is_active=True)

            if student_status == "Inactive":
                print("inside if")
                query_list = query_list.filter(is_active=False)

            if student_status == "Expired":
                print("inside if expired")
                query_list = query_list.filter(expiry_date__lte=datetime.now())

            if student_studio != "All Studios":
                print("inside all studio if")
                query_list = query_list.filter(studio_id=student_studio)

            if student_level != "All Levels":
                print("inside all level if")
                query_list = query_list.filter(current_level_id=student_level)

            if student_major != "All Majors":
                print("inside all major if")
                query_list = query_list.filter(major_id=student_major)

            if student_added_by_selection != "Added By":
                print("inside all added by if")
                query_list = query_list.filter(added_by_type=student_added_by_selection)

            if (student_added_by_selection_id != 'List of Added By' and
                    student_added_by_selection_id != 'Added By Admin'
                    and student_added_by_selection_id != 'Added By Principal' and
                    student_added_by_selection_id != 'Added By Learning Consultant'):
                print("inside if added by selection id")
                query_list = query_list.filter(added_by_id=student_added_by_selection_id)

            data = [{"id": student.id, "code": student.student_code, "name": student.full_name,
                     "age": calculate_age(student.dob),
                     "level": student.current_level.level if student.current_level is not None else "None",
                     "major": student.major.major if student.major is not None else "None"} for
                    student in query_list]

            if student_age != "Age Range":
                print("inside all age if")
                print(student_age)
                ages = student_age.split("-")
                student_ages = list(map(int, ages))
                print(student_ages)
                temp_data = []
                for student in data:
                    if student_ages[0] <= student['age'] <= student_ages[1]:
                        temp_data.append(student)

                data.clear()
                data = temp_data

            return data

        except Exception as e:
            print("exception: ", e)
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(data={'data': queryset}, status=status.HTTP_200_OK)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def faq(request):
    # try:
    #     name, user_type, user = gbl(request, request.session['token'])
    # except Exception as e:
    #     print('inside exception : ', e)
    #     return redirect('login/')
    try:
        if request.method == 'GET':
            print("in get")
            
            faq_data = FAQ.objects.filter(is_active=True)
            context = {'faq_data':faq_data, 'faq_class_active': True}
            return render(request, 'curioo_admin/faq.html', context)
            
        if request.method == 'POST':
            faq = request.POST.get
            if FAQ.objects.filter(question__iexact=faq('question'),answer__iexact=faq('answer')).exists():
                msg = "FAQ already exists in this subject."
                return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)

            data = FAQ(question=faq('question'),answer=faq('answer'))
            data.save()
            return render(request,'curioo_admin/faq.html')
    except Exception as e:
        print(e)
        return redirect('dashboard')

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def faq_delete(request):

    if request.method == "POST":
        data = request.POST.get
        print("Hiiiiii")
        print("data---",request.POST['id'])
        faq_data = FAQ.objects.get(pk=request.POST['id'])
        print("FAQ Data",faq_data)
        # faq_data.delete()
        faq_data.is_active = False
        faq_data.save()
        return redirect('add_faq')

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def edit_faq(request,id):
    try:
        if request.method == "GET":
            data = request.POST.get
            faq_data = FAQ.objects.get(id=id)
            return render(request=request, template_name='curioo_admin/faq.html',context={'faq_class_active': True, 'faq_data': faq_data})
    except Exception as identifier:
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='Invalid Request')

    try:
        if request.method == "POST":
            data = request.POST.get
            if FAQ.objects.filter(question__iexact=data('question'),answer__iexact=data('answer')).exists():
                msg = "FAQ already exists in this subject."
                return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
            update_data = FAQ.objects.filter(id=data('id')).update(question=data('question'),answer=data('answer'))
            get_update_data = FAQ.objects.get(id=data('id'))
            return JsonResponse(status=status.HTTP_200_OK, data={'success_text': "FAQ update successfully",'question': get_update_data.question, 'answer': get_update_data.answer,'faq_class_active': True})
    except Exception as identifier:
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='Invalid Request')


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def notifications(request):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')
    try:
        if user_type == "admin":
            notifications_seen = AdminNotification.objects.filter(admin=user).order_by("-created_at")
            # announcements = Announcement.objects.all().order_by("-created_at")
        elif user_type == "principal":
            notifications_seen = AdminNotification.objects.filter(principal=user).order_by("-created_at")
            # announcements = Announcement.objects.all()
        else:
            notifications_seen = AdminNotification.objects.all().order_by("-created_at")
            # announcements = Announcement.objects.all().order_by("-created_at")

        # sorted_qs = sorted(chain(notifications_seen, announcements), key=lambda obj: obj.created_at, reverse=True)
        # print(sorted_qs)

        context = {"notifications_seen":notifications_seen}

        for notification in notifications_seen:
            print(hasattr(notification, 'notification'))
            notification.is_seen = True
            notification.save()
    except Exception as e:
        print(e)
    return render(request, template_name='curioo_admin/notification.html', context=context)


@api_view(['GET', 'POST'])
@permission_classes([AllowAny, ])
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def upload_csv(request):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    print(" sgdvabes", request.POST['csv_data'].split("\n")[:-1])
    csv_data = request.POST['csv_data'].split("\n")[:-1]
    try:
        for i in range(len(csv_data)):
            if i == 0:
                name_row = csv_data[i].split(",")
                name_row[-1] = name_row[-1].strip("\r")
                for text in range(len(name_row)):
                    if name_row[text] == "" or name_row[text] == "\r":
                        error = "Header Invalid"
                        return Response(error)
                print("name row",name_row)
            else:
                row = csv_data[i].split(",")
                data = []
                row[-1] = row[-1].strip("\r")
                for text in range(len(row)):
                    if row[text] == "" or row[text] == "\r":
                        error = str(name_row[text])+" Empty entry in row "+str(i)
                        return Response(error)
                    else:
                        print("row",row)
                        data.append(row[text])
                data_dict = {}
                try:
                    data_dict["studio"] = data[8]
                    data_dict["full_name"] = data[0]
                    data_dict["father_name"] = data[10]
                    data_dict["father_phone"] = int(data[11])
                    data_dict["father_email"] = data[12]
                    data_dict["mother_name"] = data[13]
                    data_dict["mother_phone"] = int(data[14])
                    data_dict["mother_email"] = data[15]
                    data_dict["dob"] = datetime.strptime(data[1], "%m/%d/%Y").strftime("%Y-%m-%d")
                    # data_dict["dob"] = data[1]
                    data_dict["doj"] = datetime.strptime(data[5], "%m/%d/%Y").strftime("%Y-%m-%d")
                    data_dict["expiry_date"] = datetime.strptime(data[6], "%m/%d/%Y").strftime("%Y-%m-%d")
                    data_dict["cbas_testDate"] = datetime.strptime(data[4], "%m/%d/%Y").strftime("%Y-%m-%d")
                    data_dict["gender"] = data[2]
                    data_dict["nationality"] = data[3]
                    data_dict["school_name"] = data[7]
                    data_dict["school_grade"] = int(data[9])
                except Exception as e:
                    print(e)
                    return Response("data invalid")
                # data_dict["image"] = "img/user-img-pr.png"
                studio_id = data_dict['studio']
                studio = Studio.objects.get(studio_code=studio_id)
                data_dict['studio'] = studio.pk
                data_dict['student_code'] = student_code(studio.studio_code, studio.id)
                print("data", data_dict)
                serializer = CsvStudentSerializer(data = data_dict)
                try:
                    if serializer.is_valid(raise_exception=True):
                        created_user = serializer.create(serializer.validated_data)
                        print('user : ', created_user)
                        created_user.added_by_id = user.id
                        created_user.added_by_type = user_type.capitalize()
                        created_user.save()
                    else:
                        print("Not valid")
                except Exception as e:
                    if created_user:
                        created_user.delete()
                    print(e)
                    return Response("data invalid")
                    
        print("endingggggggg")
        # context = {"hello" : "hello"}
        return Response("success")
    except Exception as e:
        print(e)
        return Response("data invalid")


class NotificationCount(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):

        try:
            name, user_type, user = gbl(request, request.session['token'])
        except Exception as e:
            print('inside exception : ', e)
            return redirect('login/')

        try:
            print("inside try")
            print(user_type, user)
            if user_type == 'admin':
                count = AdminNotification.objects.filter(admin=user, is_seen=False).count()
                print(count)
            elif user_type == "principal":
                count = AdminNotification.objects.filter(principal=user, is_seen=False).count()
            else:
                count = 0
            return Response(data={'count': count}, status=status.HTTP_200_OK)

        except Exception as e:
            print(e)
            return Response(data={"data": "unable to fetch notification count."}, status=status.HTTP_400_BAD_REQUEST)

class DateStudentFilterGraph(APIView):  
    permission_classes = (AllowAny,)

    def post(self,request):
        try:
            print("post123")
            import datetime
            from_date = request.POST['from_date']
            to_date = request.POST['to_date']
            sid = int(request.data['sid'])
            print("sid---",sid,type(sid))
            print("from date---",from_date,type(from_date), "to date---",to_date, type(to_date))

            from_date = from_date+' 00:00:00.000000'
            to_date = to_date+' 00:00:00.000000'

            from_date = datetime.datetime.strptime(from_date, '%Y-%m-%d %H:%M:%S.%f')
            to_date = datetime.datetime.strptime(to_date, '%Y-%m-%d %H:%M:%S.%f')

            print("from date--1-",from_date,type(from_date), "to date---",to_date, type(to_date))

            print("from_date------------",from_date, type(from_date))
            print("to_date------------",to_date, type(to_date))

            if Student.objects.filter(id=sid).exists():
                sobj = Student.objects.get(id=sid)

                allReport = StudentReportData.objects.filter(created_at__range = (from_date,to_date),student=sobj)
                print("allreport----",allReport)
                
                behaviour_data = {}
                sub_data = {}

                for r1 in allReport:
                    # print("r1-----",r1.student)
                    # behaviour_data['student_'+str(r1.student.id)] = {}
                    # behaviour_data['student_'+str(r1.student.id)] = BehaviourData.objects.filter(student=r1.student)

                    behaviour_data = BehaviourData.objects.filter(student=r1.student)
                    print("subjecbehaviour_datatObj----",behaviour_data)
                    for obj in behaviour_data:
                        sub_data[obj.subject.id] = obj.subject.subject_name
                
                print("behaviour_data-----",behaviour_data)
                print('subjectObj',sub_data)


                motivational_dict = {}
                labels_dict = {}
                default_items_dict = {}
                wellbeing_dict = {}


                # for key,value in behaviour_data.items():
                    # print("key----",key,'value---',value)


                #For pie chart and subject chart
                for behaviour in behaviour_data:
                    default_items_dict[behaviour.subject.id] = {}
                    labels_dict[behaviour.subject.id] = []
                for behaviour in behaviour_data:
                    default_items_dict[behaviour.subject.id][behaviour.behaviour.behaviour] = []
                    labels_dict[behaviour.subject.id].append(behaviour.behaviour.behaviour)
                for behaviour in behaviour_data:
                    default_items_dict[behaviour.subject.id][behaviour.behaviour.behaviour].append(behaviour.value)
                for sub in default_items_dict:
                    for behav in default_items_dict[sub]:
                        default_items_dict[sub][behav] = sum(default_items_dict[sub][behav])/len(default_items_dict[sub][behav])
                print("default_items_dict", default_items_dict)


                #For motivational chart
                for student in allReport:
                    # print("student---",student)
                    if student.subject.id in motivational_dict:
                        motivational_dict[student.subject.subject_name].append(student.over_all_rating.value)
                    else:
                        motivational_dict[student.subject.subject_name] = []
                        motivational_dict[student.subject.subject_name].append(student.over_all_rating.value)
                    
                    if student.subject.id in wellbeing_dict:
                        wellbeing_dict[student.subject.subject_name].append(student.well_being.value)
                    else:
                        wellbeing_dict[student.subject.subject_name] = []
                        wellbeing_dict[student.subject.subject_name].append(student.well_being.value)
                
                for student in motivational_dict:
                    motivational_dict[student] = sum(motivational_dict[student])/len(motivational_dict[student])
                for student in wellbeing_dict:
                    wellbeing_dict[student] = sum(wellbeing_dict[student])/len(wellbeing_dict[student])+1
                
                print("motivational_dict", motivational_dict)
                print("wellbeing_dict", wellbeing_dict)
                print("avg_default_items_dict--",default_items_dict)
                print("labels_dict---",labels_dict)
                print('===============',len(motivational_dict))
                # if len(motivational_dict) == 0 and 
                data = {
                        "motivation": motivational_dict,
                        "labels": labels_dict,
                        "default": default_items_dict,
                        "wellbeing": wellbeing_dict,
                        'subjectObj':sub_data
                }
                return Response(data)
            

        except Exception as e:
            print("error ----",e)
            return Response(data=error_message(400,"error",str(e),[]), status=status.HTTP_400_BAD_REQUEST)

    
    def get(self,request):
        print("get")
