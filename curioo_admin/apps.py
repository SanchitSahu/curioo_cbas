from django.apps import AppConfig


class CuriooAdminConfig(AppConfig):
    name = 'curioo_admin'
