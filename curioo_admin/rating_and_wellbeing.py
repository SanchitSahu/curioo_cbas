from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.decorators.cache import cache_control
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from curioo_admin.models import OverallRatingScale, WellBeingScale
from teacher.getUser import gbl


class RatingAndWellBeing(APIView):
    permission_classes = (AllowAny,)

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, request):
        try:
            try:
                name, user_type, user = gbl(request, request.session['token'])
            except Exception as e:
                print('inside exception : ', e)
                return redirect('login/')

            data = request.GET
            param = data.get('param')
            if param == "Rating":
                objects = OverallRatingScale.objects.filter(is_active=True)
                ret_data = [{'id': obj.id, 'name': obj.overall_rating, 'value':obj.value} for obj in objects]
            elif param == "Wellbeing":
                objects = WellBeingScale.objects.filter(is_active=True)
                ret_data = [{'id': obj.id, 'name': obj.well_being_scale, 'value':obj.value} for obj in objects]
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST, data={})
            return Response(status=status.HTTP_200_OK, data={'data': ret_data})
        except Exception as e:
            print('exception : ', e)
            return Response(status=status.HTTP_400_BAD_REQUEST, data={})

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def post(self, request):
        try:
            name, user_type, user = gbl(request, request.session['token'])
        except Exception as e:
            print('inside exception : ', e)
            return redirect('login/')
        try:
            data = request.POST
            print('data : ', data)
            param = data.get('param')
            criteria = data.get('criteria')
            value = data.get('value')
            if param == "Rating":
                if OverallRatingScale.objects.filter(overall_rating__iexact=criteria).exists():
                    msg = "Rating criteria already exists."
                    return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
                if OverallRatingScale.objects.filter(value=value).exists():
                    msg = "Rating criteria value already exists."
                    return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
                OverallRatingScale.objects.create(overall_rating = criteria, value=value)
            elif param == "Wellbeing":
                if WellBeingScale.objects.filter(well_being_scale__iexact=criteria).exists():
                    msg = "Well being criteria already exists."
                    return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
                if WellBeingScale.objects.filter(value=value).exists():
                    msg = "Well being criteria value already exists."
                    return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
                WellBeingScale.objects.create(well_being_scale=criteria, value=value)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST, data={'data': "Incorrect data"})
            return Response(status=status.HTTP_200_OK, data={})

        except Exception as e:
            print('exception : ', e)
            return Response(status=status.HTTP_400_BAD_REQUEST, data={})

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def put(self, request):
        try:
            name, user_type, user = gbl(request, request.session['token'])
        except Exception as e:
            print('inside exception : ', e)
            return redirect('login/')
        try:
            data = request.POST
            print('data put : ', data)
            param = data.get('param')
            existing_id = data.get('id')
            criteria = data['criteria']
            value = data.get('value')
            if param == "Rating":
                obj_rating = OverallRatingScale.objects.get(id=existing_id)
                if obj_rating.overall_rating != criteria:
                    if OverallRatingScale.objects.filter(overall_rating__iexact=criteria).exists():
                        msg = "Overall rating criteria already exists."
                        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
                if obj_rating.value != int(value):
                    if OverallRatingScale.objects.filter(value=value).exists():
                        msg = "Overall rating criteria value already exists."
                        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
                print('enside rating', param, existing_id, criteria)
                OverallRatingScale.objects.filter(id=existing_id).update(overall_rating=criteria, value=value)
            elif param == "Wellbeing":
                well_rating = WellBeingScale.objects.get(id=existing_id)
                if well_rating.well_being_scale != criteria:
                    if WellBeingScale.objects.filter(well_being_scale__iexact=criteria).exists():
                        msg = "Well being criteria already exists."
                        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
                if well_rating.value != int(value):
                    if WellBeingScale.objects.filter(value=value).exists():
                        msg = "Well being criteria value already exists."
                        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
                WellBeingScale.objects.filter(id=existing_id).update(well_being_scale=criteria, value=value)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST, data="Bad Request")
            return Response(status=status.HTTP_200_OK, data={})

        except Exception as e:
            print('exception : ', e)
            return Response(status=status.HTTP_400_BAD_REQUEST, data="Bad Request")

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def delete(self, request):
        try:
            name, user_type, user = gbl(request, request.session['token'])
        except Exception as e:
            print('inside exception : ', e)
            return redirect('login/')
        try:
            data = request.POST
            print('data : ', data)
            param = data.get('param')
            existing_id = data.get('id')
            if param == "Rating":
                OverallRatingScale.objects.filter(id=existing_id).update(is_active=False)
            elif param == "Wellbeing":
                WellBeingScale.objects.filter(id=existing_id).update(is_active=False)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST, data="Please pass 'param' parameter")
            return Response(status=status.HTTP_200_OK, data={})

        except Exception as e:
            print('exception : ', e)
            return Response(status=status.HTTP_400_BAD_REQUEST, data="Please pass required parameter")
