"""
A urls.py file for curioo_admin app which have prefix of 'admin' in root urls.py.
"""
from django.conf.urls.static import static
from django.urls import path
from rest_framework.routers import DefaultRouter
from curioo_cbas import settings

from curioo_admin.lead import lead_list, add_lead, view_lead, delete_lead, edit_lead, edit_contact_lead, convert_lead, \
    LeadListApi
from curioo_admin.levels import level_list, level_add, level_put, level_delete, LevelListApi

from curioo_admin.rating_and_wellbeing import RatingAndWellBeing
from curioo_admin.subject_behaviour import subject_behaviour_list, specific_subject_id, subject_behaviour_edit, \
    subject_behaviour_delete, subject_behaviour_add
from curioo_admin.views import dashboard, reset_password, password, change_password, Students, \
  view_students, comments, profile, delete_students, add_student, get_users, edit_student, edit_student_contact, \
  StudentFilterAPI, notifications, upload_csv, NotificationCount, faq, faq_delete, edit_faq , DateStudentFilterGraph, delete_comment
from curioo_admin.teacher import teachers_list, add_teacher, view_teacher, edit_teacher, delete_teacher, TeacherListApi, ViewTeacherReport, ChartTeacherData, SubwiseFeedback, SubjectTeacherChart, TeacherFeedback, GetTeacherStudioWise
from curioo_admin.pdf_download import download_students
from .teacher import *
# router = DefaultRouter()
# router.register('scale', RatingAndWellBeing)

urlpatterns = [
  # path('', login, name='admin/'),
  # path('logout', logout, name='admin/logout'),

  path('dashboard', dashboard, name='admin/dashboard'),
  path('changePassword', change_password, name='admin/changePassword/'),

  path('resetPassword/', reset_password, name='admin/resetPassword/'),
  path('password/<str:uidb64>/<str:user_type>', password, name='admin/password/'),

  path('comments/<str:role>/<int:commented_on_id>', comments, name='admin/comments'),
  path('commentDelete/<int:comment_id>', delete_comment, name='admin/commentDelete/'),


  path('students', Students.as_view(), name='admin/students'),
  path('student', add_student, name='admin/student'),

  path('students/<int:student_id>', view_students, name='admin/students/'),
  path('student/<int:student_id>/', edit_student, name='admin/student/'),
  path('editStudentContact/<int:student_id>/', edit_student_contact, name='admin/editStudentContact/'),
  path('deleteStudent/<int:student_id>', delete_students, name='admin/deleteStudent/'),
  path('student-api', StudentFilterAPI.as_view(), name='admin/student-api'),

  path('teachers/', teachers_list, name='admin/teachers'),
  path('teacher', add_teacher, name='admin/teacher'),
  path('teacher/<int:teacher_id>', view_teacher, name='admin/teacher'),
  path('teacher/<int:teacher_id>/', edit_teacher, name='admin/teacher/'),
  path('teacherDelete/<int:teacher_id>', delete_teacher, name='admin/teacherDelete/'),
  path('teachers-api', TeacherListApi.as_view(), name='admin/teachers-api'),

  path('behaviours', subject_behaviour_list, name='admin/behaviours'),
  path('behaviour/<int:subject_id>', specific_subject_id, name='behaviour/'),
  path('addBehaviour/<int:subject_id>', subject_behaviour_add, name='admin/addBehaviour/'),
  path('editBehaviour/<int:behaviour_id>', subject_behaviour_edit, name='admin/editBehaviour/'),
  path('deleteBehaviour/<int:behaviour_id>', subject_behaviour_delete, name='admin/deleteBehaviour/'),
  path('scale', RatingAndWellBeing.as_view(), name='admin/scale'),
  path('levels', level_list, name="levels"),
  path('level', level_add),
  path('level/<int:level_id>/', level_put),
  path('level/<int:level_id>', level_delete),
  path('level-api', LevelListApi.as_view(), name='admin/level-api'),

  path('leads', lead_list, name='admin/leads'),
  path('lead/', add_lead, name='admin/lead/'),
  path('leadConvert/<int:lead_id>', convert_lead, name='admin/leadConvert/'),
  path('leadDelete/<int:lead_id>', delete_lead, name='admin/leadDelete/'),
  path('lead/<int:lead_id>', view_lead, name='admin/lead'),
  path('lead/<int:lead_id>/', edit_lead, name='admin/lead/'),
  path('leadContact/<int:lead_id>', edit_contact_lead, name='admin/leadContact'),
  path('leads-api', LeadListApi.as_view(), name='admin/leads-api'),

  path('uploadCsv', upload_csv, name='admin/uploadCsv'),

  path('profile', profile, name='admin/profile'),
  path('users', get_users, name='admin/users'),
  path('faq', faq, name='add_faq'),
  path('faq_delete', faq_delete, name='faq_delete'),
  path('edit_faq/<int:id>', edit_faq, name='edit_faq_req'),

  path('notifications', notifications, name="notifications"),
  path('notifications-api', NotificationCount.as_view(), name="notifications-api"),

  path('download_report/<int:student_id>/', download_students, name="admin/download_report"),
  path('filter/graph/',DateStudentFilterGraph.as_view(), name="filter/graph/"),


  # All Teacher Graph
  path('teacher/reports', ViewTeacherReport, name="admin/teacher/reports"),
  path('teacher/charts/', ChartTeacherData),
  path('teacher/feedback/', SubwiseFeedback),

  # particular teacher
  path('teacher/feedback/<str:tid>', TeacherFeedback),
  path('teacher/feedback/charts/', SubjectTeacherChart),

  # get teacher studion on change
  path('teacher/report/studio/', GetTeacherStudioWise),
  path('teacher/chart/pie/', GetPieData),
  path('teacher/chart/bar/', GetBarData),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
