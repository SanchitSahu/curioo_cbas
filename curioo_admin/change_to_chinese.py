from django import shortcuts
from django.contrib.auth import middleware
from django.http import HttpResponse
from django.shortcuts import redirect
from django.utils import translation
from django.utils.translation import activate
from django.views.i18n import set_language
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from django.utils.translation import trans_real as translation, LANGUAGE_SESSION_KEY
from django.utils.translation import LANGUAGE_SESSION_KEY
from curioo_cbas import settings
from teacher.getUser import gbl
from django.middleware.locale import LocaleMiddleware
from django.utils.translation import ugettext_lazy as _


class Chinese(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        try:
            name, user_type, user = gbl(request, request.session['token'])
        except Exception as e:
            print('inside exception : ', e)
            return redirect('login/')

        try:
            middleware = LocaleMiddleware()
            data = request.POST
            print('request.COOKIES : ', request.session[LANGUAGE_SESSION_KEY])
            print('English in exiting lan : ', _('English'))

            print('data : ', data['selected_language'])
            # print('translation.LANGUAGE_SESSION_KEY : ', translation.LANGUAGE_SESSION_KEY)
            # print('request.LANGUAGE_CODE : ', request.LANGUAGE_CODE)
            if data['selected_language'] == "zh-hans":
                print('inside chinese')
                request.session[LANGUAGE_SESSION_KEY] = 'zh-hans'
                middleware.process_request(request)
                activate('zh-hans')
            elif data['selected_language'] == "en":
                request.session[LANGUAGE_SESSION_KEY] = 'en'
                middleware.process_request(request)
                activate('en')
            else:
                pass
            return HttpResponse(status=status.HTTP_200_OK)
        except Exception as e:
            print('exception : ', e)
