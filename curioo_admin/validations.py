from datetime import date


def calculate_age(dob):
    today = date.today()
    try:
        birthday = dob.replace(year=today.year)
    except ValueError:
        birthday = dob.replace(year=today.year, day=dob.day - 1)
    if birthday > today:
        return today.year - dob.year - 1
    else:
        return today.year - dob.year
