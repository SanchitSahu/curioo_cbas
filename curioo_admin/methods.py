import json
import logging
from datetime import datetime

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.cache import cache_control
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from curioo_cbas import settings
from teacher.getUser import gbl
from teacher.status_message import error_message
from .validations import calculate_age

logger = logging.getLogger(__name__)
from .models import *

from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core import mail
import random
import string
from random import randint


#######################################################################################
# ADMIN ANNOUNCEMENT ##################################################################
#######################################################################################

@method_decorator(csrf_exempt, name='dispatch')
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def announcement(request):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        return redirect('login/')
    if request.method == "GET":
        try:
            if user.login_type == 'principal':
                data = Announcement.objects.filter(principal_id=user.id)
            if user.login_type == 'admin':
                data = Announcement.objects.filter(admin_id=user.id)

            return render(request, "curioo_admin/Announcement.html",
                          {"announcement": data, 'announcement_class_active': True})
        except:
            redirect("dashboard")
    elif request.method == "POST":
        try:
            if request.is_ajax():
                data = request.POST.get("announcement")

                if user.login_type == 'principal':
                    principal_datail = Principal.objects.get(id=user.id)
                    obj=Announcement.objects.create(announcement=data,principal_id=user.id,studio_id=principal_datail.studio.id)
                    principals = Principal.objects.filter(studio_id=principal_datail.studio.id,is_active=True)
                    teachers = Teacher.objects.filter(studio_id=principal_datail.studio.id,is_active=True)
                    learning_consultants = LearningConsultant.objects.filter(studio_id=principal_datail.studio.id,is_active=True)
                    for principal in principals:
                        AdminNotification.objects.create(notification=data,principal=principal,is_announcement=True,announcement=obj)
                    # for teacher in teachers:
                    #     WebNotification.objects.create(notification=data,respective_user_type=teacher.login_type,respective_user_id=teacher.id)
                        
                if user.login_type == 'admin':
                    obj=Announcement.objects.create(announcement=data,admin_id=user.id)
                    admins = Admin.objects.all()
                    principals = Principal.objects.all()
                    for principal in principals:
                        AdminNotification.objects.create(notification=data,principal=principal,is_announcement=True,announcement=obj)
                        
                    for admin in admins:
                        AdminNotification.objects.create(notification=data, admin=admin,is_announcement=True,announcement=obj)
                # obj=Announcement.objects.create(announcement=data,user_id=user.id,login_type=user.login_type)
                
                # if user.log

                
                

                logger.info('Announcement Done')
                return HttpResponse(f"success")
        except Exception as e:
            redirect("admin/dashboard")


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def announcement_list(request):

    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        return redirect('login/')
    try:
        data = Announcement.objects.all()
        return render(request, "Announcement.html", {"data": data, 'announcement_class_active': True})
    except Exception as e:
        redirect("admin/dashboard")


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def announcements_delete(request, id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')
    try:
        data = Announcement.objects.get(id=id).delete()

        return redirect("announcement")
    except Exception as e:
        redirect("admin/dashboard")


#############################################################################################
# PRINCIPAL #################################################################################
#############################################################################################

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def principal(request):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')
    try:
        if request.method == "GET":
            studio = Studio.objects.all().filter(is_active=True, status='Active')
            majors = Major.objects.filter(is_active=True)
            return render(request, "curioo_admin/addNewPrincipal.html", {"studio": studio,'majors':majors, 'principal_class': True})
        elif request.method == "POST":
            try:
                pair = request.POST.get
                image = request.FILES.get('image')
                if Principal.objects.filter(email=pair('email')).exists() or \
                    LearningConsultant.objects.filter(email=pair('email')).exists() or \
                    Admin.objects.filter(email=pair('email')).exists() or Teacher.objects.filter(email=pair('email')).exists():
                    msg = "email address already exists."
                    return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
                else:
                    try:
                        # major_obj = Major.objects.get(id=pair('education_major'))
                        image = request.FILES['image']
                        from curioo_admin.teacher import employee_code
                        studio_obj = Studio.objects.get(id=int(pair('studioId')))
                        employee_code = employee_code(pair('studioId'), studio_obj.studio_code)
                        principal = Principal.objects.create(principal_code=employee_code,
                            full_name=pair('name'), phone=pair('number'),
                            email=pair('email'),
                            dob=pair('dob'),
                            doj=pair('doj'), image=image, nationality=pair("nationality"),
                            position=pair("position"), positionId=pair("positionId"),
                            studio_id=pair("studioId"),
                            highest_education=pair('highest_education'),
                            education_major=pair('education_major'),
                            emergency_contact_name=pair("emr_name"), emergency_number=pair("emergency_number"),
                            gender=pair("gender"))
                        try:
                            user_code = "".join(random.choices(string.ascii_uppercase + string.digits, k=20)) + "_u_p0" + str(
                                principal.id)
                            current_site = get_current_site(request)
                            domain = current_site.domain
                            url = f"http://{domain}/setPassword/" + user_code
                        
                            subject = 'Set Password Link for CURIOO'
                            html_message = render_to_string('teacher/mail.html', {'first_name': principal.full_name, 'url': url, 'studio': principal.studio.name,
                                                                                'password': principal.password, 'appointed_role': 'Principal', 'role_creation': True})
                            plain_message = strip_tags(html_message)
                            from_email = settings.DEFAULT_FROM_EMAIL
                            to = principal.email
                            mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)
                        except Exception as error:
                            print("error is",error)
                            return HttpResponse(content=error_message(511,"error","Please check your network connection.",{}), status=status.HTTP_511_NETWORK_AUTHENTICATION_REQUIRED)
                        msg = "A principal is created successfully."
                        return HttpResponse(status=status.HTTP_200_OK, content=msg)     
                    except Exception as e:
                        print(e)
                        msg = "Not create some went wrong."
                        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)     
            except Exception as e:
                print(e)
                redirect("dashboard")
    except Exception as e:
        redirect("admin/dashboard")


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def principals_list(request):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')
    try:
        principal = Principal.objects.all().filter(is_active=True)
        return render(request, "curioo_admin/principal.html", {"principal": principal, 'principal_class': True})
    except Exception as e:
        redirect("admin/dashboard")


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def principal_details(request, id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')
    try:
        details = Principal.objects.get(id=id)
        return render(request, "curioo_admin/principalDetail.html", {"details": details, 'principal_class': True})
    except Exception as e:
        redirect("admin/dashboard")


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def principal_edit(request, id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')
    try:
        if request.method == "GET":
            details = Principal.objects.get(id=id)
            studio = Studio.objects.all().filter(is_active=True, status='Active')
            majors = Major.objects.filter(is_active=True)
            return render(request, "curioo_admin/editPrincipalDeatils.html",
                          {"details": details, "studio": studio,'majors':majors, 'principal_class': True})
        elif request.method == "POST":
            try:
                details = Principal.objects.get(id=id)
                pair = request.POST.get
                image = request.FILES.get('image')
                if image != None:
                    print("in none")
                    details.image = image
                    details.save()
                final_data = Principal.objects.filter(id=details.id).update(
                    full_name=pair('name'), phone=pair('number'),
                    address=pair('address'), email=pair('email'), doj=pair('doj'), dob=pair('dob'),
                    nationality=pair("nationality"),
                    position=pair("position"), positionId=pair("positionId"), studio_id=pair("studioId"),
                    highest_education=pair('highest_education'), education_major=pair("education_major"),
                    emergency_contact_name=pair("emr_name"), emergency_number=pair("emargency_number"),
                    gender=pair("gender"))

                return render(request, "curioo_admin/editPrincipalDeatils.html",
                              {"details": details, "success":True, 'principal_class': True})
            except Exception as e:
                details = Principal.objects.get(id=id)
                studio = Studio.objects.all().filter(is_active=True, status='Active')
                majors = Major.objects.filter(is_active=True)
                return render(request, "curioo_admin/editPrincipalDeatils.html",
                              {"details": details, "studio": studio, 'majors': majors,'error':"Email id already Exists!",'error_is':True, 'principal_class': True})

    except Exception as e:
        redirect("admin/dashboard")


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def principal_delete(request, id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')
    try:
        data = Principal.objects.filter(id=id).update(is_active=False)
        return redirect("principals_list")
    except Exception as e:
        redirect("admin/dashboard")


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def passwd(request):
    return render(request, 'principal_passwd.html')


##########################################################################################################
# LEARNING CONSULTANT ####################################################################################
##########################################################################################################

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def learning_consultant(request):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        return redirect('login/')
    if request.method == "GET":
        if user_type == "principal" or user_type == "Principal":
            studio_details = Studio.objects.all().filter(is_active=True, status='Active')
            levels = Level.objects.filter(is_active=True)
            majors = Major.objects.filter(is_active=True)
            return render(request, 'curioo_admin/addNewLc.html',
                      {'studio_details': studio_details, 'user': user, 'learning_consultant_class': True, "levels": levels, "majors": majors, "error_is": False})
        studio_details = Studio.objects.all().filter(is_active=True, status='Active')
        levels = Level.objects.filter(is_active=True)
        majors = Major.objects.filter(is_active=True)
        return render(request, 'curioo_admin/addNewLc.html',
                      {'studio_details': studio_details, 'learning_consultant_class': True, "levels": levels, "majors": majors, "error_is": False, 'user': user})

    elif request.method == "POST":
        try:
            consultant = request.POST.get
            image = request.FILES.get
            if LearningConsultant.objects.filter(email=consultant('email')).exists() or \
                    Admin.objects.filter(email=consultant('email')).exists() or \
                    Principal.objects.filter(email=consultant('email')).exists() or \
                    Teacher.objects.filter(email=consultant('email')).exists():
                

                studio_details = Studio.objects.all().filter(is_active=True, status='Active')
                levels = Level.objects.filter(is_active=True)
                majors = Major.objects.filter(is_active=True)
                # return render(request, 'curioo_admin/addNewLc.html',
                            #   {'error': error, 'studio_details': studio_details, 'learning_consultant_class': True, "levels": levels, "majors": majors, "error_is": True, 'user': user})
                msg = "User with this email already exists."
                return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
            elif LearningConsultant.objects.filter(positionId=consultant('positionId')).exists():
                
                studio_details = Studio.objects.all().filter(is_active=True, status='Active')
                levels = Level.objects.filter(is_active=True)
                majors = Major.objects.filter(is_active=True)
                # return render(request, 'curioo_admin/addNewLc.html',
                            #   {'error': error, 'studio_details': studio_details, 'learning_consultant_class': True, "levels": levels, "majors": majors, "error_is": True, 'user': user})
                msg = "Learning Consultant with this Position Id already exists."
                return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
            else:
                studio_id = int(consultant('studioId'))
                print('request.POST ', request.POST, '\nstudio_id : ', studio_id)
                studio_obj = Studio.objects.get(id=studio_id)
                print('studio_obj : ', studio_obj)
                from curioo_admin.teacher import employee_code
                employee_codee = employee_code(studio_id, studio_obj.studio_code)
                print('employee_codee: ', employee_codee)
                Learningconsultant=LearningConsultant.objects.create(learning_consultant_code=employee_codee,
                    full_name=consultant('name'), doj=consultant('date_of_joining'),
                                                  email=consultant('email'), phone=consultant('contact'),
                                                  address=consultant('address'),
                                                  dob=consultant('dob'), image=image('image'),
                                                  nationality=consultant("nationality"),
                                                  position=consultant("position"), positionId=consultant("positionId"),
                                                  studio_id=consultant("studioId"),
                                                  highest_education=consultant('highest_education'),
                                                  education_major=consultant("education_major"),
                                                  emergency_contact_name=consultant("emr_name"),
                                                  emergency_number=consultant("emr_number"),
                                                  gender=consultant("gender"))

                Principals = Principal.objects.filter(is_active=True, studio=Learningconsultant.studio)
                Admins = Admin.objects.all()
                notification_msg = "A new learning consultant was added in " + Learningconsultant.studio.name + " Studio."

                for Principal_obj in Principals:
                    AdminNotification.objects.create(notification=notification_msg, principal=Principal_obj)

                for Admin_obj in Admins:
                    AdminNotification.objects.create(notification=notification_msg, admin=Admin_obj)

                try:
                    user_code = "".join(random.choices(string.ascii_uppercase + string.digits, k=20)) + "_u_l0" + str(
                        Learningconsultant.id)
                    current_site = get_current_site(request)
                    domain = current_site.domain
                    url = f"http://{domain}/resetPassword/" + user_code
                    subject = 'Forget Password Link for CURIOO'
                    html_message = render_to_string('teacher/mail.html',
                                                    {'first_name': Learningconsultant.full_name, 'url': url, 'studio': Learningconsultant.studio.name,
                                                    'password': Learningconsultant.password, 'appointed_role': 'Learning Consultant', 'role_creation': True})
                    plain_message = strip_tags(html_message)
                    from_email = settings.DEFAULT_FROM_EMAIL
                    to = Learningconsultant.email
                    mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)
                
                except Exception as error:
                    print("error in forgot mail-----",error)
                    return Response(data=error_message(511,"error","Please check your network connection.",{}), status=status.HTTP_511_NETWORK_AUTHENTICATION_REQUIRED)
                msg = 'A learning consoluatnt is created successfully.'
                return HttpResponse(status=status.HTTP_200_OK, content=msg)
        except Exception as e:
            print(e)
            redirect("admin/dashboard")



@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def learning_consultant_list(request):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        return redirect('login/')
    try:
        if user_type == "principal":
            studio_detail = Studio.objects.all().filter(is_active=True, status='Active')
            lc_details = LearningConsultant.objects.select_related('studio').filter(is_active=True, studio=user.studio.id)

            context = {
                "lc_details": lc_details,
                "studio_detail": studio_detail,
                'learning_consultant_class': True
            }
            return render(request, 'curioo_admin/learning_consultant.html', context)
        else:
            studio_detail = Studio.objects.all().filter(is_active=True, status='Active')
            lc_details = LearningConsultant.objects.select_related('studio').filter(is_active=True)

            context = {
                "lc_details": lc_details,
                "studio_detail": studio_detail,
                'learning_consultant_class': True
            }
            return render(request, 'curioo_admin/learning_consultant.html', context)
    except Exception as e:
        redirect("admin/dashboard")


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def learning_consultant_details(request, id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        return redirect('login/')
    try:
        lc_details = LearningConsultant.objects.select_related('studio').get(id=id)

        if request.method == "GET":
            return render(request, 'curioo_admin/learning_consultant_details.html',
                          {"lc_details": lc_details, 'learning_consultant_class': True})
    except Exception as e:
        redirect("admin/dashboard")


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def learning_consultant_edit(request, id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        return redirect('login/')
    try:
        data = LearningConsultant.objects.get(id=id)
        studio_details = Studio.objects.all().filter(is_active=True, status='Active')
        levels = Level.objects.filter(is_active=True)
        majors = Major.objects.filter(is_active=True)
        # studio_list = []
        # for studio_detail in studio_details:
        #     studio_list.append(studio_detail.id)
        # studio_detail_zip = zip(studio_details,studio_list)
        if request.method == "GET":
            if user_type == "principal" or user_type == "Principal":
                studio_details = Studio.objects.all().filter(is_active=True, status='Active')
                levels = Level.objects.filter(is_active=True)
                majors = Major.objects.filter(is_active=True)
                return render(request, 'curioo_admin/edit_learning_consultant.html',
                        {"data": data, 'studio_details': studio_details, 'user':user, 'learning_consultant_class': True, 'error_is': False, 'levels': levels, 'majors': majors})
            # Higheducation_major_list = ['highschool','diploma','bachelor','masters','phd']
            return render(request, 'curioo_admin/edit_learning_consultant.html',
                          {"data": data, 'studio_details': studio_details, 'learning_consultant_class': True, 'levels': levels, 'majors': majors, 'error_is': False, 'user':user})

        elif request.method == "POST":
            consultant = request.POST.get
            print("data", consultant)
            print("lc", data.email, data.positionId)
            if LearningConsultant.objects.filter(email=consultant('email')).exists() and data.email != consultant('email'):
                error = "Learning Consultant with this email already exists."
                studio_details = Studio.objects.all().filter(is_active=True, status='Active')
                levels = Level.objects.filter(is_active=True)
                majors = Major.objects.filter(is_active=True)
                return render(request, 'curioo_admin/edit_learning_consultant.html',
                              {"data": data, 'studio_details': studio_details, 'learning_consultant_class': True,
                               'levels': levels, 'majors': majors, "error": error, 'error_is': True, 'success': False, 'user':user})
            elif LearningConsultant.objects.filter(positionId=consultant('positionId')).exists() and data.positionId != consultant('positionId'):
                error = "Learning Consultant with this Position Id already exists."
                studio_details = Studio.objects.all().filter(is_active=True, status='Active')
                levels = Level.objects.filter(is_active=True)
                majors = Major.objects.filter(is_active=True)
                return render(request, 'curioo_admin/edit_learning_consultant.html',
                              {"data": data, 'studio_details': studio_details, 'learning_consultant_class': True,
                               'levels': levels, 'majors': majors, 'error_is': True, 'success': False, "error": error, 'user':user})

            else:
                print("Hey")
                if user_type == "principal" or user_type == "Principal":
                    data_update = LearningConsultant.objects.filter(id=id).update(full_name=consultant('name'),
                                                                                  doj=consultant('date_of_joining'),
                                                                                  email=consultant('email'),
                                                                                  phone=consultant('contact'),
                                                                                  address=consultant('address'),
                                                                                  dob=consultant('date_of_birth'),
                                                                                  nationality=consultant("nationality"),
                                                                                  position=consultant("position"),
                                                                                  positionId=consultant("positionId"),
                                                                                  highest_education=consultant(
                                                                                      'highest_education'),
                                                                                  education_major=consultant(
                                                                                      "education_major"),
                                                                                  emergency_contact_name=consultant(
                                                                                      "emr_name"),
                                                                                  emergency_number=consultant(
                                                                                      "emr_number"),
                                                                                  gender=consultant("gender")
                                                                                  )
                else:
                    data_update = LearningConsultant.objects.filter(id=id).update(full_name=consultant('name'),
                                                                                  doj=consultant('date_of_joining'),
                                                                                  email=consultant('email'),
                                                                                  phone=consultant('contact'),
                                                                                  address=consultant('address'),
                                                                                  dob=consultant('date_of_birth'),
                                                                                  nationality=consultant("nationality"),
                                                                                  position=consultant("position"),
                                                                                  positionId=consultant("positionId"),
                                                                                  studio_id=consultant("studioId"),
                                                                                  highest_education=consultant(
                                                                                      'highest_education'),
                                                                                  education_major=consultant(
                                                                                      "education_major"),
                                                                                  emergency_contact_name=consultant(
                                                                                      "emr_name"),
                                                                                  emergency_number=consultant(
                                                                                      "emr_number"),
                                                                                  gender=consultant("gender")
                                                                                  )
                lc_image = request.FILES.get('image')
                if lc_image is not None:
                    lc_data = LearningConsultant.objects.get(id=id)
                    lc_data.image = lc_image
                    lc_data.save()
                print("data", data)
                return render(request, 'curioo_admin/edit_learning_consultant.html',
                              {"data": data, 'studio_details': studio_details, 'learning_consultant_class': True,
                               'levels': levels, 'majors': majors, 'error_is': False, 'success': True, 'user':user})
    except Exception as e:
        print(e)
        redirect("admin/dashboard")


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def learning_consultant_delete(request, id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')
    try:
        if request.method == 'POST':
            id = request.POST.get('id')

            lc_data = LearningConsultant.objects.filter(id=id).update(is_active=False)

            return redirect('list_learning_consultant')
    except Exception as e:
        redirect("admin/dashboard")


class learningConsultantListApi(APIView):
    permission_classes = (AllowAny,)

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, request):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            learning_studio = request.GET['studio']
            learning_obj = LearningConsultant.objects.filter(is_active=True)

            if learning_studio != "All Studio":
                learning_obj = learning_obj.filter(studio=learning_studio)

            data = [{"id": objs.id, "code": objs.learning_consultant_code, "name": objs.full_name,
                     "Studio": objs.studio.studio_code + ', ' + objs.studio.name + ', ' + objs.studio.city} for
                    objs in learning_obj]
            return Response(data={'data': data}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(data=error_message(400, "error", str(e), {}), status=status.HTTP_400_BAD_REQUEST)


######################################################################################################
# STUDIO #############################################################################################
######################################################################################################

@method_decorator(csrf_exempt, name='dispatch')
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def studio(request):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        return redirect('login/')
    try:
        if request.method == "GET":
            return render(request, "curioo_admin/studio.html", {'studio_class_active': True})
        elif request.method == "POST":
            studio = request.POST.get
            if Studio.objects.filter(email=studio('email')).exists():
                msg = "Email Already exists!!"
                return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg) 
                # return render(request, "curioo_admin/studio.html", {'error': error, 'studio_class_active': True})
            elif Studio.objects.filter(studio_code__iexact=studio('studio_code')).exists():
                msg = "Studio Code exists. Please try another code."
                return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg) 
            elif Studio.objects.filter(name=studio('name')).exists():
                msg = "Studio name exists. Please try another name."
                return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg) 
            else:
                data = Studio.objects.create(studio_code=studio('studio_code').upper(), name=studio('name'),
                                             country=studio('country'), state=studio('state'),
                                             location=studio('location'),
                                             city=studio('city'),
                                             opening_date=studio('o_date'), status=studio('status'),
                                             contact_name=studio('c_name'), contact_number=studio('c_number'),
                                             email=studio('email'), address=studio('address'))
                msg = "A studio is created successfully."
                return HttpResponse(status=status.HTTP_201_CREATED, content=msg)
    except Exception as e:
        print("e is =====>>>>>>",e)
        return redirect("admin/dashboard")


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def studio_list(request):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        return redirect('login/')
    try:

        studios = Studio.objects.all().filter(is_active=True)
        return render(request, "curioo_admin/studios.html", {"studios": studios, 'studio_class_active': True})
    except Exception as e:
        return redirect("admin/dashboard")


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def studio_edit(request, id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        return redirect('login/')
    try:
        data = Studio.objects.get(id=id)
        if request.method == "GET":
            studio_status = ['Active', 'Inactive', 'Future']
            return render(request, "curioo_admin/editStudio.html",
                          {"data": data, 'studio_status': studio_status, 'studio_class_active': True, "id": id})
        elif request.method == "POST":
            print("inside studio_edit")
            if request.is_ajax():
                studio = request.POST.get
                print("studio", studio)
                print(data.email, data.id)
                if data.email != studio('email'):
                    if Studio.objects.filter(email=studio('email')).exists():
                        msg = "Email Already exists. Please use another email."
                        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
                if data.studio_code != studio('studio_code'):
                    if Studio.objects.filter(studio_code__iexact=studio('studio_code')).exists():
                        msg = "Studio Code exists. Please try another code."
                        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)
                if data.name != studio('name'):
                    if Studio.objects.filter(name=studio('name')).exists():
                        msg = "Studio name exists. Please try another name."
                        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)

                data = Studio.objects.filter(id=id).update(studio_code=studio('studio_code'), name=studio('name'),
                                                           country=studio('country'), city=studio('city'),
                                                           state=studio('state'), location=studio('location'),
                                                           opening_date=studio('o_date'), status=studio('status'),
                                                           contact_name=studio('c_name'), contact_number=studio('c_number'),
                                                           email=studio('email'), address=studio('address'))
                print("hey",data)
            return HttpResponse(status=status.HTTP_200_OK, content="success")
    except Exception as e:
        print('exception : ', e)
        return redirect("admin/dashboard")


@method_decorator(csrf_exempt, name='dispatch')
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def studio_delete(request, id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        return redirect('login/')
    try:
        queryset = Studio.objects.filter(id=id).update(is_active=False)
        return redirect('studio_list')
    except Exception as e:
        return redirect("admin/dashboard")


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def studio_details(request, id):
    try:
        name, user_type, user = gbl(request, request.session['token'])

    except Exception as e:
        return redirect('login/')

    try:
        queryset = Studio.objects.get(id=id)
        teacher = Teacher.objects.filter(studio_id=id, is_active=True)
        student = Student.objects.filter(studio_id=id, is_active=True)
        # return render(request, "curioo_admin/studioDetail.html", {"data": queryset})
        dob_list = []
        for stud in student:
            dob = calculate_age(stud.dob)
            dob_list.append(dob)

        zipped_data = zip(student, dob_list)
        return render(request, "curioo_admin/studioDetail.html",
                      {"data": queryset, "teacher": teacher, "student": student, 'studio_class_active': True, "zipped_data": zipped_data})
    except Exception as e:
        print('exception e : ', e)
        return redirect("admin/dashboard")

###########################################################################################################
# SUBJECT #################################################################################################
###########################################################################################################


@method_decorator(csrf_exempt, name='dispatch')
class SubjectView(View):
    http_method_names = ['get', 'post']

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            data = Subject.objects.all().filter(is_active=True)
            return render(self.request, "curioo_admin/subjects.html", {"data": data, 'subject_class': True})
        except Exception as e:
            return redirect("admin/dashboard")

    @method_decorator(csrf_exempt)
    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def post(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            if self.request.is_ajax():
                subject_data = self.request.POST.get
                if Subject.objects.filter(subject_name__iexact=subject_data('subject')).exists():
                    return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='Subject with this name already exists')
                if Subject.objects.filter(subject_code__iexact=subject_data('subject_code')).exists():
                    return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='Subject with this shortcut code already exists')
                    
                # filtersubject_code = Subject.objects.get(subject_code = subject_data('subject_code')).filter(is_active=False)
                # filtersubject = Subject.objects.get(subject_name=subject_data('subject')).exists()

                

                # print("filtersubject",filtersubject_code)
                # print("filtersubject_code",filtersubject_code.subject_name)

                subject = Subject.objects.create(subject_code = subject_data('subject_code'),subject_name=subject_data('subject'))
                major = Major.objects.create(major=subject_data('subject'))
                response = {'status': 1, 'message': "success", 'url': 'subject_list', 'subject_class': True}
                return HttpResponse(json.dumps(response), content_type='application/json')
        except Exception as e:
            return redirect("admin/dashboard")


class SubjectListView(View):
    http_method_names = ['get']

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            data = Subject.objects.all().filter(is_active=True)
            return render(self.request, "subjectlist.html", {"data": data, 'subject_class': True})
        except Exception as e:
            return redirect("admin/dashboard")


class SubjectUpdateView(View):
    http_method_names = ['get', 'post']

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            data = Subject.objects.get(id=kwargs['id'])
            return render(self.request, "subject_edit.html", {"data": data, 'subject_class': True})
        except Exception as e:
            return redirect("admin/dashboard")

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def post(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            subject = self.request.POST.get
            print("In update ", subject)
            subject_obj = Subject.objects.get(id=kwargs['id'])
            if subject_obj.subject_name != subject('subject'):
                if Subject.objects.filter(subject_name__iexact=subject('subject')).exists():
                    return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='Subject with this name already exists')
            if subject_obj.subject_code != subject('code'):
                if Subject.objects.filter(subject_code__iexact=subject('code')).exists():
                    return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='Subject with this shortcut already exists')
            data = Subject.objects.filter(id=kwargs['id']).update(subject_name=subject('subject'), subject_code=subject('code'))
            data = Major.objects.filter(id=kwargs['id']).update(major=subject('subject'))
            response = {'status': 1, 'message': "success", 'url': 'subject_list', 'subject_class': True}
            return HttpResponse(json.dumps(response), content_type='application/json')
        except Exception as e:
            return redirect("admin/dashboard")


@method_decorator(csrf_exempt)
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def subject_delete(request, id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        return redirect('login/')
    try:
        queryset = Subject.objects.filter(id=id).update(is_active=False)
        queryset = Major.objects.filter(id=id).update(is_active=False)
        return redirect('subjects')
    except Exception as e:
        return redirect("admin/dashboard")


###########################################################################################################
# LESSONS #################################################################################################
##########################################################################################################
# To generate lesson code in system
def create_lesson_code(level_code, subjects_code):
    try:
        l =  Lesson.objects.filter(level__level_shortcut=level_code, subject__subject_code=subjects_code).count()
        l_code = int(l) + 1
        code = level_code + '-' + subjects_code + str(l_code)
    except Exception as e:
        code = level_code + '-' + subjects_code + str(1)
    return code.upper()


@method_decorator(csrf_exempt, name='dispatch')
class LessonView(View):
    http_method_names = ['get', 'post']

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            print('inside lesson get')

            if user_type == 'admin':
                subjects = Subject.objects.filter(is_active=True)
                levels = Level.objects.filter(is_active=True)
                lessons = Lesson.objects.all().filter(is_active=True)
                students = Student.objects.filter(is_active=True)
                studios = Studio.objects.all().filter(is_active=True, status='Active')
                return render(self.request, "curioo_admin/lesson.html",
                              {"subjects": subjects, "levels": levels, "lessons": lessons, "students": students,
                               'studios': studios, 'lesson_class': True, "user": user})
            if user_type == 'principal':
                subjects = Subject.objects.filter(is_active=True)
                levels = Level.objects.filter(is_active=True)
                lessons = Lesson.objects.filter(is_active=True, studio_id=user.studio.id)
                students = Student.objects.filter(is_active=True)
                studios = Studio.objects.all().filter(is_active=True, status='Active')
                return render(self.request, "curioo_admin/lesson.html",
                              {"subjects": subjects, "levels": levels, "lessons": lessons, "students": students,
                               'studios': studios, 'lesson_class': True, "user": user})
        except Exception as e:
            print("lesson view ",e)
            return redirect("admin/dashboard")

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def post(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:

            # def lesson_code():
            #     try:
            #         s = Lesson.objects.latest('id')
            #         return 'Le'+str(s.id + 1)
            #     except Exception as e:
            #         return 'Le'+str(1)
            
            # studio_id = int(consultant('studioId'))
            # print('request.POST ', request.POST, '\nstudio_id : ', studio_id)
            # studio_obj = Studio.objects.get(id=studio_id)
            # print('studio_obj : ', studio_obj)
            # from curioo_admin.teacher import employee_code
            # employee_codee = employee_code(studio_id, studio_obj.studio_code)
            


            print("inside lesson")
            lesson = self.request.POST.get
            subject_name = self.request.POST['subject']
            level_name = self.request.POST['level']
            studio_code = self.request.POST['studio']

            lesson_code = create_lesson_code(level_name, subject_name)
            data = Lesson.objects.create(lesson_code=lesson_code,
                                         level_id=Level.objects.get(level_shortcut=level_name).id,
                                         subject_id=Subject.objects.get(subject_code=subject_name).id,
                                         studio_id=Studio.objects.get(studio_code=studio_code).id)
            data.save()
            response = {'status': 1, 'message': "success", 'url': 'lesson', 'lesson_class': True}
            return HttpResponse(status=status.HTTP_200_OK, content='Lesson added successfully')
        except Exception as e:
            print('exception : ', e)
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content="Invalid Request")


class LessonDetailsView(View):
    http_method_names = ['get', "post"]

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            print("leson detail api view",e)
            return redirect('login/')
        try:
            queryset = Lesson.objects.get(id=kwargs["id"])
            if user_type == 'principal':
                students = Student.objects.filter(is_active=True, expiry_date__gt=datetime.now(), studio=user.studio)
            else:
                students = Student.objects.filter(is_active=True, expiry_date__gt=datetime.now())
            subjects = Subject.objects.filter(is_active=True)
            levels = Level.objects.filter(is_active=True)
            studios = Studio.objects.filter(is_active=True, status='Active')
            ages = []
            for x in queryset.student.all():
                ages.append(calculate_age(x.dob))
            zip_data = zip(queryset.student.all(), ages)
            total_students = queryset.student.all().count()
            return render(self.request, "curioo_admin/lessonDetails.html",
                          {"queryset": queryset, "zip_data": zip_data, "students": students, "subjects": subjects, "levels": levels,
                           "total_students": total_students, 'lesson_class': True, "studios":studios, "id": kwargs["id"], "user": user})
        except Exception as e:
            print("leson detail api view", e)
            return redirect("admin/dashboard")

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def post(self, *args, **kwargs):
        try:
            try:
                name, user_type, user = gbl(self.request, self.request.session['token'])
            except Exception as e:
                return redirect('login/')
            print("in try")
            dataobj= self.request.POST.get
            lesson_id = dataobj('add_std_id')
            Student_id = dataobj('lession_id')


            student = Student.objects.get(id=dataobj('add_std_id'))
            lesson = Lesson.objects.get(id=dataobj('lession_id'))

            # if Lesson.objects.filter(id=dataobj('lession_id')).exists():
                # return HttpResponse({'Error': 'Student already added.'})
            # else:
            lesson.student.add(student)
            lesson.save()
            lesson = self.request.POST.get
            data = Lesson.objects.filter(id=kwargs["id"]).update(level_id=lesson("level"), subject_id=lesson("subject"))
            return HttpResponse(status=status.HTTP_200_OK, content='Lesson updated successfully')
        except Exception as e:
            return redirect("admin/dashboard")


class LessonUpdateView(View):
    http_method_names = ['get', 'post']

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            print('inside exception : ', e)
            return redirect('login/')
        try:
            queryset = Lesson.objects.get(id=kwargs["id"])
            return render(self.request, "curioo_admin/lessonDetails.html", {"data": queryset, 'lesson_class': True})
        except Exception as e:
            return redirect("admin/dashboard")

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def post(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            lesson = self.request.POST.get
            print("in lesson update view")
            print("update_lesson id ",lesson('update_lesson_id'))
            print("level id ",lesson("level"))
            print("subject in try post", lesson("subject"))
            print("studio", lesson("studio_code"))
            
            data = Lesson.objects.filter(id=lesson('update_lesson_id')).update(level_id=lesson("level"), subject_id=lesson("subject"), studio_id=Studio.objects.get(studio_code=lesson("studio_code")).id)
            response = {'status': 1, 'message': "success", 'url': 'lessonList', 'lesson_class': True}
            # return HttpResponse(json.dumps(response), content_type='application/json')
            return HttpResponse(status=status.HTTP_200_OK, content={"message",'Update successfully'})
            # return redirect("lessonList")
        except Exception as e:
            return redirect("admin/dashboard")


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def delete_lesson(request, id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        return redirect('login/')
    try:
        queryset = Lesson.objects.filter(id=id).update(is_active=False)
        # queryset.delete()
        return redirect("lesson")
    except Exception as e:
        return redirect("admin/dashboard")


class lessonListApi(APIView):
    permission_classes = (AllowAny,)

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, request):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            print("lesson list api",e)
            return redirect('login/')
        try:
            level = request.GET['level']
            subject = request.GET['subject']
            studio_obj = request.GET['studio']
            lesson_all = Lesson.objects.all().filter(is_active=True)
            if studio_obj != 'All Studio':
                lesson_all = lesson_all.filter(studio=studio_obj)
            if level != "All Level":
                lesson_all = lesson_all.filter(level=level)
            if subject != "All Subject":
                lesson_all = lesson_all.filter(subject=subject)

            data = [{"studio_code": lesson.studio.studio_code, "id": lesson.id, "code": lesson.lesson_code, "name": lesson.subject.subject_name} for lesson in
                    lesson_all]
            
            return Response(data={'data': data}, status=status.HTTP_200_OK)

        except Exception as e:
            print("lesson list api",e)
            return redirect("admin/dashboard")


###########################################################################################
# SCHEDULE A NEW LESSON ###################################################################
###########################################################################################


class ScheduleCalanderView(View):

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self,request,*args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            lesson = Lesson.objects.filter(is_active=True, is_scheduled=False)
            if user_type == "principal":
                teacher = Teacher.objects.filter(studio=user.studio,is_active=True)
                studio = user.studio

            if user_type == "admin":
                teacher = Teacher.objects.filter(is_active=True)
                studio = Studio.objects.all().filter(is_active=True, status='Active')
            schedule = ScheduleClass.objects.filter(is_active=True)

            try:
                if kwargs["id"] != "":
                    lesson = Lesson.objects.filter(is_active=True, is_scheduled=False)
                    teacher = Teacher.objects.filter(is_active=True)
                    schedule = ScheduleClass.objects.get(id=kwargs["id"]).scheduled_date
                    schedule = ScheduleClass.objects.filter(scheduled_date=schedule, is_active=True)
                    return render(self.request, "curioo_admin/scheduleDetail.html",
                                  {"lesson": lesson, "teacher": teacher, "schedule": schedule,"schedule_details_class":True})
            except:
                print("i'm herer")
                return render(self.request, "curioo_admin/schedules.html", {"lesson":lesson,"teacher":teacher,"schedule":schedule,"studio":studio,"schedule_details_class":True})
        except Exception as e:
            print(e)
            return redirect("admin/dashboard")

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def post(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            
            data = self.request.POST.get
            schedule = ScheduleClass.objects.create(title = data("title"),studio_id=data("studio"), scheduled_date=data("date"),
                                                    lesson_id=data("lesson"),
                                                    teacher_id=data("teacher"), room_number=data("room"),
                                                    class_start_time=data("start_time"),
                                                    class_end_time=data("end_time"))
            print("schedule",schedule)
            lesson_id = data("lesson")
            Lesson.objects.filter(id=lesson_id).update(is_scheduled=True)
            WebNotification.objects.create(notification="You have been assigned to a new Lesson "+schedule.lesson.lesson_code, respective_user_type=schedule.teacher.login_type,
                                           respective_user_id=schedule.teacher.id)
            return redirect('scheduleCalander')
        except Exception as e:
            return redirect("admin/dashboard")

# class ScheduleView(View):
#     http_method_names = ['get', 'post']
#
#     def get(self, *args, **kwargs):
#         print("kwargs==>wedwedwe",kwargs)
#         schedule = ScheduleClass.objects.filter(id=kwargs["id"])
#         return render(self.request, "curioo_admin/scheduleDetail.html",{"schedule":schedule})
#Ajax
#     def post(self, *args, **kwargs):
#         try:
#             data = self.request.POST.get
#             schedule = ScheduleClass.objects.create(studio_id=int(data("studio")), scheduled_date=data("date"),
#                                                     lesson_id=int(data("lesson")),
#                                                     teacher_id=int(data("teacher")), room_number=data("room"),
#                                                     class_start_datetime=data("s_time"),
#                                                     class_end_datetime=data("e_time"))
#             return redirect("schedule")
#         except Exception as e:
#             return HttpResponse(f"error==>{e}")

@method_decorator(csrf_exempt, name='dispatch')
class ScheduleEditView(View):

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            print("edit view schedule==>GET")
            queryset = ScheduleClass.objects.get(id=kwargs["id"])
            return render(self.request, "curioo_admin/schedules.html", {"data": queryset})
        except Exception as e:
            return HttpResponse(f"error==>{e}")

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def post(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            data = self.request.POST.get
            date = data("date").split("T")[0]

            data1 = ScheduleClass.objects.get(id=int(data("id")))
            lessoncode = Lesson.objects.get(id=data1.lesson.id)
            lesson_code = create_lesson_code(data1.lesson.level.level_shortcut, data1.lesson.subject.subject_code)
            my_data = Lesson.objects.create(lesson_code=lesson_code,studio_id=lessoncode.studio.id,level_id=lessoncode.level.id,subject_id=lessoncode.subject.id)
            dict_data = {"studio_id":data1.studio.id,"scheduled_date":date,"class_start_time":data1.class_start_time,"class_end_time":data1.class_end_time,
                         "teacher_id":data1.teacher.id,"lesson_id":my_data.id,"room_number":data1.room_number}
            dd = ScheduleClass.objects.create(**dict_data)
            my_data.is_scheduled = True
            my_data.save()
            # data1.scheduled_date = date
            # data1.save()
            # queryset = ScheduleClass.objects.filter(id=int(data("id"))).update(studio_id=int(data("studio")),
            #                                                                 lesson_id=int(data("lesson")),
            #                                                                 teacher_id=int(data("teacher")),
            #                                                                 room_number=data("room"),
            #                                                                 class_start_datetime=data("s_time"),
            #                                                                 class_end_datetime=data("e_time"))
            # print("success==>",queryset)
            return redirect("admin/dashboard")
        except Exception as e:
            print("error==>",e)
            return redirect("admin/dashboard")


class ScheduleLessonEditView(View):

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            print("schedule lesson edit view==>GET")
            queryset = ScheduleClass.objects.get(id=kwargs["id"])
            return render(self.request, "curioo_admin/schedules.html", {"data": queryset})
        except Exception as e:
            return redirect("admin/dashboard")

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def post(self,*args,**kwargs):
        print("inside post of edit schedule")
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            print("schedule lesson edit view==>POST")
            data = self.request.POST.get
            print(data)
            schedule = ScheduleClass.objects.get(id=data('id'))
            print(schedule)
            queryset = ScheduleClass.objects.filter(id=data("id")).update(studio_id=schedule.studio.id,
                                                                            lesson_id=schedule.lesson.id,
                                                                            teacher_id=int(data("teacher")),
                                                                            room_number=data("room"),
                                                                            class_start_time=data("s_time"),
                                                                            class_end_time=data("e_time"))
            return redirect("scheduleView",id=data("id"))
        except Exception as e:
            print("exception", e)
            return redirect("admin/dashboard")


class ScheduleListView(View):

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            queryset = ScheduleClass.objects.all()
            return render(self.request, "curioo_admin/studentDetail.html",
                          {"data": queryset, 'schedule_details_class': True})
        except Exception as e:
            return redirect("admin/dashboard")


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def schedule_delete(request, id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        return redirect('login/')
    try:
        queryset = ScheduleClass.objects.filter(id=id).update(is_active=False)
        return HttpResponse("schedule deleted successfully")
    except Exception as e:
        return redirect("admin/dashboard")


class ScheduleView(View):
    http_method_names = ['get', 'post']

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        lesson = Lesson.objects.all()
        teacher = Teacher.objects.all()
        schedule = ScheduleClass.objects.get(id=kwargs["id"]).scheduled_date
        schedule = ScheduleClass.objects.filter(scheduled_date=schedule)
        print("schdeule in new==>",schedule)
        return render(self.request, "curioo_admin/scheduleDetail.html",{"lesson":lesson,"teacher":teacher,"schedule":schedule, "schedule_details_class":True})

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def post(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            print("schedule VIEW LAST==> POST")
            data = self.request.POST.get
            schedule = ScheduleClass.objects.create(studio_id=data("studio"), scheduled_date=data("date"),
                                                    lesson_id=data("lesson"),
                                                    teacher_id=data("teacher"), room_number=data("room"),
                                                    class_start_time=data("start_time"),
                                                    class_end_time=data("end_time"))
            lesson_id = data("lesson")
            Lesson.objects.filter(id=lesson_id).update(is_scheduled=True)
            return redirect('schedule')
        except Exception as e:
            return redirect("admin/dashboard")

    # def post(self, *args, **kwargs):
    #     try:
    #         data = self.request.POST.get
    #         schedule = ScheduleClass.objects.create(studio_id=int(data("studio")), scheduled_date=data("date"),
    #                                                 lesson_id=int(data("lesson")),
    #                                                 teacher_id=int(data("teacher")), room_number=data("room"),
    #                                                 class_start_datetime=data("s_time"),
    #                                                 class_end_datetime=data("e_time"))
    #         return HttpResponse("schedule saved")
    #     except Exception as e:
    #         return HttpResponse(f"error==>{e}")


# class ScheduleListViewCalnderData(View):
#     http_method_names = ['get', 'post']
#
#     def get(self, *args, **kwargs):
#         # try:
#         #     name, user_type, user = gbl(self.request, self.request.session['token'])
#         # except Exception as e:
#         #     return redirect('login/')
#         try:
#             schedule = ScheduleClass.objects.filter(studio_id=kwargs["id"])
#             data = [data.id for data in schedule]
#             print("data==>",data)
#             print("schedi-->",schedule)
#             return JsonResponse(data,safe=True)
#         except Exception as e:
#             return redirect("admin/dashboard")

class ScheduleListViewCalnderData(APIView):
    permission_classes = (AllowAny,)

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, request):
        try:
            print("in try")
            # teacher_status = request.GET['status']
            print("req==>", request.query_params['id'])
            studio = request.query_params['id']
            print("studio==>", studio)
            schedule = ScheduleClass.objects.filter(studio_id=studio)
            data = [data.id for data in schedule]
            data = [{"start": str(data.scheduled_date)+"T"+str(data.class_start_time), "end": str(data.scheduled_date)+"T"+str(data.class_end_time), "title": data.lesson.lesson_code,
                     "url": data.id, "studio_id": data.studio.id} for
                    data in schedule]

            print("data==>", data)
            print("schedi-->", schedule)
            return Response(data={'data': data}, status=status.HTTP_200_OK)
        except Exception as e:
            print(e)
            return redirect("admin/dashboard")


#######################################################################################
# ADD NEW STUDENT IN LESSONS ##########################################################
#######################################################################################


class AddLessonStudent(View):

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, *args, **kwargs):
        # try:
        #     name, user_type, user = gbl(self.request, self.request.session['token'])
        # except Exception as e:
        #     return redirect('login/')
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            return render(self.request, "curioo_admin/lessonDetails.html", {'lesson_class': True})
        except Exception as e:
            return redirect("dashboard")

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def post(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            data = self.request.POST.get
            student = Student.objects.get(id=data('add_Student'))
            lesson = Lesson.objects.get(id=data("id"))
            lesson.student.add(student)
            lesson.save()
            schedules = ScheduleClass.objects.filter(lesson_id=lesson.id)
            for schedule in schedules:
                WebNotification.objects.create(notification="A new student was added in your "+schedule.lesson.subject.subject_name+" lesson "+schedule.lesson.lesson_code,
                                               respective_user_type=schedule.teacher.login_type,
                                               respective_user_id=schedule.teacher.id)
            return HttpResponse(status=status.HTTP_200_OK, content={'message': 'Student added successfully.'})
        except Exception as e:
            return redirect("dashboard")


class RemoveLessonStudent(View):
    http_method_names = ['get', 'post']

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            return render(self.request, "curioo_admin/lessonDetails.html", {'lesson_class': True})
        except Exception as e:
            return redirect("dashboard")

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def post(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            std_id = self.request.POST.get
            student = Student.objects.get(id=std_id('id'))
            lesson = Lesson.objects.get(id=std_id('lesson_id'))
            lesson.student.remove(student)
            return redirect("lesson")
        except Exception as e:
            return redirect("dashboard")

@api_view(['GET'])
@permission_classes([AllowAny])
def get_teachers(request):
    try:
        requested_studio = request.GET.get('id')
        Teachers = Teacher.objects.filter(studio = requested_studio,is_active=True)
        data = [{"id":obj.id,"full_name":obj.full_name,"teacher_code":obj.teacher_code} for obj in Teachers]
        return Response(data)
    except Exception as e:
        print(e)
        return JsonResponse(data=[], safe=False)


class GetLessonStudent(View):
    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, *args, **kwargs):
        try:
            name, user_type, user = gbl(self.request, self.request.session['token'])
        except Exception as e:
            return redirect('login/')
        try:
            # data = self.request.GET.get
            # lesson = Lesson.objects.get(id=data('lesson_id'))
            # studentList = [] 
            # for student in lesson.student.all():
            #     studentList.append(student.id)
            # return JsonResponse(studentList,safe=False)
            data = self.request.GET.get
            lesson = Lesson.objects.get(id=data('lesson_id'))
            studio_obj = lesson.studio_id
            students = Student.objects.filter(is_active=True, expiry_date__gt=datetime.now(), studio_id=studio_obj)
            lesson_students = lesson.student.all()
            for student in lesson_students:
                students = students.exclude(id=student.id)
            data = [ {'id': student.id, 'name': student.full_name} for student in students ]
            return HttpResponse(json.dumps(data))
        except Exception as e:
            return redirect("dashboard")
            
@api_view(['GET'])
@permission_classes([AllowAny])
def get_teachers_lesson(request):
    try:
        print("inside try 1", request.GET)
        requested_lesson = request.GET.get('lesson')
        text = request.GET.get('text')
        studio_code = text.split(",")
        print(studio_code)
        teachers = Teacher.objects.filter(studio__studio_code=studio_code[0], is_active=True)
        lesson = Lesson.objects.get(id=requested_lesson)
        lesson_sub = lesson.subject.subject_code
        print(lesson.subject.subject_name)
        teacher_list = []
        for teacher in teachers:
            subjects = teacher.subject.all()
            for subject in subjects:
                if lesson_sub == subject.subject_code:
                    teacher_list.append({"id": teacher.id, "name": teacher.full_name})
        print(teacher_list)
        return Response(status=status.HTTP_200_OK, data=teacher_list)
    except Exception as e:
        print(e)
        return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@permission_classes([AllowAny])
def get_lesson(request):
    try:
        text = request.GET.get('text')
        studio_code = text.split(",")
        print(studio_code)
        lessons = Lesson.objects.filter(studio__studio_code=studio_code[0], is_scheduled=False, is_active=True)
        lesson_list = []
        for lesson in lessons:
            lesson_list.append({"id": lesson.id, "name": lesson.lesson_code})
        print("inside get lesson", lesson_list)
        return Response(status=status.HTTP_200_OK, data=lesson_list)
    except Exception as e:
        print(e)
        return Response(status=status.HTTP_400_BAD_REQUEST)
