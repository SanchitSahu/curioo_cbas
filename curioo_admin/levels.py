from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.cache import cache_control
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from curioo_admin.models import Level
from teacher.status_message import error_message


@csrf_exempt
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def level_list(request):
    if request.method == 'GET':
        levels = Level.objects.filter(is_active=True)
        # levels = [{'level_id': level.id, 'level_name': level.level} for level in levels]
        print('levels : ', levels)
        # return render(request, 'curioo_admin/levels.html', context={'levels': levels, 'level_class': True})
        return render(request, 'curioo_admin/levels.html', context={'level_class': True, 'levels': levels})


@csrf_exempt
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def level_add(request):
    if request.method == 'POST':
        level_shortcut = request.POST['level_shortcut']
        level = request.POST['level']

        if Level.objects.filter(level__iexact=level).exists():
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='Level already exists')
        if Level.objects.filter(level_shortcut__iexact=level_shortcut).exists():
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='Level shortcut already exists')
        Level.objects.create(level=level,level_shortcut=level_shortcut)
        return HttpResponse(status=status.HTTP_200_OK)


@csrf_exempt
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def level_put(request, level_id):
    try:
        if request.method == 'POST':
            level_shortcut = request.POST['level_shortcut']
            level = request.POST['level']
            level_obj = Level.objects.get(id=level_id)
            if level_obj.level != level:
                if Level.objects.filter(level__iexact=level).exists():
                    return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='Level already exists')
            if level_obj.level_shortcut != level_shortcut:
                if Level.objects.filter(level_shortcut__iexact=level_shortcut).exists():
                    return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='Level shortcut already exists')

            Level.objects.filter(id=level_id).update(level=level, level_shortcut=level_shortcut)
            return HttpResponse(status=status.HTTP_200_OK)
    except Exception as e:
        print(e)


@csrf_exempt
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def level_delete(request, level_id):
    if request.method == 'GET':
        Level.objects.filter(id=level_id).update(is_active=False)
        return HttpResponse(status=status.HTTP_200_OK)


class LevelListApi(APIView):
    permission_classes = (AllowAny,)

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, request):
        try:
            levels = Level.objects.filter(is_active=True)
            data = [{"id": level.id, "level": level.level, "level_code": level.level_shortcut} for level in levels]
            return Response(data={'data': data}, status=status.HTTP_200_OK)

        except Exception as e:
            return Response(data=error_message(400, "error", str(e), {}), status=status.HTTP_400_BAD_REQUEST)
