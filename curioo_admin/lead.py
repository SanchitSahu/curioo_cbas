"""
A module for lead Create, Update, List, View and Delete operations for admin panel.
"""
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.cache import cache_control
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from curioo_admin.models import Lead, LearningConsultant, Student, Admin, Principal, AdminNotification
from curioo_admin.teacher import get_studios
from curioo_admin.validations import calculate_age
from curioo_admin.views import student_code
from teacher.getUser import gbl
from teacher.status_message import error_message


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def lead_list(request):
    """ To get only active lead List in Admin Panel. """
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')
    try:
        print(user_type)
        if user_type == "admin":
            leads = Lead.objects.filter(is_active=True, is_converted=False)
            ages = [calculate_age(x.dob) for x in leads]
            comb = zip(leads, ages)
            studios = get_studios()
            learning_consultants = LearningConsultant.objects.filter(is_active=True)
            return render(request, template_name="curioo_admin/leads.html",
                          context={"studios": studios, "comb": comb, "learning_consultants": learning_consultants,
                                   'lead_class': True})

        if user_type == "principal":
            pricipal_studio = user.studio
            leads = Lead.objects.filter(is_active=True, is_converted=False,studio=pricipal_studio)
            ages = [calculate_age(x.dob) for x in leads]
            comb = zip(leads, ages)
            studios = get_studios()
            learning_consultants = LearningConsultant.objects.filter(is_active=True)
            return render(request, template_name="curioo_admin/leads.html",
                          context={"studios": studios, "comb": comb, "learning_consultants": learning_consultants,
                                   'lead_class': True,'user':user})

    except Exception as e:
        print('e : ', e)
        return redirect(to='admin/dashboard')


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def add_lead(request):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')
    if request.method == "GET":
        studios = get_studios()
        grade_dropdown = [x for x in range(1, 13)]

        return render(request, template_name="curioo_admin/addNewLead.html",
                      context={'studios': studios, 'lead_class': True, 'grade_dropdown': grade_dropdown,'user':user})


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def view_lead(request, lead_id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')
    if request.method == "GET":
        lead = Lead.objects.get(id=lead_id)
        added_by_type = lead.added_by_type
        if added_by_type == "Admin": obj = Admin.objects.get(id=lead.added_by_id)
        if added_by_type == "Principal": obj = Principal.objects.get(id=lead.added_by_id)
        if added_by_type == "Learning Consultant": obj = LearningConsultant.objects.get(id=lead.added_by_id)

        age = calculate_age(lead.dob)
        studios = get_studios()
        return render(request, template_name="curioo_admin/leadDetail.html",
                      context={'studios': studios, "lead": lead, "age": age, 'lead_class': True, 'added_by': obj})


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def convert_lead(request, lead_id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    if request.method == "GET":
        try:
            enrollment_date = request.GET['enrollment']
            expiry_date = request.GET['expiry']
            lead = Lead.objects.get(id=lead_id)
            s=Student.objects.create(student_code=student_code(lead.studio.studio_code, lead.studio.id), studio=lead.studio, full_name=lead.full_name,
                                   father_name=lead.father_name, father_phone=lead.father_phone,
                                   father_email=lead.father_email, mother_name=lead.mother_name,
                                   mother_phone=lead.mother_phone, mother_email=lead.mother_email, dob=lead.dob,
                                   doj=enrollment_date, expiry_date=expiry_date,
                                   cbas_testDate=lead.cbas_testDate, gender=lead.gender, nationality=lead.nationality,
                                   school_name=lead.school_name, school_grade=lead.school_grade,
                                   start_level=lead.start_curioo_level, current_level=lead.current_curioo_level,
                                   major=lead.major, image=lead.image, added_by_id=lead.added_by_id,
                                   added_by_type=lead.added_by_type)
            Lead.objects.filter(id=lead_id).update(is_converted=True,expiry_date=expiry_date,enrollment_date=enrollment_date)
            print('jhkjhkj : ', s)

            Principals = Principal.objects.filter(is_active=True, studio=lead.studio)
            Admins = Admin.objects.all()
            notification_msg = "A new lead converted to student and added in " + lead.studio.name + " Studio."
            for Principal_obj in Principals:
                AdminNotification.objects.create(notification=notification_msg, principal=Principal_obj)

            for Admin_obj in Admins:
                AdminNotification.objects.create(notification=notification_msg, admin=Admin_obj)

            return HttpResponse(status=status.HTTP_200_OK, content="Lead converted to student successfully.")
        except Exception as e:
            print(e)
            msg = "Invalid Request."
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def delete_lead(request, lead_id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')

    try:
        Lead.objects.filter(id=lead_id).update(is_active=False)
        return HttpResponse(status=status.HTTP_200_OK)
    except Exception:
        msg = "Invalid Request."  # No teacher can be fetched for specific teacher_id
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content=msg)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def edit_lead(request, lead_id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')
    if request.method == "GET":
        lead = Lead.objects.get(id=lead_id)
        age = calculate_age(lead.dob)
        studios = get_studios()
        if lead.added_by_type == "Admin": added_by = Admin.objects.all()
        if lead.added_by_type == "Principal": added_by = Principal.objects.filter(is_active=True)
        if lead.added_by_type == "Learning Consultant": added_by = LearningConsultant.objects.filter(
            is_active=True)
        return render(request, template_name="curioo_admin/editLeadDetail.html",
                      context={'studios': studios, "lead": lead, "age": age, 'lead_class': True, 'added_by': added_by})


@csrf_exempt
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def edit_contact_lead(request, lead_id):
    try:
        name, user_type, user = gbl(request, request.session['token'])
    except Exception as e:
        print('inside exception : ', e)
        return redirect('login/')
    if request.method == "GET":
        print("inside edit lead contact")
        lead = Lead.objects.get(id=lead_id)
        age = calculate_age(lead.dob)
        studios = get_studios()
        return render(request, template_name="curioo_admin/editLeadContact.html",
                      context={'studios': studios, "lead": lead, "age": age, 'lead_class': True})


class LeadListApi(APIView):
    permission_classes = (AllowAny,)

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, request):
        try:
            name, user_type, user = gbl(request, request.session['token'])
        except Exception as e:
            print('inside exception : ', e)
            return redirect('login/')

        try:

            lead_studio = request.GET['studio']
            lead_added_by = request.GET['added_by_selction']
            lead_list_of_added_by = request.GET['list_of_added_by']

            if user_type == "admin":

                leads = Lead.objects.filter(is_active=True,is_converted=False)
                if lead_studio!= "All Studio":
                    leads=leads.filter(studio=lead_studio)
                if lead_added_by!="Added By":
                    leads=leads.filter(added_by_type=lead_added_by)
                if lead_added_by!="Added By" and lead_list_of_added_by != 'List of Added By' and lead_list_of_added_by != 'Added By Admin' and lead_list_of_added_by != 'Added By Principal' and lead_list_of_added_by != 'Added By Learning Consultant' and lead_list_of_added_by!= 'Added By Added By':
                    leads = leads.filter(added_by_id=lead_list_of_added_by)

                data = [{"id": lead.id, "code": lead.lead_code, "name": lead.full_name,
                         "age":calculate_age(lead.dob)} for
                        lead in leads]
                return Response(data={'data': data}, status=status.HTTP_200_OK)

            if user_type == "principal":

                leads = Lead.objects.filter(is_active=True, is_converted=False,studio=lead_studio)
                if lead_added_by != "Added By":
                    leads = leads.filter(added_by_type=lead_added_by)
                if lead_added_by != "Added By" and lead_list_of_added_by != 'List of Added By' and lead_list_of_added_by != 'Added By Admin' and lead_list_of_added_by != 'Added By Principal' and lead_list_of_added_by != 'Added By Learning Consultant' and lead_list_of_added_by != 'Added By Added By':
                    print(lead_list_of_added_by)
                    leads = leads.filter(added_by_id=lead_list_of_added_by)

                data = [{"id": lead.id, "code": lead.lead_code, "name": lead.full_name,
                         "age": calculate_age(lead.dob)} for
                        lead in leads]
                return Response(data={'data': data}, status=status.HTTP_200_OK)

        except Exception as e:
            print("eroor",e)
            return Response(data=error_message(400, "error", str(e), {}), status=status.HTTP_400_BAD_REQUEST)


