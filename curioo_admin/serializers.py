""" Serializers for curioo_admin """
from rest_framework import serializers
from curioo_admin.models import Student, Comments, Admin, Principal, Teacher, Studio, LearningConsultant
from curioo_admin.validations import calculate_age


class StudentSerializer(serializers.ModelSerializer):
    """ Student data for Get Student list in Admin Panel. """
    level = serializers.SerializerMethodField('get_level')
    major = serializers.SerializerMethodField('get_major')
    age = serializers.SerializerMethodField('get_age')

    class Meta:
        model = Student
        fields = ['id', 'student_code', 'full_name', 'level', 'major', 'age']

    def get_level(self, data):
        try:
            level = data.current_level.level
        except Exception:
            level = None
        return level

    def get_major(self, data):
        try:
            major = data.major.major
        except Exception:
            major = None
        return major

    def get_age(self, data):
        return calculate_age(data.dob)


class FullStudentSerializer(serializers.ModelSerializer):
    """ Full Student Data for Admin Panel Student creation. """

    def create(self, validated_data):
        print("Create validated data")
        try:
            print("validated_data['dob']---",validated_data['dob'], type(validated_data['dob']))
            print("validated_data['doj']---",validated_data['doj'], type(validated_data['doj']))
            print("validated_data['cbas_testDate']---",validated_data['cbas_testDate'],type(validated_data['cbas_testDate']))
            print("validated_data['expiry_date']----",validated_data['expiry_date'], type(validated_data['expiry_date']))
            student = Student(student_code=validated_data['student_code'], studio=validated_data['studio'],
                              full_name=validated_data['full_name'],
                              father_name=validated_data['father_name'],
                              father_phone=int(validated_data['father_phone']),
                              father_email=validated_data['father_email'], mother_name=validated_data['mother_name'],
                              mother_phone=int(validated_data['mother_phone']),
                              mother_email=validated_data['mother_email'],
                              dob=validated_data['dob'], doj=validated_data['doj'],
                              cbas_testDate=validated_data['cbas_testDate'],
                              expiry_date=validated_data['expiry_date'],
                              gender=validated_data['gender'], nationality=validated_data['nationality'],
                              school_name=validated_data['school_name'], school_grade=validated_data['school_grade'],
                              image=validated_data['image'])
            student.save()
            return student

        except Exception as e:
            return False

    level = serializers.SerializerMethodField('get_level')
    major = serializers.SerializerMethodField('get_major')
    age = serializers.SerializerMethodField('get_age')

    class Meta:
        model = Student

        fields = ['id', 'student_code', 'studio', 'full_name', 'father_name', 'father_phone', 'father_email',
                  'mother_name',
                  'mother_phone', 'mother_email', 'dob', 'doj', 'expiry_date', 'cbas_testDate', 'gender', 'nationality',
                  'school_name', 'school_grade', 'level', 'major', 'image', 'age']

    def get_level(self, data):
        try:
            level = data.level.level
        except Exception:
            level = None
        return level

    def get_major(self, data):
        try:
            major = data.major.major
        except Exception:
            major = None
        return major

    def get_age(self, data):
        return calculate_age(data.dob)

class CsvStudentSerializer(serializers.ModelSerializer):
    """ Full Student Data for Admin Panel Student creation. """

    def create(self, validated_data):
        print("Create validated data")
        try:
            student = Student(student_code=validated_data['student_code'], studio=validated_data['studio'],
                              full_name=validated_data['full_name'],
                              father_name=validated_data['father_name'],
                              father_phone=int(validated_data['father_phone']),
                              father_email=validated_data['father_email'], mother_name=validated_data['mother_name'],
                              mother_phone=int(validated_data['mother_phone']),
                              mother_email=validated_data['mother_email'],
                              dob=validated_data['dob'], doj=validated_data['doj'],
                              cbas_testDate=validated_data['cbas_testDate'],
                              expiry_date=validated_data['expiry_date'],
                              gender=validated_data['gender'], nationality=validated_data['nationality'],
                              school_name=validated_data['school_name'], school_grade=validated_data['school_grade'])
            student.save()
            return student

        except Exception as e:
            import traceback
            traceback.print_tb(e.__traceback__)
            return False

    level = serializers.SerializerMethodField('get_level')
    major = serializers.SerializerMethodField('get_major')
    age = serializers.SerializerMethodField('get_age')

    class Meta:
        model = Student

        fields = ['id', 'student_code', 'studio', 'full_name', 'father_name', 'father_phone', 'father_email',
                  'mother_name',
                  'mother_phone', 'mother_email', 'dob', 'doj', 'expiry_date', 'cbas_testDate', 'gender', 'nationality',
                  'school_name', 'school_grade', 'level', 'major', 'age']

    def get_level(self, data):
        try:
            level = data.level.level
        except Exception:
            level = None
        return level

    def get_major(self, data):
        try:
            major = data.major.major
        except Exception:
            major = None
        return major

    def get_age(self, data):
        return calculate_age(data.dob)


class CommentsSerializer(serializers.ModelSerializer):
    """ Serializer for Comments to show in Admin Panel. """
    commented_by_id = serializers.SerializerMethodField('get_commented_by')
    image = serializers.SerializerMethodField('get_image')
    created_at = serializers.SerializerMethodField('get_created_at')

    class Meta:
        model = Comments
        fields = ['id','commented_by_id', 'commented_by_role', 'message', 'created_at', 'image']

    def get_commented_by(self, data):
        commented_by_role = data.commented_by_role
        commented_by_id = data.commented_by_id

        if commented_by_role == 'Admin':
            user = Admin.objects.get(id=commented_by_id)
        elif commented_by_role == 'Principal':
            user = Principal.objects.get(id=commented_by_id)
        elif commented_by_role == 'Teacher':
            user = Teacher.objects.get(id=commented_by_id)
        else:
            return None
        return user.full_name

    def get_image(self, data):
        commented_by_role = data.commented_by_role
        commented_by_id = data.commented_by_id
        if commented_by_role == 'Admin':
            user = Admin.objects.get(id=commented_by_id)
        elif commented_by_role == 'Principal':
            user = Principal.objects.get(id=commented_by_id)
        elif commented_by_role == 'Teacher':
            user = Teacher.objects.get(id=commented_by_id)
        else:
            return None
        return user.image.url

    def get_created_at(self, data):
        created_at = data.created_at
        return created_at.strftime('%d:%m:%Y %H:%M')


class TeacherSerializer(serializers.ModelSerializer):
    """ Serializer for Teacher List in Admin Panel. """
    studio = serializers.SerializerMethodField('get_studio_str')

    class Meta:
        model = Teacher
        fields = ['id', 'teacher_code', 'full_name', 'studio']

    def get_studio_str(self, data):
        return f"{data.studio.studio_code}, {data.studio.name}, {data.studio.city}"


class StudioSerializer(serializers.ModelSerializer):
    """ Serializer for Studio Listing dropdown. """

    class Meta:
        model = Studio
        fields = ['id', 'name', 'studio_code']


class EditTeacherSerializer(serializers.ModelSerializer):
    """ Serializer for Edit Teacher Data in Admin Panel. """

    class Meta:
        model = Teacher
        fields = ['full_name', 'dob', 'gender', 'nationality', 'doj', 'position', 'position_id', 'studio_id',
                  'education_level', 'education_major', 'email', 'phone', 'emergency_contact_name', 'emergency_phone']


class AdminStudentDropdownSerializer(serializers.ModelSerializer):
    """ Serializer for Student list dropdown for added by selected user in Admin Panel. """

    class Meta:
        model = Admin
        fields = ['id', 'full_name']


class PrincipalStudentDropdownSerializer(serializers.ModelSerializer):
    """ Serializer for Student list dropdown for added by selected user in Admin Panel. """

    class Meta:
        model = Principal
        fields = ['id', 'full_name']


class LearningConsultantStudentDropdownSerializer(serializers.ModelSerializer):
    """ Serializer for Student list dropdown for added by selected user in Admin Panel. """

    class Meta:
        model = LearningConsultant
        fields = ['id', 'full_name']


class EditStudentSerializer(serializers.ModelSerializer):
    """ Serializer for Student edit in Admin Panel. """

    class Meta:
        model = Student

        fields = ['full_name', 'father_name', 'mother_name', 'dob', 'doj', 'expiry_date', 'cbas_testDate', 'gender',
                  'nationality', 'school_name', 'school_grade', 'major', 'start_level', 'current_level',
                  'added_by_type', 'added_by_id']


class EditStudentContactSerializer(serializers.ModelSerializer):
    """ Serializer for Student contact edit in Admin Panel. """

    class Meta:
        model = Student

        fields = ['father_phone', 'father_email', 'mother_phone', 'mother_email']


class StudentFilterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = '__all__'
